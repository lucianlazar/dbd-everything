using UnityEngine;


public class Screen8 : Singleton8<Screen8>
{
	public float Width {get; private set;}
	public float Height {get; private set;}
	public float XUnitSize {get; private set;}
	public float YUnitSize {get; private set;}
	public float AvgUnitSize {get; private set;}
	public Rect DefaultGUIAreaRect {get; private set;}

	public override void Init()
	{
		Width = Screen.width;
		Height = Screen.height;
        ushort _ScreenDivisions = Numbers8.SCREEN_DIVISIONS;
		XUnitSize = Width / _ScreenDivisions;
		YUnitSize = Height / _ScreenDivisions;
	    AvgUnitSize = (YUnitSize + XUnitSize) / 2;

		DefaultGUIAreaRect = 
			new Rect(
				AvgUnitSize, 
				AvgUnitSize, 
				Width - AvgUnitSize*2, 
				Height - AvgUnitSize*2
			);
    }










}
