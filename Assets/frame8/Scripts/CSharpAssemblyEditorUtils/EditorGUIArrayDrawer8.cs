using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace frame8.CSharpAssemblyEditorUtils
{
#if UNITY_EDITOR
	public class EditorGUIArrayDrawer8<T>
	{
		public float SpaceBetweenItems { get; set; }
		public bool IsFoldedOut { get { return _FoldOut; } }
		//	protected string _LastUsedLabel;
		protected string _LastUsedItemName;
		protected bool _FoldOut;
		// Called for each item when needs to be drawn
		protected IPropertyDrawerAndUpdater _LastUsedPropertyDrawerAndUpdater;
		protected T[] _DrawnArray;
		protected string _DrawnArrayLabel;
		protected int _DrawnArrayItemsCount;
		protected bool[] _ItemsFoldOuts = new bool[1];
		EditorGUIUtils8 editorUtils;
		int _ItemsCountEnteredValue = -1;

		// NeedToBeSetBeforeEveryDraw, if it's editable (Editable=true)
		// Do not set it to null. use new T[0] instead
		public T[] DrawnArray
		{
			get { return _DrawnArray; }
			set
			{
				T[] newArray = value;
				int currentItemsCount = 0;
				if (_DrawnArray != null)
					currentItemsCount = _DrawnArray.Length; // _DrawnArrayItemsCount is used at the same time, so we shall not access it from here; instead, call Length on the new array
				int newItemsCount = newArray.Length;

				// Grow the array of item foldouts bools, if a bigger array is provided
				if (currentItemsCount < newItemsCount)
				{
					bool[] currentFoldOuts = _ItemsFoldOuts.Clone() as bool[];
					_ItemsFoldOuts = new bool[newItemsCount];
					for (int i = 0; i < currentItemsCount; ++i)
						_ItemsFoldOuts[i] = currentFoldOuts[i];
				}
				_DrawnArray = newArray;
				_DrawnArrayItemsCount = newItemsCount;

				// This is only assigned here once, when the array is first given
				if (_ItemsCountEnteredValue == -1)
					_ItemsCountEnteredValue = _DrawnArrayItemsCount;
			}
		}
		public string DrawnArrayLabel
		{
			get { return _DrawnArrayLabel; }
			set
			{
				_DrawnArrayLabel = value;
				if (_DrawnArrayLabel == null)
					_DrawnArrayLabel = "";
				if (_DrawnArrayLabel.Length > 0)
				{
					//			_LastUsedLabel = value;
					_LastUsedItemName = _DrawnArrayLabel.Substring(0, _DrawnArrayLabel.Length - 1);
				}
				else
					_LastUsedItemName = "";

			}
		}

		public bool DrawItemsWithFoldOuts { get; set; }
		public bool DrawItemsInline { get; set; }
		/// <summary>
		/// Default value is true
		/// </summary>
		public bool Editable { get; set; }

		float _FoldOutFixedWidth = -1;
		//public float FoldOutFixedWidth { set
		//	{
		//		if (_FoldOutFixedWidth == value)
		//			return;

		//		_FoldOutFixedWidth = value;
		//		UpdateFoldOutGUIStyle();
		//	} }


		GUIStyle _GUIStyleArrayItem;
		GUIStyle GUIStyleArrayItem { get { if (_GUIStyleArrayItem == null) _GUIStyleArrayItem = new GUIStyle(EditorStyles.toolbar);  return _GUIStyleArrayItem; } }

		GUIStyle _GUIStyleFoldOut;
		GUIStyle GUIStyleFoldOut { get { 
				UpdateFoldOutGUIStyle();
				return _GUIStyleFoldOut; } }



		public EditorGUIArrayDrawer8(bool initialFoldOut = false)
		{
			_FoldOut = initialFoldOut;
			editorUtils = EditorGUIUtils8.Instance;
			Editable = true;
		}

		public virtual void Init(
			IPropertyDrawerAndUpdater itemDrawerAndUpdater
		)
		{
			Init(
				itemDrawerAndUpdater,
				"Array",
				"Item"
			);
		}

		public virtual void Init(
			IPropertyDrawerAndUpdater itemDrawerAndUpdater,
			string label
		)
		{
			Init(
				itemDrawerAndUpdater,
				label,
				string.IsNullOrEmpty(label) ? "" : label.Substring(0, label.Length - 1)
			);
		}

		public virtual void Init(
			IPropertyDrawerAndUpdater itemDrawerAndUpdater,
			string label,
			string itemName
		)
		{
			Init(
				itemDrawerAndUpdater,
				label,
				string.IsNullOrEmpty(label) ? "" : label.Substring(0, label.Length - 1),
				new T[0]
			);
		}

		public virtual void Init(
			IPropertyDrawerAndUpdater itemDrawerAndUpdater,
			string label,
			string itemName,
			T[] initialItems
		)
		{
			_LastUsedPropertyDrawerAndUpdater = itemDrawerAndUpdater;
			_DrawnArrayLabel = label;
			//		_LastUsedLabel = label;
			_LastUsedItemName = itemName + " ";
			DrawnArray = initialItems;
		}
		/*
		public T[] Draw(IPropertyDrawerAndUpdater itemDrawerAndUpdater, string label, int itemsCount, T[] array)
		{
			Draw(itemDrawerAndUpdater, label, label.Substring(0, label.Length - 1), itemsCount, array);
		}

		public T[] Draw(IPropertyDrawerAndUpdater itemDrawerAndUpdater, string label, string itemName, int itemsCount, T[] array)
		{
			_LastUsedItemDrawerAndUpdater = itemDrawerAndUpdater;
			_LastUsedLabel = label;
			_LastUsedItemName = itemName;
			Draw(itemsCount, array);
		}
		*/

		protected virtual T NewElement()
		{
			return default(T);
		}

		//	public T[] Draw(string label, int itemsCount, T[] otherArray)
		//	{
		//		_LastUsedLabel = label;
		//		_LastUsedItemName = label.Substring(0, label.Length - 1);
		//		return Draw(itemsCount, otherArray);
		//	}

		//	public virtual T[] Draw(T[] array)
		//	{
		//		return Draw(array.Length, array);
		//	}

		// @return the new array, if the user has changed its size
		// old data is preserver as much as possible
		//	public virtual T[] Draw(int itemsCount, T[] array)
		//	{
		//		EditorGUILayout.BeginHorizontal();
		//		_FoldOut = EditorGUILayout.Foldout(_FoldOut, _LastUsedLabel);
		//		itemsCount = EditorGUILayout.IntField("Count", itemsCount);
		//		itemsCount = Mathf.Clamp(itemsCount, 1, 500);
		//		EditorGUILayout.EndHorizontal();
		//		
		//		if (_FoldOut)
		//		{			
		//			// Preserve the existing data when growing the size
		//			// Note if the size shrinks, the extra data at the end of the array that will be erased can't be recovered 
		//			++EditorGUI.indentLevel;
		//			if (itemsCount != array.Length) // size changed; proces data; do the drawing in the next frame
		//			{
		//				int itemsOldCount = array.Length,
		//				min = Mathf.Min(itemsCount, itemsOldCount);
		//				T[] oldArray = array;
		//				array = new T[itemsCount];
		//				
		//				// Assign as much old values as we can 
		//				for (int i = 0; i < min; ++i)
		//					array[i] = oldArray[i];
		//				
		//				// Fill the remaining configs(if any) with new instances
		//				for (int i = min; i < itemsCount; ++i)
		//					array[i] = NewElement();
		//			}
		//			else // size not changed; draw all configs as usual
		//			{
		//				for (int i = 0; i < itemsCount; ++i)
		//				{
		//					EditorGUILayout.HelpBox(_LastUsedItemName + i, MessageType.None);
		//
		//					++EditorGUI.indentLevel;
		//					array[i] = _LastUsedPropertyDrawerAndUpdater.OnElementGUI( array[i] );
		//					--EditorGUI.indentLevel;
		//				}
		//			}
		//			--EditorGUI.indentLevel;
		//		}
		//
		//		return array;
		//	}

		// @return the new array, if the user has changed its size
		// old data is preserved as much as possible
		public virtual T[] Draw()
		{
			EditorGUILayout.BeginHorizontal();
			{
				//_FoldOut = editorUtils.DrawFoldOutWithGUIStyle(_FoldOut, _DrawnArrayLabel, GUIStyleFoldOut);
				_FoldOut = EditorGUILayout.Foldout(_FoldOut, _DrawnArrayLabel, GUIStyleFoldOut);
				//_FoldOut = editorUtils.DrawFoldOutWithGUIStyle(_FoldOut, _DrawnArrayLabel, null);
				if (Editable)
				{
					editorUtils.DrawCongestedLabel("Count", null, GUILayout.MaxWidth(40));

					_ItemsCountEnteredValue = EditorGUILayout.IntField(_ItemsCountEnteredValue, GUILayout.Width(100));
					EditorGUILayout.Space();
					EditorGUILayout.Space();

					_ItemsCountEnteredValue = Mathf.Clamp(_ItemsCountEnteredValue, 0, 512);
					if (_ItemsCountEnteredValue != _DrawnArrayItemsCount)
					{
						if (Editable && GUILayout.Button("Set", GUILayout.MaxWidth(40)))
							_DrawnArrayItemsCount = _ItemsCountEnteredValue;
					}
				}
				else
				{
					editorUtils.DrawCongestedLabel("Count: " + _DrawnArrayItemsCount);
				}
			}
			EditorGUILayout.EndHorizontal();

			if (_FoldOut)
			{
				// Preserve the existing data when growing the size
				// Note if the size shrinks, the extra data at the end of the _DrawnArray that will be erased can't be recovered 
				++EditorGUI.indentLevel;
				if (_DrawnArrayItemsCount != _DrawnArray.Length) // size changed; proces data; do the drawing in the next frame
				{
					int itemsOldCount = _DrawnArray.Length,
					min = Mathf.Min(_DrawnArrayItemsCount, itemsOldCount);
					T[] oldArray = _DrawnArray;
					DrawnArray = new T[_DrawnArrayItemsCount]; // Assigning the new drawn array through the property setter, so it'll do another initializations

					// Assign as much old values as we can 
					for (int i = 0; i < min; ++i)
						_DrawnArray[i] = oldArray[i];

					// Fill the remaining configs(if any) with new instances
					for (int i = min; i < _DrawnArrayItemsCount; ++i)
						_DrawnArray[i] = NewElement();
				}
				else // size not changed; draw all configs as usual
				{
					for (int i = 0; i < _DrawnArrayItemsCount; ++i)
					{
						string indexQualifiedItemName = _LastUsedItemName + i;
						bool draw = true;

						if (DrawItemsInline)
							EditorGUILayout.BeginHorizontal();

						if (DrawItemsWithFoldOuts)
							draw = _ItemsFoldOuts[i] = editorUtils.DrawFoldOutWithGUIStyle(
									_ItemsFoldOuts[i],
									indexQualifiedItemName,
									GUIStyleArrayItem
								);
						else
							EditorGUILayout.HelpBox(indexQualifiedItemName, MessageType.None);

						if (draw)
						{
							++EditorGUI.indentLevel;
							_DrawnArray[i] = _LastUsedPropertyDrawerAndUpdater.OnElementGUI(_DrawnArray[i]);
							--EditorGUI.indentLevel;

							if (SpaceBetweenItems > 0)
								GUILayout.Space(SpaceBetweenItems);
						}

						if (DrawItemsInline)
							EditorGUILayout.EndHorizontal();
					}
				}
				--EditorGUI.indentLevel;
			}

			return _DrawnArray;
		}

		void UpdateFoldOutGUIStyle()
		{
			if (_GUIStyleFoldOut == null)
				_GUIStyleFoldOut = new GUIStyle(EditorStyles.foldout);
			//_GUIStyleFoldOut = new GUIStyle(GUIStyle.none);
			//_GUIStyleFoldOut.margin = new RectOffset(0, (int)_FoldOutFixedWidth, 0, 0);
			//_GUIStyleFoldOut.stretchWidth = _FoldOutFixedWidth != -1;
		}

		public interface IPropertyDrawerAndUpdater
		{
			// @param item the current value
			// @returns the updated value
			T OnElementGUI(T item);
		}
	}
#endif
}

