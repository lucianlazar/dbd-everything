using UnityEngine;
using System.Collections;

namespace frame8.CSharpAssemblyEditorUtils
{
#if UNITY_EDITOR
	// This class is used for arrays of IDrawableProperty8
	// The purpose is to automatically call IDrawableProperty8.OnGUI() from within this class,
	// so the user won't be forced to provide an item draw callback
	public class EditorGUIScriptableObjectsArrayDrawer<T>
	: EditorGUIArrayDrawer8<T>,
	  EditorGUIArrayDrawer8<T>.IPropertyDrawerAndUpdater // make this class able to draw its own items in the array
		where T : IOnGUICallbacksReceiver8
	{

		public EditorGUIScriptableObjectsArrayDrawer(bool initialFoldOut = false)
			:base(initialFoldOut)
		{

		}

		public void Init(string label, string itemName)
		{
			// Overriding the drawerUpdater to be <this>, since for this class we'll always use OnElementGUI
			base.Init(this, label, itemName);
		}

		public void Init(string label, string itemName, T[] initialItems)
		{
			// Overriding the drawerUpdater to be <this>, since for this class we'll always use OnElementGUI
			base.Init(this, label, itemName, initialItems);
		}

		#region ItemDrawerAndUpdater implementation
		// Don't call this outside this class!
		public T OnElementGUI(T elementOfTheArray)
		{
			elementOfTheArray.OnGUI8();

			return elementOfTheArray;
		}
		#endregion
	}
#endif
}

