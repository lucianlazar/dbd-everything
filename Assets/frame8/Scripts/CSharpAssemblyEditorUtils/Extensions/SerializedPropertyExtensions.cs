﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

namespace frame8.CSharpAssemblyEditorUtils.Extensions
{
#if UNITY_EDITOR
    public static class SerializedPropertyExtensions
    {
        public static void SetValue(this SerializedProperty thisSP, object value)
        {
            switch (thisSP.propertyType)
            {
                case SerializedPropertyType.LayerMask:
                case SerializedPropertyType.Integer:
                case SerializedPropertyType.ArraySize:
                case SerializedPropertyType.Character:
                    thisSP.intValue = (int)value; break;
                case SerializedPropertyType.Boolean:
                    thisSP.boolValue = (bool)value; break;
                case SerializedPropertyType.Float:
                    thisSP.floatValue = (float)value; break;
                case SerializedPropertyType.String:
                    thisSP.stringValue = (string)value; break;
                case SerializedPropertyType.Color:
                    thisSP.colorValue = (Color)value; break;
                case SerializedPropertyType.ObjectReference:
                    thisSP.objectReferenceValue = (UnityEngine.Object)value; break;
                case SerializedPropertyType.Enum:
                    thisSP.enumValueIndex = (int)value; break;
                case SerializedPropertyType.Vector2:
                    thisSP.vector2Value = (Vector2)value; break;
                case SerializedPropertyType.Vector3:
                    thisSP.vector3Value = (Vector3)value; break;
                case SerializedPropertyType.Rect:
                    thisSP.rectValue = (Rect)value; break;
                case SerializedPropertyType.AnimationCurve:
                    thisSP.animationCurveValue = (AnimationCurve)value; break;
                case SerializedPropertyType.Bounds:
                    thisSP.boundsValue = (Bounds)value; break;
                //case SerializedPropertyType.Gradient: // TODO implement this when needed
                //    SafeGradientValue(thisSP) = (int)value; break;
                case SerializedPropertyType.Quaternion:
                    thisSP.quaternionValue = (Quaternion)value; break;

                default:
                    throw new NotImplementedException("Unimplemented propertyType " + thisSP.propertyType + ".");
            }
        }

        public static ValueT Value<ValueT>(this SerializedProperty thisSP)
        {
            Type valueType = typeof(ValueT);

            // First, do special Type checks
            if (valueType.IsEnum)
                return (ValueT)Enum.ToObject(valueType, thisSP.enumValueIndex);

            // Next, check for literal UnityEngine struct-types
            // @note: ->object->ValueT double-casts because C# is too dumb to realize that the ValueT in each situation is the exact type needed.
            // 	e.g. `return thisSP.colorValue` spits _error CS0029: Cannot implicitly convert type `UnityEngine.Color' to `ValueT'_
            // 	and `return (ValueT)thisSP.colorValue;` spits _error CS0030: Cannot convert type `UnityEngine.Color' to `ValueT'_
            if (typeof(Color).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.colorValue;
            else if (typeof(LayerMask).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.intValue;
            else if (typeof(Vector2).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.vector2Value;
            else if (typeof(Vector3).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.vector3Value;
            else if (typeof(Rect).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.rectValue;
            else if (typeof(AnimationCurve).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.animationCurveValue;
            else if (typeof(Bounds).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.boundsValue;
            else if (typeof(Gradient).IsAssignableFrom(valueType))
                return (ValueT)(object)SafeGradientValue(thisSP);
            else if (typeof(Quaternion).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.quaternionValue;

            // Next, check if derived from UnityEngine.Object base class
            if (typeof(UnityEngine.Object).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.objectReferenceValue;

            // Finally, check for native type-families
            if (typeof(int).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.intValue;
            else if (typeof(bool).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.boolValue;
            else if (typeof(float).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.floatValue;
            else if (typeof(string).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.stringValue;
            else if (typeof(char).IsAssignableFrom(valueType))
                return (ValueT)(object)thisSP.intValue;

            // And if all fails, throw an exception.
            throw new NotImplementedException("Unimplemented propertyType " + thisSP.propertyType + ".");
        }

        //public static dynamic Value(this SerializedProperty thisSP)
        //{
        //    switch (thisSP.propertyType)
        //    {
        //        case SerializedPropertyType.Integer:
        //            return thisSP.intValue;
        //        case SerializedPropertyType.Boolean:
        //            return thisSP.boolValue;
        //        case SerializedPropertyType.Float:
        //            return thisSP.floatValue;
        //        case SerializedPropertyType.String:
        //            return thisSP.stringValue;
        //        case SerializedPropertyType.Color:
        //            return thisSP.colorValue;
        //        case SerializedPropertyType.ObjectReference:
        //            return thisSP.objectReferenceValue;
        //        case SerializedPropertyType.LayerMask:
        //            return thisSP.intValue;
        //        case SerializedPropertyType.Enum:
        //            int enumI = thisSP.enumValueIndex;
        //            return new KeyValuePair<int, string>(enumI, thisSP.enumNames[enumI]);
        //        case SerializedPropertyType.Vector2:
        //            return thisSP.vector2Value;
        //        case SerializedPropertyType.Vector3:
        //            return thisSP.vector3Value;
        //        case SerializedPropertyType.Rect:
        //            return thisSP.rectValue;
        //        case SerializedPropertyType.ArraySize:
        //            return thisSP.intValue;
        //        case SerializedPropertyType.Character:
        //            return (char)thisSP.intValue;
        //        case SerializedPropertyType.AnimationCurve:
        //            return thisSP.animationCurveValue;
        //        case SerializedPropertyType.Bounds:
        //            return thisSP.boundsValue;
        //        case SerializedPropertyType.Gradient:
        //            return SafeGradientValue(thisSP);
        //        case SerializedPropertyType.Quaternion:
        //            return thisSP.quaternionValue;

        //        default:
        //            throw new NotImplementedException("Unimplemented propertyType " + thisSP.propertyType + ".");
        //    }
        //}

        /// Access to SerializedProperty's internal gradientValue property getter, in a manner that'll only soft break (returning null) if the property changes or disappears in future Unity revs.
        static Gradient SafeGradientValue(SerializedProperty sp)
        {
            BindingFlags instanceAnyPrivacyBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            PropertyInfo propertyInfo = typeof(SerializedProperty).GetProperty(
                "gradientValue",
                instanceAnyPrivacyBindingFlags,
                null,
                typeof(Gradient),
                new Type[0],
                null
            );
            if (propertyInfo == null)
                return null;

            Gradient gradientValue = propertyInfo.GetValue(sp, null) as Gradient;
            return gradientValue;
        }
    }
#endif
}
