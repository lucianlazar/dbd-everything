using UnityEngine;
using frame8.CSharpAssemblyEditorUtils.Extensions;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace frame8.CSharpAssemblyEditorUtils
{
#if UNITY_EDITOR
	public class EditorGUIUtils8 : Singleton8<EditorGUIUtils8>
	{
		#region implemented abstract members of Singleton8
		public override void Init()
		{
		}
		#endregion

		public void DrawRedLineSeparator()
		{
			EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1f), Color.red);
		}

		public void DrawGreyLineSeparator()
		{
			EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1f), Color.grey);
		}

		public bool DrawFoldOutWithGUIStyle(bool foldOut, string label, GUIStyle guiStyle)
		{
			return DrawFoldOutInternal(foldOut, label, guiStyle, MessageType.None);
		}

		public bool DrawFoldOutWithMessageBoxStyle(bool foldOut, string label, MessageType messageType)
		{
			return DrawFoldOutInternal(foldOut, label, null, messageType);
		}

		public void BeginIndentedZone(params GUILayoutOption[] options)
		{
			BeginIndentedZone(null, null, options);
		}

		public void BeginIndentedZone(float? spacePixels = null, params GUILayoutOption[] options)
		{
			BeginIndentedZone(null, spacePixels, options);
		}

		public void BeginIndentedZone(GUIStyle style, float? spacePixels = null, params GUILayoutOption[] options)
		{
			if (style == null)
				EditorGUILayout.BeginHorizontal(options);
			else
				EditorGUILayout.BeginHorizontal(style, options);

			if (spacePixels == null)
				EditorGUILayout.Space();
			else
				GUILayout.Space(spacePixels.Value);

			EditorGUILayout.BeginVertical();
		}

		public void EndIndentedZone()
		{
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();
		}

		public float GetCalculatedWidthForText(string label, bool expandBy10Percent = true)
		{
			GUIContent guiContent = new GUIContent(label + " "); // make sure label is at least one char long
			float width = GUI.skin.label.CalcSize(guiContent).x;
			if (expandBy10Percent)
				width *= 1.1f;

			return width;
		}

		bool DrawFoldOutInternal(bool foldOut, string label, GUIStyle guiStyle, MessageType messageType)
		{
			EditorGUILayout.BeginHorizontal();
			foldOut = EditorGUILayout.Foldout(foldOut, " ");

			// This is a hack for drawing the label ontop the arrow (the effect is not visible, since the arrow has a small width)
			Rect rect = EditorGUILayout.GetControlRect(true, 20);
			rect.center = new Vector2((rect.center.x / 2) + rect.center.x * 0.07f, rect.center.y);

			if (guiStyle == null)
				EditorGUI.HelpBox(rect, label, messageType);
			else
				EditorGUI.LabelField(rect, label, guiStyle);
			//EditorGUILayout.LabelField(label, guiStyle);
			EditorGUILayout.EndHorizontal();

			return foldOut;
		}

		GUILayoutOption[] GetGUILayoutParametersForCongestedElement(float labelWidth, float contentWidth = 0f)
		{
			return new GUILayoutOption[] { GUILayout.ExpandWidth(false), GUILayout.MinWidth(labelWidth + contentWidth) };
		}

		// content is a string => compute text's width
		GUILayoutOption[] GetGUILayoutParametersForCongestedElement(float labelWidth, string content)
		{
			return GetGUILayoutParametersForCongestedElement(labelWidth, GetCalculatedWidthForText(content));
		}

		// content is a bool => use a toggle's width
		GUILayoutOption[] GetGUILayoutParametersForCongestedElement(float labelWidth, bool content)
		{
			return GetGUILayoutParametersForCongestedElement(labelWidth, GUI.skin.toggle.fixedWidth);
		}


		/// <summary>
		/// Create a EditorGUILayout. TextField with no space between label and text field
		/// </summary>
		public void DrawCongestedLabel(string label, GUIStyle style = null, params GUILayoutOption[] options)
		{
			//if (style == null)
			//    style = GUIStyle.none;

			// Caching original width in order to restore it after draw
			float originalWidth = EditorGUIUtility.labelWidth;
			float labelWidth = GetCalculatedWidthForText(label);

			// Setting the label width, drawing for a width of labelWidth
			EditorGUIUtility.labelWidth = labelWidth;
			ArrayUtility.AddRange(ref options, GetGUILayoutParametersForCongestedElement(labelWidth));
			if (style == null)
				GUILayout.Label(label, options);
			else
				GUILayout.Label(label, style, options);

			// Restoring original width
			EditorGUIUtility.labelWidth = originalWidth;
		}

		/// <summary>
		/// Create a EditorGUILayout. TextField with no space between label and text field
		/// </summary>
		public string DrawCongestedTextField(string label, string value)
		{
			// Caching original width in order to restore it after draw
			float originalWidth = EditorGUIUtility.labelWidth;
			float labelWidth = GetCalculatedWidthForText(label);

			// Setting the label width, drawing for a total width of (labelWidth + valueBoxWidth) and getting the value
			EditorGUIUtility.labelWidth = labelWidth;
			//var conditionFieldsGUIStyle = new GUIStyle(EditorStyles.textField);
			//conditionFieldsGUIStyle.alignment = TextAnchor.MiddleLeft;
			string newText = EditorGUILayout.TextField(label, value, GetGUILayoutParametersForCongestedElement(labelWidth, value));
			//string newText = EditorGUILayout.TextField(label, value, GetGUILayoutParametersForCongestedElement(labelWidth, value));

			// Restoring original width
			EditorGUIUtility.labelWidth = originalWidth;

			return newText;
		}

		/// <summary>
		/// Create a EditorGUILayout. TextField with no space between label and text field
		/// </summary>
		public bool DrawCongestedToggle(string label, bool value, GUIStyle guiStyle, bool expandBy10Percent = true)
		{
			// Caching original width in order to restore it after draw
			float originalWidth = EditorGUIUtility.labelWidth;
			float labelWidth = GetCalculatedWidthForText(label, expandBy10Percent);

			// Setting the label width, drawing for a total width of (labelWidth + valueBoxWidth) and getting the value
			EditorGUIUtility.labelWidth = labelWidth;
			bool newValue;
			if (guiStyle == null)
				newValue = EditorGUILayout.Toggle(label, value, GetGUILayoutParametersForCongestedElement(labelWidth, value));
			else
				newValue = EditorGUILayout.Toggle(label, value, guiStyle, GetGUILayoutParametersForCongestedElement(labelWidth, value));

			// Restoring original width
			EditorGUIUtility.labelWidth = originalWidth;

			return newValue;
		}

		public bool DrawCongestedToggle(string label, bool value, bool expandBy10Percent)
		{
			return DrawCongestedToggle(label, value, null, expandBy10Percent);
		}

		public bool DrawCongestedToggle(string label, bool value)
		{
			return DrawCongestedToggle(label, value, null);
		}

		public bool DrawCongestedButton(string text, GUIStyle guiStyle = null, bool addAnotherTenUnitsOnWidth = true)
		{
			Rect rect;
			bool hasNoFixedHeight = guiStyle == null || guiStyle.fixedHeight < 1f;
			if (hasNoFixedHeight)
				rect = EditorGUILayout.GetControlRect(false);
			else
				rect = EditorGUILayout.GetControlRect(false, guiStyle.fixedHeight, guiStyle);

			if (hasNoFixedHeight)
				rect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(text, false) + (addAnotherTenUnitsOnWidth ? 10 : 0);

			if (guiStyle == null)
				return GUI.Button(rect, text);
			return GUI.Button(rect, text, guiStyle);
		}

		public bool DrawCongestedButton(GUIContent content, GUIStyle guiStyle = null, bool addAnotherTenUnitsOnWidth = true)
		{
			Rect rect;
			bool hasNoFixedHeight = guiStyle == null || guiStyle.fixedHeight < 1f;
			if (hasNoFixedHeight)
				rect = EditorGUILayout.GetControlRect(false);
			else
				rect = EditorGUILayout.GetControlRect(false, guiStyle.fixedHeight, guiStyle);

			if (hasNoFixedHeight)
			{
				rect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(content.text ?? "", false) + (addAnotherTenUnitsOnWidth ? 10 : 0);
			}
			if (content.image)
			{
				rect.width += content.image.width;
				if (hasNoFixedHeight)
					rect.height = Mathf.Max(rect.height, content.image.width);
			}

			if (guiStyle == null)
				return GUI.Button(rect, content);
			return GUI.Button(rect, content, guiStyle);
		}

		// When typeHint is a type that inherits from UnityEngine.Object, the value 
		// field will require that specific type (Image, Transform etc.) instead of UnityEngine.Object 
		public T DrawSerializedPropertyImproved<T>(SerializedProperty serializedProperty, System.Type typeHint, string label = null)
		{
			if (typeof(Object).IsAssignableFrom(typeHint))
			{
				UnityEngine.Object o;
				if (label == null)
					o = EditorGUILayout.ObjectField(serializedProperty.objectReferenceValue, typeHint, true);
				else
					o = EditorGUILayout.ObjectField(label, serializedProperty.objectReferenceValue, typeHint, true);
				serializedProperty.objectReferenceValue = o;

				return (T)(object)o;
			}
			else
			{
				if (label == null)
					EditorGUILayout.PropertyField(serializedProperty, true);
				else
					EditorGUILayout.PropertyField(serializedProperty, new GUIContent(label), true);

				return serializedProperty.Value<T>();
			}
		}
	}
#endif
}

