

// Used for any kind of succesive animation steps
public abstract class AnimationStepInterval8
{
	public const float FAST = .04f;
	public const float MODERATE = .07f;
	public const float SLOW = .1f;
}

