﻿public enum ChainRelation8
{
    NOT_ON_THE_SAME_CHAIN = 0,
    // They are the same
    A_IS_B,
    // <this> will be <b> at some point if we continuously do <b = b->dest>
    A_IS_BEFORE_B,
    // <this> will be <b> at some point if we continuously do <b = b->source>
    A_IS_AFTER_B
};