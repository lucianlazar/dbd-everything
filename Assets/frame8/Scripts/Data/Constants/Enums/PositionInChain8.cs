﻿
[System.Flags]
public enum PositionInChain8
{
    NotInAChain = 1,
    Start = 1 << 1,
    SomewhereInTheMiddle = 1 << 2,
    End = 1 << 3
};