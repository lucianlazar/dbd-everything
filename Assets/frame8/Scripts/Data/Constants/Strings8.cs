// Here are the most general strings in the game
public class Strings8
{
	// Initialized in GameMnager
	public static string APP_PERSISTENT_DATA_ABSOLUTE_PATH = null;

	public const string UNDEFINED = "undefined";

	// UI
	//public const string INSTANCE_CANVASES_ROOT_GO_NAME = "InstanceCanvasesRoot";
	public const string BACKGROUND_CANVAS_GO_NAME = "BackgroundCanvas";

	// Colors
	public const string RED = "red";
	public const string GREEN = "green";
	public const string BLUE = "blue";
	public const string CYAN = "cyan";
	public const string YELLOW = "yellow";
	public const string MAGENTA = "magenta";
	public const string WHITE = "white";
	public const string BLACK = "black";

	// Runtime Serialization data
	public const string GAME_DATA_DIRECTORY_PATH = "GameData";
	const string GAME_DATA_FILENAME = "GameData.dat";
	public static string GAME_DATA_FILE_PATH = System.IO.Path.Combine(GAME_DATA_DIRECTORY_PATH, GAME_DATA_FILENAME);
	
// Error Strings	
	public const string ERROR_PREFIX = "[Error] "; 
	public const string GAME_SESSION_MANAGER_INSTANCE_ACCESSED_BEFORE_CREATED = ERROR_PREFIX + "GameSessionManager8 accessed, but not yet initialized!";
};

