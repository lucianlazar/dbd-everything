using UnityEngine;
using System.Collections;

public class Objects8 : Singleton8<Objects8>
{
	public GUIStyle guiStyleBig1Text,
					guiStyleBig2Text,
					//guiStyleArrayItem,
					guiStyleItallicText;

	#region implemented abstract members of Singleton8
	public override void Init ()
	{	


		guiStyleBig1Text = new GUIStyle();
		guiStyleBig1Text.fontSize = Numbers8.FONT_SIZE_NORMAL4;

		guiStyleBig2Text = new GUIStyle();
		guiStyleBig2Text.fontSize = Numbers8.FONT_SIZE_3;

		
		guiStyleItallicText = new GUIStyle();
		guiStyleItallicText.fontStyle = FontStyle.Italic;

#if UNITY_EDITOR
		//guiStyleArrayItem = new GUIStyle(UnityEditor.EditorStyles.toolbar);
//		guiStyleArrayItem.fontStyle = FontStyle.Italic;
#endif
	}

	#endregion
}

