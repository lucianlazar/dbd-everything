/*
 * Remember to use CreateInstance() in the derrived class's 
 * for creating instances of the serializable classes
 * 
 * 
 * 
 */

using UnityEngine;

[System.Serializable]
public class BaseScriptableObjectForSerialization8 : ScriptableObject
{
	[HideInInspector]
	[SerializeField]
	string _Dummy = "";

	// The constructor must not be implemented here. The OnEnabled will be used, since it's
	// called after Unity tried to de-serialize the fields
	// Note: make sure this is ALWAYS called
	public virtual void OnEnable()
	{			
		if (_Dummy == "")
		{
			// Let the derived class create its instances using CreateInstance()
			OnFirstInit();
			_Dummy = "initialized";
		}

		// The derived class may want to re-init non-serializable fields
		// after Unity finishes de-serialization
		OnInitNonSerializableData();
	}
	
	// Called once
	public virtual void OnFirstInit() 
	{}

	// The last initializations
	public virtual void OnInitNonSerializableData() 
	{}

	// Called as the MonoBehaviour's one
	public virtual void OnGUI() 
	{}
}

