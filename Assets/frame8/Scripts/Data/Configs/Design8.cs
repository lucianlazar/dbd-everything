using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

//[System.Serializable]
[ExecuteInEditMode]
public class Design8 : MonoBehaviour
{
	protected virtual void OnEnable()
	{
		// Init non-serializable data here
	}
	
	protected virtual void Awake () {}
	
#if UNITY_EDITOR
	public virtual void OnGUI8()
	{
		// Draw properties here
	}
#endif
}

