﻿using System.Collections;
using UnityEngine;

namespace Delegates
{
	//public delegate void Void0();
	//public delegate void Void1Int(int i);
	//public delegate void Void1Float(float i);
	//public delegate void Void1String(string s);
	//public delegate void Void1Bool(bool b);
	//public delegate void Void1Activity(Activity8 activity);
	//public delegate void Void1ListOfActivities(System.Collections.Generic.List<Activity8> activity);
	//public delegate bool Bool0();
	//public delegate bool Bool1Int(int i);
	//public delegate void Void1Transform(UnityEngine.Transform transform);
	//public delegate void Void1Transform2Int(UnityEngine.Transform transform, int n);
	//public delegate void Void1ControllableObject(ControllableObject obj);
	//public delegate bool Bool1ControllableObject(ControllableObject obj);
	//public delegate int Int2Collider(Collider obj1, Collider obj2);
	//public delegate int Int0();
	//public delegate int Int1Int(int i);
	//public delegate IEnumerator IEnumerator0();
	//public delegate void Void1Collider(Collider collider);
	//public delegate void Void1Vector3(Vector3 vector);
	//public delegate void Void1BytesArray(byte[] bytes);
}