using UnityEngine;
#if FRAME8_PROTOBUF_INTEGRATION
using ProtobufSerializables8.frame8Serializables;

public static class SerializableVector3Extensions
{
	public static SerializableVector3 WithValueSetFromVector3(this SerializableVector3 @this, Vector3 vector) 
	{ 
		@this.x = vector.x; @this.y = vector.y; @this.z = vector.z; 
		return @this;
	}
	
	public static void SetFromVector3(this SerializableVector3 @this, Vector3 vector) 
	{ 
		@this.x = vector.x; @this.y = vector.y; @this.z = vector.z; 
	}
	
	public static Vector3 ToVector3(this SerializableVector3 @this) 
	{ 
		return new Vector3(@this.x, @this.y, @this.z);
	}
}
#endif

