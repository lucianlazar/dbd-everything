using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerRequest8
{
	public float TimeWhenStartedExecution {get; set;}
	public float TimeOut {get; set;}
	public int ServiceType {get; protected set;}
	public RequestType Type {get; protected set;}
	public Dictionary<string, string> StringParams {get; protected set;}
	public IServerRequestListener8 RequestListener {get{return _RequestListener;}}
	
	// Only one of these must be used
	public Dictionary<string, byte[]> BinaryParams {get; protected set;}
	public byte[] RawBinaryDataForPOSTOnly {get; set;}
	
	IServerRequestListener8 _RequestListener;
	
	public ServerRequest8(int serviceType, RequestType requestType, IServerRequestListener8 requestListener, float timeOut = 3f)
	{
		TimeWhenStartedExecution = 0f;
		StringParams = new Dictionary<string, string>();
		BinaryParams = new Dictionary<string, byte[]>();
		ServiceType = serviceType;
		Type = requestType;
		_RequestListener = requestListener;
		TimeOut = timeOut;
	}
		
	public enum RequestType
	{
		GET,
		POST_WITH_NAMED_PARAMS,
		POST_WITH_RAW_BINARY,
	}
}

