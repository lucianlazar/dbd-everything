using UnityEngine;

public struct Vector3i
{
    public int x, y, z;

    public Vector3i(int i)
    {
        x = y = z = i;
    }
	
	public Vector3i(int xx, int yy, int zz)
	{
		x = xx;
		y = yy;
		z = zz;
	}
	
	public Vector3i(float xx, float yy, float zz)
	{
		x = (int)xx;
		y = (int)yy;
		z = (int)zz;
	}

	public Vector3i(Vector3 v)
	{
		x = (int)v.x;
		y = (int)v.y;
		z = (int)v.z;
	}

    public static Vector3i operator +(Vector3i a, Vector3i b)
    {
        a.x += b.x;
        a.y += b.y;
        a.z += b.z;

        return a;
    }
}