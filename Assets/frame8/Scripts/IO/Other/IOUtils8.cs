using UnityEngine;
using System.Collections;
using System.IO;
using System.Security.AccessControl;

public class IOUtils8 : Singleton8<IOUtils8>
{	
	public string GetCorrectPathReplacingInvalidSlashes(string path)
	{
		char directorySeparatorCharToBeReplaced = '/';
//		char systemDirectorySeparatorChar = GetCorrectDirectoriesSeparatorChar();
		char systemDirectorySeparatorChar = Path.DirectorySeparatorChar;
		if (directorySeparatorCharToBeReplaced == systemDirectorySeparatorChar)
			directorySeparatorCharToBeReplaced = '\\';
		
		return path.Replace(directorySeparatorCharToBeReplaced, systemDirectorySeparatorChar);
	}
	
//	public char GetCorrectDirectoriesSeparatorChar()
//	{
//		string dummy = Path.Combine("test", "test");
//		return dummy[dummy.Length - 1 - 4];
//	}

	public string GetCorrectPersistentDataPath (string pathToAppendToPersistentDataPath)
	{
		return GetCorrectPathReplacingInvalidSlashes(Path.Combine(Strings8.APP_PERSISTENT_DATA_ABSOLUTE_PATH, pathToAppendToPersistentDataPath));
	}

//	public void CreateNecesarryDirectoriesForPathToBeExistent(string path)
//	{
//		string[] directoryNames = path.Split(Path.DirectorySeparatorChar);
//		
//		string currentDirectoryPath = directoryNames[0]; // root
//		for (int i = 1; i < directoryNames.Length; ++i)
//		{
//			if (directoryNames[i].Length > 0)
//			{
//				currentDirectoryPath = Path.Combine(currentDirectoryPath, directoryNames[i]);
//				if (!Directory.Exists(currentDirectoryPath))
//					Directory.CreateDirectory(currentDirectoryPath);
//			}
//		}
//	}
	

	public static System.Action<byte[]> ReverseIfLEMachine = InitReverseIfLEMachineDelegate;
	static void InitReverseIfLEMachineDelegate(byte[] bytes)
	{
		if (System.BitConverter.IsLittleEndian)
		{
			ReverseIfLEMachine = (bytes2) => System.Array.Reverse(bytes2);
			System.Array.Reverse(bytes); // on first call we need to explicitly call the ReverseIfLEMachine's implementation
		}
		else
			ReverseIfLEMachine = (bytes2) => {};
	}
	
	
	
	
//	public byte[] GetBytesBE(int val)
//	{	
//		byte[] bytes = System.BitConverter.GetBytes(val);
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(bytes);
//		
//		return bytes;
//	}
//	
//	public byte[] GetBytesBE(float val)
//	{	
//		byte[] bytes = System.BitConverter.GetBytes(val);
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(bytes);
//		
//		return bytes;
//	}
//	
//	public byte[] GetBytesBE(double val)
//	{	
//		byte[] bytes = System.BitConverter.GetBytes(val);
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(bytes);
//		
//		return bytes;
//	}
//	
//	public byte[] GetBytesBE(long val)
//	{	
//		byte[] bytes = System.BitConverter.GetBytes(val);
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(bytes);
//		
//		return bytes;
//	}
//	
//	public byte[] GetBytesBE(char val)
//	{	
//		byte[] bytes = System.BitConverter.GetBytes(val);
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(bytes);
//		
//		return bytes;
//	}
	
	public byte[] GetBytesBE(char[] val)
	{	
		int numChars = val.Length;
		byte[] resultBytes = new byte[numChars * 2];
		byte[] curBytes;
		for (int i = 0, j = 0; i < numChars; ++i)
		{
			curBytes = System.BitConverter.GetBytes(val[i]);
			ReverseIfLEMachine(curBytes);
			resultBytes[j++] = curBytes[0];
			resultBytes[j++] = curBytes[1];
		}
		
		return resultBytes;
	}
	
//	public int GetIntFromBytesBE(byte[] bytesVal)
//	{	
//		byte[] clonedBytes = bytesVal.Clone() as byte[];
//		
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(clonedBytes);
//		
//		return System.BitConverter.ToInt32(clonedBytes);
//	}
//	
//	public float GetFloatFromBytesBE(byte[] bytesVal)
//	{	
//		byte[] clonedBytes = bytesVal.Clone() as byte[];
//		
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(clonedBytes);
//		
//		return System.BitConverter.ToSingle(clonedBytes, 0);
//	}
//	
//	public char GetCharFromBytesBE(byte[] bytesVal)
//	{	
//		byte[] clonedBytes = bytesVal.Clone() as byte[];
//		if (System.BitConverter.IsLittleEndian)
//			System.Array.Reverse(clonedBytes);
//		
//		return System.BitConverter.ToChar(clonedBytes, 0);
//	}
	
	public char[] GetCharsFromBytesBE(byte[] bytesVal)
	{	
		int numBytes = bytesVal.Length;
		int numChars = numBytes / 2;
		char[] resultedChars = new char[numChars];
		byte[] bytesForCurChar = new byte[2];
		for (int i = 0, j = 0; i < numChars; ++i)
		{
			bytesForCurChar[0] = bytesVal[j++];
			bytesForCurChar[1] = bytesVal[j++];
			ReverseIfLEMachine(bytesForCurChar);
			resultedChars[i] = System.BitConverter.ToChar(bytesForCurChar, 0);
		}
		
		return resultedChars;
	}
	
	
	
	
	#region implemented abstract members of Singleton8
	public override void Init ()
	{
	}
	#endregion
	
	// Returns false if directory doesn't exist or the contents can't be read 
//	public bool CanReadDirectoryContents(string directoryPath)
//	{
//		if (!Directory.Exists(directoryPath))
//			return false;
//	
//		var readAllow = false;
//		var readDeny = false;
//		var accessControlList = Directory.GetAccessControl(directoryPath);
//		if(accessControlList == null)
//			return false;
//		var accessRules = accessControlList.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));
//		if(accessRules == null || accessRules.Count == 0)
//			return false;
//		
//		foreach (FileSystemAccessRule rule in accessRules)
//		{
//			if ((FileSystemRights.Traverse & rule.FileSystemRights) != FileSystemRights.Read) 
//				continue;
//			
//			if (rule.AccessControlType == AccessControlType.Allow)
//				readAllow = true;
//			else if (rule.AccessControlType == AccessControlType.Deny)
//				readDeny = true;
//		}
//		
//		return readAllow && !readDeny;
//	}
}

