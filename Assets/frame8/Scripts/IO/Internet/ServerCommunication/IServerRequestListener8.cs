
public interface IServerRequestListener8
{
	// The receiver must dispose the WWW!
	void OnTimeoutOrNetworkError(ServerRequest8 request, UnityEngine.WWW wwwObject);
	// The receiver must dispose the WWW!
	void OnResponseReceived(ServerRequest8 request, UnityEngine.WWW wwwObject);
}

