
// IMPORTANT!: The chance for a request to fail due to Unity's coroutines and not due to server is zero only if there's
// one frame between the last request. Use Task8 with delaybetweenExecutions=0f for multiple, inline requests.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public sealed class ServerCommunicator8 : MonoBehaviour, IDisposable
{
	public static Dictionary<int, string> MapServiceTypeToURL {get{return _MapServiceTypeToURL;} set{_MapServiceTypeToURL = value;}}
	static Dictionary<int, string> _MapServiceTypeToURL = new Dictionary<int, string>();
	
	Dictionary<ServerRequest8, WWW> _MapCurrentExecutingRequestsToTheirWWWs = new Dictionary<ServerRequest8, WWW>();
	Dictionary<ServerRequest8, WWW> _MapUpcomingExecutingRequestsToTheirWWWs = new Dictionary<ServerRequest8, WWW>();
	bool _CheckingProgressForWWWsAndDispatchEventsCoroutine;
	
	public static ServerCommunicator8 CreateInstance()
	{
		if (_MapServiceTypeToURL.Count == 0)
			throw new UnityException("_MapServiceTypeToURL is empty! Populate this.MapServiceTypeToURL in your app's initialization code block.");
			
		ServerCommunicator8 instance = new GameObject(typeof(ServerCommunicator8).Name).AddComponent<ServerCommunicator8>();
		
		return instance;
	}
	
	public void ExecuteRequest(ServerRequest8 request)
	{
		StartCoroutine(ExecuteRequestInternal(request));
	}
	
	IEnumerator ExecuteRequestInternal(ServerRequest8 request)
	{
		WWW newWWW;
		ServerRequest8.RequestType type = request.Type;
		switch (type)
		{
		case ServerRequest8.RequestType.GET:
			// TODO
			newWWW = null;
			break;
		case ServerRequest8.RequestType.POST_WITH_NAMED_PARAMS:
			// TODO
			WWWForm wwwForm = GetWWWFormFor(request);
			newWWW = null;
			break;
		case ServerRequest8.RequestType.POST_WITH_RAW_BINARY:
			newWWW = new WWW(_MapServiceTypeToURL[request.ServiceType], request.RawBinaryDataForPOSTOnly);
			break;
		default:
			throw new UnityException("RequestType not valid!");
		}
		
//		// Wait for current requests to be sent
//		while (_CheckingProgressForWWWsAndDispatchEventsCoroutine)
//			yield return new WaitForSeconds(.1f);
		
		_MapUpcomingExecutingRequestsToTheirWWWs[request] = newWWW;
		request.TimeWhenStartedExecution = Time.time;
//		Task8.CreateAndStart(.1f, 0f, () => {StartCoroutine(CheckProgressForWWWsAndDispatchEvents()); return false;}).name = "ServerRequest8 " + request.TimeWhenStartedExecution; 
		if (!_CheckingProgressForWWWsAndDispatchEventsCoroutine)
		{
			_CheckingProgressForWWWsAndDispatchEventsCoroutine = true;
			StartCoroutine(CheckProgressForWWWsAndDispatchEvents());
		}
		
		yield return newWWW;
	}
	
	IEnumerator CheckProgressForWWWsAndDispatchEvents()
	{
		// Wait one frame, so when this is called from "ExecuteRequestInternal()", it'll wait for the www to start;
		// and when it's called from recursion, it'll wait for other requests to be added to "_MapUpcomingExecutingRequestsToTheirWWWs"
		yield return null;
	
		// Remove null requests from both maps
		bool foundNull;
		Dictionary<ServerRequest8, WWW> currentMapToCheck = null;
		Dictionary<ServerRequest8, WWW>.KeyCollection keys;
		Func<bool> removeNullWWWsFromCurrentMapToCkeckAndReturnFoundNullFlag = () =>
		{
			keys = currentMapToCheck.Keys;
			foreach (ServerRequest8 request in keys)
			{
				if (currentMapToCheck[request] == null)
				{
					currentMapToCheck.Remove(request);
					return true;
				}
			}
			return false;
		};
		do
		{
			currentMapToCheck = _MapUpcomingExecutingRequestsToTheirWWWs;
			foundNull = removeNullWWWsFromCurrentMapToCkeckAndReturnFoundNullFlag();
			currentMapToCheck = _MapCurrentExecutingRequestsToTheirWWWs;
			foundNull = (removeNullWWWsFromCurrentMapToCkeckAndReturnFoundNullFlag() || foundNull);
		}
		while (foundNull);
		
		// Move upcoming requests to current executing requests
		keys = _MapUpcomingExecutingRequestsToTheirWWWs.Keys;
		foreach (ServerRequest8 request in keys)
			_MapCurrentExecutingRequestsToTheirWWWs[request] = _MapUpcomingExecutingRequestsToTheirWWWs[request];
		_MapUpcomingExecutingRequestsToTheirWWWs.Clear();
			
		keys = _MapCurrentExecutingRequestsToTheirWWWs.Keys;
		var keysEnumerator = keys.GetEnumerator();
		// No valid requests to check for OR no requests were added after last recursive step => end
		if (!keysEnumerator.MoveNext())
		{
			_CheckingProgressForWWWsAndDispatchEventsCoroutine = false;
		}
		// Dispatch events for finished/timedOut requests, dispose+remove them from "_MapCurrentExecutingRequestsToTheirWWWs"
		else
		{
			WWW curWWw;
			IServerRequestListener8 curListener;
			List<ServerRequest8> requestsToStopChecking = new List<ServerRequest8>();
			int numRequestsToStopChecking = 0;
			ServerRequest8 request;
			do
			{
				request = keysEnumerator.Current;
				curWWw = _MapCurrentExecutingRequestsToTheirWWWs[request];
				curListener = request.RequestListener;
				if (curWWw.isDone)
				{
					if (curListener != null)
					{
						// www executed
						if (string.IsNullOrEmpty(curWWw.error))
							curListener.OnResponseReceived(request, curWWw);
						else
							curListener.OnTimeoutOrNetworkError(request, curWWw);
					}
					requestsToStopChecking.Add(request);
					++numRequestsToStopChecking;
				}
				else
				{
					if (Time.time - request.TimeWhenStartedExecution > request.TimeOut)
					{
						if (curListener != null)
							curListener.OnTimeoutOrNetworkError(request, curWWw);
						requestsToStopChecking.Add(request);
						++numRequestsToStopChecking;
					}
				}
			}
			while (keysEnumerator.MoveNext());
			
			// Stop checking for done or timed out requests
			for (int i = 0; i < numRequestsToStopChecking; ++i)
			{
				request = requestsToStopChecking[i];
				_MapCurrentExecutingRequestsToTheirWWWs[request].Dispose();
				_MapCurrentExecutingRequestsToTheirWWWs.Remove(request);
			}
			
			StartCoroutine(CheckProgressForWWWsAndDispatchEvents());
		}
		yield break;
	}
	
	WWWForm GetWWWFormFor(ServerRequest8 request)
	{
		WWWForm wwwForm = new WWWForm();
		// Take the string params
		Dictionary<string, string> parameters = request.StringParams;
		var keys = parameters.Keys;
		foreach (var key in keys)
			wwwForm.AddField(key, parameters[key]);
		
		// Take the binary params
		Dictionary<string, byte[]> binaryParameters = request.BinaryParams;
		var binaryKeys = binaryParameters.Keys;
		foreach (var key in binaryKeys)
			wwwForm.AddBinaryData(key, binaryParameters[key]);
		
		return wwwForm;
	}

	#region IDisposable implementation
	public void Dispose ()
	{
		GameObject.Destroy(gameObject);
	}
	#endregion	
	
	void OnDestroy()
	{
	
	}
}

