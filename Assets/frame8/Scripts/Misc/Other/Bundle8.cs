using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bundle8
{
	Dictionary<string, object> _Arguments = new Dictionary<string, object>();
	
	public void Put(string key, object value) {_Arguments[key] = value;}
	public T Get<T>(string key) 
	{
		if (_Arguments.ContainsKey(key)) 
			return (T)_Arguments[key]; 

		throw new System.Exception("No object found for key '"+key+"'");
	}
	public T Get<T>(string key, T valueIfKeyNotFound) 
	{
		if (_Arguments.ContainsKey(key)) 
			return (T)_Arguments[key]; 
		return valueIfKeyNotFound;
	}
}

