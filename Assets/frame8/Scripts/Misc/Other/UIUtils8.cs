using UnityEngine;
using System.Collections;

public class UIUtils8 : Singleton8<UIUtils8>
{

	// It assumes it haves a parent
	public Vector2 GetBottomLeftCornerDisplacementFromParentBottomLeftCorner(RectTransform rt)
	{
		Rect rect = rt.rect;
		Rect parentRect = (rt.parent as RectTransform).rect;
		Vector3 locPos = rt.localPosition;
		return new Vector2(rect.x - parentRect.x + locPos.x, rect.y - parentRect.y + locPos.y);
	}
	
	public void BringRectToAnchorsFor(Transform[] transforms)
	{
		foreach (Transform tr in transforms)
			BringRectToAnchorsFor(tr);
	}
	
	public void BringRectToAnchorsFor(Transform transform)
	{
		RectTransform rt = transform as RectTransform;
		if (rt != null)
			rt.offsetMin = rt.offsetMax = Vector2.zero;
	}
	
	public void BringAnchorsToRectFor(Transform[] transforms)
	{
		foreach (Transform tr in transforms)
			BringAnchorsToRectFor(tr);
	}
	
	public void BringAnchorsToRectFor(Transform transform)
	{
		RectTransform parentRT;
		RectTransform rt = transform as RectTransform;
		if (transform != null)
		{
			parentRT = rt.parent as RectTransform;
			if (parentRT == null) // rt is the root
			{
				rt.anchorMin = Vector2.zero;
				rt.anchorMax = Vector2.one;
			}
			else
			{
				Rect rect = rt.rect;
				Rect parentRect = parentRT.rect;
				Vector2 dispFromParentBottomLeftCorner = 
					UIUtils8.Instance.GetBottomLeftCornerDisplacementFromParentBottomLeftCorner(rt);
				
				if (parentRect.width > 0f && parentRect.height > 0f)
				{
					rt.anchorMin = 
						new Vector2(
							Mathf.Clamp01(dispFromParentBottomLeftCorner.x / parentRect.width),
							Mathf.Clamp01(dispFromParentBottomLeftCorner.y / parentRect.height));
					
					rt.anchorMax = 
						new Vector2(
							Mathf.Clamp01((dispFromParentBottomLeftCorner.x + rect.width) / parentRect.width),
							Mathf.Clamp01((dispFromParentBottomLeftCorner.y + rect.height) / parentRect.height));
				}
			}
			rt.offsetMin = rt.offsetMax = Vector2.zero;
		}
	}
	
	// Assuming <rectTransform> has a parent
	public void GrowSizeDirectionally(RectTransform rectTransform, RectTransform.Edge edge, float amount)
	{	
		Vector2 currentSize = rectTransform.rect.size;
//		Debug.Log("currentSize=" + currentSize);
		RectTransform parentRectTransform = rectTransform.parent as RectTransform; 
		Vector2 parentSize = parentRectTransform.rect.size;
//		Debug.Log("parentSize=" + parentSize);
		Vector2 vParentBottomLeftCornerToBottomLeftCorner = GetBottomLeftCornerDisplacementFromParentBottomLeftCorner(rectTransform);
//		Debug.Log("vParentBottomLeftCornerToBottomLeftCorner=" + vParentBottomLeftCornerToBottomLeftCorner);
		Vector2 vUpRightCornerToParrentUpRightCorner = parentSize - (vParentBottomLeftCornerToBottomLeftCorner + currentSize);
//		Debug.Log("vUpRightCornerToParrentUpRightCorner=" + vUpRightCornerToParrentUpRightCorner);
		Vector2 anchorMin = rectTransform.anchorMin, anchorMax = rectTransform.anchorMax;
		float inset, finalSize;
		
		// ... so we'll write "SetInsetAndSizeFromParentEdge" only one time below
//		switch (edge)
//		{
//		// Move the left edge
//		case RectTransform.Edge.Left:
//			edge = RectTransform.Edge.Right;
//			inset = vUpRightCornerToParrentUpRightCorner.x;
//			finalSize = currentSize.x + amount;
//			Debug.Log(0);
//			break;
//			
//		case RectTransform.Edge.Top:
//			edge = RectTransform.Edge.Bottom;
//			inset = vParentBottomLeftCornerToBottomLeftCorner.y;
//			finalSize = currentSize.y + amount;
//			Debug.Log(1);
//			break;
//			
//		case RectTransform.Edge.Right:
//			edge = RectTransform.Edge.Left;
//			inset = vParentBottomLeftCornerToBottomLeftCorner.x;
//			finalSize = currentSize.x + amount;
//			Debug.Log(2);
//			break;
//			
//		default:
//			edge = RectTransform.Edge.Top;
//			inset = vUpRightCornerToParrentUpRightCorner.y;
//			finalSize = currentSize.y + amount;
//			Debug.Log(3);
//			break;
//		}
//		Debug.Log(edge+";"+inset+";"+finalSize+";");
//		rectTransform.SetInsetAndSizeFromParentEdge(edge, inset, finalSize);
//		rectTransform.anchorMin = anchorMin;
		//		rectTransform.anchorMax = anchorMax;
		bool edgeRight = edge == RectTransform.Edge.Right;
		bool horizontal = edgeRight || edge == RectTransform.Edge.Left;
		bool edgeTop = edge == RectTransform.Edge.Top;
		rectTransform.SetSizeWithCurrentAnchors( horizontal ? 
				RectTransform.Axis.Horizontal : RectTransform.Axis.Vertical, horizontal ? currentSize.x + amount : currentSize.y + amount);
	
		Vector3 localPosDiplacementToAdd = Vector3.zero;
		if (horizontal)
		{
			if (edgeRight)
				localPosDiplacementToAdd.x += amount/2;
			else 
				localPosDiplacementToAdd.x -= amount/2;
		}
		else
		{
			if (edgeTop)
				localPosDiplacementToAdd.y += amount/2;
			else 
				localPosDiplacementToAdd.y -= amount/2;
		}
		
		rectTransform.localPosition += localPosDiplacementToAdd;
	}

	public override void Init ()
	{
	}
}

