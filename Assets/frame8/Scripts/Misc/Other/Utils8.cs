using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;


public class Utils8 : Singleton8<Utils8>
{
	Dictionary<Color, String> _MapColorToColorName;
    Numbers8 numbers;

    public override void Init()
    {
        _MapColorToColorName = new Dictionary<Color, String>();
        RegisterColorNames();
    }

	// It also registeres colors that have 0f alpha
	void RegisterColorNames()
	{
		Color 
			red = Color.red,
			green = Color.green,
			blue = Color.blue,
			cyan = Color.cyan,
			magenta = Color.magenta,
			yellow = Color.yellow,
			yellow2 = new Color(1f,1f,0,1f), // unity's yellow is slightly different
			white = Color.white,
			black = Color.black;

		for (int i = 0; i < 2; ++i)
		{
			if (i == 1)
			{
				red.a = green.a = blue.a = cyan.a = magenta.a = yellow.a = yellow2.a = white.a = black.a = 0f;
			}
			
			_MapColorToColorName.Add(red, Strings8.RED);
			_MapColorToColorName.Add(green, Strings8.GREEN);
			_MapColorToColorName.Add(blue, Strings8.BLUE);
			_MapColorToColorName.Add(cyan, Strings8.CYAN);
			_MapColorToColorName.Add(magenta, Strings8.MAGENTA);
			_MapColorToColorName.Add(yellow, Strings8.YELLOW);
			_MapColorToColorName.Add(yellow2, Strings8.YELLOW);
			_MapColorToColorName.Add(white, Strings8.WHITE);
			_MapColorToColorName.Add(black, Strings8.BLACK);
		}
	}

	// +- 1 combinations for x,y,z, excluding the position itself and invalid position
	public List<int[]> NearValidDiscretePositions(int[] discretePos, int depth, int minDiscretePosComponentExclusive, int maxDiscretePosComponentInclusive)
	{	
		// Imagine this is a rubik cube's blocks's positions surrounding discrete position (0,0,0)
		List<int[]> combinations = GetAllCombinations(-depth, depth, 3);
		// We'll alter allCombinations, adding to all its positions the discretePos, in order to obtain the actual
		// surrounding discrete positions
		List<int[]> toRemove = new List<int[]>();
		foreach (int[] comb in combinations)
		{
			AddValues3(comb, comb, discretePos);	
			// We're removing invalid positions on the way
			if (AreEqualByValue(comb, discretePos) // also remove the discretePos
			    || InvalidDiscretePosition(comb, minDiscretePosComponentExclusive, maxDiscretePosComponentInclusive))
			{
				toRemove.Add(comb);
			}
		}
		// Actually remove them
		foreach (int[] comb in toRemove)
		{
			combinations.Remove(comb);	
		}
		toRemove.Clear();
		// Only the valid ones
		return combinations;
	}
	
	public bool InvalidDiscretePosition(int[] pos, int minExclusive, int maxInclusive){
		for (int i = 0; i < 3; ++i)
			if (pos[i] <= minExclusive || pos[i] > maxInclusive)
				return true;	
		return false;
	}

	public int RandomSign()
	{
		if (UnityEngine.Random.Range(0, 2) == 0)
			return 1;
		
		return -1;
	}

    public void AddValues3(int[] result, int[] values1, int[] values2)
	{
		for (int i = 0; i < 3; ++i)
		{
            result[i] = values1[i] + values2[i];
		}
	}
	
    public List<int[]> GetAllCombinations(int min, int max, int combinationLength)
    {
        if (min > max || combinationLength < 1)
            return new List<int[]>(); // empty list; null is too ugly :d

        return GetAllCombinationsRec(min, max + 1, 0, combinationLength, new int[combinationLength]);
    }

    // Note maxVal is excluded
    private List<int[]> GetAllCombinationsRec(
        int minVal, int maxVal,
        int currentLen, int targetLen,
        int[] currentComb)
    {
        List<int[]> result = new List<int[]>();

        int newCurrentLen = currentLen + 1;
        int[] newCurrentComb;

        if (newCurrentLen == targetLen)
        {
            // Add the final combinations (the leafs of the tree) to the result list
            for (int val = minVal; val < maxVal; ++val)
            {
                newCurrentComb = new int[targetLen];
                currentComb.CopyTo(newCurrentComb, 0);
                newCurrentComb[currentLen] = val;

                result.Add(newCurrentComb);
            }

        }
        // If the combination is not fully constructed, we go one step deeper in the tree, 
        // creating another |[minVal,maxVal)| child nodes of this one, and each of their's return results will be 
        // concatenated with the current list
        else
        {
            List<int[]> furtherResult;
            for (int val = minVal; val < maxVal; ++val)
            {
                newCurrentComb = new int[targetLen];
                currentComb.CopyTo(newCurrentComb, 0);
                newCurrentComb[currentLen] = val;

                furtherResult = GetAllCombinationsRec(minVal, maxVal, newCurrentLen, targetLen, newCurrentComb);

                result.AddRange(furtherResult);
            }
				
        }

        return result;
    }
	
	// By-value
	public bool AreEqualByValue(int[] a, int[] b)
	{
		int sizeA = a.Length;
		if (sizeA != b.Length)
			return false;
		
		for (int i = 0; i < sizeA; ++i)
		{
			if (a[i] != b[i])
				return false;
		}
		
		return true;		
	}
	
	public void Swap<T>(ref T a, ref T b) {
		T t = a;
		a = b;
		b = t;
	}
	
	public bool IsPointInFrustum(Vector3 point, Plane[] planes) {
		
		for (int i = 0; i < 5; ++i) {
			if (planes[i].GetDistanceToPoint(point) < 0)
				return false;
		}
		return true;
	}

	public bool AreAllInFrustum (IEnumerable<GameObject> gos, Camera cam, bool completelyVisible)
	{
		// Planes order: left, right, bottom, top, near, far.
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);
		
		foreach (GameObject go in gos)
		{
			Renderer renderer = go.GetComponent<Renderer>();
			if (renderer == null)
				// Check only by position
				return IsPointInFrustum(go.transform.position, planes);
			
			if (!completelyVisible)
				// Check by bounds
				return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
			
			MeshFilter objectMeshFilter = go.GetComponent<MeshFilter>();
			if (objectMeshFilter == null)
				// Check only by position
				return IsPointInFrustum(go.transform.position, planes);
			
			Transform goTransform = go.transform;
			Vector3[] vertices = objectMeshFilter.mesh.vertices;
			foreach (Plane plane in planes)
				foreach (Vector3 vertex in vertices)
					if(plane.GetDistanceToPoint(goTransform.TransformPoint(vertex)) < 0)
						return false;
		}
		
		return true;
	}

	// If it doesn't have a renderer, it'll check for collider; if it doesn't have either, it's assumed that it isn't in frustum
    public bool IsInFrustum(GameObject go, Camera cam, bool completelyVisible, bool recursive)
	{
		if (recursive) {
			foreach (Transform childTransform in go.transform) {
				if (!IsInFrustum(childTransform.gameObject, cam, completelyVisible, true))
					return false;
			}
		}
		
		// Planes order: left, right, bottom, top, near, far.
    	Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);

		Renderer renderer = go.GetComponent<Renderer>();
		if (renderer == null)
			// Check only by position
			return IsPointInFrustum(go.transform.position, planes);
		
		if (!completelyVisible)
			// Check by bounds
			return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
		
		MeshFilter objectMeshFilter = go.GetComponent<MeshFilter>();
		if (objectMeshFilter == null)
			// Check only by position
			return IsPointInFrustum(go.transform.position, planes);
		
		Transform goTransform = go.transform;
		Vector3[] vertices = objectMeshFilter.mesh.vertices;
		foreach (Plane plane in planes)
			foreach (Vector3 vertex in vertices)
				if(plane.GetDistanceToPoint(goTransform.TransformPoint(vertex)) < 0)
					return false;

		return true;
    }

	public int IndexOf<T>(T o, T[] arr) 
	{
		int size = arr.Length;

		for (int i = 0; i < size; ++i)
		{
			if ((arr[i] == null && o == null) || (arr[i] != null && o != null && arr[i].Equals(o)))
				return i;
		}
		return -1;
	}
	
	// It only recognizes the colors you register in constructor
	public String ColorName(Color color)
	{
		if (_MapColorToColorName.ContainsKey(color))
			return _MapColorToColorName[color];
		return Strings8.UNDEFINED;
	}

	public UnityEngine.Object[] ToArrayOfObjects<T> (T[] objects) where T : UnityEngine.Object
	{
		UnityEngine.Object[] result = new UnityEngine.Object[objects.Length];

		int length = result.Length;
		for (int i = 0; i < length; ++i)
		{
			result[i] = objects[i];
		}

		return result;
	}
	// Test this
//	public double Avg(double[] values) {
//		long i = 1L;
//		double avg = 0d;
//		foreach (double x in values) {
//			avg += (x - avg) / i;
//			++i;
//		}
//		
//		return avg;
//	}
	
	public float Avg2(float[] values) {
		long i = 1L;
		float avg = 0f;
		foreach (float x in values) {
			avg += (x - avg) / i;
			++i;
		}
		
		return avg;
	}
	
	// more precise
	public long Avg2(long[] values)
	{
		int numValues = values.Length;
		long avg = 0L;

		// If their sum is < than maxLong, then the avg's accuracy will be 100%
		try
		{
			long sum = 0L;
			for (int i = 0; i < numValues; ++i)
			{
				sum += values[i];
			}
			
			long avgWithDoubleCasting = (long)(((double)sum) / numValues);
			long avgNormal = sum / numValues;

			avg = Math.Max (avgNormal, avgWithDoubleCasting);
		}
		catch (OverflowException)
		{
			// If not, a lossy algorithm will be used
			avg = 0L;
			
			if (numValues < int.MaxValue / 10000)
			{
				long maxLongValueForCastingToDouble = long.MaxValue / 10000;
				
				// Don't cast to double for large longs, since the double will lose a bit on precision then
				for (int i = 0; i < numValues; ++i)
				{
					if (values[i] > maxLongValueForCastingToDouble)
						avg += values[i] / numValues;
					else
						avg += (long)(((double)values[i]) / numValues);
				}
			}
			else
			{
				for (int i = 0; i < numValues; ++i)
					avg += (long)(((double)values[i]) / numValues);
			}
		}

		return avg;
	}

	// TODO test this
	// The most precise
	public long Avg3(long[] values)
	{
		BigInteger sum = 0;
		long numValues = values.Length;
		for (long i = 0L; i < numValues; ++i)
		{
			sum += values[i];
		}
		
		return (sum / numValues).LongValue();
	}
	
	// Precise, but the values must be actually smaller than double.MaxValue
	public double Avg3DoublePrecision(long[] values)
	{
		BigInteger sum = 0;
		long numValues = values.Length;
		for (long i = 0L; i < numValues; ++i)
		{
			sum += values[i];
		}
		
		return ((double) sum.LongValue()) / numValues;
	}
	
	public double Avg3DoublePrecision(int[] values)
	{
		BigInteger sum = 0;
		long numValues = values.Length;
		for (long i = 0L; i < numValues; ++i)
		{
			sum += values[i];
		}
		
		return ((double) sum.LongValue()) / numValues;
	}
	
	public float AvgSimple(float[] values) {
		int numValues = values.Length;
		float sum = 0f;
		for (int i = 0; i < numValues; ++i)
		{
			sum += values[i];
		}
		
		return sum / numValues;
	}

	public int AvgSimple(int[] values)
	{
		int numValues = values.Length;
		int sum = 0;
		for (int i = 0; i < numValues; ++i)
		{
			sum += values[i];
		}
		
		return sum / numValues;
	}

	public double Clamp01(double value) 
	{
		if (value < 0d)
			return 0d;

		if (value > 1d)
			return 1d;

		return value;
	}

	public void PopulateWithNewObjects<T>(T[] array) where T : new()
	{
		int length = array.Length;
		for (int i = 0; i < length; ++i)
			array[i] = new T();
	}

	/// <summary>
	/// Casts the ray from screen point.
	/// </summary>
	/// <returns>The hit GameObject, or null, if none.</returns>
	/// <param name="screenPoint">Screen point.</param>
	public GameObject CastRayFromScreenPointAndGetHitObject(Vector2 screenPoint)
	{
		return CastRayFromScreenPointAndGetHitObject(screenPoint, ~0); // all layers
	}
	
	public GameObject CastRayFromScreenPointAndGetHitObject(Vector2 screenPoint, int layerMask)
	{
		Vector3 screenPositionV3 = 
			new Vector3(screenPoint.x, screenPoint.y , 0f);
		
		// Check if there was a hit object
		RaycastHit raycastHit;
		Ray ray = Camera.main.ScreenPointToRay (screenPositionV3);
		if (Physics.Raycast (
			ray,
			out raycastHit,
			Mathf.Infinity,
			layerMask
			)
		    ) 
		{
			return raycastHit.collider.gameObject;
		}

		return null;
	}


	
	public T InstantiateGOAndGetComponent<T>(string prefabGONameFromScene, bool instantiateAsEnabled) where T : Component
	{
		return InstantiateGOAndGetComponent<T>(prefabGONameFromScene, prefabGONameFromScene + "_Instance", instantiateAsEnabled);
	}

	public T InstantiateGOAndGetComponent<T>(string prefabGONameFromScene, string newGOInstanceName, bool instantiateAsEnabled) where T : Component
	{
		GameObject canvasesPrefabsHolder = GameObject.Find("ActivityCanvasesPrefabsHolder");
		GameObject prefabFromScene = canvasesPrefabsHolder.transform.Find(prefabGONameFromScene).gameObject;
		T result = null;
		if (prefabFromScene != null && prefabFromScene.GetComponent<T>() != null)
		{
			GameObject instance = (GameObject)GameObject.Instantiate(prefabFromScene);
			if (instantiateAsEnabled)
				instance.SetActive(true);
			instance.name = newGOInstanceName;
			result = instance.GetComponent<T>();
		}

		return result;
	}

	/// <summary>
	/// Logarithmic smoothing function (deceleration constantly grows until end)
	/// </summary>
	/// <param name="x">the input; it's clamped to (0, 1]</param>
	/// <param name="decelerationGrowFactor">clamped to [1, 10]</param>
	/// <returns>a value in [0, 1] representing the evaluation of x</returns>
	public float Log01(float x, int decelerationGrowFactor)
	{
		return Mathf.Clamp01(
					Mathf.Log(
						Mathf.Clamp(x, 0.0000001f, 1f), 
			1024 * Mathf.Clamp(decelerationGrowFactor, 1, 10)
					) + 1
				); // near 0, the formula is negative, that's why we clamp it
	}
	
	// 
	/// <summary>
	/// Computes the hierarchy center position.
	/// </summary>
	/// <returns>The hierarchy center position.</returns>
	/// <param name="root">Root.</param>
	/// <param name="maxDepth"> 0 = max; 1 = direct children, 2 = grandchildren etc.</param>
	/// <remarks>The returned position is in global space</remarks>
	/// <remarks>It ignores the root's position</remarks>
	public Vector3 ComputeHierarchyCenterPosition(Transform root, int maxDepth)
	{
		List<Transform> transforms = new List<Transform>();

		// No cycle checking, since in a tree there are no cycles
		Action<Transform, int> DFCollectChildren = null;
		DFCollectChildren = delegate(Transform currentRoot, int recStepsRemained)
		{
			foreach (Transform child in currentRoot)
			{
				transforms.Add(child);
				
				int newRecStepsRemained = recStepsRemained - 1;
				if (newRecStepsRemained != 0) // if negative, it means it must go as far as deep as possible (maxDepth passed as 0)
					DFCollectChildren(child, newRecStepsRemained);
			}
		};
		DFCollectChildren(root, maxDepth);

		return ComputeGroupCenterPosition(transforms);
	}

	public Vector3 ComputeGroupCenterPosition(List<Transform> transforms)
	{
		Vector3 minBounds = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
		Vector3 maxBounds = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
		Vector3 currentNodePosition;

		foreach (Transform nodeTransform in transforms)
		{
			currentNodePosition = nodeTransform.position;
			
			if (currentNodePosition.x < minBounds.x)
				minBounds.x = currentNodePosition.x;
			if (currentNodePosition.y < minBounds.y)
				minBounds.y = currentNodePosition.y;
			if (currentNodePosition.z < minBounds.z)
				minBounds.z = currentNodePosition.z;
			
			if (currentNodePosition.x > maxBounds.x)
				maxBounds.x = currentNodePosition.x;
			if (currentNodePosition.y > maxBounds.y)
				maxBounds.y = currentNodePosition.y;
			if (currentNodePosition.z > maxBounds.z)
				maxBounds.z = currentNodePosition.z;
		}
		return (minBounds + maxBounds) / 2;
	}
	
	public void SetHierarchyMaterialRenderQueue(Transform root, int queue)
	{
		try 
		{
			root.gameObject.GetComponent<Renderer>().material.renderQueue = queue;
		}
		catch {}
		
		if (root.childCount > 0)
		{
			foreach(Transform t in root)
			{
				SetHierarchyMaterialRenderQueue(t, queue);
			}		
		}
	}

	
	
	// range [0, int.MaxValue]
	// Make sure the list haves ONLY positive values in it
	public int GetSmallestPositiveIntNotIn(List<int> listOfInts)
	{
		int count = listOfInts.Count;
		int assumedFirstGappedInt = 0;
		for (int i = 0; i < int.MaxValue; ++i)
		{
			if (i < count)
			{
				if (listOfInts[i] > assumedFirstGappedInt)
				{
					return assumedFirstGappedInt;
				}
				else
				{
					++assumedFirstGappedInt;
				}
			}
			// Exceeded list size and found no gaps => list is contiguous => next id is <count>
			else
			{
				return count;
			}
		}
		
		return -1; // no available ids; where there are too much id's for a type; very rare case
	}
	
	// Get the point lying onto an imaginary sphere of radius <x> and origin <point2> 
	// where the sphere's surface normal in that point intersects the <ray>
	// I.e. the point on the sphere's surface that's the closest to the ray
	public Vector3 GetPointOnSphereClosestToRay(Vector3 sphereOrigin, float sphereRadius, Ray ray)
	{
		Vector3 vRayOriginToSphereOrigin = sphereOrigin - ray.origin;
		float alphaAngle = Vector3.Angle(ray.direction, vRayOriginToSphereOrigin);
		// In this case, the wanted point on the sphere is the closest one from the ray's origin, 
		// since the ray is moving away from the sphere
		if (alphaAngle >= 90)
			return sphereOrigin + (-vRayOriginToSphereOrigin.normalized * sphereRadius);
		
		alphaAngle *= Mathf.Deg2Rad;
		
		float distFromRayOriginToSphereOrigin = vRayOriginToSphereOrigin.magnitude;
		float distFromRayOriginToClosestPointOnRayToSphere =
			// using formula: cos (alphaAngle) = distFromRayOriginToClosestPointOnRayToObject / distFromRayOriginToSphereOrigin
			Mathf.Cos(alphaAngle) * distFromRayOriginToSphereOrigin;
		
		Vector3 closestPointOnRayToSphere = ray.GetPoint(distFromRayOriginToClosestPointOnRayToSphere);
		Vector3 dirSphereOriginToClosestPointOnRayToSphereOrigin = (closestPointOnRayToSphere - sphereOrigin).normalized;
		
		return sphereOrigin + dirSphereOriginToClosestPointOnRayToSphereOrigin * sphereRadius;
	}
	
	// the ideal points (i.e. not deviated) contstruct two backfaced vectors of length <segmentLength> 
	// that lie on origin.forward and start from origin.position
	// The result can be easily interpreted by the bool
	public bool MostDeviatedPointIsFirst(Vector3 firstPoint, Transform origin, Vector3 secondPoint, float segmentLength)
	{
		return MostDeviatedPointIsFirst(firstPoint, origin.position, origin.forward, secondPoint, segmentLength);
	}

	/// <param name="segmentLongitudinalOrientation"> needed only to know the direction(regardless if it's pointing to + or -).</param>
	public static bool MostDeviatedPointIsFirst(Vector3 firstPoint, Vector3 segmentOrigin, Vector3 segmentLongitudinalOrientation, Vector3 secondPoint, float segmentLength)
	{
		// Angular is relative to the linker's Z axis; so when the node is in place, this angle is 0 or almost 0
		float startPointAngularDeviationFromUsual = Vector3.Angle(firstPoint - segmentOrigin, segmentLongitudinalOrientation);
		if (startPointAngularDeviationFromUsual > 90)
			startPointAngularDeviationFromUsual = 180 - startPointAngularDeviationFromUsual;
		
		float endPointAngularDeviationFromUsual = Vector3.Angle(secondPoint - segmentOrigin, segmentLongitudinalOrientation);
		if (endPointAngularDeviationFromUsual > 90)
			endPointAngularDeviationFromUsual = 180 - endPointAngularDeviationFromUsual;
		
		float startPointPositionDeviationFromUsual = 
			Mathf.Abs(segmentLength - Vector3.Distance(segmentOrigin, firstPoint));
		
		float endPointPositionDeviationFromUsual = 
			Mathf.Abs(segmentLength - Vector3.Distance(segmentOrigin, secondPoint));
		
		float startPointDeviation = startPointAngularDeviationFromUsual * startPointPositionDeviationFromUsual;
		float endPointDeviation = endPointAngularDeviationFromUsual * endPointPositionDeviationFromUsual;
		
		return startPointDeviation > endPointDeviation;
	}
	
	
	// Note it uses only defined colors
	public Color RandomColor(bool withRandomAlpha, Color[] colors)
	{
		Color result = colors[UnityEngine.Random.Range(0, colors.Length)];
		
		result.a = withRandomAlpha ? UnityEngine.Random.Range(0f, 1f) : 1f;
		
		return result;
	}

	public string GetColorHex (Color color)
	{
		return "#" 
				+ ((int)(color.r * 255)).ToString("X2")
				+ ((int)(color.g * 255)).ToString("X2") 
				+ ((int)(color.b * 255)).ToString("X2") 
				+ ((int)(color.a * 255)).ToString("X2");
	}
	
	// Can be used with Collection.RemoveAll
	public static bool IsNull(object o) {return o==null;}

	// The elements of type T on the depth2 arrays are not cloned, if T is a reference type
	public T[][][] GetArrayDeepClone<T>(T[][][] arrayToClone)
	{
		T[][][] depth0 = arrayToClone;
		T[][] depth1;
		T[] depth2;
		T[][][] result = new T[depth0.Length][][];
		
		for (int i = 0; i < depth0.Length; ++i)
		{
			depth1 = depth0[i];
			result[i] = new T[depth1.Length][];
			for (int j = 0; j < depth1.Length; ++j)
			{
				depth2 = depth1[j];
				result[i][j] = new T[depth2.Length];
				
				for (int k = 0; k < depth2.Length; ++k)
					result[i][j][k] = depth2[k];
			}
		}
		
		return result;
	}


	public float GetChanceSampleABiggerThanSampleB(float minA, float maxA, float minB, float maxB)
	{
		if (minA > maxB)
			return 1f;

		if (minB > maxA)
			return 0f;

		// At this point, they surely intersect
		float aLen = maxA - minA;
		float bLen = maxB - minB;
		if (aLen == 0f)
			return (maxA - minB) / bLen;
		if (bLen == 0f)
			return (maxA - maxB) / aLen; // maxB == minB

		float aBefore, bBefore;
		float intersectLen;
		float aAfter, bAfter;

		if (minA > minB)
		{
			aBefore = 0f;
			bBefore = minA - minB;
		}
		else
		{
			aBefore = minB - minA;
			bBefore = 0f;
		}
		if (maxA > maxB)
		{
			aAfter = maxA - maxB;
			bAfter = 0f;
		}
		else
		{
			aAfter = 0f;
			bAfter = maxB - maxA;
		}

		intersectLen = maxB - minA - bAfter - aBefore;

		return aAfter / aLen + (intersectLen / aLen) * (bBefore / bLen + (intersectLen / bLen) / 2);
	}
	
}