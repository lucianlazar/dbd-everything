﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace frame8.Logic.Misc.Reflection
{
    public class TypeInfo
    {
        public Type type;
        public List<FieldInfo> fieldsInfos;
        public List<PropertyInfo> propertiesInfos;
        public List<MemberInfo> allMembersInfos;
        public List<Type> allMembersTypes;
        public List<string> fieldsNames;
        public List<string> propertiesNames;
        public List<string> allMembersNames;
        public BindingFlags bindingFlags;


        const BindingFlags DEFAULT_BINDING_FLAGS_FOR_MEMBER_QUERY = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

        public static FieldInfo[] GetFieldInfosIncludingBaseClasses(Type type, BindingFlags bindingFlags)
        {
            FieldInfo[] fieldInfos = type.GetFields(bindingFlags);

            // If this class doesn't have a base, don't waste any time
            if (type.BaseType == typeof(object))
            {
                return fieldInfos;
            }
            else
            {   // Otherwise, collect all types up to the furthest base class
                var currentType = type;
                var fieldComparer = new MemberInfoComparer<FieldInfo>();
                var fieldInfoList = new HashSet<FieldInfo>(fieldInfos, fieldComparer);
                while (currentType != typeof(object))
                {
                    fieldInfos = currentType.GetFields(bindingFlags);
                    fieldInfoList.UnionWith(fieldInfos);
                    currentType = currentType.BaseType;
                }
                return fieldInfoList.ToArray();
            }
        }

        public static PropertyInfo[] GetPropertyInfosIncludingBaseClasses(Type type, BindingFlags bindingFlags)
        {
            PropertyInfo[] propertyInfos = type.GetProperties(bindingFlags);

            // If this class doesn't have a base, don't waste any time
            if (type.BaseType == typeof(object))
            {
                return propertyInfos;
            }
            else
            {   // Otherwise, collect all types up to the furthest base class
                var currentType = type;
                var fieldComparer = new MemberInfoComparer<PropertyInfo>();
                var fieldInfoList = new HashSet<PropertyInfo>(propertyInfos, fieldComparer);
                while (currentType != typeof(object))
                {
                    propertyInfos = currentType.GetProperties(bindingFlags);
                    fieldInfoList.UnionWith(propertyInfos);
                    currentType = currentType.BaseType;
                }
                return fieldInfoList.ToArray();
            }
        }

        public TypeInfo(Type type, bool includeInheritedMembers, bool includeProperties, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS_FOR_MEMBER_QUERY)
        {
            Initialize(type, includeInheritedMembers, includeProperties, bindingFlags);
        }

        public void Initialize(Type type, bool includeInheritedMembers, bool includeProperties, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS_FOR_MEMBER_QUERY)
        {
            this.type = type;
            if (fieldsInfos == null)
                fieldsInfos = new List<FieldInfo>();
            if (propertiesInfos == null)
                propertiesInfos = new List<PropertyInfo>();
            if (allMembersInfos == null)
                allMembersInfos = new List<MemberInfo>();
            if (allMembersTypes == null)
                allMembersTypes = new List<Type>();
            if (fieldsNames == null)
                fieldsNames = new List<string>();
            if (propertiesNames == null)
                propertiesNames = new List<string>();
            if (allMembersNames == null)
                allMembersNames = new List<string>();
            this.bindingFlags = bindingFlags;

            ReloadFields(includeInheritedMembers, includeProperties);
        }

        public FieldInfo GetFieldInfo(string name)
        {
            int index = fieldsNames.IndexOf(name);

            if (index >= 0)
                return fieldsInfos[index];

            return null;
        }

        public PropertyInfo GetPropertyInfo(string name)
        {
            int index = propertiesNames.IndexOf(name);

            if (index >= 0)
                return propertiesInfos[index];

            return null;
        }

        public MemberInfo GetMemberInfo(string name)
        {
            int index = allMembersNames.IndexOf(name);

            if (index >= 0)
                return allMembersInfos[index];

            return null;
        }

        public Type GetMemberType(string name)
        {
            int index = allMembersNames.IndexOf(name);

            if (index >= 0)
                return allMembersTypes[index];

            return null;
        }

        void ReloadFields(bool includeInheritedMembers, bool includeProperties)
        {
            fieldsInfos.Clear();
            fieldsNames.Clear();
            propertiesInfos.Clear();
            allMembersInfos.Clear();
            allMembersTypes.Clear();
            propertiesNames.Clear();
            allMembersNames.Clear();

            BindingFlags flags = bindingFlags;
            if (!includeInheritedMembers)
                flags |= BindingFlags.DeclaredOnly;

            // Store fieldInfos and their names
            // Also store them in the allMembersXXX arrays
            if (includeInheritedMembers)
                fieldsInfos.AddRange(GetFieldInfosIncludingBaseClasses(type, flags));
            else
                fieldsInfos.AddRange(type.GetFields(flags));
            foreach (var fieldInfo in fieldsInfos)
            {
                fieldsNames.Add(fieldInfo.Name);

                allMembersInfos.Add(fieldInfo);
                allMembersTypes.Add(fieldInfo.FieldType);
                allMembersNames.Add(fieldInfo.Name);
            }

            // Private fields are not returned in "GetFields".. so we guess private fields based on what properties are there (i.e. 
            // if there's a "sprite"/"Sprite" property, most probably there's a "m_Sprite" or "_Sprite" private field with the same type)
            // Also store the properties on the fly
            PropertyInfo[] collectedPropertiesInfos;
            if (includeInheritedMembers)
                collectedPropertiesInfos = GetPropertyInfosIncludingBaseClasses(type, flags);
            else
                collectedPropertiesInfos = type.GetProperties(flags);
            int len = collectedPropertiesInfos.Length;
            string propertyName, backingFieldGuessedName;
            PropertyInfo propertyInfo;
            for (int i = 0; i < len; ++i)
            {
                propertyInfo = collectedPropertiesInfos[i];

                propertyName = propertyInfo.Name;
                if (char.IsLetter(propertyName[0]))
                {
                    // Transform "sprite" into m_Sprite
                    backingFieldGuessedName = "m_" + char.ToUpper(propertyName[0]) + propertyName.Substring(1);
                    FieldInfo fieldInfo = type.GetField(backingFieldGuessedName, flags);
                    if (fieldInfo == null)
                    {
                        backingFieldGuessedName = backingFieldGuessedName.Substring(1); // remove the heading "m"; some use "_Field" instead of "m_Field"
                        fieldInfo = type.GetField(backingFieldGuessedName, flags);

                        if (fieldInfo == null)
                        {
                            // some use "_field" instead of "m_Field" or "_Field"
                            backingFieldGuessedName = '_' + propertyName;
                            fieldInfo = type.GetField(backingFieldGuessedName, flags);
                        }
                    }

                    if (fieldInfo != null)
                    {
                        // Already found by the regular GetFields() method => skip it
                        if (fieldsNames.Contains(backingFieldGuessedName))
                            continue;

                        // Only store it if it's of the same declared type
                        if (fieldInfo.FieldType == propertyInfo.PropertyType)
                        {
                            fieldsInfos.Add(fieldInfo);
                            allMembersInfos.Add(fieldInfo);
                            allMembersTypes.Add(fieldInfo.FieldType);

                            fieldsNames.Add(backingFieldGuessedName);
                            allMembersNames.Add(backingFieldGuessedName);
                        }
                    }
                }

                // Also store the propertyName
                if (includeProperties)
                {
                    propertiesInfos.Add(propertyInfo);
                    propertiesNames.Add(propertyName);
                }
            }

            // Put properties at the end (if there are any)
            foreach (var pi in propertiesInfos)
            {
                allMembersInfos.Add(pi);
                allMembersTypes.Add(pi.PropertyType);
            }
            allMembersNames.AddRange(propertiesNames);
        }
        

        class MemberInfoComparer<T> : IEqualityComparer<T> where T : MemberInfo
        {
            public bool Equals(T x, T y)
            {
                return x.DeclaringType == y.DeclaringType && x.Name == y.Name;
            }

            public int GetHashCode(T obj)
            {
                return obj.Name.GetHashCode() ^ obj.DeclaringType.GetHashCode();
            }
        }
    }
}
