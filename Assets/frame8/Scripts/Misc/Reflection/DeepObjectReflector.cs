﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Logic.Misc.Reflection
{
    public class DeepObjectReflector
    {
        string _TargetPath;

        DataNode _DataNode;

        public string TargetPath { get { return _TargetPath; }  set { _TargetPath = value; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetPath">field1.Property2.Property3.field4 ... etc</param>
        public DeepObjectReflector(string targetPath)
        {

        }


        void CollectAndCacheDataNodes(Type rootType)
        {
            string[] paths = _TargetPath.Split('.');
            int numSegments = paths.Length;

            _DataNode = new DataNode();
            _DataNode.type = rootType;

            var currentDataNode = _DataNode;
            string curSegment;
            int curSegmentIdx = 0;
            DataNode nextDataNode;
            do
            {
                curSegment = paths[curSegmentIdx];

                nextDataNode = new DataNode();
                nextDataNode.name = curSegment;
                nextDataNode.fieldInfo = currentDataNode.type.GetField(curSegment);
                if (nextDataNode.fieldInfo == null)
                {
                    nextDataNode.propertyInfo = currentDataNode.type.GetProperty(curSegment);

                    if (nextDataNode.propertyInfo == null)
                    {
                        _DataNode = null;
                        throw new UnityException("No field or property found with name " + curSegment + " at fieldPath segment " + curSegmentIdx + " in class " + currentDataNode.type.FullName);
                    }

                    nextDataNode.type = nextDataNode.propertyInfo.PropertyType;
                    nextDataNode.GetValueDelegate = nextDataNode.GetPropertyValue;
                }
                else
                {
                    nextDataNode.type = nextDataNode.fieldInfo.FieldType;
                    nextDataNode.GetValueDelegate = nextDataNode.GetFieldValue;
                }

                currentDataNode.next = nextDataNode;
                currentDataNode = nextDataNode;
                ++curSegmentIdx;
            }
            while (curSegmentIdx < numSegments);
        }

        public object GetTargetValue(object model)
        {
            // Make sure we have the root field info cached AND is of the same type as (or of a base type of) the provided model's type
            Type modelType = model.GetType();
            if (_DataNode == null || _DataNode.type.IsAssignableFrom(modelType))
                CollectAndCacheDataNodes(modelType);

            DataNode curNode = _DataNode;
            object curNodeContainerValue = model;
            object curNodeValue = null;
            while ((curNode = curNode.next) != null)
            {
                if (curNodeContainerValue == null)
                    return null;

                curNodeValue = curNode.GetValueDelegate(curNodeContainerValue);

                curNodeContainerValue = curNodeValue;
            }

            return curNodeValue;
        }


        class DataNode
        {
            public Type type;
            public string name;
            public System.Reflection.FieldInfo fieldInfo;
            public System.Reflection.PropertyInfo propertyInfo;

            public Func<object, object> GetValueDelegate;

            public object GetFieldValue(object container) { return fieldInfo.GetValue(container); }
            public object GetPropertyValue(object container) { return propertyInfo.GetValue(container, null); }

            public DataNode next;
        }
    }
}
