﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using frame8.Logic.Misc.Other.Extensions;
using UnityEngine.Events;

namespace frame8.Logic.Misc.Visual.UI.MonoBehaviours
{
    public class RadioToggleGroup : ToggleGroup
    {
		public bool autoRegisterAllHierarchyToggles;

		public UnityToggleEvent onNewSelected;
		public UnityEvent onNoneSelected;

		List<Toggle> registeredRadioToggles = new List<Toggle>();


		protected override void Awake()
		{
			base.Awake();

			if (autoRegisterAllHierarchyToggles)
				RegisterRadioToggles(transform.GetDescendants().ConvertAll(d => d.GetComponent<Toggle>()));
		}


		public void RegisterRadioToggle(Toggle toggle)
		{
			if (toggle && !registeredRadioToggles.Contains(toggle))
			{
				toggle.group = this;
				toggle.onValueChanged.AddListener((bool _) => OnToggleValueChanged(toggle));
				registeredRadioToggles.Add(toggle);
			}
		}

		public void RegisterRadioToggles(List<Toggle> toggles)
		{
			foreach (var toggle in toggles)
				RegisterRadioToggle(toggle);
		}

		public void OnToggleValueChanged(Toggle toggle)
		{
			List<Toggle> activeToggles = new List<Toggle>(ActiveToggles());

			if (toggle.isOn)
			{
				// Disable others
				foreach (var activeToggle in activeToggles)
				{
					if (activeToggle != toggle)
						activeToggle.isOn = false;
				}

				if (onNewSelected != null)
					onNewSelected.Invoke(toggle);
			}
			else
			{
				if (activeToggles.Count == 0 || activeToggles.Count == 1 && activeToggles.Contains(toggle))
					if (onNoneSelected != null)
						onNoneSelected.Invoke();
			}
		}

		[Serializable]
		public class UnityToggleEvent : UnityEvent<Toggle> {}
	}
}