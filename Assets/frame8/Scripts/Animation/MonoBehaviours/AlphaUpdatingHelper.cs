﻿using UnityEngine;
using System.Collections;

// First created to dynamically change a material's alpha through Animator
public class AlphaUpdatingHelper : MonoBehaviour
{
    public float alpha = 1f;

    Material _CurrentMaterial;
    Color color;


    void Awake()
    {
        _CurrentMaterial = GetComponent<Renderer>().material;
    }

    void Update()
    {
        color = _CurrentMaterial.color;
        color.a = alpha;
        _CurrentMaterial.color = color;
    }
}
