using UnityEngine;
using System.Collections;

public class AnimatedTiledTexture8 : MonoBehaviour
{
	public int _Columns = 2;
	public int _Rows = 2;
	public float _FramesPerSecond = 10f;
	
	//the current frame to display
	int _CurrentTileIndex;
	Renderer _Renderer;
	float _DelayBetweenFrames;
	
	void Start()
	{
		_FramesPerSecond = Mathf.Max(_FramesPerSecond, .00001f);
		_DelayBetweenFrames = 1f / _FramesPerSecond;
		_Renderer = GetComponent<Renderer>();
		
		//set the tile size of the texture (in UV units), based on the rows and columns
		Vector2 size = new Vector2(1f / _Columns, 1f / _Rows);
		_Renderer.sharedMaterial.SetTextureScale("_MainTex", size);
		
		StartCoroutine(UpdateTiling());
	}
	
	IEnumerator UpdateTiling()
	{
		while (true)
		{
			//move to the next index
			_CurrentTileIndex++;
			if (_CurrentTileIndex >= _Rows * _Columns)
				_CurrentTileIndex = 0;
			
			//split into x and y indexes
			Vector2 offset = new Vector2((float)_CurrentTileIndex / _Columns - (_CurrentTileIndex / _Columns), //x index
			                             (_CurrentTileIndex / _Columns) / (float)_Rows);          //y index
			
			_Renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
			
			yield return new WaitForSeconds(_DelayBetweenFrames);
		}
	}
}

