using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// Linear Interpolation
using System;

public class Animation8 : IDisposable
{
	public string Name {get{return _Name;} set {_Name = string.IsNullOrEmpty(value) ? "Animation8" : value;}}
	public float AnimationTime {get {return _AnimationTime;} set {_AnimationTime = value;}}
	public AnimationSmoothnessLevel8 AnimationSmoothness {get; set;}
	public float ElapsedTime {get {return _ElapsedTimeWithoutStartDelay;}}
	public bool IsPlaying {get{return _CurrentAnimationTask != null && !_CurrentAnimationTask.IsPaused && _CurrentAnimationTask.ExecutionInProgress;}}
	public bool DontDestroyOnNaturalEnd {get; set;}
	
	protected string _Name = "Animation8";
	protected float _AnimationTime;
	protected float _StartTime;
	protected float _ElapsedTimeWithoutStartDelay;
	protected float _AnimationProgress = 0f; // [0f..1f]
	protected bool _StopRequested;
	protected Utils8 _Utils;
	protected List<AnimatedPropertiesGroup8> _AnimatedPropertiesGroups = new List<AnimatedPropertiesGroup8>();
	
	Task8 _CurrentAnimationTask;
	Action _InterpolationFunction;
	InterpolationFunction8 _InterpolationFunctionEnumValue = InterpolationFunction8.LINEAR;
	
	public InterpolationFunction8 InterpolationFunction 
	{
		get{return _InterpolationFunctionEnumValue;}
		set
		{
			_InterpolationFunctionEnumValue = value;
			switch(_InterpolationFunctionEnumValue)
			{
			case InterpolationFunction8.LOG_FASTIN_SLOWOUT:
				_InterpolationFunction = LogFastInSlowOutInterp;
				break;
			case InterpolationFunction8.SIN_SLOWOUT:
				_InterpolationFunction = SinSlowOutInterpolation;
				break;
			case InterpolationFunction8.COS_SLOWIN:
				_InterpolationFunction = CosSlowInInterpolation;
				break;
			case InterpolationFunction8.EXP_SLOWIN_FASTOUT:
				_InterpolationFunction = SlowInFastOutExpInterpolation;
				break;
			case InterpolationFunction8.SMOOTH_SLOWIN_SLOWOUT:
				_InterpolationFunction = SlowInOutSmoothStepInterpolation;
				break;
			case InterpolationFunction8.SMOOTHER_SLOWIN_SLOWOUT:
				_InterpolationFunction = SlowInOutSmootherStepInterpolation;
				break;
				
			default:
				_InterpolationFunction = LinearInterpolation;
				_InterpolationFunctionEnumValue = InterpolationFunction8.LINEAR;
				break;
			}
		}
	}
	
	public Animation8() 
		:this(InterpolationFunction8.LINEAR)
	{ }
	
	public Animation8(InterpolationFunction8 interpolationFunction)
	{
		_Utils = Utils8.Instance;
		AnimationSmoothness = AnimationSmoothnessLevel8.ONE;
		InterpolationFunction = interpolationFunction;
	}
	
	public void AddAnimatedPropertiesGroup(AnimatedPropertiesGroup8 animatedPropertiesGroup)
	{
		_AnimatedPropertiesGroups.Add(animatedPropertiesGroup);
	}

    public AnimatedPropertiesGroup8 GetAnimatedPropertiesGroup(int index)
    {
        return _AnimatedPropertiesGroups[index];
    }
	
	public void ClearAnimatedPropertiesGroups()
	{
		_AnimatedPropertiesGroups.Clear();
	}

    public void Start(Action endCallback)
    {
        Start(0f, endCallback);
    }

    public void Start(float delay, Action endCallback)
	{
		_StopRequested = false;
		_StartTime = Time.time + delay;
		_ElapsedTimeWithoutStartDelay = 0f;
		int numSteps = ((int)AnimationSmoothness * 40);
		float delayBetweenExecutions = _AnimationTime / numSteps;
		if (_CurrentAnimationTask == null)
		{
			_CurrentAnimationTask = 
				Task8.Create(
					numSteps,
					delayBetweenExecutions,
					0f,
					Step,
					endCallback
				);
			//_CurrentAnimationTask.DontDestroyOnNaturalEnd = DontDestroyOnNaturalEnd;
		}
		else
		{
			_CurrentAnimationTask.Init(
				numSteps,
				delayBetweenExecutions,
				0f,
				Step,
				endCallback
			);
        }
        _CurrentAnimationTask.DontDestroyOnNaturalEnd = DontDestroyOnNaturalEnd;
        _CurrentAnimationTask.name = Name;
		_CurrentAnimationTask.StartOrResumeExecution(delay);
	}
	
	public void Stop(bool dispatchEndEvent, bool dispose)
	{
		_StopRequested = true;
		if (_CurrentAnimationTask != null)
		{
			_CurrentAnimationTask.EndExecution(dispatchEndEvent, dispose);
			
			if (dispose)
				// Animation task must be disposed here, so when calling Start, the variable will be null (condition needed to instantiate another one)
				_CurrentAnimationTask = null;
		}
	}
	
	bool Step()
	{
		if (_StopRequested)
			return false; // maybe true?

		_ElapsedTimeWithoutStartDelay = Mathf.Max(0f, Time.time - _StartTime); // in some cases where start delay is also specified when starting the animation, this can be negative, so we adjust it
		if (_ElapsedTimeWithoutStartDelay > _AnimationTime)
		{
			// Make sure the animated properties groups receive the last step event 
			// with _AnimationProgress = 1f and _ElapsedTime = totalAnimationTime, 
			// so the animation is done completely
			_ElapsedTimeWithoutStartDelay = _AnimationTime;
			_AnimationProgress = 1f;
			DispatchStepEventToAnimatedPropertiesGroups();
			
			// End the animation
			return false;
		}
			
		_InterpolationFunction();
		
		// Let the animation step decide if the animation will continue or not
		return DispatchStepEventToAnimatedPropertiesGroups();
	}
	
	void LinearInterpolation()
	{
		_AnimationProgress = _ElapsedTimeWithoutStartDelay / _AnimationTime;
	}
	
	void LogFastInSlowOutInterp()
	{
		_AnimationProgress = 
			_Utils.Log01(
				_ElapsedTimeWithoutStartDelay / _AnimationTime, // constantly decelerating, starting from 0
				4
			); 
	}
	
	void CosSlowInInterpolation()
	{
		_AnimationProgress = 1f - Mathf.Cos((_ElapsedTimeWithoutStartDelay / _AnimationTime) * Mathf.PI/2);
	}
	
	void SinSlowOutInterpolation()
	{	
		_AnimationProgress = Mathf.Sin((_ElapsedTimeWithoutStartDelay / _AnimationTime) * Mathf.PI/2);
	}
	
	void SlowInFastOutExpInterpolation()
	{
		float t = _ElapsedTimeWithoutStartDelay / _AnimationTime;
		_AnimationProgress = t*t;
	}
	
	void SlowInOutSmoothStepInterpolation()
	{
		float t = _ElapsedTimeWithoutStartDelay / _AnimationTime;
		_AnimationProgress = t*t * (3f - 2f*t);
	}
	
	void SlowInOutSmootherStepInterpolation()
	{
		float t = _ElapsedTimeWithoutStartDelay / _AnimationTime;
		_AnimationProgress = t*t*t * (t * (6f*t - 15f) + 10f);
	}
	
	protected bool DispatchStepEventToAnimatedPropertiesGroups()
	{
		foreach (AnimatedPropertiesGroup8 animatedPropertiesGroup in _AnimatedPropertiesGroups)
		{
			if (animatedPropertiesGroup != null)
			{
				if(!animatedPropertiesGroup.UpdatePropertiesAndCallOnAnimationProgresscallback(_AnimationProgress, _ElapsedTimeWithoutStartDelay))
					return false;
			}
		}

		return true;
	}

	public void Dispose ()
	{
		Stop(false, true);
	}
}

