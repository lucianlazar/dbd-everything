﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Logic.Core
{
    public class Frame8Settings : Singleton8<Frame8Settings>
    {
        const string PREFIX = "frame8_";
        const string USE_INTERRUPTION_EVENTS = PREFIX + "UseInterruptionEvents";
        const string SHOW_POST_BUILD_PLATFORM_CONFIRMATION = PREFIX + "ShowPostBuildPlatformNoticeConfirmation";


        public override void Init()
        {
            if (!PlayerPrefs.HasKey(USE_INTERRUPTION_EVENTS))
                UseInterruptionEvents = true;

            if (!PlayerPrefs.HasKey(SHOW_POST_BUILD_PLATFORM_CONFIRMATION))
                ShowPostBuildPlatformNoticeConfirmation = true;
        }


        public bool UseInterruptionEvents
        {
            get { return PlayerPrefs.GetInt(USE_INTERRUPTION_EVENTS, 0) == 1; }
            set { PlayerPrefs.SetInt(USE_INTERRUPTION_EVENTS, value ? 1 : 0); }
        }

        public bool ShowPostBuildPlatformNoticeConfirmation
        {
            get { return PlayerPrefs.GetInt(SHOW_POST_BUILD_PLATFORM_CONFIRMATION, 0) == 1; }
            set { PlayerPrefs.SetInt(SHOW_POST_BUILD_PLATFORM_CONFIRMATION, value ? 1 : 0); }
        }
    }
}
