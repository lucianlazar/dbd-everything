﻿using UnityEngine;
using UnityEditor;
using frame8.Logic.Misc.Other.Attributes;

namespace frame8.Editor.Inspector.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(ReadOnlyProperty8Attribute))]
    public class ReadOnly8PropertyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }
}
