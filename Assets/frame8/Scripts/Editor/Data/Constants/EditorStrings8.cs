﻿public class EditorStrings8
{
    public const string PREF_AUTO_LAUNCH_SCRIPT_EDITOR = "pref_frame8_auto_launch_script_editor_on_unity_start";
    public const string PREF_TMP_FILE_PATH_FOR_EDITOR_SESSION = "pref_frame8_tmp_file_for_editor_session";
    public const string PREF_FORCE_LOAD_VR_DEVICE = "pref_frame8_force_load_vr_device";
};
