﻿using UnityEngine;
using UnityEditor;
using UEditor = UnityEditor.Editor;
using UObject = UnityEngine.Object;
using System.Collections.Generic;
using System;
using frame8.CSharpAssemblyEditorUtils;

namespace frame8.Editor.Windows
{
    // The objects must either be GameObjects, either Components of the same type, either assets of the same type
    public class GameObjectsInspector8 : EditorWindow
    {
        UObject[] _Objects = new UObject[0];
        UEditor[] _Editors = new UEditor[0];
        bool[] _EditorsFoldouts;
        bool _AllObjectsHaveTheSameComponents; // applicable if _Objects has objects of type GameObject
        bool _UsingGameObjects;
        //bool _CanRepaint

        //void OnEnable()
        //{

        //}

        //void OnDisable()
        //{

        //}


        void OnInspectorUpdate()
        {
            Repaint();
        }


        // Assumes no null objects are passed
        // either only GameObjects or only Objects are allowed (no mixed types)
        public bool SetObjects<T>(T[] objects) where T : UObject
        {
            if (!AreAllObjectsFromSameType(objects))
                return false;

            var newObjects = new UObject[objects.Length];
            System.Array.Copy(objects, newObjects, objects.Length);
            // Destroy Old editors
            foreach (var ed in _Editors)
                DestroyImmediate(ed);

            _Objects = newObjects;
            _UsingGameObjects = false;
            if (_Objects.Length > 0)
            {
                _UsingGameObjects = typeof(GameObject).IsAssignableFrom(_Objects[0].GetType());
                CreateEditors();
            }

            return true;
        }

        void UpdateObjects()
        {
            RemoveNullObjects();
        }

        void RemoveNullObjects()
        {
            _Objects = System.Array.FindAll(_Objects, o => o != null);
        }

        // Assumes there is at least one UObject and there's no null ones
        bool AreAllObjectsFromSameType(UObject[] objects)
        {
            int len = objects.Length;
            if (len == 0)
                return true;

            var typeOfFirst = objects[0].GetType();
            for (int i = 1; i < len; i++)
            {
                if (objects[i].GetType() != typeOfFirst)
                    return false;
            }
            return true;
        }

        void CreateEditors()
        {
            _AllObjectsHaveTheSameComponents = true;
            if (_UsingGameObjects) // show all components, mixed
            {
                var mapComponentTypeToListOfComponents = new Dictionary<System.Type, List<Component>>();
                System.Type type;
                foreach (var go in _Objects)
                {
                    foreach (var comp in (go as GameObject).GetComponents<Component>())
                    {
                        type = comp.GetType();
                        if (!mapComponentTypeToListOfComponents.ContainsKey(type))
                            mapComponentTypeToListOfComponents[type] = new List<Component>();

                        mapComponentTypeToListOfComponents[type].Add(comp);
                    }
                }

                int numObjects = _Objects.Length;
                List<UEditor> editors = new List<UEditor>();
                System.Type transformType = typeof(Transform);
                foreach (var kv in mapComponentTypeToListOfComponents)
                {
                    var comps = kv.Value;
                    if (comps.Count == numObjects) // only create editors for components that are present on ALL objects
                    {
                        var editorInstance = UEditor.CreateEditor(comps.ToArray());
                        if (transformType.IsAssignableFrom(kv.Key))
                            editors.Insert(0, editorInstance); // make transforms first, just so it'll look more like the unity's inspector
                        else
                            editors.Add(editorInstance);
                    }
                    else
                        _AllObjectsHaveTheSameComponents = false;
                }

                editors.Insert(0, UEditor.CreateEditor(_Objects)); // for the header
                _Editors = editors.ToArray();
            }
            else // show a multi-component or multi-asset
            {
                _Editors = new UEditor[]{UEditor.CreateEditor(_Objects)};
            }

            _EditorsFoldouts = new bool[_Editors.Length];
            for (int i = 0; i < _Editors.Length; ++i)
                _EditorsFoldouts[i] = true;
        }

        void DrawEditors()
        {
            int startI = 0;
            if (_UsingGameObjects)
            {
                _Editors[0].DrawHeader();
                startI = 1;
            }
            for (int i = startI; i < _Editors.Length; ++i)
            {
                _EditorsFoldouts[i] = EditorGUILayout.InspectorTitlebar(_EditorsFoldouts[i], _Editors[i].targets);
                EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
                {
                    // Setting the label/field widths to be like in the inspector (LookLikeInspector is deprecated)
                    float prevLabelWidth = EditorGUIUtility.labelWidth;
                    float prevFieldWidth = EditorGUIUtility.fieldWidth;
                    EditorGUIUtility.labelWidth = position.width * .4f; // label width should be dependent on window's width
                    EditorGUIUtility.fieldWidth = 0f;

                    if (_EditorsFoldouts[i])
                    {
                        try { _Editors[i].OnInspectorGUI(); } catch { };

                    }

                    // Restoring prev label/field widths
                    EditorGUIUtility.labelWidth = prevLabelWidth;
                    EditorGUIUtility.fieldWidth = prevFieldWidth;
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUIUtils8.Instance.DrawGreyLineSeparator();

            if (!_AllObjectsHaveTheSameComponents)
            {
                EditorGUILayout.Separator();
                EditorGUILayout.Space();
                EditorGUILayout.HelpBox("Components present only on some of the selected GameObjects cannot be multi-edited", MessageType.Info);

                EditorGUIUtils8.Instance.DrawGreyLineSeparator();
            }

            if (_Editors.Length == 1)
            {
                if (_Editors[0].HasPreviewGUI())
                    _Editors[0].DrawPreview(EditorGUILayout.GetControlRect(false, 400, GUILayout.ExpandWidth(true)));
            }
        }

        Vector2 _ScrollPosition;
        void OnGUI()
        {
            _ScrollPosition = EditorGUILayout.BeginScrollView(_ScrollPosition);
            if (_Editors.Length > 0)
                DrawEditors();
            EditorGUILayout.EndScrollView();
        }
    }
}
