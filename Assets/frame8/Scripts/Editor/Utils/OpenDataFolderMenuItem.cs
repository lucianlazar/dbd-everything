﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils
{
	public static class OpenDataFolderMenuItem
	{

		[MenuItem("frame8/Open data folder")]
		static void OpenDataFolder()
		{
			Application.OpenURL(Application.persistentDataPath);
		}
	}
}
