using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
// TODO for "Bring rect to anchors" take into account the children (checkbox "don't affect them")
public class UIUtilsEditorWindow8 : EditorWindow
{
	int currentUpdateModulo100 = 0;
	Vector2 _ScrollViewPosition;
	float _DirectionalSizeSliderValue = 1f;
	UIUtils8 _UIUtils = UIUtils8.Instance;
//	bool _PreserveAnchorsRealSizeDeltaOnSelf = true, _PreserveAnchorsRealSizeOnChildren = true;
	
	[MenuItem ("frame8/UIUtils")]
	static void Init () {
	
		// Get existing open window or if none, make a new one:
		GetWindow<UIUtilsEditorWindow8> ();
	}
	
	void OnGUI () {
		_ScrollViewPosition = EditorGUILayout.BeginScrollView(_ScrollViewPosition);
		
		if (Selection.transforms.Length > 0)
		{
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Bring anchors to rect on:");
				if (GUILayout.Button("self"))
					_UIUtils.BringAnchorsToRectFor(Selection.transforms);
				if (GUILayout.Button("children"))
				{
					var trs = Selection.transforms;
					foreach (Transform tr in trs)
						foreach(Transform chTr in tr)
							_UIUtils.BringAnchorsToRectFor(chTr);
				}
			EditorGUILayout.EndHorizontal();
			
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Bring rect to anchors on:");
				if (GUILayout.Button("self"))
					_UIUtils.BringRectToAnchorsFor(Selection.transforms);
				if (GUILayout.Button("children"))
				{
					var trs = Selection.transforms;
					foreach (Transform tr in trs)
						foreach(Transform chTr in tr)
							_UIUtils.BringRectToAnchorsFor(chTr);
				}
			EditorGUILayout.EndHorizontal();
			
			if (Selection.transforms.Length == 1)
			{
				RectTransform rectTransform = Selection.transforms[0] as RectTransform;
				RectTransform rectTransformParent;
				if (rectTransform != null && (rectTransformParent=rectTransform.parent as RectTransform) != null)
				{
					RectTransform.Edge edgeToMove = RectTransform.Edge.Bottom;
					bool userPressedButton = false;
//					Vector2 selfSizeDelta = rectTransform.sizeDelta;
					int rtChildCount = rectTransform.childCount;
					List<RectTransform> rtChildren = new List<RectTransform>(rtChildCount);
//					List<Vector2> childrenSizeDeltas = new List<Vector2>(rtChildCount);
					RectTransform curRT;
					foreach (Transform chTr in rectTransform)
					{
						curRT = chTr as RectTransform;
						if (chTr == null)
							--rtChildCount;
						else
						{
							rtChildren.Add(curRT);
//							childrenSizeDeltas.Add(curRT.sizeDelta);
						}
					}
				
					GUILayout.Space(30f);
					GUILayout.Label("Grow size w/o affecting children's size:");
//					EditorGUILayout.BeginHorizontal();
//					GUILayout.Label("Preserve AnchorsRealSize on");
//					_PreserveAnchorsRealSizeDeltaOnSelf = GUILayout.Toggle(_PreserveAnchorsRealSizeDeltaOnSelf, "self");
//					_PreserveAnchorsRealSizeOnChildren = GUILayout.Toggle(_PreserveAnchorsRealSizeOnChildren, "children");
//					EditorGUILayout.EndHorizontal();
					GUIStyle s = new GUIStyle();
					Rect rectForButtonsArea = GUILayoutUtility.GetLastRect();
					s.fixedWidth = rectForButtonsArea.width / 2;
					
					EditorGUILayout.BeginHorizontal();
						if (GUILayout.Button("<"))
						{
							edgeToMove = RectTransform.Edge.Left;
							userPressedButton = true;
						}
						if (GUILayout.Button("/\\"))
						{
							edgeToMove = RectTransform.Edge.Top;
							userPressedButton = true;
						}
						if (GUILayout.Button(">"))
						{
							edgeToMove = RectTransform.Edge.Right;
							userPressedButton = true;
						}
						if (GUILayout.Button("\\/"))
						{
							edgeToMove = RectTransform.Edge.Bottom;
							userPressedButton = true;
						}						
						_DirectionalSizeSliderValue = EditorGUILayout.Slider(_DirectionalSizeSliderValue, -10f, 10f);
					EditorGUILayout.EndHorizontal();
					
					if (userPressedButton)
					{
						foreach (RectTransform rt in rtChildren)
							rt.SetParent(rectTransformParent, true);
							
//						Vector2 sizeBefore = rectTransform.rect.size;
						_UIUtils.GrowSizeDirectionally(rectTransform, edgeToMove, _DirectionalSizeSliderValue);
//						Vector2 sizeAfter = rectTransform.rect.size;
//						
//						Vector2 beforeToAfterRatioClampedTo01 = Vector2.zero;
//						if (sizeAfter.x > 0f)
//							beforeToAfterRatioClampedTo01.x = Mathf.Min(sizeBefore.x / sizeAfter.x, 1f);
//						if (sizeAfter.y > 0f)
//							beforeToAfterRatioClampedTo01.y = Mathf.Min(sizeBefore.y / sizeAfter.y, 1f);
//							
//						if (_PreserveAnchorsRealSizeDeltaOnSelf)
//						{
//							Vector2 anchorMin = rectTransform.anchorMin;
//							Vector2 anchorMax = rectTransform.anchorMax;
//							anchorMin.x *= beforeToAfterRatioClampedTo01.x;
//							anchorMin.y *= beforeToAfterRatioClampedTo01.y;
//							anchorMax.x *= beforeToAfterRatioClampedTo01.x;
//							anchorMax.y *= beforeToAfterRatioClampedTo01.y;
//							rectTransform.anchorMin = anchorMin;
//							rectTransform.anchorMax = anchorMax;
//						}
						
						int i = 0;
//						if (_PreserveAnchorsRealSizeOnChildren)
//						{
//							while (--i > -1)
//							{
//								curRT = rtChildren[i];
//								curRT.SetParent(rectTransform, true);
//								
//								Vector2 anchorMin = curRT.anchorMin;
//								Vector2 anchorMax = curRT.anchorMax;
//								anchorMin.x *= beforeToAfterRatioClampedTo01.x;
//								anchorMin.y *= beforeToAfterRatioClampedTo01.y;
//								anchorMax.x *= beforeToAfterRatioClampedTo01.x;
//								anchorMax.y *= beforeToAfterRatioClampedTo01.y;
//								curRT.anchorMin = anchorMin;
//								curRT.anchorMax = anchorMax;
//							}
//						}
//						else
						while (i < rtChildCount)
							rtChildren[i++].SetParent(rectTransform, true);
					}
				}
			}
		}
		EditorGUILayout.EndScrollView();
	}

	void Update()
	{
		if (currentUpdateModulo100 % 20 == 0)
			Repaint();

		currentUpdateModulo100 = (currentUpdateModulo100+1) % 100;
	}

	void OnSelectionChange()
	{
		Repaint();
	}
}

