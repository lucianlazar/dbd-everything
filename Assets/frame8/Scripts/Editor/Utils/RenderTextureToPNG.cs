//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEditor;
using UnityEngine;


public class RenderTextureToPNG : EditorWindow
{
    //Vector2 _ScrollViewPosition;
    [MenuItem ("frame8/RenderTextureToPNG")]
	static void Init () {
		GetWindow<RenderTextureToPNG>();
	}

	void OnGUI () {
		if (Selection.activeGameObject != null)
        {
            Camera selectedCamera = Selection.activeGameObject.GetComponent<Camera>();
            if (selectedCamera == null)
                GUILayout.Label("Select a camera");
            else
            {
                if (GUILayout.Button("Render camera to PNG in Assets/"))
                {
                    RenderTexture originallyActiveRenderTexture = RenderTexture.active;
                    RenderTexture tmpRenderTexture = new RenderTexture((int)selectedCamera.pixelWidth, (int)selectedCamera.pixelHeight, 24);
                    selectedCamera.targetTexture = tmpRenderTexture;
                    RenderTexture.active = tmpRenderTexture;
                    selectedCamera.Render();

                    Texture2D tex2D = new Texture2D((int)selectedCamera.pixelWidth, (int)selectedCamera.pixelHeight, TextureFormat.ARGB32, true);
                    tex2D.ReadPixels(selectedCamera.pixelRect, 0, 0);
                    tex2D.Apply();
                    RenderTexture.active = originallyActiveRenderTexture;
                    selectedCamera.targetTexture = null;

                    string filename = Application.dataPath + "/RenderTexture.png";
                    if (System.IO.File.Exists(filename))
                        Debug.Log("file " + filename + " already exists!");
                    else
                        try
                        {
                            byte[] bytes = tex2D.EncodeToPNG();
                            System.IO.File.WriteAllBytes(filename, bytes);
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        } 
                }
            }
		}
	}

	void OnSelectionChange()
	{
		Repaint();
	}
}

