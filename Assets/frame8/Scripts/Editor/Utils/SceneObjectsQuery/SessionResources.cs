﻿#pragma warning disable 0168 // The variable x is declared but never used
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery
{
    public class SessionResources : IDisposable
    {
        public static bool Initialized { get { return _Instance != null; } }
        
        public static SessionResources Instance
        {
            get
            {
                if (_Instance == null)
                {
                    if (!InitializeInstance())
                        throw new UnityException("Can't access Instance now, because it can't be initialized due to some Unity's internal stuff not being available yet");
                }

                return _Instance;
            }
        }

        public Texture2D iconTexture2D;
        public Texture2D searchIconTexture2D;
        public Texture2D conditionBackgroundTexture2D; 
        public Texture2D conditionHeadlineTexture2D;
        public Texture2D conditionHeadlineFieldsTexture2D;
        public Texture2D genericBoxTexture2D;
        public Texture2D searchAreaTexture2D;

        public Texture2D unitySelectionBlueTexture2D; // not in resources

        public GUIStyle conditionGUIStyle;
        public GUIStyle conditionFieldsGUIStyle;
        public GUIStyle headlineGUIStyle;
        public GUIStyle headlineFieldsGUIStyle;
        public GUIStyle conditionFoldOutGUIStyle;
        public GUIStyle negateButtonGUIStyle;
        public GUIStyle fieldPopupGUIStyle;
        public GUIStyle fieldConditionBetweenAndAndButtonGUIStyle;
        public GUIStyle queriedFieldAreaGUIStyle;
        public GUIStyle searchButtonGUIStyle;
        public GUIStyle searchAreaGUIStyle;
        public GUIStyle resultButtonGUIStyle;
        public GUIStyle richTextGUIStyle;
        public GUIStyle disabledTextGUIStyle;
        public GUIStyle grayTextGUIStyle;

        public GUIStyle genericBoxGUIStyle;

        static SessionResources _Instance;


        public static bool InitializeInstance()
        {
            // Accessing built-in editor styles throws a unity internal exception the first 1-2 frames after an assembly reload, so we must wait for it
            try { var v = EditorStyles.boldLabel; } 
            catch { /*Debug.Log("nope");*/ return false; }

            _Instance = new SessionResources();
            //Debug.Log("done");

            return true;
        }

        SessionResources()
        {
            LoadAll();
        }


        void LoadAll()
        {
            //string prefix = "SceneObjectsQuery/";
            string prefix = "Assets/SceneObjectsQuery/BaseAssets/";
            iconTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/icon2.png", typeof(Texture2D)) as Texture2D;
            searchIconTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/search_icon.png", typeof(Texture2D)) as Texture2D;
            
            conditionBackgroundTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/ConditionBackground.png", typeof(Texture2D)) as Texture2D;
            conditionHeadlineTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/ConditionHeadline.png", typeof(Texture2D)) as Texture2D;
            conditionHeadlineFieldsTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/ConditionHeadlineFields.png", typeof(Texture2D)) as Texture2D;
            genericBoxTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/QueriedFieldArea.png", typeof(Texture2D)) as Texture2D;
            searchAreaTexture2D = AssetDatabase.LoadAssetAtPath(prefix + "Textures/SearchArea.png", typeof(Texture2D)) as Texture2D;
            unitySelectionBlueTexture2D = new Texture2D(1, 1);
            unitySelectionBlueTexture2D.hideFlags = HideFlags.HideAndDontSave; // objects created dynamically must be set this, if won't be saved to disk (at least in our case)
            unitySelectionBlueTexture2D.SetPixel(0, 0, new Color(70f / 255, 109f / 255, 232f / 255));
            unitySelectionBlueTexture2D.Apply();

            CreateGUIStyles();
        }

        void CreateGUIStyles()
        {
            int borderSize;

            conditionGUIStyle = new GUIStyle();
            GUIStyleState conditionGUIStyleState = new GUIStyleState();
            conditionGUIStyleState.background = conditionBackgroundTexture2D;
            conditionGUIStyle.normal = conditionGUIStyleState;
            borderSize = 7;
            conditionGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);
            //conditionGUIStyle.padding = new RectOffset(5, 5, 5, 5);

            // Headline style
            headlineGUIStyle = new GUIStyle();
            GUIStyleState headlineGUIStyleState = new GUIStyleState();
            headlineGUIStyleState.background = conditionHeadlineTexture2D;
            headlineGUIStyle.normal = headlineGUIStyleState;
            headlineGUIStyle.alignment = TextAnchor.MiddleLeft;
            headlineGUIStyle.fixedHeight = 28f;
            borderSize = 14;
            headlineGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);

            // Headline fields style
            headlineFieldsGUIStyle = new GUIStyle(EditorStyles.boldLabel);
            GUIStyleState headlineFieldsGUIStyleState = new GUIStyleState();
            headlineFieldsGUIStyleState.background = conditionHeadlineFieldsTexture2D;
            borderSize = 4;
            headlineFieldsGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);
            headlineFieldsGUIStyle.normal = headlineFieldsGUIStyleState;
            //headlineFieldsGUIStyle.margin = new RectOffset(1, 0, 3, 2);
            headlineFieldsGUIStyle.margin = new RectOffset(10, 6, 6, 6);
            headlineFieldsGUIStyle.stretchWidth = false;
            headlineFieldsGUIStyle.alignment = TextAnchor.MiddleLeft;

            // Condition fields style
            conditionFieldsGUIStyle = new GUIStyle();
            conditionFieldsGUIStyle.padding = new RectOffset(0, 0, 5, 5);
            conditionFieldsGUIStyle.alignment = TextAnchor.MiddleLeft;

            // Foldout with no label style
            conditionFoldOutGUIStyle = new GUIStyle(EditorStyles.foldout);
            conditionFoldOutGUIStyle.fixedWidth = 5;
            conditionFoldOutGUIStyle.fixedHeight = headlineGUIStyle.fixedHeight;
            conditionFoldOutGUIStyle.margin.top = 6; // hotfix to center it vertically

            negateButtonGUIStyle = new GUIStyle(EditorStyles.miniButton);
            //negateButtonGUIStyle.fixedHeight = 18f;
            negateButtonGUIStyle.alignment = TextAnchor.MiddleCenter;
            negateButtonGUIStyle.padding = new RectOffset(2, 3, 2, 2);


            fieldPopupGUIStyle = new GUIStyle(EditorStyles.popup);
            //negateButtonGUIStyle.fixedHeight = 18f;
            fieldPopupGUIStyle.fixedWidth = 150f;
            //fieldPopupGUIStyle.margin = new RectOffset(2, 3, 2, 2);

            fieldConditionBetweenAndAndButtonGUIStyle = new GUIStyle(EditorStyles.miniLabel);


            // The area with the selected field dropdown and its values
            queriedFieldAreaGUIStyle = new GUIStyle();
            GUIStyleState queriedFieldAreaGUIStyleState = new GUIStyleState();
            queriedFieldAreaGUIStyleState.background = genericBoxTexture2D;
            borderSize = 4;
            queriedFieldAreaGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);
            queriedFieldAreaGUIStyle.normal = queriedFieldAreaGUIStyleState;
            //queriedFieldAreaGUIStyle.margin = new RectOffset(1, 0, 3, 2);
            queriedFieldAreaGUIStyle.margin = new RectOffset(10, 6, 6, 6);
            queriedFieldAreaGUIStyle.stretchWidth = false;
            queriedFieldAreaGUIStyle.alignment = TextAnchor.MiddleLeft;


            searchButtonGUIStyle = new GUIStyle(EditorStyles.toolbarButton);
            searchButtonGUIStyle.fixedHeight = 33f;
            searchButtonGUIStyle.fixedWidth = 90f;
            searchButtonGUIStyle.fontSize = 13;
            searchButtonGUIStyle.fontStyle = FontStyle.Bold;
            searchButtonGUIStyle.margin = new RectOffset(30, 0, 7, 0);
            borderSize = 4;
            searchButtonGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);

            searchAreaGUIStyle = new GUIStyle(EditorStyles.inspectorDefaultMargins);
            var searchAreaGUIStyleState = new GUIStyleState();
            searchAreaGUIStyleState.background = searchAreaTexture2D;
            searchAreaGUIStyle.normal = searchAreaGUIStyleState;
            borderSize = 4;
            searchAreaGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);
            //searchAreaGUIStyle.margin = new RectOffset(0, 0, 10, 10);
            searchAreaGUIStyle.padding = new RectOffset(0, 0, 10, 10);


            resultButtonGUIStyle = new GUIStyle(EditorStyles.label);
            resultButtonGUIStyle.fixedHeight = 16f;
            resultButtonGUIStyle.stretchWidth = true;
            resultButtonGUIStyle.margin = new RectOffset();
            resultButtonGUIStyle.alignment = TextAnchor.MiddleLeft;
            var normalState = new GUIStyleState();
            normalState.background = null;
            normalState.textColor = Color.black;
            resultButtonGUIStyle.normal = normalState;
            var activeState = new GUIStyleState();
            activeState.background = unitySelectionBlueTexture2D;
            activeState.textColor = Color.white;
            resultButtonGUIStyle.active = activeState;


            genericBoxGUIStyle = new GUIStyle();
            GUIStyleState genericBoxGUIStyleState = new GUIStyleState();
            genericBoxGUIStyleState.background = genericBoxTexture2D;
            borderSize = 4;
            genericBoxGUIStyle.border = new RectOffset(borderSize, borderSize, borderSize, borderSize);
            genericBoxGUIStyle.normal = genericBoxGUIStyleState;

            //richTextGUIStyle = new GUIStyle("HelpBox");
            richTextGUIStyle = new GUIStyle(EditorStyles.label);
            richTextGUIStyle.fixedHeight = 30f;
            richTextGUIStyle.margin = new RectOffset();
            richTextGUIStyle.padding = new RectOffset(10, 0, 0, 0);
            richTextGUIStyle.alignment = TextAnchor.MiddleLeft;
            richTextGUIStyle.fontSize = 9;
            richTextGUIStyle.richText = true;

            disabledTextGUIStyle = new GUIStyle(EditorStyles.label);
            disabledTextGUIStyle.normal = new GUIStyleState();
            disabledTextGUIStyle.normal.textColor = new Color(.7f, .7f, .7f);

            grayTextGUIStyle = new GUIStyle();
            GUIStyleState state = new GUIStyleState();
            state.textColor = new Color(.1f, .1f, .1f);
            grayTextGUIStyle.normal = state;
        }

        void UnloadAll()
        {
            Resources.UnloadAsset(iconTexture2D); 
            Resources.UnloadAsset(searchIconTexture2D); 
            Resources.UnloadAsset(conditionBackgroundTexture2D);
            Resources.UnloadAsset(conditionHeadlineTexture2D);
            Resources.UnloadAsset(conditionHeadlineFieldsTexture2D);
            Resources.UnloadAsset(genericBoxTexture2D);
            Resources.UnloadAsset(searchAreaTexture2D);
            UnityEngine.Object.DestroyImmediate(unitySelectionBlueTexture2D); // created at runtime; our responsibility to destroy it
            iconTexture2D = searchIconTexture2D = conditionBackgroundTexture2D = conditionHeadlineTexture2D = 
            conditionHeadlineFieldsTexture2D = genericBoxTexture2D = searchAreaTexture2D = unitySelectionBlueTexture2D = null;
            conditionGUIStyle = headlineGUIStyle = headlineFieldsGUIStyle = conditionFieldsGUIStyle =
            conditionFoldOutGUIStyle = negateButtonGUIStyle = fieldConditionBetweenAndAndButtonGUIStyle =
            searchButtonGUIStyle = searchAreaGUIStyle = resultButtonGUIStyle = genericBoxGUIStyle =
            richTextGUIStyle = grayTextGUIStyle = null;

        }

        public void Dispose()
        {
            UnloadAll();
            _Instance = null;
        }

        public class UnityComponentsInfo
        {
            public static UnityComponentsInfo Instance
            {
                get
                {
                    if (_Instance == null)
                        _Instance = new UnityComponentsInfo();

                    return _Instance;
                }
            }
            static UnityComponentsInfo _Instance;

            internal Type[] allUnityComponentTypes;
            internal int allUnityComponentTypesNumber;


            internal UnityComponentsInfo()
            {
                GetAllUnityComponentTypes();
            }

            internal Type GetComponentTypeByName(string componentTypeName)
            {
                Type type;
                for (int i = 0; i < allUnityComponentTypesNumber; ++i)
                {
                    type = allUnityComponentTypes[i];
                    if (type != null && type.Name == componentTypeName)
                        return type;
                }

                return null;
            }

            void GetAllUnityComponentTypes()
            {
                allUnityComponentTypes = new Type[0];

                Type componentType = typeof(Component);
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        if (type.IsAbstract)
                            continue;

                        if (componentType.IsAssignableFrom(type))
                            ArrayUtility.Add(ref allUnityComponentTypes, type);
                    }
                }

                // It's very convenient to also add the GameObject as a "component";
                ArrayUtility.Add(ref allUnityComponentTypes, typeof(GameObject));

                allUnityComponentTypesNumber = allUnityComponentTypes.Length;
            }
        }
    }
}
#pragma warning restore 0168 // The variable x is declared but never used
