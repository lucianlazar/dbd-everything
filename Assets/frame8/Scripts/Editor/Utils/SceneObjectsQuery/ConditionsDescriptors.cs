﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    static class ConditionsDescriptors
    {
        public static ConditionDescriptor[] Descriptors { get { return _ConditionDescriptors; } }

        //public static string[] Names
        //{
        //    get
        //    {
        //        if (_Names == null)
        //        {
        //            _Names = Array.ConvertAll(conditionDescriptors, d => d.sampleInstance.HumanReadableConditionType);
        //        }

        //        return _Names;
        //    }
        //}

        public static Type[] Types { get { return _Types; } }


        static ConditionDescriptor[] _ConditionDescriptors;
        static string[] _Names;
        static Type[] _Types;


        static ConditionsDescriptors()
        {
            List<ConditionDescriptor> conditionDescriptorList = new List<ConditionDescriptor>();
            var queryConditionType = typeof(QueryCondition);
            var conditionsChainType = typeof(ConditionsChain);
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach( var type in assembly.GetTypes())
                {
                    if (type.IsAbstract)
                        continue;

                    if (queryConditionType.IsAssignableFrom(type))
                    {
                        var descriptor = new ConditionDescriptor();
                        descriptor.isChain = conditionsChainType.IsAssignableFrom(type);
                        descriptor.type = type;
                        // descriptor.sampleInstance will be lazy-initialized (when first accessed)
                        conditionDescriptorList.Add(descriptor);
                    }
                }
            };
            _ConditionDescriptors = conditionDescriptorList.ToArray();
            _Types = Array.ConvertAll(_ConditionDescriptors, d => d.type);

            // Commented: _Names is lazy-initialized, for the same reason all descriptor's "sampleInstance" property is.
            //_Names = Array.ConvertAll(conditionDescriptors, d => d.sampleInstance.HumanReadableConditionType);
        }


        public class ConditionDescriptor : IDisposable
        {
            public bool isChain;
            public Type type;

            // Lazy-initialized, because the constructors of some conditions' need the ConditionsDescriptors.Descriptors 
            // array initialized, so we can't call them when 
            // UPDATE: more reasons to lazy-initialize: we need to always check for null, since QueryCondition now inherits from ScriptableObject
            public QueryCondition sampleInstance
            {
                get
                {
                    if (_SampleInstance == null)
                    {
                        _SampleInstance = 
                            ConditionCreator.Create(
                                type,
                                UnityEngine.HideFlags.HideAndDontSave // this will be manually destroyed using Object.DestroyImmediate
                            );
                    }

                    return _SampleInstance;
                }
            }

            QueryCondition _SampleInstance;

            public void Dispose()
            {
                if (_SampleInstance)
                    UnityEngine.Object.DestroyImmediate(_SampleInstance);
            }
        }

        /// <summary>
        /// This callse Dispose on each descriptor(initially, needed to dispose the sampleInstance field of each condition because is of type QueryCondition and it shouldn't be persisted)
        /// </summary>
        public static void DisposeResources()
        {
            if (_ConditionDescriptors != null)
                foreach (var d in _ConditionDescriptors)
                    d.Dispose();
        }
    }
}
