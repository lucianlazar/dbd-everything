using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using frame8.Editor.Utils.SceneObjectsQuery.Conditions;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers;
using System;
using frame8.Editor.Utils;

// Select the objects in whose hierarchies you want to search. Select none to search in all of the scene's root objects
// Compose your query
// Press "Find" or "Find and select"
namespace frame8.Editor.Utils.SceneObjectsQuery
{
    [System.Serializable]
    public class Query : ScriptableObject, ConditionsChain.IConvertChainListener, IScriptableObjectNode
    {
        public string ExpressionDisplayName
        {
            get
            {
                if (_RootCondition == null)
                    return "(empty query)";

                return _RootCondition.HumanReadableExpandedExpression;
            }
        }
        public int NumLeafConditions { get; private set; }
        public int NumChainConditions { get; private set; }


        [SerializeField]
        QueryCondition _RootCondition;

        [SerializeField] [HideInInspector]
        bool _Initialized;

        AddConditionDropdownDrawer _AddConditionDropdownDrawer;
        ConditionsChain _PendingChainToConvert;


        //public Query() { Debug.Log("Created Query: " + GetType().Name); }

        //public Query(QueryCondition rootCondition)
        //{
        //    _AddConditionDropdownDrawer = new AddConditionDropdownDrawer();
        //    _RootCondition = rootCondition;
        //    InspectAndUpdateConditionsTree();
        //}
        //public Query() { Debug.Log("Created Query: " + name); }

        void OnEnable()
        {
            //Debug.Log("OnEnable Query: " + name);
            // If not serialized, create a default one
            if (!_Initialized)
            {
                if (_RootCondition == null)
                    _RootCondition = ConditionCreator.Create<ANDedConditionsChain>(hideFlags);
                _Initialized = true;
            }

            _AddConditionDropdownDrawer = new AddConditionDropdownDrawer();
            InspectAndUpdateConditionsTree();
        }

        /// <summary>
        /// Query the GameObjects at the scene's root
        /// </summary>
        /// <param name="deepSearch">If true, all the existing GameObjects in the scene are queried</param>
        /// <returns>The GameObjects that meet the criteria</returns>
        public List<Transform> ExecuteOnSceneRootObjects(bool deepSearch)
        {
#if UNITY_EDITOR

            return ExecuteOn(EditorUtils8.Instance.GetSceneRootGameObjects().ConvertAll(o => o.transform), deepSearch);

#else // TODO test for the runtime if checking for hideflags is enough!

            // All the game objects have the default flags set to HideFlags.None
            Transform[] transforms = Array.FindAll(Resources.FindObjectsOfTypeAll<Transform>(), t => t.gameObject.hideFlags == HideFlags.None);
            List<Transform> results = new List<Transform>();
            Transform transform;
            int len = transforms.Length;
            for (int i = 0; i < len; ++i)
            {
                transform = transforms[i];
                if (deepSearch || transform.parent == null)
                {
                    if (_RootCondition.Evaluate(transform))
                        results.Add(transform);
                }
            }

            return results;
#endif
        }

        /// <summary>
        /// See <see cref="ExecuteOn(List{Transform}, bool)"></see>
        /// </summary>
        public List<Transform> ExecuteOn(Transform[] objects, bool deepSearch)
        {
            return ExecuteOn(new List<Transform>(objects), deepSearch);
        }

        /// <summary>
        /// Query the provided GameObjects. Make sure you don't have an object included multiple times or, worse,
        /// some objects being children of other ones in the <paramref name="objects"/> parameter
        /// </summary>
        /// <param name="deepSearch">If true, the query also searches deep into the each provided GameObject's hierarchy (i.e. children, grand-children etc.)</param>
        /// <returns>The GameObjects that meet the criteria</returns>
        public List<Transform> ExecuteOn(List<Transform> objects, bool deepSearch)
        {
            List<Transform> results = new List<Transform>();
            int len = objects.Count;

            // More optimal to split into 2 for's
            if (deepSearch)
            {
                for (int i = 0; i < len; ++i)
                    results.AddRange(RecursivelyEvaluate(objects[i]));
            }
            else
            {
                Transform tr;
                for (int i = 0; i < len; ++i)
                {
                    tr = objects[i];
                    if (_RootCondition.Evaluate(tr).GetValueOrDefault())
                        results.Add(tr);
                }
            }

            return results;
        }

        List<Transform> RecursivelyEvaluate(Transform tr)
        {
            List<Transform> transforms = new List<Transform>();

            if (_RootCondition.Evaluate(tr).GetValueOrDefault())
                transforms.Add(tr);

            foreach (Transform child in tr)
                transforms.AddRange(RecursivelyEvaluate(child));

            return transforms;
        }

        #region IScriptableObjectNode
        public virtual void SetHideFlagsInSelfAndHierarchy(HideFlags flags)
        {
            hideFlags = flags;
            if (_RootCondition)
                _RootCondition.SetHideFlagsInSelfAndHierarchy(flags);
        }

        public virtual void GetFlattenedHierarchy(List<ScriptableObject> hierarchy)
        {
            if (_RootCondition)
            {
                hierarchy.Add(_RootCondition);
                _RootCondition.GetFlattenedHierarchy(hierarchy);
            }
        }
        #endregion

        internal void Update()
        {
            if (_PendingChainToConvert)
                ConvertChain();
        }

        internal bool OnGUI()
        {
            bool changed = false;
            if (_RootCondition == null) // it's null on initialization and it's also null when it's destroyed (the remove button is pressed)
            {
                if (GUILayout.Button("Add root condition", GUILayout.Width(150), GUILayout.Height(17)))
                {
                    _AddConditionDropdownDrawer.Show(typeIndex =>
                    {
                        if (typeIndex < 0)
                            return;

                        _RootCondition = ConditionCreator.Create(typeIndex, hideFlags);
                    });
                }
            }
            else
                changed = _RootCondition.OnGUI();
            InspectAndUpdateConditionsTree();

            return changed;
        }

        void OnDestroy()
        {
            if (_RootCondition)
            {
                if (string.IsNullOrEmpty(AssetDatabase.GetAssetPath(_RootCondition)))
                    UnityEngine.Object.DestroyImmediate(_RootCondition);
                else
                    Resources.UnloadAsset(_RootCondition);
                _RootCondition = null;
            }
            if (_PendingChainToConvert)
            {
                if (string.IsNullOrEmpty(AssetDatabase.GetAssetPath(_PendingChainToConvert)))
                    UnityEngine.Object.DestroyImmediate(_PendingChainToConvert);
                else
                    Resources.UnloadAsset(_PendingChainToConvert);
                _PendingChainToConvert = null;
            }
            //Debug.Log("Destroyed Query: " + GetType().Name);
        }

        // 1. Assign numbers as conditions' ids
        // 2. Assign expressions as chains' ids
        // 3. Assign <this> as "convert chain" listener to any chain in tree
        void InspectAndUpdateConditionsTree()
        {
            if (_RootCondition == null)
                NumLeafConditions = NumChainConditions = 0;
            else if (_RootCondition is ConditionsChain)
            {
                var leafs = new List<QueryCondition>();
                var chains = new List<ConditionsChain>();
                (_RootCondition as ConditionsChain).GetGraphRecursivelyInTopDownOrder(leafs, chains);

                int len = NumLeafConditions = leafs.Count;
                for (int i = 0; i < len; ++i)
                    leafs[i].id = i + "";

                len = NumChainConditions = chains.Count;
                for (int i = 0; i < len; ++i)
                {
                    // Assign expression
                    chains[i].id = (char)('A' + i) + "";

                    // Assign conversion listener to <this>
                    chains[i].ConvertChainListener = this;
                }
            }
            else
            {
                NumLeafConditions = 1;
                NumChainConditions = 0;
            }
        }

        void ConditionsChain.IConvertChainListener.OnConvertChainRequested(ConditionsChain chainToConvert)
        {
            _PendingChainToConvert = chainToConvert;
        }

        void ConvertChain()
        {
            // Create an instance of the next chain type available in assembly
            var chainTypes = Array.ConvertAll(Array.FindAll(ConditionsDescriptors.Descriptors, d => d.isChain), d => d.type);
            int nextChainTypeIndex = (Array.IndexOf(chainTypes, _PendingChainToConvert.GetType()) + 1) % chainTypes.Length;
            ConditionsChain newChainInstance = ConditionCreator.Create(chainTypes[nextChainTypeIndex], _PendingChainToConvert.hideFlags) as ConditionsChain;

            // Move the conditions and other relevant info from <chainToConvert> into the new chain
            _PendingChainToConvert.TransferStateTo(newChainInstance);

            // The root condition will be converted
            if (_PendingChainToConvert == _RootCondition)
            {
                _RootCondition = newChainInstance;
            }
            // The chain to convert is somewhere inside the tree
            else
            {
                ConditionsChain convertedChainParent = _PendingChainToConvert.Parent;
                convertedChainParent.ReplaceCondition(_PendingChainToConvert, newChainInstance);
            }

            // Destroy the old chain object
            UnityEngine.Object.DestroyImmediate(_PendingChainToConvert, true);
        }
    }
}

