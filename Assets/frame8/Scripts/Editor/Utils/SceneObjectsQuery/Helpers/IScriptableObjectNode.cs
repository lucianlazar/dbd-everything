﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Helpers
{
    interface IScriptableObjectNode
    {
        void SetHideFlagsInSelfAndHierarchy(HideFlags flags);
        void GetFlattenedHierarchy(List<ScriptableObject> hierarchy);
    }
}
