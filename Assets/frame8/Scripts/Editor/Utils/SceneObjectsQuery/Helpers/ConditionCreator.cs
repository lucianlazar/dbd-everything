﻿using frame8.Editor.Utils.SceneObjectsQuery.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Helpers
{
    internal static class ConditionCreator
    {
        public static T Create<T>(HideFlags hideFlags) where T : QueryCondition
        {
            //return (T)Activator.CreateInstance(typeof(T));
            var res = ScriptableObject.CreateInstance<T>();
            res.SetHideFlagsInSelfAndHierarchy(hideFlags);

            return res;
        }

        public static QueryCondition Create(Type type, HideFlags hideFlags)
        {
            var res = ScriptableObject.CreateInstance(type) as QueryCondition;
            res.SetHideFlagsInSelfAndHierarchy(hideFlags);

            return res;
        }

        public static QueryCondition Create(int conditionTypeIndex, HideFlags hideFlags)
        {
            return Create(ConditionsDescriptors.Types[conditionTypeIndex], hideFlags);
        }
    }
}
