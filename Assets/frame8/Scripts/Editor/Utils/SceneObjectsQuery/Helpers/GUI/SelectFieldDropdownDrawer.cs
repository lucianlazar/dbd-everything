﻿using frame8.Editor.Utils.SceneObjectsQuery.Conditions;
using frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions;
using frame8.Logic.Misc.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI
{
    public class SelectFieldDropdownDrawer
    {
        public bool WaitingForItemSelection { get; private set; } 
        public int PreviouslySelected { get { return _PrevouslySelected; } private set { _PrevouslySelected = value; } }
        public int Selected
        {
            get
            {
                return _Selected;
            }

            set
            {
                PreviouslySelected = _Selected;
                _Selected = value;
            }
        }

        TypeInfo _ComponentInfo;
        GenericMenu _Menu;
        Action<int> _SelectionListener;
        int _Selected = -1, _PrevouslySelected = -1;


        void InitializeNewMenu()
        {
            _Menu = new GenericMenu();

            Action<int, int> addItemsAction = (start, endExclusive) =>
            {
                MemberInfo memberInfo;
                for (int i = start; i < endExclusive; ++i)
                {
                    memberInfo = _ComponentInfo.allMembersInfos[i];
                    int copyOfI = i;
                    var nameContentType = new GUIContent("(" + GetTypeFriendlyName(_ComponentInfo.allMembersTypes[i].Name) + ") " + memberInfo.Name);
                    if (FieldCondition.GetConditionClassTypeForFieldType(_ComponentInfo.allMembersTypes[i]) == null)
                        _Menu.AddDisabledItem(nameContentType);
                    else
                        _Menu.AddItem(
                            nameContentType,
                            Selected == i,
                            () => OnSelectedItem(copyOfI)
                        );
                }

            };

            int numFields = _ComponentInfo.fieldsInfos.Count;
            int numProperties = _ComponentInfo.propertiesInfos.Count;
            int numMembers = _ComponentInfo.allMembersInfos.Count;

            // Add simple conditions types
            if (numFields > 0)
            {
                _Menu.AddSeparator("");
                _Menu.AddDisabledItem(new GUIContent("[-Fields-]"));
                addItemsAction(0, numFields);
            }

            _Menu.AddSeparator("");
            if (numProperties > 0)
            {
                _Menu.AddDisabledItem(new GUIContent("[-Properties-]"));
                addItemsAction(numFields, numMembers-1);
                _Menu.AddSeparator("");
            }
        }

        string GetTypeFriendlyName(string name)
        {
            if (name == "Single")
                return "Float";
            if (name == "Int32")
                return "Integer";

            return name;
        }

        // The class is Lazy-initialized here, because in the constructor we need the "sampleInstance" property of all conditions' descriptors, 
        // which are also lazy-initialized when first accessed, thus calling their constructors, but if some condition classes need to create
        // one instance of this class, the cycle repeats and something not so funny happens..
        public void Show(TypeInfo componentInfo, Action<int> resultCallback = null)
        {
            WaitingForItemSelection = true;
            _ComponentInfo = componentInfo;
            InitializeNewMenu();
            _SelectionListener = resultCallback;
            _Menu.ShowAsContext();
            //_Menu.DropDown(EditorGUILayout.GetControlRect());
        }

        void OnSelectedItem(int index)
        {
            Selected = index;

            if (_SelectionListener == null)
                return;

            _SelectionListener(index);
            WaitingForItemSelection = false;
        }
    }
}
