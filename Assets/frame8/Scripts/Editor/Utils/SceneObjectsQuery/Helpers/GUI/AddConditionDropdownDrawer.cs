﻿using frame8.Editor.Utils.SceneObjectsQuery.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI
{
    public class AddConditionDropdownDrawer
    {
        bool _Initialized;
        GenericMenu _Menu;
        Action<int> _SelectionListener;

        void LazyInitialize()
        {
            _Menu = new GenericMenu();

            // Add simple conditions types
            var oneConditionDescriptors = Array.FindAll(ConditionsDescriptors.Descriptors, d => !d.isChain);
            int len = oneConditionDescriptors.Length;
            for (int i = 0; i < len; ++i)
            {
                int conditionTypeIndex = Array.IndexOf(ConditionsDescriptors.Descriptors, oneConditionDescriptors[i]);
                _Menu.AddItem(
                    new GUIContent(oneConditionDescriptors[i].sampleInstance.HumanReadableConditionType),
                    false,
                    () => OnSelectedConditionTypeToAdd(conditionTypeIndex)
                );
            }

            // Visually separate simple conditions and chained ones
            _Menu.AddSeparator("");

            // Add chained conditions types
            string chainPrefix = "Conditions Chain/";
            var chainConditionDescriptors = Array.FindAll(ConditionsDescriptors.Descriptors, d => d.isChain);
            len = chainConditionDescriptors.Length;
            for (int i = 0; i < len; ++i)
            {
                int conditionTypeIndex = Array.IndexOf(ConditionsDescriptors.Descriptors, chainConditionDescriptors[i]);
                _Menu.AddItem(
                    new GUIContent(chainPrefix + chainConditionDescriptors[i].sampleInstance.HumanReadableConditionType),
                    false,
                    () => OnSelectedConditionTypeToAdd(conditionTypeIndex)
                );
            }
            _Initialized = true;
        }

        // The class is Lazy-initialized here, because in the constructor we need the "sampleInstance" property of all conditions' descriptors, 
        // which are also lazy-initialized when first accessed, thus calling their constructors, but if some condition classes need to create
        // one instance of this class, the cycle repeats and something not so funny happens..
        public void Show(Action<int> resultCallback)
        {
            if (!_Initialized)
                LazyInitialize();

            _SelectionListener = resultCallback;
            _Menu.ShowAsContext();
            //_Menu.DropDown(EditorGUILayout.GetControlRect());
        }

        void OnSelectedConditionTypeToAdd(int conditionTypeIndex)
        {
            if (_SelectionListener == null)
                return;

            _SelectionListener(conditionTypeIndex);
        }
    }
}
