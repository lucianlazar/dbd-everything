﻿using frame8.CSharpAssemblyEditorUtils;
using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityGUI = UnityEngine.GUI;

namespace frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI
{
    public class ResultsListDrawer : IDisposable
    {
        public Action onSelectionChanged;
        public Action onAutoSelectOptionChanged;

        public int DrawnObjectsCount { get { return _DrawnObjects.Length; } }
        public List<Transform> CurrentlySelectedObjects { get { return _CurrentlySelected; } }
        public bool InspectIndividualObjects { get; set; }
        public bool AutoSelectInHierarchy { get; set; }

        bool _LastFrameValueOfAutoSelectInHierarchy;
        Transform[] _DrawnObjects = new Transform[0];
        DrawableResult[] _DrawnObjectsWrappers = new DrawableResult[0];
        List<Transform> _CurrentlySelected = new List<Transform>();
        GUIStyle _SelectLabelGUIStyle, _SelectButtonGUIStyle;


        public ResultsListDrawer()
        {
            _SelectLabelGUIStyle = new GUIStyle(EditorStyles.miniLabel);
            _SelectLabelGUIStyle.alignment = TextAnchor.MiddleLeft;

            _SelectButtonGUIStyle = new GUIStyle(EditorStyles.toolbarButton);
            _SelectButtonGUIStyle.fixedHeight = 14f;
            _SelectButtonGUIStyle.margin = new RectOffset(0, 0, 4, 0);
        }

        public void SetDrawnObjects(Transform[] objects)
        {
            if (objects == null)
                objects = new Transform[0];

            _DrawnObjects = objects;
            _DrawnObjectsWrappers =
                Array.ConvertAll(
                    objects,
                    o =>
                    {
                        var instance = new DrawableResult(o);
                        instance.onHoverPress = OnHoverPress;

                        return instance;
                    }
                );

            OnSelectedItemsChanged(false);
        }

        public void Update()
        {
            //if (_ObjectToPing == null)
            //    return;

            //EditorGUIUtility.PingObject(_ObjectToPing.GetInstanceID());
            //_ObjectToPing = null;
            if (_LastFrameValueOfAutoSelectInHierarchy != AutoSelectInHierarchy)
            {
                if (onAutoSelectOptionChanged != null)
                    onAutoSelectOptionChanged();

                _LastFrameValueOfAutoSelectInHierarchy = AutoSelectInHierarchy;
            }
        }

        public bool OnGUI()
        {
            if (Event.current.type == EventType.KeyDown)
            {
                if (Event.current.keyCode == KeyCode.DownArrow)
                    OnUpOrDownArrowPressed(false);
                else if (Event.current.keyCode == KeyCode.UpArrow)
                    OnUpOrDownArrowPressed(true);
                return false;
            }

            bool changed = false;
            if (RemoveDestroyed())
                changed = true;

            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal(SessionResources.Instance.genericBoxGUIStyle);
            {
                GUILayout.Space(14);
                EditorGUILayout.BeginVertical();
                {
                    EditorGUIUtils8.Instance.DrawCongestedLabel("Found " + _DrawnObjects.Length + " result" + (_DrawnObjects.Length == 1 ? "" : "s"), EditorStyles.whiteBoldLabel);
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            EditorGUILayout.LabelField("Select:", _SelectLabelGUIStyle, GUILayout.Width(40));
                            float height = _SelectButtonGUIStyle.fixedHeight;
                            string text = "all";
                            Rect rect = EditorGUILayout.GetControlRect(false, height, _SelectButtonGUIStyle, GUILayout.Width(40));

                            if (UnityGUI.Button(rect, "all", _SelectButtonGUIStyle))
                                SetAllSelected(true);

                            text = "none";
                            rect = EditorGUILayout.GetControlRect(false, height, _SelectButtonGUIStyle, GUILayout.Width(40));

                            if (UnityGUI.Button(rect, text, _SelectButtonGUIStyle))
                                SetAllSelected(false);
                        }
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
                        {
                            bool auto_WasActive = AutoSelectInHierarchy, inspect_WasActive = InspectIndividualObjects;

                            AutoSelectInHierarchy = EditorGUIUtils8.Instance.DrawCongestedToggle("Also select in hierarchy", AutoSelectInHierarchy, false);
                            GUILayout.Space(20f);
                            InspectIndividualObjects = EditorGUIUtils8.Instance.DrawCongestedToggle("Inspect selected", InspectIndividualObjects, false);

                            // Make sure they are mutually exclusive
                            if (AutoSelectInHierarchy)
                            {
                                if (!auto_WasActive)
                                {
                                    InspectIndividualObjects = false;
                                }
                            }
                            if (InspectIndividualObjects)
                            {
                                if (!inspect_WasActive)
                                {
                                    AutoSelectInHierarchy = false;
                                }
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(6);
                    EditorGUILayout.BeginVertical(SessionResources.Instance.genericBoxGUIStyle);
                        for (int i = 0; i < _DrawnObjectsWrappers.Length; ++i)
                            changed = _DrawnObjectsWrappers[i].OnGUI() || changed;
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();

            return changed;
        }

        void OnSelectedItemsChanged(bool callCallback)
        {
            _CurrentlySelected.Clear();
            foreach (var o in _DrawnObjectsWrappers)
                if (o.selected)
                    _CurrentlySelected.Add(o.transform);

            if (callCallback && onSelectionChanged != null)
                onSelectionChanged();
        }

        void SetAllSelected(bool selected)
        {
            for (int i = 0; i < _DrawnObjectsWrappers.Length; ++i)
                _DrawnObjectsWrappers[i].selected = selected;

            OnSelectedItemsChanged(true);
        }

        bool RemoveDestroyed()
        {
            var allNonNull = Array.FindAll(_DrawnObjects, o => o != null);
            if (allNonNull.Length == _DrawnObjects.Length)
                return false;

            SetDrawnObjects(allNonNull);

            return true;
        }

        // Will become selected => de-select others;
        bool OnHoverPress(DrawableResult pressedItem)
        {
            // Allow multiple selections/de-selections
            if (Event.current.control)
                pressedItem.selected = !pressedItem.selected;
            else
            {
                pressedItem.selected = true;

                // Allow range selections
                if (Event.current.shift)
                {
                    pressedItem.selected = true;
                    int len = _DrawnObjectsWrappers.Length, indexOfFirstToSelect = 0, indexOfLastToSelect;
                    DrawableResult firstSelectedObject = null, currentObject;
                    for(int i = 0; i < len; ++i)
                    {
                        currentObject = _DrawnObjectsWrappers[i];
                        if (currentObject.selected)
                        {
                            firstSelectedObject = currentObject;
                            indexOfFirstToSelect = i;
                            break;
                        }
                    }

                    if (firstSelectedObject == pressedItem)
                    {
                        indexOfLastToSelect = indexOfFirstToSelect;
                        for (int i = indexOfFirstToSelect; i < len; i++) // search from pressedItem 'till the bottom
                        {
                            currentObject = _DrawnObjectsWrappers[i];
                            if (currentObject.selected)
                                indexOfLastToSelect = i;
                        }
                    }
                    else
                    {
                        indexOfLastToSelect = Array.IndexOf(_DrawnObjectsWrappers, pressedItem); // select from first selected to the currently pressedItem
                        for (int i = indexOfLastToSelect+1; i < len; ++i) // de-select the ones after the last item, if any
                            _DrawnObjectsWrappers[i].selected = false;
                    }

                    for (int i = indexOfFirstToSelect; i <= indexOfLastToSelect; ++i)
                        _DrawnObjectsWrappers[i].selected = true;
                }
                else
                {
                    foreach (var item in _DrawnObjectsWrappers)
                        if (item != pressedItem)
                            item.selected = false;
                }
            }

            OnSelectedItemsChanged(true);

            return true;
        }


        // Add prev/next objects to selection when down/up arrows are pressed
        void OnUpOrDownArrowPressed(bool up)
        {
            // Ignore when there are no objects already selected
            if (_CurrentlySelected.Count == 0)
                return;

            int indexOfNewObjectToSelect;
            if (up)
            {
                indexOfNewObjectToSelect = Array.IndexOf(_DrawnObjects, _CurrentlySelected[0]) - 1;
                if (indexOfNewObjectToSelect == -1)
                    return;
            }
            else
            {
                indexOfNewObjectToSelect = Array.IndexOf(_DrawnObjects, CurrentlySelectedObjects[CurrentlySelectedObjects.Count-1]) + 1;
                if (indexOfNewObjectToSelect == _DrawnObjects.Length)
                    return;
            }

            // Un-select others if no ctrl/shift is pressed
            if (!(Event.current.control || Event.current.shift))
                foreach (var w in _DrawnObjectsWrappers)
                    w.selected = false;

            _DrawnObjectsWrappers[indexOfNewObjectToSelect].selected = true;

            OnSelectedItemsChanged(true);
        }

        public void Dispose()
        {
            _DrawnObjects = null;
            _DrawnObjectsWrappers = null;
            _CurrentlySelected = null;
            _SelectLabelGUIStyle = _SelectButtonGUIStyle = null;
        }

        class DrawableResult
        {
            public Transform transform;
            public bool selected;

            public Func<DrawableResult, bool> onHoverPress;

            GUIStyle _GUIStyle, _GUIStyleDisabled;
            

            public DrawableResult(Transform transform)
            {
                _GUIStyle = SessionResources.Instance.resultButtonGUIStyle;
                _GUIStyleDisabled = new GUIStyle(_GUIStyle);
                _GUIStyleDisabled.normal.textColor = new Color(.4f, .4f, .4f);
                _GUIStyleDisabled.active.textColor = _GUIStyleDisabled.normal.textColor * 2f;

                this.transform = transform;
            }

            // Focus object in scene view on double-click
            void OnDoubleClicked()
            {
                var currentlyActive = Selection.activeGameObject;
                var selectedObjects = Selection.objects;

                //Selection.objects = new UnityEngine.Object[]{ transform.gameObject };
                Selection.activeGameObject = transform.gameObject;
                (SceneView.lastActiveSceneView ?? (SceneView.currentDrawingSceneView ?? SceneView.sceneViews[0] as SceneView)).FrameSelected();

                Selection.activeGameObject = currentlyActive;
                Selection.objects = selectedObjects;


                //SceneView.lastActiveSceneView.LookAt(transform.position, SceneView.lastActiveSceneView.rotation, transform.localScale.magnitude, false, false);
            }

            public bool OnGUI()
            {
                EditorGUILayout.BeginHorizontal();
                string name = transform.name;

                int numOfParents = transform.GetNumberOfAncestors();
                for (int i = 0; i < numOfParents; ++i)
                    name = "    " + name;

                Rect rect = EditorGUILayout.GetControlRect(false, _GUIStyle.fixedHeight, _GUIStyle);

                var currentEvent = Event.current;
                //if (currentEvent.isMouse && currentEvent.type == EventType.MouseDown)
                //    Debug.Log("e.isMouse=" + currentEvent.isMouse + "; MouseDown=" + (currentEvent.type == EventType.MouseDown) + "; clicks=" + currentEvent.clickCount + "; conta=" + rect.Contains(currentEvent.mousePosition));

                bool? returnValue = null;
                if (rect.Contains(currentEvent.mousePosition))
                {
                    // Focus object in scene view on double-click
                    if (currentEvent.isMouse && currentEvent.type == EventType.MouseDown)
                    {
                        if (currentEvent.clickCount == 1)
                            EditorGUIUtility.PingObject(transform.gameObject);
                        else if (currentEvent.clickCount == 2)
                        {
                            OnDoubleClicked();
                            returnValue = false;
                        }
                    }
                }

                if (returnValue == null && UnityGUI.Button(rect, "", GUIStyle.none))
                {
                    // Dispatch "hover press" event
                    if (onHoverPress != null)
                        returnValue = onHoverPress(this);
                    else
                        returnValue = false;
                }

                if (returnValue == null && currentEvent.type == EventType.Repaint)
                {
                    GUIStyle style = transform.gameObject.activeInHierarchy ? _GUIStyle : _GUIStyleDisabled;
                    style.Draw(rect, name, selected, selected, false, false);

                    if (selected)
                    {
                        rect.xMin = rect.xMax - 200;
                        UnityGUI.Label(rect, "Double-click to focus in scene", SessionResources.Instance.grayTextGUIStyle);
                    }
                }
                EditorGUILayout.EndHorizontal();

                return returnValue ?? false;
            }
        }
    }
}
