//#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using frame8.Editor.Utils.SceneObjectsQuery.Conditions;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers;
using System;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI;
using System.Reflection;
using frame8.Editor.Windows;
using frame8.CSharpAssemblyEditorUtils;

// Select the objects in whose hierarchies you want to search. Select none to search in all of the scene's root objects
// Compose your query
// Press "Find" or "Find and select"
namespace frame8.Editor.Utils.SceneObjectsQuery
{
    //[Serializable]
    public class Finder : EditorWindow
    {
        const string WINDIOW_NAME = "SOQ";
        const long AUTOSAVE_INTERVAL_MS = 5000;
        const int SLOW_UPDATE_SKIPPED_FRAMES = 5;
        const string QUERY_ASSETS_DIRECTORY_PATH = "Assets/SceneObjectsQuery/SavedQueries";
        const string QUERY_ASSET_PATH = QUERY_ASSETS_DIRECTORY_PATH + "/last_query.asset";


        class MySaveAssetsProcessor : UnityEditor.AssetModificationProcessor
        {
            public static bool SaveRequestedManually { get; set; }
            static string[] OnWillSaveAssets(string[] paths)
            {
                if (paths != null && 
                    (paths.Length > 1 // if we're saving other assets
                    || Array.IndexOf(paths, QUERY_ASSET_PATH) == -1))
                    SaveRequestedManually = true;
                return paths;
            }
        }


        //[SerializeField]
        Query _Query;
        [SerializeField]
        bool _DeepSearch = true;
        [SerializeField]
        bool _AutoSave = true; // todo show on GUI

        bool SaveChangesPending
        {
            set
            {
                string newTitle = WINDIOW_NAME;
                if (value)
                    newTitle = "*" + newTitle;

                //Debug.Log("A="+value);
                if (_SaveChangesPending != value)
                {
                    //Debug.Log("B=" + _SaveChangesPending);
                    _SaveChangesPending = value;
                    if (_SaveChangesPending)
                        _TimeWhenFirstChangesOccured = DateTime.Now.Ticks / 10000;
                }
#if UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4_OR_NEWER
                if (this.titleContent == null || this.titleContent.text != newTitle)
                    this.titleContent = new GUIContent(newTitle);
#else
                this.title = newTitle;
#endif
            }
        }

        int _CurrentFrameInSlowUpdateCycle;
        bool _InitializationPending, _CanDoOnGUI;
        Vector2 _ScrollPosition;
        GameObjectsInspector8 _InspectorWindow;
        ResultsListDrawer _ResultsListDrawer;
        bool _SaveChangesPending;
        long _TimeWhenFirstChangesOccured;


        [MenuItem("frame8/SceneObjectsQuery %#&f")]
        static void Init()
        {
            //Debug.Log("Finder: static Init");
            // Get existing open window or if none, make a new one:
            Finder windowInstance = GetWindow<Finder>(WINDIOW_NAME);
            windowInstance.minSize = new Vector2(800f, 600f);
        }

        //public Finder() { Debug.Log("Created Finder: " + name); }

        void OnEnable()
        {
            //Debug.Log("Finder: OnEnable; SessionResources.Initialized=" + SessionResources.Initialized);
            //var v = ScriptableObject.CreateInstance<Conditions.WithPropertyAdvanced.PropertyConditions.Color_FieldCondition>();
            _InitializationPending = true;
        }

        void OnDisable()
        {
            //Debug.Log("Finder: OnDisable");
            if (_Query)
            {

                //// Override the existing one, if any
                //_Query.SetHideFlags(HideFlags.None);
                //AssetDatabase.CreateAsset(_Query, QUERY_ASSET_PATH);
                ////Debug.Log("AAA" + AssetDatabase.GetAssetPath(_Query));
                //Resources.UnloadAsset(_Query);

                //_Query.SetHideFlags(HideFlags.None);
                CommitQueryAsset();
                Resources.UnloadAsset(_Query);
                _Query = null;
            }

            DestroyInstanceData(false);
        }

        void CommitQueryAsset()
        {
            var childrenInHierarchy = new List<ScriptableObject>();
            _Query.GetFlattenedHierarchy(childrenInHierarchy);
            _Query.SetHideFlagsInSelfAndHierarchy(HideFlags.None); // make them save-able
            foreach (var ch in childrenInHierarchy)
            {
                if (string.IsNullOrEmpty(AssetDatabase.GetAssetPath(ch)))
                    AssetDatabase.AddObjectToAsset(ch, _Query);
                else
                    EditorUtility.SetDirty(ch);
            }

            _Query.name = "_Query"; // appending a symbol at the beginning, so it'll be the first asset alphabetically, and it'll become the main asset as per http://answers.unity3d.com/questions/1164341/can-a-scriptableobject-contain-a-list-of-scriptabl.html
            EditorUtility.SetDirty(_Query);
            AssetDatabase.SaveAssets();

            _Query.SetHideFlagsInSelfAndHierarchy(HideFlags.DontSave); // back to editing
        }

        void InitInstanceData()
        {
            //Debug.Log("InitInstanceData: _Query = " + (_Query == null ? "null" : "not null"));
            // If first time init, assign default values
            if (_Query == null)
            {
                //var assetQuery = AssetDatabase.LoadAssetAtPath(QUERY_ASSET_PATH, typeof(Query)) as Query;
                //if (assetQuery == null)
                //{
                //    assetQuery = ScriptableObject.CreateInstance<Query>();
                //    if (!System.IO.Directory.Exists(QUERY_ASSETS_DIRECTORY_PATH))
                //        System.IO.Directory.CreateDirectory(QUERY_ASSETS_DIRECTORY_PATH);
                //    AssetDatabase.CreateAsset(assetQuery, QUERY_ASSET_PATH);
                //}
                //_Query = ScriptableObject.Instantiate(assetQuery) as Query;
                //_Query.SetHideFlags(HideFlags.DontSave); // will be saved on window close
                //Resources.UnloadAsset(assetQuery);
                ////_DeepSearch = true;
                _Query = AssetDatabase.LoadAssetAtPath(QUERY_ASSET_PATH, typeof(Query)) as Query;
                if (_Query == null)
                {
                    _Query = ScriptableObject.CreateInstance<Query>();
                    if (!System.IO.Directory.Exists(QUERY_ASSETS_DIRECTORY_PATH))
                        System.IO.Directory.CreateDirectory(QUERY_ASSETS_DIRECTORY_PATH);
                    AssetDatabase.CreateAsset(_Query, QUERY_ASSET_PATH);
                    CommitQueryAsset();
                }
            }
            _Query.SetHideFlagsInSelfAndHierarchy(HideFlags.DontSave);

            if (_ResultsListDrawer != null)
                _ResultsListDrawer.SetDrawnObjects(null);

            _CanDoOnGUI = true;
        }

        void InitResultsListDrawer()
        {
            _ResultsListDrawer = new ResultsListDrawer();

            _ResultsListDrawer.onSelectionChanged =
                () =>
                {
                    // "Auto-selecting in hierarchy" stuff
                    if (_ResultsListDrawer.AutoSelectInHierarchy)
                    {
                        UpdateSceneSelectionFromResultsSelection();
                    }
                    // Inspecting stuff
                    else if (_ResultsListDrawer.InspectIndividualObjects)
                    {
                        if (_ResultsListDrawer.CurrentlySelectedObjects.Count == 0) // don't show an empty window
                        {
                            if (_InspectorWindow)
                                _InspectorWindow.Close();
                        }
                        else
                        {
                            CreateOrUpdateInspectorWindow();
                            _InspectorWindow.Show();
                        }
                    }
                };

            _ResultsListDrawer.onAutoSelectOptionChanged =
                () =>
                {
                    if (_ResultsListDrawer.AutoSelectInHierarchy)
                        UpdateSceneSelectionFromResultsSelection();
                };
        }

        void DestroyInstanceData(bool destroyQuery)
        {
            if (destroyQuery && _Query)
            {
                if (string.IsNullOrEmpty(AssetDatabase.GetAssetPath(_Query)))
                    DestroyImmediate(_Query);
                else
                    Resources.UnloadAsset(_Query);
                _Query = null;
            }
            SaveChangesPending = false;
            _CanDoOnGUI = false;
            if (_ResultsListDrawer != null)
            {
                _ResultsListDrawer.Dispose();
                _ResultsListDrawer = null;
            }

            if (_InspectorWindow)
            {
                _InspectorWindow.Close();
                _InspectorWindow = null;
            }
        }

        void Update()
        {
            if (_InitializationPending)
            {
                if (SessionResources.Initialized || SessionResources.InitializeInstance())
                {
                    InitInstanceData();
                    _InitializationPending = false;
                }

                return;
            }

            if (_CanDoOnGUI)
            {
                // SlowUpdate calling
                if (_CurrentFrameInSlowUpdateCycle % SLOW_UPDATE_SKIPPED_FRAMES == 0)
                {
                    _CurrentFrameInSlowUpdateCycle = 0;
                    SlowUpdate();
                }
                else
                    ++_CurrentFrameInSlowUpdateCycle;
            }

            if (_InspectorWindow)
            {
                float horSpace = 5f, verticalBugDiffererence = 3f;

                // Solving a resizing bug
                //if (this.position.xMax > Screen.currentResolution.width - horSpace * 3)
                //    return;

                var inspectorRect = _InspectorWindow.position;
                //if (Vector2.Angle(thisLastRect.position, this.position.position) > .1f) // this window was moved
                //{
                    inspectorRect.x = this.position.xMax + horSpace;
                    inspectorRect.y = this.position.yMin + this.position.height / 2 - inspectorRect.height / 2 - verticalBugDiffererence;

                if (inspectorRect.x < Screen.currentResolution.width)
                    _InspectorWindow.position = inspectorRect;
                //}
                //else if (Vector2.Angle(inspectorLastRect.position, inspectorRect.position) > .1f) // inspector window was moved
                //{
                //    Rect thisNewPos = this.position;
                //    thisNewPos.x = inspectorRect.xMin - thisNewPos.width - horSpace;
                //    thisNewPos.y = inspectorRect.yMin - thisNewPos.height / 2 + inspectorRect.height / 2 + verticalBugDiffererence;
                //    this.position = thisNewPos;
                //}
                //thisLastRect = this.position;
                //inspectorLastRect = inspectorRect;
            }
        }

        void SlowUpdate()
        {
            Repaint();

            if (_ResultsListDrawer != null)
                _ResultsListDrawer.Update();

            _Query.Update();

            if (_SaveChangesPending)
            {
                if (_AutoSave && System.DateTime.Now.Ticks / 10000 - _TimeWhenFirstChangesOccured > AUTOSAVE_INTERVAL_MS
                    || MySaveAssetsProcessor.SaveRequestedManually)
                {
                    SaveChangesPending = false;
                    MySaveAssetsProcessor.SaveRequestedManually = false;
                    CommitQueryAsset();
                }
            }
        }

        void OnGUI()
        {
            if (_CanDoOnGUI)
            {
                try { OnGUIImpl(); }
                catch (Exception e) { Debug.LogException(e); DestroyImmediate(this); }
            }
        }

        void OnGUIImpl()
        {
            if (_ResultsListDrawer == null)
                InitResultsListDrawer();


            //float windowHeight = this.position.height;
            //float scrollViewHeight = windowHeight * .7f;
            //float bottomScrollViewMinHeight = windowHeight - scrollViewHeight;
            bool leafConditionsArePresent = _Query.NumLeafConditions != 0;
            string deepSearchText = "Deep search";
            bool noObjectsSelected = true;

            EditorGUILayout.BeginHorizontal(SessionResources.Instance.searchAreaGUIStyle);
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUI.BeginDisabledGroup(!leafConditionsArePresent);
                    bool searchPressed = EditorGUIUtils8.Instance.DrawCongestedButton(new GUIContent("Search", SessionResources.Instance.searchIconTexture2D), SessionResources.Instance.searchButtonGUIStyle);
                    EditorGUI.EndDisabledGroup();
                    var selectedTransforms = GetSelectedTransformsTopLevel();
                    noObjectsSelected = selectedTransforms.Length == 0;
                    if (searchPressed)
                    {
                        List<Transform> results;
                        if (noObjectsSelected) // search through the scene
                            results = _Query.ExecuteOnSceneRootObjects(_DeepSearch);
                        else
                            results = _Query.ExecuteOn(selectedTransforms, _DeepSearch);

                        _ResultsListDrawer.SetDrawnObjects(results.ToArray());
                    }

                    // Commented this: because the condition "leafConditionsArePresent" can change from true to false 
                    // and vice-versa AFTER this control is already drawn, unity complains, 
                    // as it needs things to remain constant in one pass of *multiple OnGUI calls*.
                    // In this case it's like this: Consider a *pass* has 2 OnGUI calls. "EditorGUILayout.HelpBox" is not being called in call 0, 
                    // but in call 1 <leafConditionsArePresent> becomes true, thus the "EditorGUILayout.HelpBox" IS being called, 
                    // but Unity didn't expected a new control to appear, so it complains
                    //if (leafConditionsArePresent)
                    //{

                    if (noObjectsSelected)
                        EditorGUILayout.HelpBox(
                            "No objects selected for search. All the scene's root objects will be considered" + 
                            (_DeepSearch ? "" : ". To include all the objects in the scene, check \"" + 
                            deepSearchText + "\""), MessageType.Info
                        );
                    else
                        EditorGUILayout.HelpBox(selectedTransforms.Length + 
                            " objects selected for search. If you want to search the whole scene instead, please make sure no objects are selected" + 
                            (_DeepSearch ? "" : " and \"" + deepSearchText + "\" is checked"), MessageType.Info
                        );
                    //}
                }
                EditorGUILayout.EndHorizontal();

                EditorGUI.BeginDisabledGroup(!leafConditionsArePresent);
                {
                        EditorGUI.indentLevel += 2;
                    {
                        GUIContent content = 
                            new GUIContent(
                                deepSearchText, 
                                "If on, the search will be also done on children, grand-children etc." + 
                                    (noObjectsSelected ? " of the root objects (basically, all the objects in the scene)" : " of the selected objects")
                            );
                        _DeepSearch = EditorGUILayout.ToggleLeft(content, _DeepSearch);

                        content =
                            new GUIContent(
                                "Auto-Save Query",
                                "If on, the query will be auto-saved every "+(AUTOSAVE_INTERVAL_MS/1000)+ " seconds"
                            );
                        _AutoSave = EditorGUILayout.ToggleLeft(content, _AutoSave);
                    }
                    EditorGUI.indentLevel -= 2;
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndVertical();
            GUILayout.Label(SessionResources.Instance.iconTexture2D);
            EditorGUILayout.EndHorizontal();


            // Query GUI
            _ScrollPosition = EditorGUILayout.BeginScrollView(_ScrollPosition);
            if (_Query.OnGUI())
                SaveChangesPending = true;
            leafConditionsArePresent = _Query.NumLeafConditions != 0; // update this, as the query may have changed


            // This warning must be here, because <leafConditionsArePresent> may change between consecutive OnGUI calls
            if (!leafConditionsArePresent)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(-140f);
                EditorGUILayout.HelpBox("", MessageType.Warning, false);
                var rectForLabel = GUILayoutUtility.GetLastRect();
                rectForLabel.x += 175;
                rectForLabel.y += 3;
                //rectForLabel.x += 50;
                //rectForLabel.width = 200;
                GUI.Label(rectForLabel, "Add at least one <b>executable</b> condition", SessionResources.Instance.richTextGUIStyle);
                EditorGUILayout.EndHorizontal();
            }


            // Results GUI
            if (_ResultsListDrawer.DrawnObjectsCount == 0)
            {
                //EditorGUILayout.HelpBox("No results" + (_DeepSearch ? "" : ". Check \"" + deepSearchText + "\" if you want to search the whole hierarchy"), MessageType.Info);
            }
            else
            {
                if (leafConditionsArePresent)
                {
                    bool inspectOnSelection_WasTrue = _ResultsListDrawer.InspectIndividualObjects;
                    _ResultsListDrawer.OnGUI();
                    if (inspectOnSelection_WasTrue != _ResultsListDrawer.InspectIndividualObjects)
                    {
                        if (_ResultsListDrawer.InspectIndividualObjects)
                        {
                            if (_ResultsListDrawer.CurrentlySelectedObjects.Count > 0)
                            {
                                CreateOrUpdateInspectorWindow();
                                _InspectorWindow.Show();
                                //if (Array.IndexOf(FindObjectsOfType<EditorWindow>(), _InspectorWindow) == -1) // Show Only if not shown
                                //_InspectorWindow.Show();
                            }
                        }
                        else
                        {
                            if (_InspectorWindow)
                            {
                                //if (Array.IndexOf(FindObjectsOfType<EditorWindow>(), _InspectorWindow) == -1) // Close Only if shown
                                    _InspectorWindow.Close();
                            }
                        }
                    }
                }
                else
                    _ResultsListDrawer.SetDrawnObjects(null);
            }

            EditorGUILayout.EndScrollView();
        }

        void CreateOrUpdateInspectorWindow()
        {
            if (_InspectorWindow == null)
                _InspectorWindow = GetWindow<GameObjectsInspector8>("Inspector8");

            _InspectorWindow.SetObjects(_ResultsListDrawer.CurrentlySelectedObjects.ConvertAll(tr => tr.gameObject).ToArray());
        }

        void UpdateSceneSelectionFromResultsSelection()
        {
            Selection.objects = _ResultsListDrawer.CurrentlySelectedObjects.ConvertAll(o => o.gameObject as UnityEngine.Object).ToArray();
        }


        /// <summary>
        /// If there are selected objects. It does not return children of selected objects
        /// </summary>
        /// <returns></returns>
        Transform[] GetSelectedTransformsTopLevel()
        {
            return Selection.GetTransforms(SelectionMode.TopLevel);
        }

        void OnDestroy()
        {
            //if (_Query)
            //{
            //    EditorUtility.SetDirty(_Query);
            //    AssetDatabase.SaveAssets();
            //}

            //Debug.Log("Finder: OnDestroy");
            if (SessionResources.Initialized)
                SessionResources.Instance.Dispose();
            ConditionsDescriptors.DisposeResources();

            DestroyInstanceData(true);
        }
    }
}
//#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body

