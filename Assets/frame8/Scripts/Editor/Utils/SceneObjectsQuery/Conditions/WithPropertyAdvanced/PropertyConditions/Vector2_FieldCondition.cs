﻿using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class Vector2_FieldCondition : GenericTwoFieldCondition<Vector2>
    {
        protected override Vector2 DefaultMinValue
        {
            get
            {
                return new Vector2().SetToNegativeInfinity();
            }
        }

        protected override Vector2 DefaultMaxValue
        {
            get
            {
                return new Vector2().SetToInfinity();
            }
        }


        protected override bool EvaluateValueInternal(Vector2 value)
        {
            return value.GetValues().IsBetween(_FieldValue.GetValues(), _FieldValue2.GetValues());
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(_FieldValue.GetValues().Concat(_FieldValue2.GetValues()));
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<float[]>();
            if (vals.Length != 4)
                return;

            _FieldValue = new Vector2(vals[0], vals[1]);
            _FieldValue2 = new Vector2(vals[2], vals[3]);
        }
    }

}
