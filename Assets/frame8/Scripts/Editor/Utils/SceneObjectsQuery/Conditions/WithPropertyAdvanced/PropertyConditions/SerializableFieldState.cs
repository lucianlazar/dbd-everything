﻿using frame8.Logic.IO.Serialization.UnitySerializationFriendlyObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    public class SerializableFieldState
    {
        internal bool Initialized
        {
            get
            {
                return _Initialized;
            }
        }

        internal string FieldName
        {
            get
            {
                return _FieldName;
            }
        }

        internal Type FieldType
        {
            get
            {
                return _FieldSerializableType.SystemType;
            }
        }


        // Field name and type
        [SerializeField] [HideInInspector]
        protected bool _Initialized;
        [SerializeField]
        protected string _FieldName;
        [SerializeField]
        protected SerializableSystemType _FieldSerializableType;


        // Field value
        [SerializeField]
        protected int[] intArray; // used for bool too
        [SerializeField]
        protected float[] floatArray;
        [SerializeField]
        protected string stringValue;
        [SerializeField]
        protected UnityEngine.Object unityObjectReferenceValue;



        // Public empty constructor, for serialization
        public SerializableFieldState() { }

        //public SerializableFieldState(string name, Type type)
        //{
        //    Initialize(name, type);
        //}


        public void Initialize(string name, Type type)
        {
            _FieldName = name;
            _FieldSerializableType = new SerializableSystemType(type);
            _Initialized = true;
            intArray = null;
            floatArray = null;
            stringValue = null;
            unityObjectReferenceValue = null;
        }

        // returns if the value was set correctly
        public bool SetValue(object value)
        {
            if (value is bool)
                SetValue((bool)value);
            else if (value is int[])
                SetValue((int[])value);
            else if (value is float[])
                SetValue((float[])value);
            else if (value is string)
                SetValue((string)value);
            else if (value is UnityEngine.Object)
                SetValue((UnityEngine.Object)value);
            else
            {
                return false;
            }

            return true;
        }

        public void SetValue(bool value)
        {
            if (intArray == null || intArray.Length != 1)
                intArray = new int[1];

            intArray[0] = value ? 1 : 0;
        }

        public void SetValue(int[] value)
        {
            intArray = value;
        }

        public void SetValue(float[] value)
        {
            floatArray = value;
        }

        public void SetValue(string value)
        {
            stringValue = value;
        }

        public void SetValue(UnityEngine.Object value)
        {
            unityObjectReferenceValue = value;
        }

        public T GetValue<T>()
        {
            Type type = typeof(T);

            object val = null;
            if (type == typeof(bool))
                val = GetBoolValue();
            else if (type == typeof(int))
                val = GetIntArrayValue()[0];
            else if (type == typeof(float))
                val = GetFloatArrayValue()[0];
            else if (type == typeof(int[]))
                val = GetIntArrayValue();
            else if (type == typeof(float[]))
                val = GetFloatArrayValue();
            else if (type == typeof(string))
                val = GetStringValue();
            else if (typeof(UnityEngine.Object).IsAssignableFrom(type))
                val = GetUnityEngineObjectValue();

            return (T)val;
        }

        // Bool
        bool GetBoolValue()
        {
            //if (intArray == null)
            //    return null;

            //if (intArray.Length != 1)
            //    return null;

            return intArray[0] == 1;
        }

        // Int/IntArray
        int[] GetIntArrayValue()
        {
            return intArray;
        }

        // Float/FloatArray
        float[] GetFloatArrayValue()
        {
            return floatArray;
        }

        // String
        string GetStringValue()
        {
            return stringValue;
        }

        // UnityEngine.Object reference
        UnityEngine.Object GetUnityEngineObjectValue()
        {
            return unityObjectReferenceValue;
        }
    }
}
