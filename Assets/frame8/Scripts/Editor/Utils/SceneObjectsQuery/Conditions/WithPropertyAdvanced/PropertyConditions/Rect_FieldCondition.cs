﻿using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class Rect_FieldCondition : GenericTwoFieldCondition<Rect>
    {
        protected override Rect DefaultMinValue
        {
            get
            {
                return new Rect().SetToNegativeInfinity();
            }
        }

        protected override Rect DefaultMaxValue
        {
            get
            {
                return new Rect().SetToInfinity();
            }
        }

        protected override bool EvaluateValueInternal(Rect value)
        {
            return value.GetValues().IsBetween(_FieldValue.GetValues(), _FieldValue2.GetValues());
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(_FieldValue.GetValues().Concat(_FieldValue2.GetValues()));
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<float[]>();
            if (vals.Length != 8)
                return;

            _FieldValue = new Rect(vals[0], vals[1], vals[2], vals[3]);
            _FieldValue2 = new Rect(vals[4], vals[5], vals[6], vals[7]);
        }
    }
}
