﻿using frame8.CSharpAssemblyEditorUtils;
using frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI;
using frame8.Logic.Misc.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced
{
    [Serializable]
    public class WithPropertyAdvanced : QueryCondition
    {
        const string PROPERTIES_CHECKED_WARNING = "Please be VERY careful when using condition type \"WithPropertyAdvanced" +
                                    "\" with " + PROPERTIES_CHECKED_WARNING_SHORT;

        const string PROPERTIES_CHECKED_WARNING_SHORT = "\"+Properties\" checked - checking for properties whose implementation " +
                                    "is unknown is very risky and may introduce undetectable bugs and resource leaks! Make sure you know what you're doing";

        const string DIDNT_FOUND_COMPONENT = "No component with that name exists";


        [SerializeField]
        string _WhatToQuery = ""; // the queried component's name OR can also have the value "GameObject"
        [SerializeField]
        bool _IncludeInheritedMembers;
        [SerializeField]
        bool _IncludeProperties;
        [SerializeField]
        bool _AutoLoadMembers = true;

        [SerializeField]
        FieldCondition _FieldCondition;

        TypeInfo _CachedInfoForQueriedType;
        SelectFieldDropdownDrawer _SelectFieldDropdownDrawer;

        public override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();
        }

        public override void OnAfterDeserialize()
        {
            base.OnAfterDeserialize();

            _SelectFieldDropdownDrawer = new SelectFieldDropdownDrawer();
            UpdateCachedInfoForQueriedType();
            if (_CachedInfoForQueriedType == null)
            {
                if (_FieldCondition)
                {
                    DestroyImmediate(_FieldCondition, true);
                    _FieldCondition = null;
                }
            }
            else
            {
                if (_FieldCondition && _FieldCondition.State != null && _FieldCondition.State.Initialized)
                {
                    string fieldName = _FieldCondition.State.FieldName;
                    Type fieldType;
                    int index = -1;
                    if (!string.IsNullOrEmpty(fieldName) && (fieldType=_FieldCondition.State.FieldType) != null)
                    {
                        for (int i = 0; i < _CachedInfoForQueriedType.allMembersInfos.Count; ++i)
                        {
                            var info = _CachedInfoForQueriedType.allMembersInfos[i];
                            if (info.Name == _FieldCondition.State.FieldName)
                            {
                                if (info is PropertyInfo)
                                {
                                    if ((info as PropertyInfo).PropertyType.IsAssignableFrom(fieldType))
                                        index = i;
                                }
                                else
                                {
                                    if ((info as FieldInfo).FieldType.IsAssignableFrom(fieldType))
                                        index = i;
                                }
                                break;
                            }
                        }
                    }

                    _SelectFieldDropdownDrawer.Selected = index;
                }
            }
        }

        #region IScriptableObjectNode
        public override void SetHideFlagsInSelfAndHierarchy(HideFlags flags)
        {
            base.SetHideFlagsInSelfAndHierarchy(flags);

            if (_FieldCondition)
                _FieldCondition.SetHideFlagsInSelfAndHierarchy(flags);
        }

        public override void GetFlattenedHierarchy(List<ScriptableObject> hierarchy)
        {
            base.GetFlattenedHierarchy(hierarchy);

            if (_FieldCondition)
            {
                hierarchy.Add(_FieldCondition);
                _FieldCondition.GetFlattenedHierarchy(hierarchy);
            }
        }
        #endregion


        protected override void OnEnable2()
        {
            base.OnEnable2();

            // This happens first time this object is created
            if (_SelectFieldDropdownDrawer == null)
                _SelectFieldDropdownDrawer = new SelectFieldDropdownDrawer();
        }

        protected override bool? EvaluateIgnoringNegationModifier(Transform transform)
        {
            // Can't evaluate
            if (_FieldCondition == null)
                return null;

            UnityEngine.Object queriedGameObjectOrComponent; // bugfix: using UnityEngine.Object, because the comparison with null doesn't work as expected with System.object
            if (_WhatToQuery == "GameObject")
                queriedGameObjectOrComponent = transform.gameObject;
            else
            {
                queriedGameObjectOrComponent = transform.GetComponent(_WhatToQuery);
                if (queriedGameObjectOrComponent == null)
                    return false;
            }
            
            // First, search for a field that meets the condition
            FieldInfo queriedFieldInfo = _CachedInfoForQueriedType.GetFieldInfo(_FieldCondition.State.FieldName);
            if (queriedFieldInfo != null)
            {
                if (queriedFieldInfo.FieldType.IsAssignableFrom(_FieldCondition.State.FieldType))
                    return _FieldCondition.EvaluateValue(queriedFieldInfo.GetValue(queriedGameObjectOrComponent));
            }

            if (_IncludeProperties)
            {
                // If there's no field and if the caller wants to include properties, search for a property
                PropertyInfo queriedPropertyInfo = _CachedInfoForQueriedType.GetPropertyInfo(_FieldCondition.State.FieldName);
                if (queriedPropertyInfo != null)
                {
                    // Doesn't have a get method
                    if (queriedPropertyInfo.GetGetMethod(true) == null)
                        return false;

                    if (queriedPropertyInfo.PropertyType.IsAssignableFrom(_FieldCondition.State.FieldType))
                    {
                        bool result = false;
                        try
                        {
                            result = _FieldCondition.EvaluateValue(queriedPropertyInfo.GetValue(queriedGameObjectOrComponent, null));
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);

                            Debug.LogError("[frame8..SceneObjectsQuery]: " + _FieldCondition + PROPERTIES_CHECKED_WARNING);
                        }

                        return result;
                    }
                }
            }

            return false;
        }

        protected override bool OnGUIInternal()
        {
            bool changed = false;
            string previousComponentName = _WhatToQuery;
            bool includeInheritedMembers_WasTrue = _IncludeInheritedMembers;
            bool includeProperties_WasTrue = _IncludeProperties;
            bool autoLoadComponentInfo_WasTrue = _AutoLoadMembers;
            bool componentType_WasGameObject = _WhatToQuery == "GameObject";
            bool componentType_IsGameObject = componentType_WasGameObject;
            EditorGUILayout.BeginHorizontal();
            {
                //string previousComponentName = _ComponentName ?? "";
                EditorGUIUtils8.Instance.DrawCongestedLabel("What to query?:");
                string buttonName = componentType_WasGameObject ? "GameObject" : "GameObject's component";

                GUILayout.Space(8f);
                if (EditorGUIUtils8.Instance.DrawCongestedButton(buttonName))
                {
                    changed = true;
                    componentType_IsGameObject = !componentType_IsGameObject;

                    if (componentType_IsGameObject)
                    {
                        _IncludeInheritedMembers = _IncludeProperties = _AutoLoadMembers = true;
                    }
                    else
                    {
                        _IncludeInheritedMembers = _IncludeProperties = false; // Auto-load must not be changed in this case
                    }

                    OnIsGameObjectChanged(componentType_IsGameObject);
                }

                //// Make sure it's alpha-numeric and if there are underscores, they're not the first char
                //for (int i = 0; i < _ComponentName.Length;)
                //{
                //    char c = _ComponentName[i];
                //    if (c == '_' || char.IsLetter(c) || char.IsDigit(c))
                //        ++i;
                //    else
                //        _ComponentName.Remove(i, 1);
                //}

                EditorGUI.BeginDisabledGroup(componentType_IsGameObject);
                {
                    EditorGUI.BeginChangeCheck();
                    _IncludeInheritedMembers = EditorGUIUtils8.Instance.DrawCongestedToggle("+Inherited members", _IncludeInheritedMembers, false);
                    GUILayout.Space(20f);

                    _IncludeProperties = EditorGUIUtils8.Instance.DrawCongestedToggle("+Properties", _IncludeProperties, false);
                    GUILayout.Space(20f);

                    _AutoLoadMembers = EditorGUIUtils8.Instance.DrawCongestedToggle("Auto-load class", _AutoLoadMembers, false);
                    changed = EditorGUI.EndChangeCheck() || changed;
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5f);


            bool loadPressedOrAutoLoad = _AutoLoadMembers;
            EditorGUILayout.BeginHorizontal();
            {
                if (componentType_IsGameObject)
                {

                }
                else
                {
                    _WhatToQuery = EditorGUIUtils8.Instance.DrawCongestedTextField("Component Name", _WhatToQuery);
                    if (_WhatToQuery == "GameObject")
                    {
                        changed = true;
                        componentType_IsGameObject = true;
                        OnIsGameObjectChanged(true);
                    }
                    //if (_ComponentName == null)
                    //    _ComponentName = "";
                }

                if (!_AutoLoadMembers)
                {
                    string loadButtonText = "Load class info";
                    Rect loadButtonRect = EditorGUILayout.GetControlRect();
                    //loadButtonRect.xMin += loadButtonRect.width; // offset to the right, as the button will be to the right of the last element
                    loadButtonRect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(loadButtonText, false) + 10;
                    loadPressedOrAutoLoad = GUI.Button(loadButtonRect, loadButtonText);
                    changed = loadPressedOrAutoLoad || changed;
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            if (loadPressedOrAutoLoad)
            {
                var cachedInfoForQueriedTypeBefore = _CachedInfoForQueriedType;
                if (_CachedInfoForQueriedType == null 
                    || _WhatToQuery != previousComponentName 
                    || !autoLoadComponentInfo_WasTrue
                    || includeInheritedMembers_WasTrue != _IncludeInheritedMembers
                    || includeProperties_WasTrue != _IncludeProperties)
                {
                    UpdateCachedInfoForQueriedType();
                    // Commented: the "current selected member" field in the fields drawer must be reset, 
                    // because the members array will change its size and the fields and properties will 
                    // probably change their positions too, so it's a lazy but safe measure to keep everything consistent
                    //if (_CachedComponentInfo == null
                    //    || includeInheritedMembers_WasTrue && !_IncludeInheritedMembers
                    //    || includeProperties_WasTrue && !_IncludeProperties)
                    //{
                        InvalidateSelectedFieldCondition();
                    //}
                    if (cachedInfoForQueriedTypeBefore != _CachedInfoForQueriedType)
                        changed = true;
                }
            }

            EditorGUILayout.BeginVertical();
            {
                if (_CachedInfoForQueriedType == null || _CachedInfoForQueriedType.type == null || _CachedInfoForQueriedType.type.Name != _WhatToQuery)
                {
                    if (_AutoLoadMembers)
                    {
                        if (_WhatToQuery.Length > 0)
                            EditorGUILayout.HelpBox("Class not found", MessageType.Info);
                    }
                    else
                        EditorGUILayout.HelpBox("Not loaded", MessageType.Info);
                }
                else
                {
                    if (_CachedInfoForQueriedType.allMembersNames.Count > 0)
                    {
                        changed = DrawSelectedField() || changed;
                    }
                    else
                        EditorGUILayout.HelpBox("No fields found in this class" + (_IncludeProperties ? "" : ". Try including the properties too"), MessageType.Info);
                }
            }
            EditorGUILayout.EndVertical();

            if (_IncludeProperties)
            {
                //if (!includePropertiesWasTrue)
                //    Debug.LogWarning(PROPERTIES_CHECKED_WARNING);
                if (!componentType_IsGameObject) // in the GameObject's case, we know it's safe to also include properties
                    EditorGUILayout.HelpBox(PROPERTIES_CHECKED_WARNING_SHORT, MessageType.Warning, true);
            }

            return changed;
        }

        protected override void OnDestroy()
        {
            //if ((hideFlags & HideFlags.DontSave) != 0)
                if (_FieldCondition)
                {
                    DestroyImmediate(_FieldCondition, true);
                    _FieldCondition = null;
                }

            base.OnDestroy();
        }

        bool DrawSelectedField()
        {
            bool changed = false;
            EditorGUILayout.BeginVertical(SessionResources.Instance.queriedFieldAreaGUIStyle);
            {
                EditorGUIUtils8.Instance.DrawCongestedLabel("Field: ");

                //if (_SelectFieldDropdownDrawer.Selected >= _CachedComponentInfo.allMembersNames.Count)
                //    _SelectFieldDropdownDrawer.Selected = -1;

                string selectedFieldName;
                if (_SelectFieldDropdownDrawer.Selected < 0)
                    selectedFieldName = "(not selected)";
                else
                    selectedFieldName = _CachedInfoForQueriedType.allMembersNames[_SelectFieldDropdownDrawer.Selected];

                Rect rect = GUILayoutUtility.GetLastRect();
                rect.xMin += rect.width; // offset to the right, as the button will be to the right of the last element
                rect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(selectedFieldName, false) + 10; // set the proper width for the size of id
                if (GUI.Button(rect, selectedFieldName))
                {
                    _SelectFieldDropdownDrawer.Show(_CachedInfoForQueriedType, OnSelectedField);
                }

                EditorGUILayout.Space();

                //if (!_SelectFieldDropdownDrawer.WaitingForItemSelection && _SelectFieldDropdownDrawer.Selected >= 0 && )
                if (_FieldCondition != null)
                    changed = _FieldCondition.OnGUI();
            }
            EditorGUILayout.EndVertical();
            return changed;
        }

        void OnIsGameObjectChanged(bool isGameObject)
        {
            _IncludeInheritedMembers = _IncludeProperties = isGameObject;
            if (isGameObject)
            {
                _WhatToQuery = "GameObject";
                _AutoLoadMembers = true;
            }
            else
                _WhatToQuery = "";
        }

        void OnSelectedField(int index)
        {
            if (_SelectFieldDropdownDrawer.PreviouslySelected == index)
                return;

            if (_FieldCondition)
                DestroyImmediate(_FieldCondition, true);

            _FieldCondition = 
                FieldCondition.Create(
                    _CachedInfoForQueriedType.allMembersNames[index],
                    _CachedInfoForQueriedType.allMembersTypes[index],
                    hideFlags // using <this> object's hideFlags, since it's the parent
                );
        }
        
        void UpdateCachedInfoForQueriedType()
        {
            Type componentType = null;
            if (_WhatToQuery.Length > 0)
                componentType = SessionResources.UnityComponentsInfo.Instance.GetComponentTypeByName(_WhatToQuery);

            if (componentType == null)
                _CachedInfoForQueriedType = null;
            else
                _CachedInfoForQueriedType = new TypeInfo(componentType, _IncludeInheritedMembers, _IncludeProperties);
        }

        void InvalidateSelectedFieldCondition()
        {
            _SelectFieldDropdownDrawer.Selected = -1;

            if (_FieldCondition)
            {
                DestroyImmediate(_FieldCondition, true);
                _FieldCondition = null;
            }
        }
    }
}
