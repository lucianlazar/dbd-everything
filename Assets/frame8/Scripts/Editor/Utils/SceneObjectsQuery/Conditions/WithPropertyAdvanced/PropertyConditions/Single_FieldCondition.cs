﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    // Float
    [Serializable]
    internal class Single_FieldCondition : GenericTwoNumberFieldCondition<float>
    {
        protected override float DefaultMinValue
        {
            get
            {
                return Mathf.NegativeInfinity;
            }
        }

        protected override float DefaultMaxValue
        {
            get
            {
                return Mathf.Infinity;
            }
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<float[]>();
            if (vals.Length != 2)
                return;

            _FieldValue = vals[0];
            _FieldValue2 = vals[1];
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(new float[] { _FieldValue, _FieldValue2 });
        }
    }
}
