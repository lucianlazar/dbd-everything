﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    // UnityEngine.Object
    [Serializable]
    internal class Object_FieldCondition : GenericFieldCondition<UnityEngine.Object>
    {
        protected override bool EvaluateValueInternal(UnityEngine.Object value)
        {
            return _FieldValue == value;
        }
    }
}
