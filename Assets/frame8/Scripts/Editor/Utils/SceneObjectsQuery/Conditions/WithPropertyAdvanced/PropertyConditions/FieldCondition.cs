﻿using frame8.CSharpAssemblyEditorUtils;
using frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal abstract class FieldCondition : ScriptableObject, ISerializationCallbackReceiver, IScriptableObjectNode
    {
        internal SerializableFieldState State { get { return _SerializableFieldState; } }


        [SerializeField]
        protected SerializableFieldState _SerializableFieldState;


        // Used for drawing the field
        protected UnityEditor.SerializedObject _SerializedObject;
        protected UnityEditor.SerializedProperty _SerializedProperty;

        public static Type GetConditionClassTypeForFieldType(Type fieldType)
        {
            string fieldConditionClassPrefix = fieldType.FullName;

            if (typeof(UnityEngine.Object).IsAssignableFrom(fieldType))
                fieldConditionClassPrefix = "Object";
            else if (fieldConditionClassPrefix.Contains("."))
                fieldConditionClassPrefix = fieldConditionClassPrefix.Substring(fieldConditionClassPrefix.LastIndexOf('.') + 1);// a.b.Class => Class

            return Type.GetType("frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions." + fieldConditionClassPrefix + "_FieldCondition"); // Single => "Single_FieldCondition"; Vector3 => "Vector3_FieldCondition"
        }

        public static FieldCondition Create(string fieldName, Type fieldType, HideFlags flags)
        {
            var type = GetConditionClassTypeForFieldType(fieldType);
            if (type == null)
                return null;

            FieldCondition result = ScriptableObject.CreateInstance(type) as FieldCondition;
            result.SetHideFlagsInSelfAndHierarchy(flags);
            result._SerializableFieldState.Initialize(fieldName, fieldType);

            return result;
        }


        //public FieldCondition() { Debug.Log("Created FieldCondition: " + GetType().Name); }


        public virtual void OnAfterDeserialize()
        {
        }

        public virtual void OnBeforeSerialize()
        {
        }

        #region IScriptableObjectNode
        public virtual void SetHideFlagsInSelfAndHierarchy(HideFlags flags)
        {
            hideFlags = flags;
        }

        public virtual void GetFlattenedHierarchy(List<ScriptableObject> hierarchy)
        {
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns>true if a value was changed</returns>
        public bool OnGUI()
        {
            UnityEditor.EditorGUI.BeginChangeCheck();
            if (_SerializedProperty == null)
                EditorGUIUtils8.Instance.DrawCongestedLabel("No valid property selected...");
            else
                OnGUIInternal();

            return UnityEditor.EditorGUI.EndChangeCheck();
        }

        public override string ToString()
        {
            if (_SerializableFieldState == null)
                return "_SerializableFieldState is (null)";

            return "fieldName=" + _SerializableFieldState.FieldName ?? "(null)" +
                   "\nfieldType=" + _SerializableFieldState.FieldType ?? "(null)";
        }

        public abstract bool EvaluateValue(object value);


        internal virtual void OnEnable()
        {
            name = GetType().Name;
        }

        internal virtual void OnGUIInternal()
        {
        }

        internal virtual void OnDestroy()
        {
            if (_SerializedObject != null)
                try
                {
                    _SerializedObject.Dispose();
                    _SerializedObject = null;
                } catch { }

            if (_SerializedProperty != null)
                try
                {
                    _SerializedProperty.Dispose();
                    _SerializedProperty = null;
                } catch { }
        }

    }
}
