﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal abstract class GenericTwoNumberFieldCondition<T> : GenericTwoFieldCondition<T>
        where T :
            struct,
            IComparable,
            IComparable<T>,
            IConvertible,
            IEquatable<T>,
            IFormattable
    {
        // Returns if the value is in the interval [_FieldValue, _FieldValue2]
        protected override bool EvaluateValueInternal(T value)
        {
            return value.CompareTo(_FieldValue) >= 0
                    && _FieldValue2.CompareTo(value) >= 0;
        }
    }
}
