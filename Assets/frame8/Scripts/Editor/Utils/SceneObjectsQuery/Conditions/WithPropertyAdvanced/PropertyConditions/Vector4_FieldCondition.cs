﻿using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class Vector4_FieldCondition : GenericTwoFieldCondition<Vector4>
    {
        protected override Vector4 DefaultMinValue
        {
            get
            {
                return new Vector4().SetToNegativeInfinity();
            }
        }

        protected override Vector4 DefaultMaxValue
        {
            get
            {
                return new Vector4().SetToInfinity();
            }
        }


        protected override bool EvaluateValueInternal(Vector4 value)
        {
            return value.GetValues().IsBetween(_FieldValue.GetValues(), _FieldValue2.GetValues());
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(_FieldValue.GetValues().Concat(_FieldValue2.GetValues()));
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<float[]>();
            if (vals.Length != 8)
                return;

            _FieldValue = new Vector4(vals[0], vals[1], vals[2], vals[3]);
            _FieldValue2 = new Vector4(vals[4], vals[5], vals[6], vals[7]);
        }
    }
}
