﻿using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class Bounds_FieldCondition : GenericTwoFieldCondition<Bounds>
    {
        protected override Bounds DefaultMinValue
        {
            get
            {
                return new Bounds().SetToNegativeInfinity();
            }
        }

        protected override Bounds DefaultMaxValue
        {
            get
            {
                return new Bounds().SetToInfinity();
            }
        }

        protected override bool EvaluateValueInternal(Bounds value)
        {
            return value.GetMinMaxValues().IsBetween(_FieldValue.GetMinMaxValues(), _FieldValue2.GetMinMaxValues());
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(_FieldValue.GetCenterAndSizeValues().Concat(_FieldValue2.GetCenterAndSizeValues()));
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<float[]>();
            if (vals.Length != 12)
                return;

            _FieldValue = new Bounds(new Vector3(vals[0], vals[1], vals[2]), new Vector3(vals[3], vals[4], vals[5]));
            _FieldValue2 = new Bounds(new Vector3(vals[6], vals[7], vals[8]), new Vector3(vals[9], vals[10], vals[11]));
        }
    }
}
