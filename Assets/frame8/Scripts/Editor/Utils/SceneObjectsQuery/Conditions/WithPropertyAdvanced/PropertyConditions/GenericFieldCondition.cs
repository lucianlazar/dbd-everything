﻿using frame8.CSharpAssemblyEditorUtils;
using frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions;
using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal abstract class GenericFieldCondition<T> : FieldCondition, ISerializationCallbackReceiver
    {
        // The true, complete serialized state lies in base._SerializableFieldState
        // This is marked as serializable only so it can be found by SerializedObject.FindProperty(string)
        // Used for drawing only; we don't care about the state
        [SerializeField]
        protected T _FieldValue;


        public override bool EvaluateValue(object value)
        {
            return EvaluateValueInternal((T)value);
        }

        public override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();

            if (_SerializableFieldState.Initialized)
                SerializeStateInternal();
        }

        public override void OnAfterDeserialize()
        {
            base.OnAfterDeserialize();

            if (_SerializableFieldState.Initialized)
            {
                DeserializeStateInternal();
            }
            else
            {
                //_SerializableFieldState.Initialize("FieldNameExample", typeof(T)); // just a default state
                //_SerializableFieldState.Initialize("FieldNameExample", HintObjectType); // just a default state
            }

        }

        public override string ToString()
        {
            return base.ToString() +
                   "\nfieldValue=" + _FieldValue ?? "(null)";
        }

        internal override void OnEnable()
        {
            base.OnEnable();

            _SerializedObject = new UnityEditor.SerializedObject(this);
            _SerializedProperty = _SerializedObject.FindProperty("_FieldValue"); // this is awesome... _FieldVale is of generic type T, but at runtime, we can create a SerializedProperty based on it elegantly, since T will be a type
        }

        internal override void OnGUIInternal()
        {
            base.OnGUIInternal();

            TryDrawField(ref _FieldValue, _SerializedProperty);
        }

        protected abstract bool EvaluateValueInternal(T value);

        protected virtual void DeserializeStateInternal()
        {
            _FieldValue = _SerializableFieldState.GetValue<T>();
        }

        protected virtual void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(_FieldValue);
        }


        /// <summary>
        /// Returns false if an exception was thrown
        /// </summary>
        /// <returns></returns>
        protected bool TryDrawField(ref T field, UnityEditor.SerializedProperty serializedProperty, string label = null)
        {
            // Solving Unity's ExitGUIException: http://answers.unity3d.com/questions/385235/editorguilayoutcolorfield-inside-guilayoutwindow-c.html
            // Initially, thrown on Color/Object fields
            try
            {
                field = EditorGUIUtils8.Instance.DrawSerializedPropertyImproved<T>(serializedProperty, _SerializableFieldState.FieldType, label);
                return true;
            }
            catch { }// (Exception e) { if (!(e is UnityException)) Debug.LogException(e); }

            return false;
        }

        internal override void OnDestroy()
        {
            //bool shouldSave = (hideFlags & HideFlags.DontSave) == 0;
            //Debug.Log("OnDestroy: " + GetType().Name + ": " + name + "; shouldSave=" + shouldSave);
            // TODO see if needed
            //if (!shouldSave)
                //_FieldValue = default(T);

            base.OnDestroy();
        }
    }
}
