﻿using frame8.CSharpAssemblyEditorUtils.Extensions;
using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    // Inheritors MUST implement DeserializeStateInternal and SerializeStateInternal
    [Serializable]
    internal abstract class GenericTwoFieldCondition<T> : GenericFieldCondition<T>
    {
        protected abstract T DefaultMinValue { get; }
        protected abstract T DefaultMaxValue { get; }

        // Used for drawing the second field
        [SerializeField]
        protected T _FieldValue2;


        // Non-serialized fields
        // Used for drawing the second field
        protected UnityEditor.SerializedProperty _SerializedProperty2;

        // Commented: no need; the base class does all it's necessary
        //public override void OnAfterDeserialize()
        //{
        //    base.OnAfterDeserialize();

        //    if (_SerializableFieldState.Initialized)
        //    {
        //    }
        //    else
        //    {
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //        //todo find what to do here
        //    }
        //}

        internal override void OnEnable()
        {
            base.OnEnable();
            _SerializedProperty2 = _SerializedObject.FindProperty("_FieldValue2");

            if (_FieldValue.Equals(default(T)))
            {
                _FieldValue = DefaultMinValue;
                _SerializedProperty.SetValue(_FieldValue);
            }

            if (_FieldValue2.Equals(default(T)))
            {
                _FieldValue2 = DefaultMaxValue;
                _SerializedProperty2.SetValue(_FieldValue2);
            }
        }

        internal override void OnGUIInternal()
        {
            //EditorUtils8.Instance.DrawCongestedButton("Between");
            //EditorUtils8.Instance.DrawCongestedLabel("Between..", SessionResources.Instance.fieldConditionBetweenAndAndButtonGUIStyle);

            // Commented: we must draw them with different labels
            //base.OnGUIInternal();

            //if (_SerializedProperty == null)
            //    return;

            //if (_SerializedProperty2 == null)
            //    return;

            if (!TryDrawField(ref _FieldValue, _SerializedProperty, "From:"))
                return;

            OnGUIInternalBetweenTheTwoFields();

            TryDrawField(ref _FieldValue2, _SerializedProperty2, "To:");
        }

        internal virtual void OnGUIInternalBetweenTheTwoFields()
        {
            //EditorUtils8.Instance.DrawCongestedLabel("..And..", SessionResources.Instance.fieldConditionBetweenAndAndButtonGUIStyle);
        }

        public override string ToString()
        {
            return base.ToString() +
                   "\nfieldSecondValue=" + _FieldValue2 ?? "(null)";
        }

        internal override void OnDestroy()
        {
            if (_SerializedProperty2 != null)
                try
                {
                    _SerializedProperty2.Dispose();
                    _SerializedProperty2 = null;
                }
                catch { }

            //bool shouldSave = (hideFlags & HideFlags.DontSave) == 0;
            //Debug.Log("OnDestroy: " +GetType().Name + ": " + name + "; shouldSave=" + shouldSave);
            // TODO see if needed
            //if (!shouldSave)
                //_FieldValue2 = default(T);

            base.OnDestroy();

        }
    }
}
