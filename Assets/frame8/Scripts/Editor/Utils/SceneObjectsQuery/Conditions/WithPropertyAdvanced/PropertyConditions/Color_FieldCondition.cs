﻿using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class Color_FieldCondition : GenericTwoFieldCondition<Color>
    {
        protected override Color DefaultMinValue
        {
            get
            {
                return Color.clear;
            }
        }

        protected override Color DefaultMaxValue
        {
            get
            {
                return Color.white;
            }
        }

        protected override bool EvaluateValueInternal(Color value)
        {
            return value.GetValues().IsBetween(_FieldValue.GetValues(), _FieldValue2.GetValues());
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(_FieldValue.GetValues().Concat(_FieldValue2.GetValues()));
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<float[]>();
            if (vals.Length != 8)
                return;

            _FieldValue = new Color(vals[0], vals[1], vals[2], vals[3]);
            _FieldValue2 = new Color(vals[4], vals[5], vals[6], vals[7]);
        }
    }
}
