﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class Int32_FieldCondition : GenericTwoNumberFieldCondition<int>
    {
        protected override int DefaultMinValue
        {
            get
            {
                return Int32.MinValue;
            }
        }

        protected override int DefaultMaxValue
        {
            get
            {
                return Int32.MaxValue;
            }
        }

        protected override void DeserializeStateInternal()
        {
            var vals = _SerializableFieldState.GetValue<int[]>();
            if (vals.Length != 2)
                return;

            _FieldValue = vals[0];
            _FieldValue2 = vals[1];
        }

        protected override void SerializeStateInternal()
        {
            _SerializableFieldState.SetValue(new int[] { _FieldValue, _FieldValue2 });
        }
    }
}
