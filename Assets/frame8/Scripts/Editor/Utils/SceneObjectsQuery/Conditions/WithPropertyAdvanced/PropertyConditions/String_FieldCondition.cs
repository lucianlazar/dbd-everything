﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

// FieldConditionCreator uses "frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions" as the namespace,
// so make sure to also modify there if this namespace changes
namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal class String_FieldCondition : GenericEquatableFieldCondition<string>
    {}
}
