﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions.WithPropertyAdvanced.PropertyConditions
{
    [Serializable]
    internal abstract class GenericEquatableFieldCondition<T> : GenericFieldCondition<T>, ISerializationCallbackReceiver
        where T :
            IEquatable<T>
    {
        protected override bool EvaluateValueInternal(T value)
        {
            // No worries about the value type comparison to null case. it'll be false in that case
            if (value == null)
                return _FieldValue == null;

            return _FieldValue != null && value.Equals(_FieldValue);
        }
    }
}
