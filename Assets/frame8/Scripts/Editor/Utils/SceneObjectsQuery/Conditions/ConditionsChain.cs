﻿using frame8.CSharpAssemblyEditorUtils;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    public abstract class ConditionsChain : QueryCondition
    {
        [SerializeField]
        protected QueryCondition[] _CompositeConditions;
        AddConditionDropdownDrawer _AddConditionDropdownDrawer;
        //bool _ConversionButtonPressed;

        public override string HumanReadableConditionType { get { return GetCompositionOperatorAsWord() + "ed conditions chain"; } }
        public override string HumanReadableExpandedExpression
        {
            get
            {
                string str = (negate ? "!" : "");
                str += "(";
                int len = _CompositeConditions.Length;
                if (len > 0)
                {
                    string compositionOperator = GetCompositionOperatorAsSymbol();
                    string appendix = " " + compositionOperator + " ";
                    for (int i = 0; i < len; ++i)
                    {
                        str += _CompositeConditions[i].HumanReadableExpandedExpression;
                        str += appendix;
                    }
                    str = str.Substring(0, str.Length - appendix.Length); // removing the last appendix
                }
                str += ")";

                return str;
            }
        }
        public IConvertChainListener ConvertChainListener { get; set; }

        //protected override float ContentIndentLevel
        //{
        //    get
        //    {
        //        return 30f;
        //    }
        //}



        protected override void OnEnable2()
        {
            base.OnEnable2();

            // Create empty array if first time initialization
            if (_CompositeConditions == null)
                _CompositeConditions = new QueryCondition[0];

            DisplayExpressionInHeadline = true;
            _AddConditionDropdownDrawer = new AddConditionDropdownDrawer();
        }

        //public override void OnAfterDeserialize()
        //{
        //    InitializeNonSerializedData();
        //}

        #region IScriptableObjectNode
        public override void SetHideFlagsInSelfAndHierarchy(HideFlags flags)
        {
            base.SetHideFlagsInSelfAndHierarchy(flags);

            if (_CompositeConditions != null)
                foreach (var c in _CompositeConditions)
                    if (c)
                        c.SetHideFlagsInSelfAndHierarchy(flags);
        }

        public override void GetFlattenedHierarchy(List<ScriptableObject> hierarchy)
        {
            base.GetFlattenedHierarchy(hierarchy);

            if (_CompositeConditions != null)
                foreach (var c in _CompositeConditions)
                    if (c)
                    {
                        hierarchy.Add(c);
                        c.GetFlattenedHierarchy(hierarchy);
                    }
        }
        #endregion

        // TODO don't add conditions while drawing them
        public void AddCondition(QueryCondition condition)
        {
            Array.Resize(ref _CompositeConditions, _CompositeConditions.Length + 1);
            _CompositeConditions[_CompositeConditions.Length - 1] = condition;
            condition.Parent = this;
        }

        public void AddConditions(QueryCondition[] conditions)
        {
            int oldLen = _CompositeConditions.Length;
            Array.Resize(ref _CompositeConditions, oldLen + conditions.Length);
            Array.Copy(conditions, 0, _CompositeConditions, oldLen, conditions.Length);
            foreach (var c in conditions)
                c.Parent = this;
        }

        public void TransferStateTo(ConditionsChain other)
        {
            other.AddConditions(_CompositeConditions);
            _CompositeConditions = new QueryCondition[0];

            other._FadeGroupAnimation = this._FadeGroupAnimation;
        }

        public void ReplaceCondition(QueryCondition toBeReplaced, QueryCondition replacement)
        {
            toBeReplaced.Parent = null;
            replacement.Parent = this;
            _CompositeConditions[Array.IndexOf(_CompositeConditions, toBeReplaced)] = replacement;
        }

        public void GetGraphRecursivelyInTopDownOrder(List<QueryCondition> leafs, List<ConditionsChain> chains)
        {
            chains.Add(this);

            int len = _CompositeConditions.Length;
            QueryCondition condition;
            ConditionsChain conditionAsChain;
            for (int i = 0; i < len; ++i)
            {
                condition = _CompositeConditions[i];
                conditionAsChain = condition as ConditionsChain;
                if (conditionAsChain == null)
                    leafs.Add(condition);
                else
                    conditionAsChain.GetGraphRecursivelyInTopDownOrder(leafs, chains);
            }
        }

        protected override bool OnHeadlineGUIInternal()
        {
            bool changed = false;
            //EditorUtils8.Instance.DrawCongestedLabel("[ "+GetCompositionOperatorAsWord() + " Chain " + id + " ]", SessionResources.Instance.headlineFieldsGUIStyle);
            //EditorGUIUtils8.Instance.DrawCongestedLabel("["+ HumanReadableConditionType + "]", SessionResources.Instance.headlineFieldsGUIStyle);
            EditorGUIUtils8.Instance.DrawCongestedLabel("[...]", SessionResources.Instance.headlineFieldsGUIStyle);

            Rect rect = GUILayoutUtility.GetLastRect();
            rect.xMin += rect.width; // offset to the right, as the button will be to the right of the last element
            rect.xMin += SessionResources.Instance.headlineFieldsGUIStyle.margin.left;
            float buttonHorizontalPadding = SessionResources.Instance.negateButtonGUIStyle.padding.left + SessionResources.Instance.negateButtonGUIStyle.padding.right;
            string text = GetCompositionOperatorAsWord() + " chain:";
            rect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(text, false) + buttonHorizontalPadding; // set the proper width for the size
            if (GUI.Button(rect, text, SessionResources.Instance.negateButtonGUIStyle))
            {
                if (ConvertChainListener != null)
                    ConvertChainListener.OnConvertChainRequested(this);
                //_ConversionButtonPressed = true; // the chain should be destroyed after OnGUI completes, not immediately
                changed = true;
            }

            //rect = GUILayoutUtility.GetLastRect();
            rect.xMin += rect.width; // offset to the right, as the button will be to the right of the last element
            rect.xMin += SessionResources.Instance.headlineFieldsGUIStyle.margin.left;
            buttonHorizontalPadding = SessionResources.Instance.negateButtonGUIStyle.padding.left + SessionResources.Instance.negateButtonGUIStyle.padding.right;
            rect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(HumanReadableExpandedExpression, false) + buttonHorizontalPadding; // set the proper width for the size of id
            if (GUI.Button(rect, HumanReadableExpandedExpression, SessionResources.Instance.negateButtonGUIStyle))
            {
                negate = !negate;
                changed = true;
            }

            //EditorUtils8.Instance.DrawCongestedLabel(HumanReadableExpandedExpression, SessionResources.Instance.headlineFieldsGUIStyle);

            return changed;
        }

        protected override bool OnGUIInternal()
        {
            bool changed = false;
            EditorGUILayout.BeginVertical();

            int len = _CompositeConditions.Length;
            for (int i = 0; i < len; )
            {
                changed = _CompositeConditions[i].OnGUI() || changed;

                // Was destroyed (remove button was pressed)
                if (_CompositeConditions[i] == null)
                {
                    changed = true;
                    ArrayUtility.RemoveAt(ref _CompositeConditions, i);
                    --len;
                }
                else
                    ++i; // go forward only if the current condition was not removed
            }

            if (GUILayout.Button("Add", GUILayout.Width(45), GUILayout.Height(17)))
            {
                changed = true;
                _AddConditionDropdownDrawer.Show(
                    typeIndex =>
                    {
                        if (typeIndex < 0)
                            return;

                        AddCondition(ConditionCreator.Create(typeIndex, hideFlags));
                    }
                );
            }
            EditorGUILayout.EndVertical();


            return changed;
        }

        protected abstract string GetCompositionOperatorAsSymbol();

        protected abstract string GetCompositionOperatorAsWord();

        protected override void OnDestroy()
        {
            //bool shouldSave = (hideFlags & HideFlags.DontSave) == 0;
            ////Debug.Log("OnDestroy: " + GetType().Name + ": " + name + "; shouldSave=" + shouldSave);
            //if (!shouldSave)
            //{
                foreach (var condition in _CompositeConditions)
                    if (condition)
                        DestroyImmediate(condition, true);
                _CompositeConditions = null;
            //}

            base.OnDestroy();
        }


        public interface IConvertChainListener
        {
            void OnConvertChainRequested(ConditionsChain chainToConvert);
        }
    }
}
