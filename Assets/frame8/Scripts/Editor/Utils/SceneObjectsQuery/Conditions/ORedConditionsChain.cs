﻿using System;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    public class ORedConditionsChain : ConditionsChain
    {
        protected override string GetCompositionOperatorAsSymbol()
        {
            return "|";
        }

        protected override string GetCompositionOperatorAsWord()
        {
            return "OR";
        }

        protected override bool? EvaluateIgnoringNegationModifier(Transform transform)
        {
            int len = _CompositeConditions.Length;

            bool? chainEvaluation = null; // an empty chain cannot be evaluated (if len == 0)
            for (int i = 0; i < len; ++i)
            {
                bool? childEvaluation = _CompositeConditions[i].Evaluate(transform);

                // Ignore conditions that can't be evaluated
                if (childEvaluation == null)
                    continue;

                // It's enough only one condition to be TRUE in order for the OR chain to be TRUE
                if (childEvaluation.Value)
                    return true;

                // The chain has all the conditions FALSE until now, so it's FALSE
                chainEvaluation = false;
            }

            return chainEvaluation;
        }
    }
}
