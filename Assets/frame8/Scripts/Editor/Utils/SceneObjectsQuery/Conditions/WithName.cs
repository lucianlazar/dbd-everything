﻿using frame8.CSharpAssemblyEditorUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    class WithName : QueryCondition
    {
        [SerializeField]
        protected string _Keyword;
        [SerializeField]
        protected bool _IgnoreCase;
        [SerializeField]
        protected bool _PartOfName;


        protected override void OnEnable2()
        {
            base.OnEnable2();

            if (_Keyword == null)
            {
                _Keyword = "";
                _IgnoreCase = _PartOfName = true;
            }
        }

        protected override bool OnGUIInternal()
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.BeginHorizontal();

            _Keyword = EditorGUIUtils8.Instance.DrawCongestedTextField("Keyword", _Keyword);
            GUILayout.Space(20f);

            _IgnoreCase = EditorGUIUtils8.Instance.DrawCongestedToggle("Ignore case", _IgnoreCase, false);
            GUILayout.Space(20f);

            _PartOfName = EditorGUIUtils8.Instance.DrawCongestedToggle("Part of name", _PartOfName, false);

            EditorGUILayout.EndHorizontal();

            return EditorGUI.EndChangeCheck();
        }

        protected override bool? EvaluateIgnoringNegationModifier(Transform transform)
        {
            string transformName = transform.name;
            string keywordToUse = _Keyword;
            if (_IgnoreCase)
            {
                keywordToUse = _Keyword.ToLower();
                transformName = transformName.ToLower();
            }

            return _PartOfName ? transformName.Contains(keywordToUse) : transformName == keywordToUse;
        }
    }
}
