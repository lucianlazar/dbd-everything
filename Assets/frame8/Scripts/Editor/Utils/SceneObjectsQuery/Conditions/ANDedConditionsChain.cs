﻿using System;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    public class ANDedConditionsChain : ConditionsChain
    {
        protected override string GetCompositionOperatorAsSymbol()
        {
            return "&";
        }

        protected override string GetCompositionOperatorAsWord()
        {
            return "AND";
        }

        protected override bool? EvaluateIgnoringNegationModifier(Transform transform)
        {
            int len = _CompositeConditions.Length;

            bool? chainEvaluation = null; // an empty chain cannot be evaluated (if len == 0)
            for (int i = 0; i < len; ++i)
            {
                bool? childEvaluation = _CompositeConditions[i].Evaluate(transform);

                // Ignore conditions that can't be evaluated
                if (childEvaluation == null)
                    continue;

                // It's enough only one condition to be FALSE in order for the AND chain to be FALSE
                if (!childEvaluation.Value)
                    return false;

                // The chain has all the conditions TRUE until now, so it's TRUE
                chainEvaluation = true;
            }

            return chainEvaluation;
        }
    }
}
