﻿using frame8.CSharpAssemblyEditorUtils;
using frame8.Editor.Utils.SceneObjectsQuery.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    public abstract class QueryCondition : ScriptableObject, ISerializationCallbackReceiver, IScriptableObjectNode
    {
        public string id;
        public bool negate;
        protected AnimBool _FadeGroupAnimation;

        public bool DisplayExpressionInHeadline { get; protected set; }
        public virtual string HumanReadableConditionType { get { return GetType().Name; } }
        public virtual string HumanReadableExpandedExpression { get { return HumanReadableNonExpandedExpression; } }
        public virtual string HumanReadableNonExpandedExpression { get { return (negate ? "!" : "") + id; } }
        public ConditionsChain Parent
        {
            get { return _Parent; }
            internal set
            {
                _Parent = value;
                if (_Parent && _Parent.hideFlags != hideFlags)
                {
                    SetHideFlagsInSelfAndHierarchy(_Parent.hideFlags);
                }
                //else
                    // Commented: will preserve whatever flags there are by default/set manually
                    //SetHideFlags(HideFlags.DontSave);
            }
        }
        protected virtual float ContentIndentLevel { get { return 15f; } }
        protected ConditionsChain _Parent;

        //public QueryCondition() { Debug.Log("Created QueryCondition: " + GetType().Name); }

        public bool? Evaluate(Transform transform)
        {
            bool? evaluationIgnoringNegationModifier = EvaluateIgnoringNegationModifier(transform);
            if (evaluationIgnoringNegationModifier == null)
                return null;

            return evaluationIgnoringNegationModifier != negate;
        }

        // Returns if a repaint is needed
        public bool OnGUI()
        {
            bool guiChanged = false;
            bool removeButtonPressed = false;
            EditorGUILayout.BeginVertical(SessionResources.Instance.conditionGUIStyle);
            {
                // Headline drawing
                EditorGUILayout.BeginHorizontal(SessionResources.Instance.headlineGUIStyle);
                {
                    _FadeGroupAnimation.target = EditorGUILayout.Foldout(_FadeGroupAnimation.target, "", SessionResources.Instance.conditionFoldOutGUIStyle);

                    // The only way I've found to bring the following controls to the left
                    GUILayout.Space(-40);

                    guiChanged = OnHeadlineGUIInternal();

                    var controlRect = EditorGUILayout.GetControlRect();
                    Vector2 buttonSize = new Vector2(60f, 15f);
                    Rect buttonRect = 
                        new Rect(
                            EditorGUIUtility.currentViewWidth - SessionResources.Instance.conditionGUIStyle.padding.right - buttonSize.x - 8 - GUI.skin.verticalScrollbar.fixedWidth, 
                            controlRect.y + SessionResources.Instance.headlineGUIStyle.padding.top + SessionResources.Instance.conditionGUIStyle.padding.top + 5,
                            buttonSize.x,
                            buttonSize.y
                        );
                    removeButtonPressed = GUI.Button(buttonRect, "Remove");
                }
                EditorGUILayout.EndHorizontal();
                //Extra block that can be toggled on and off.
                if (EditorGUILayout.BeginFadeGroup(_FadeGroupAnimation.faded))
                {
                    EditorGUILayout.BeginVertical();
                    {
                        // Content drawing
                        EditorGUIUtils8.Instance.BeginIndentedZone(SessionResources.Instance.conditionFieldsGUIStyle, ContentIndentLevel);
                        guiChanged = OnGUIInternal() || guiChanged;
                        EditorGUIUtils8.Instance.EndIndentedZone();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndFadeGroup();
            }
            EditorGUILayout.EndVertical();

            if (removeButtonPressed)
            {
                guiChanged = true;
                DestroyImmediate(this, true);
            }

            return guiChanged;
        }

        public override string ToString()
        {
            return "(ID=" + id + ") " + "(Type=" + HumanReadableConditionType + ") " + "(Expression=" + HumanReadableExpandedExpression + ")";
        }

        #region ISerializationCallbackReceiver's implementation
        public virtual void OnBeforeSerialize()
        {
        }

        public virtual void OnAfterDeserialize()
        {
            // COmmented: TODO check if this is needed. OnEnable is called after deserialization, so no need to call this, right?
            //InitializeNonSerializedData();
        }
        #endregion

        #region IScriptableObjectNode
        public virtual void SetHideFlagsInSelfAndHierarchy(HideFlags flags)
        {
            hideFlags = flags;
        }

        public virtual void GetFlattenedHierarchy(List<ScriptableObject> hierarchy)
        {

        }
        #endregion

        protected virtual void OnEnable2() { name = GetType().Name; }

        // Returns if a repaint is needed
        protected virtual bool OnGUIInternal()
        {
            return false;
        }

        // Returns if a repaint is needed
        protected virtual bool OnHeadlineGUIInternal()
        {
            bool changed = false;
            //EditorUtils8.Instance.DrawCongestedLabel(id, SessionResources.Instance.headlineFieldsGUIStyle);
            EditorGUIUtils8.Instance.DrawCongestedLabel(HumanReadableConditionType, SessionResources.Instance.headlineFieldsGUIStyle);
            
            Rect rect = GUILayoutUtility.GetLastRect();
            rect.xMin += rect.width; // offset to the right, as the button will be to the right of the last element
            rect.xMin += SessionResources.Instance.headlineFieldsGUIStyle.margin.left;
            float buttonHorizontalPadding = SessionResources.Instance.negateButtonGUIStyle.padding.left + SessionResources.Instance.negateButtonGUIStyle.padding.right;
            rect.width = EditorGUIUtils8.Instance.GetCalculatedWidthForText(HumanReadableNonExpandedExpression, false) + buttonHorizontalPadding + SessionResources.Instance.headlineFieldsGUIStyle.margin.left; // set the proper width for the size of id
            if (GUI.Button(rect, HumanReadableNonExpandedExpression, SessionResources.Instance.negateButtonGUIStyle))
            {
                negate = !negate;
                changed = true;
            }

            if (DisplayExpressionInHeadline)
                EditorGUIUtils8.Instance.DrawCongestedLabel("Expression: " + HumanReadableExpandedExpression, SessionResources.Instance.headlineFieldsGUIStyle);
            return changed;
        }

        protected abstract bool? EvaluateIgnoringNegationModifier(Transform transform);

        protected virtual void OnDestroy()
        {
            //Debug.Log("OnDestroy for " + GetType().Name);

        }

        void OnEnable()
        {
            InitializeNonSerializedData();

            OnEnable2();
        }

        void InitializeNonSerializedData()
        {
            _FadeGroupAnimation = new AnimBool(true);
            //_FadeGroupAnimation.valueChanged.AddListener(() => { _GUIChanged = true;; });
            _FadeGroupAnimation.speed = 4f;
        }
    }
}
