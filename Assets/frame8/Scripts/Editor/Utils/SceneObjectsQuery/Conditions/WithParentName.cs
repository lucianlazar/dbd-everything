﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    class WithParentName : WithName
    {
        [SerializeField]
        bool _NullParentMeansConditionIsTrue;

        protected override bool? EvaluateIgnoringNegationModifier(Transform transform)
        {
            Transform parent = transform.parent;
            if (parent == null)
                return _NullParentMeansConditionIsTrue;

            return base.EvaluateIgnoringNegationModifier(parent);
        }
    }
}
