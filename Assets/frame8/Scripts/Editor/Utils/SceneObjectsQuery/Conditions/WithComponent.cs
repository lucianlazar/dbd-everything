﻿using frame8.CSharpAssemblyEditorUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace frame8.Editor.Utils.SceneObjectsQuery.Conditions
{
    [Serializable]
    public class WithComponent : QueryCondition
    {
        [SerializeField]
        string _ComponentName;

        //public WithComponent() {}

        //public WithComponent(string componentName) { _ComponentName = componentName; }


        protected override void OnEnable2()
        {
            base.OnEnable2();

            if (_ComponentName == null)
                _ComponentName = "";
        }

        protected override bool OnGUIInternal()
        {
            EditorGUI.BeginChangeCheck();
            _ComponentName = EditorGUIUtils8.Instance.DrawCongestedTextField("Keyword", _ComponentName);

            return EditorGUI.EndChangeCheck();
        }

        protected override bool? EvaluateIgnoringNegationModifier(Transform transform)
        {
            return transform.GetComponent(_ComponentName) != null;
        }
    }
}
