//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEditor;
using UnityEngine;


public class SelectedObjectInfoWindow : EditorWindow
{
	int currentUpdateModulo100 = 0;
	Vector2 _ScrollViewPosition;

	[MenuItem ("frame8/SelectedObjectInfo")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		GetWindow<SelectedObjectInfoWindow> ();
	}

	void OnGUI () {
		_ScrollViewPosition = EditorGUILayout.BeginScrollView(_ScrollViewPosition);

		if (Selection.activeTransform != null)
		{
			Transform[] transforms = Selection.transforms;
			int numTransforms;
			if (transforms != null && (numTransforms = transforms.Length) > 0)
			{
				if (numTransforms == 1)
				{
					RectTransform rectTransform = Selection.activeTransform as RectTransform;
					if (rectTransform != null)
					{
						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField(
							"Rect: (xMin " + rectTransform.rect.xMin+ // left edge disp from pivot
							"; yMin " + rectTransform.rect.yMin+ // bottom ..
							"; xMax " + rectTransform.rect.xMax+ // ..
							"; yMax " + rectTransform.rect.yMax+")"); // ..
						EditorGUILayout.EndHorizontal();
						
						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField(
							"Rect: (x " + rectTransform.rect.x+ // same as above xMin
							"; y " + rectTransform.rect.y+ // same as above yMin
							"; width " + rectTransform.rect.width+
							"; height " + rectTransform.rect.height+")");
						EditorGUILayout.EndHorizontal();
						
						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField(
							"pos" + rectTransform.position+ // disp from the left-corner of the hierarchy's root
							"; locPos " + rectTransform.localPosition+ // disp from the parent's pivot
							"; anchPos " + rectTransform.anchoredPosition);// disp from center of anchors
						EditorGUILayout.EndHorizontal();
						
						EditorGUILayout.LabelField(
							"offsetMin " + rectTransform.offsetMin+ // offsets of edges from anchors
							"; offsetMax " + rectTransform.offsetMax);
						
						EditorGUILayout.LabelField(
							"pivot " + rectTransform.pivot+ // disp from element's bootom-left corner, divided by (width,height) (so, the pivot for right-top of the rect will be (1,1)); can exceed 1, being outside
							"; sizeDelta " + rectTransform.sizeDelta) ;// (width, height) - (the anchors "real size"(x and y distances between their edges))
							
						if (rectTransform.parent != null)
							EditorGUILayout.LabelField("bottomLeftCornerDisplacementFromParrentBottomLeftCorner:" + UIUtils8.Instance.GetBottomLeftCornerDisplacementFromParentBottomLeftCorner(rectTransform));
					}
				}
				else if (numTransforms > 1 && numTransforms < 10)
				{		
					EditorGUILayout.LabelField("Distance between selected Objects");
					for (int i = 0; i < numTransforms; ++i)
					{
						for (int j = i + 1; j < numTransforms; ++j)
						{
							EditorGUILayout.LabelField(transforms[i].name + "<->" + transforms[j].name + " :: " + (Vector3.Distance(transforms[i].position, transforms[j].position)));
						}
					}

				}
			}
		}
		EditorGUILayout.EndScrollView();
	}

	void Update()
	{
		if (currentUpdateModulo100 % 20 == 0)
			Repaint();

		currentUpdateModulo100 = (currentUpdateModulo100+1) % 100;
	}

	void OnSelectionChange()
	{
		Repaint();
	}
}

