using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace frame8.Editor
{
    public class MeshToPolygonCollider2DEditorWindow8 : EditorWindow
    {
        //const int N = 200;
        int _CurrentFrameModuloN;
        ProjectionPlane _SelectedProjectionPlane;
        float _VerticesWeldingThreshold;
        //bool _RealTimeGeneration;

        [MenuItem("frame8/MeshToPolygonCollider2D")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            var window = GetWindow<MeshToPolygonCollider2DEditorWindow8>();
            //window._RealTimeGeneration = false;
            window.title = "MeshToPolygonCollider2D";
            //window.titleContent = new GUIContent("MeshToPolygonCollider2D");
        }

        void OnGUI()
        {
            if (Eligible(true))
            {
                _SelectedProjectionPlane = (ProjectionPlane)EditorGUILayout.EnumPopup("ProjectionPlane (in object's local space)", _SelectedProjectionPlane);
                _VerticesWeldingThreshold = EditorGUILayout.Slider("Vertices welding threshold", _VerticesWeldingThreshold, 0f, 10f);
                //_RealTimeGeneration = EditorGUILayout.ToggleLeft("Realtime generation", _RealTimeGeneration);

                if (GUILayout.Button("Generate as child"))
                    Generate();
            }
        }

        //void Update()
        //{
        //    _CurrentFrameModuloN = (_CurrentFrameModuloN + 1) % N;
        //    if (_CurrentFrameModuloN == 0)
        //    {
        //        if (_RealTimeGeneration)
        //        {
        //            Generate();
        //        }
        //    }
        //}

        bool Eligible(bool showErrorMessages)
        {

            GameObject selectedGO = Selection.activeGameObject;
            if (selectedGO == null)
                return false;

            MeshFilter meshFilter = selectedGO.GetComponent<MeshFilter>();
            if (meshFilter == null)
                return false;
            Mesh mesh = meshFilter.sharedMesh;
            if (!mesh.isReadable)
            {
                if (showErrorMessages)
                    EditorGUILayout.HelpBox("Mesh is not readable. Change this in import settings", MessageType.Error);

                return false;
            }

            return true;
        }

        void Generate()
        {
            if (Eligible(false))
            {
                Transform selectedTransform = Selection.activeTransform;
                Mesh mesh = selectedTransform.GetComponent<MeshFilter>().sharedMesh;

                //PolygonCollider2D poly2D = selectedTransform.GetComponentInChildren<PolygonCollider2D>();
                //string polyName = "Poly2DCollider";
                //if (poly2D == null || poly2D.name != polyName)
                //{
                //    poly2D = new GameObject().AddComponent<PolygonCollider2D>(); ;
                //    poly2D.name = polyName;
                //    poly2D.GetComponent<Transform>().SetParent(selectedTransform);
                //}
                //else
                //{
                //    poly2D.pathCount = 0;
                //}
                    

                // Set vertices
                if (_SelectedProjectionPlane == ProjectionPlane.XZ)
                {
                    Vector3[] vertices = mesh.vertices;
                    
                    PolygonCollider2D poly2D = selectedTransform.GetComponentInChildren<PolygonCollider2D>();
                    if (poly2D == null || poly2D.gameObject.transform == selectedTransform)
                    {
                        poly2D = new GameObject().AddComponent<PolygonCollider2D>(); ;
                        poly2D.name = "Poly2DCollider";
                        poly2D.GetComponent<Transform>().SetParent(selectedTransform);
                    }
                    poly2D.pathCount = 1;

                    int numVertices = mesh.triangles.Length;
                    List<Vector2> vertices2D = new List<Vector2>(numVertices);
                    for (int i = 0; i < mesh.triangles.Length; ++i)
                    {
                        Vector3 vertex = vertices[mesh.triangles[i]];
                        vertices2D.Add(new Vector2(vertex.x, vertex.y));
                    }

                    //bool optimizedInCurrentStep;
                    //do
                    //{
                    //    optimizedInCurrentStep = false;

                    //    for (int i = 0; i < numVertices - 1; ++i)
                    //    {
                    //        Vector2 curVertex = vertices2D[i];
                    //        Vector2 nextVertex = vertices2D[i + 1];

                    //        if (Vector2.Distance(curVertex, nextVertex) < _VerticesWeldingThreshold)
                    //        {
                    //            vertices2D[i] = (curVertex + nextVertex) / 2f;
                    //            vertices2D.RemoveAt(i + 1);
                    //            --numVertices;
                    //            optimizedInCurrentStep = true;
                    //        }
                    //    }
                    //}
                    //while (optimizedInCurrentStep);

                    poly2D.SetPath(0, vertices2D.ToArray());
                    //poly2D.points = vertices2D.ToArray();
                    //poly2D.transform.localRotation = Quaternion.identity;
                    //poly2D.transform.localScale = Vector3.one;
                    poly2D.transform.localPosition = Vector3.zero;
                }
            }
        }

        void OnSelectionChange()
        {
            Repaint();
        }

        enum ProjectionPlane
        {
            XY,
            XZ,
            YZ
        }
    }
}

