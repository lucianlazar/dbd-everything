//using frame8.Editor.Utils.SceneObjectsQuery.Conditions;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using UnityEditor;
//using UnityEngine;

//namespace frame8.Editor.Utils.SceneObjectsQuery.Helpers
//{
//    internal static class GenericPropertyDrawer
//    {

//        internal static void Draw(Rect position, SerializedProperty property, GUIContent label)
//        {
//            SerializedPropertyType propertyType = property.propertyType;
//            switch (propertyType)
//            {
//                case SerializedPropertyType.Integer:
//                    {
//                        EditorGUI.BeginChangeCheck();
//                        int intValue = EditorGUI.IntField(position, label, property.intValue);
//                        if (EditorGUI.EndChangeCheck())
//                        {
//                            property.intValue = intValue;
//                        }
//                        break;
//                    }
//                case SerializedPropertyType.Boolean:
//                    {
//                        EditorGUI.BeginChangeCheck();
//                        bool boolValue = EditorGUI.Toggle(position, label, property.boolValue);
//                        if (EditorGUI.EndChangeCheck())
//                        {
//                            property.boolValue = boolValue;
//                        }
//                        break;
//                    }
//                case SerializedPropertyType.Float:
//                    {
//                        EditorGUI.BeginChangeCheck();
//                        float floatValue = EditorGUI.FloatField(position, label, property.floatValue);
//                        if (EditorGUI.EndChangeCheck())
//                        {
//                            property.floatValue = floatValue;
//                        }
//                        break;
//                    }
//                case SerializedPropertyType.String:
//                    {
//                        EditorGUI.BeginChangeCheck();
//                        string stringValue = EditorGUI.TextField(position, label, property.stringValue);
//                        if (EditorGUI.EndChangeCheck())
//                        {
//                            property.stringValue = stringValue;
//                        }
//                        break;
//                    }
//                case SerializedPropertyType.Color:
//                    {
//                        EditorGUI.BeginChangeCheck();
//                        Color colorValue = EditorGUI.ColorField(position, label, property.colorValue);
//                        if (EditorGUI.EndChangeCheck())
//                        {
//                            property.colorValue = colorValue;
//                        }
//                        break;
//                    }
//                case SerializedPropertyType.ObjectReference:

//                    EditorGUI.ObjectField(position, property, typeof(UnityEngine.Object), label);
//                    break;
//                case SerializedPropertyType.LayerMask:
//                    // TODO test
//                    property.intValue = EditorGUI.LayerField(position, label, property.intValue, EditorStyles.layerMaskField);
//                    break;
//                case SerializedPropertyType.Enum:
//                    // TODO - low importance - pass label, not label.text. this implies creating a GUIContent array for property.enumDisplayNames..
//                    property.enumValueIndex = EditorGUI.Popup(position, label.text, property.enumValueIndex, property.enumDisplayNames, EditorStyles.popup);
//                    break;
//                case SerializedPropertyType.Vector2:
//                    property.vector2Value = EditorGUI.Vector2Field(position, label, property.vector2Value);
//                    break;
//                case SerializedPropertyType.Vector3:
//                    property.vector3Value = EditorGUI.Vector3Field(position, label, property.vector3Value);
//                    break;
//                case SerializedPropertyType.Vector4:
//                    property.vector4Value = EditorGUI.Vector4Field(position, label, property.vector4Value);
//                    break;
//                case SerializedPropertyType.Rect:
//                    EditorGUI.RectField(position, property, label);
//                    break;
//                case SerializedPropertyType.Bounds:
//                    EditorGUI.BoundsField(position, property, label);
//                    break;
//            }
//        }
//    }
//}

