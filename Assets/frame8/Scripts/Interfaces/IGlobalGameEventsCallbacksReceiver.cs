
using System;
public interface IGlobalGameEventsCallbacksReceiver
{
	// This is a quick technique for resolving one-way(non-cyclic) dependencies, 
	// though it's your responsability to make sure you don't produce cyclic dependencies
	// The receiver may return true if he wants to let other receivers to get the event before him
	bool OnGameInitialized();
}

