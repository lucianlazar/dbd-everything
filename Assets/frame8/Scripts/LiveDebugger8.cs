using UnityEngine;
using UnityEngine.UI;

public static class LiveDebugger8 {
//	public static int _NewLineAt = 50;
//	static bool _TextWrap = true;
	const int DEFAULT_MAX_MESSAGES = 15;
	static int _MaxMessages = DEFAULT_MAX_MESSAGES;
	public static int MaxMessages {get{return _MaxMessages;} set{_MaxMessages = value;} }
    public static bool DontShowOutsideEditor { get; set; }

    static int _NumMessages = 0;
    static string _CurrentMessage = "";
    static Text _Text;

    static int _NumMessages2 = 0;
    static string _CurrentMessage2 = "";
    static Text _Text2;
    static float _WidthToUse = Screen8.Instance.AvgUnitSize * 6;

    public static void SetColor(Color c)
    {
        if (_Text != null)
            _Text.color = c;
    }

    public static void SetColor2(Color c)
    {
        if (_Text2 != null)
            _Text2.color = c;
    }

    public static void logLC(object obj)
    {
        clearL();
        logL(obj);
    }

    public static void logRC(object obj)
    {
        clearR();
        logR(obj);
    }

    public static void logL(object obj)
    {
#if !UNITY_EDITOR
        if (DontShowOutsideEditor)
            return;
#endif

        string mes = obj.ToString();

        if (_Text == null)
            InitText1();

        if (_NumMessages == MaxMessages) {
			_NumMessages = 0;
			_CurrentMessage = "";
		}

		_CurrentMessage = _CurrentMessage + "[" + mes + "]" + "\n";
		_Text.text = _CurrentMessage;
		++_NumMessages;
    }

    public static void logR(object obj)
    {
#if !UNITY_EDITOR
        if (DontShowOutsideEditor)
            return;
#endif
        string mes = obj.ToString();

        if (_Text2 == null)
            InitText2();

        if (_NumMessages2 == MaxMessages)
        {
            _NumMessages2 = 0;
            _CurrentMessage2 = "";
        }

        _CurrentMessage2 = _CurrentMessage2 + "[" + mes + "]" + "\n";
        _Text2.text = _CurrentMessage2;
        ++_NumMessages2;
    }

    static void InitText1()
    {
        InitText(true, out _Text);
        _Text.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0f, _WidthToUse);
    }

    static void InitText2()
    {
        InitText(false, out _Text2);
        _Text2.color *= new Color(1f, .8f, .8f, 1f);
        _Text2.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0f, _WidthToUse);
    }

    static void InitText(bool left, out Text t)
    {
        string suffix = (left ? "" : "R");
        GameObject asset = Resources.Load<GameObject>("LiveDebuggerPrefab");
        GameObject liveDebuggerParentInstance = GameObject.Instantiate(asset) as GameObject;
        //Resources.UnloadAsset(asset);
        liveDebuggerParentInstance.name = "LiveDebuggerParent8" + suffix;
        t = liveDebuggerParentInstance.GetComponentInChildren<Text>();
        t.name = "LiveDebugger8" + suffix;
        t.fontSize = (int)(Screen8.Instance.AvgUnitSize * .3f);
        if (left)
            t.alignment = TextAnchor.UpperLeft;
        else
            t.alignment = TextAnchor.UpperRight;
        //			_Text.transform.position = new Vector3(0f, 1f, 0f);
        //			_Text.color = Color.black;
        //			_Text.fontSize = 14;
    }

    public static void clearL() { _CurrentMessage = ""; if (_Text != null) _Text.text = _CurrentMessage; }
    public static void clearR() { _CurrentMessage2 = ""; if (_Text2 != null) _Text2.text = _CurrentMessage2; }
    public static void clearAll() { clearL(); clearR(); }
}
