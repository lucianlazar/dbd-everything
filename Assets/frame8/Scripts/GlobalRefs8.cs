using UnityEngine;

// The base class for some 8lue objects
// All objects that extends this class will have access to the gameplay 
// manager and gamesession manager and other frequently used entities
public class GlobalRefs8 : Singleton8<GlobalRefs8>
{
    public System.Threading.Thread unityThread { get; private set; }
 //   public GameManager8 gameManager { get; private set; }
 //   public ActivityManager8 activityManager {get; private set;}
	//public Design8 design {get; private set;}
	//public GameData8 gameData {get; private set;}
	
	#region implemented abstract members of Singleton8
	// Not used here, because we must wait until the game session manager is fully initialized
	public override void Init ()
	{}

	#endregion

    void InitGlobalReferences()
    {

        // The GameSessionManager will already have been init the other entities
  //      gameManager = GameManager8.Instance;
		//activityManager = gameManager.ActivityManager;
		//gameData = gameManager.GameData;
  //      design = gameManager.Design;
        unityThread = System.Threading.Thread.CurrentThread;
    }

    // Called by GameManager8 when it was initialized
	public void OnGameInitialized()
	{
        // Get a reference for each of the most used entities, so they can be easyly accessed
        InitGlobalReferences();
	}
}


