using UnityEngine;
using System.Collections;
using System;

public class StringUtils8 : Singleton8<StringUtils8>
{
	
	public int GetNthTokenStartIndex(string source, int nth, char skippedChar)
	{
		int strLen = source.Length;
		int numTokensSoFar = 0;
		for (int i = 0; i < strLen;)
		{
			if (source[i] != skippedChar)
			{				
				// Found it
				if (numTokensSoFar == nth)
					return i;	
				
				++numTokensSoFar;
				
				// Get i to the index of the first skippedChar after the current token
				while (++i < strLen && source[i] != skippedChar);
				
				// Get i to the index of the first non-skippedChar after the current token
				while (i < strLen && source[i] == skippedChar) ++i;
				
				continue; // don't increment i anymore, since we've found a new token OR the string ended
			}
			++i;
		}
		
		
		return -1;
	}
	
	public string GetNthToken(string source, int nth, char skippedChar)
	{
		int nthTokenStartIndex = GetNthTokenStartIndex(source, nth, skippedChar);
		if (nthTokenStartIndex < 0)
			return null;
		
		int sourceSize = source.Length;
		int sizeOfToken = 0;
		int i = nthTokenStartIndex;
		while (i < sourceSize && source[i++] != skippedChar)
			++sizeOfToken;		
		
		return source.Substring(nthTokenStartIndex, sizeOfToken);
	}
	
	public string GetNextNumberedCopyOfTheString(string str, string strToPutBeforeNumber, string strToPutAfterNumber)
	{		
		int strLen = str.Length;
		int lenStrToPutBeforeNumber = strToPutBeforeNumber.Length;
		int lenStrToPutAfterNumber = strToPutAfterNumber.Length;
		char curChar;
		bool strToPutBeforeNumberNotEmpty = (lenStrToPutBeforeNumber > 0);
		bool strToPutAfterNumberNotEmpty = (lenStrToPutAfterNumber > 0);
		int indexOfStrToPutAfterNumberInOriginalStr;
		
		if (strToPutAfterNumberNotEmpty)
		{
			indexOfStrToPutAfterNumberInOriginalStr = str.LastIndexOf(strToPutAfterNumber);
			
			if (indexOfStrToPutAfterNumberInOriginalStr < 0)
				indexOfStrToPutAfterNumberInOriginalStr = strLen;
			else
			{
				// <strToPutAfterNumber> was found, but it's not the tail of <str> 
				if (!str.EndsWith(strToPutAfterNumber))
				{
					// first copy
					return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;;
				}
			}
		}
		else
			indexOfStrToPutAfterNumberInOriginalStr = strLen;
		
		
		for (int i = indexOfStrToPutAfterNumberInOriginalStr - 1; i >= 0; --i)
		{
			curChar = str[i];
			if (Char.IsDigit(curChar))
			{
				// the string from 0 to <indexOfStrAfterNumberInOriginalStr> is a number
				// so <strToPutBeforeNumber> is not already present
				if (i == 0)
				{
					if (strToPutBeforeNumberNotEmpty) // ... but it should be, so => first copy
					{
						return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;
					}
					else
					{
						if (strToPutAfterNumberNotEmpty)
						{
							// <strAfterNumberNotEmpty> not present at the end; first copy
							if (indexOfStrToPutAfterNumberInOriginalStr == strLen)
							{
								return str +/*strbeforenum is empty here*/ "0" + strToPutAfterNumber;
							}
							else
							{
								return new StringNumber8(str.Substring(0, indexOfStrToPutAfterNumberInOriginalStr)).Increment() 
									+ strToPutAfterNumber;
							}
						}
						// the whole string is a number
						else
						{
							return new StringNumber8(str).Increment();
						}
					}
				}
			}
			else
			{
				if (i+1 == indexOfStrToPutAfterNumberInOriginalStr) // no digit found => first copy
				{
					return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;
				}
				else // one or more digits found; maybe it isn't the first copy
				{
					string curEntireStrBeforeNumber = str.Substring(0, i+1);
					int numDigitsInFoundNumber = indexOfStrToPutAfterNumberInOriginalStr - i - 1;
					string curStrNumber = str.Substring(i+1, numDigitsInFoundNumber);
					string curStrAfterNumber = str.Substring(i+1 + numDigitsInFoundNumber);
					StringNumber8 strIncrementedNum = new StringNumber8(curStrNumber);
					strIncrementedNum.Increment();
					if (strToPutBeforeNumberNotEmpty)
					{
						if (i < lenStrToPutBeforeNumber - 1) // no room for the <strBeforeNumber>; forget everything; first copy
						{
							return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;
						}
						else
						{
							if (curEntireStrBeforeNumber.EndsWith(strToPutBeforeNumber))
							{
								if (curStrAfterNumber == strToPutAfterNumber) // everything mathces
								{
									return curEntireStrBeforeNumber + strIncrementedNum + strToPutAfterNumber;
								}
								else // end of <curEntireStrBeforeNumber> doesn't match; first copy
								{
									return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;
								}
							}
							else // <strToPutBeforeNumber> doesn't already exist; first copy
							{
								return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;
							}
						}
					}
					else
					{
						if (curStrAfterNumber == strToPutAfterNumber) // everything mathces
						{
							return curEntireStrBeforeNumber + strIncrementedNum + strToPutAfterNumber;
						}
						else // tail doesn't match; first copy
						{
							return str + strToPutBeforeNumber + "0" + strToPutAfterNumber;
						}
					}
				}
			}
		}
		
		return str;
	}
	





	#region implemented abstract members of Singleton8
	public override void Init ()
	{
	
	}
	#endregion
}

