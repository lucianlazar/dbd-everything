using UnityEngine;
using System.Collections;

public struct Sphere8
{
	public Vector3 position;
	public float radius;
}

