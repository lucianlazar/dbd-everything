using UnityEngine;
using System.Collections;

// Currently, supporting only positive
public class StringNumber8
{
	string _StringNumber;
	
	
	public StringNumber8(string initialValue)
	{
		_StringNumber = initialValue;
	}
	
	// Returns the result, but updates the internal string too
	public string Increment()
	{
		ushort curByteAsUshort;
		byte[] bytes = System.Text.Encoding.ASCII.GetBytes(_StringNumber);
		int len = bytes.Length;
		for (int i = len-1; ; --i)
		{
			curByteAsUshort = bytes[i]; // to ascii
//			curByteAsUshort -= 48; // to dec
			if (curByteAsUshort == 57) // '9' in ascii
			{
				bytes[i] = 48; // '0' in ascii
				
				// 999..999 case; must add a '1' in the front
				if (i == 0)
				{
					_StringNumber = "1" + System.Text.Encoding.ASCII.GetString(bytes);
					
					break;
				}
			}
			// no further incrementing needed for the digits on the left
			else
			{
				bytes[i] = (byte)(curByteAsUshort+1);
				_StringNumber = System.Text.Encoding.ASCII.GetString(bytes);
				
				break;
			}
		}
		
		return _StringNumber;
	}
	
	public static bool IsNumber(string str)
	{
		foreach (char c in str)
			if (!char.IsDigit(c) && c != '-')
				return false;
				
		return true;
	}
	
	public override string ToString ()
	{
		return _StringNumber;
	}

}

