using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MathUtils8 : Singleton8<MathUtils8>
{
    public const float Meters2Inch = 39.3701f;
    public const float Centimeters2Inch = Meters2Inch / 100;
    public const float Milimeters2Inch = Centimeters2Inch / 10;
    public const float Inch2Milimeters = 1f / Milimeters2Inch;

    /// <summary>
    /// Version 21.09.06.
    /// It assumes only positive floats are passed as weights (it doesn't matter if their sum is bigger than 1. this will be normalized. just keep it under int.MaxValue :) ) 
    /// and that at least one of them is non-zero
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="weightedChanceValues"></param>
    /// <returns>the T object randomly chosen from the list, based on it's chance</returns>
    public T GetRandomWeightedChanceObject<T>(params KeyValuePair<T, float>[] weightedChanceValues)
    {
        // Working on a copy, so we won't modify the passed array
        var weightedChanceValues2 = weightedChanceValues.Clone() as KeyValuePair<T, float>[];

        Array.Sort(weightedChanceValues2, (a, b) =>
        {
            if (a.Value < b.Value)
                return -1;

            if (a.Value > b.Value)
                return 1;

            return 0;
        });

        // Obtain a modified set of values in the following form:
        // For 1,2,3,4 we obtain 1,3,6,10
        int numValues = weightedChanceValues2.Length;
        for (int i = 1; i < numValues; ++i)
        {
            float prevValue = weightedChanceValues2[i - 1].Value;
            float curValue = weightedChanceValues2[i].Value;
            var kv = new KeyValuePair<T, float>(weightedChanceValues2[i].Key, prevValue + curValue);
            weightedChanceValues2[i] = kv;
        }

        // So for 1,2,3,4:
        // "Throw" a random point somewhere in
        // [-][--][---][----]
        //  1   2   3    4
        float random = UnityEngine.Random.Range(.0000001f, weightedChanceValues2[numValues - 1].Value); // a number between 0+ inclusive and the sum of all weights exclusive

        // Find the object that designates the "area" the random number was thrown in
        foreach (var keyVal in weightedChanceValues2)
            if (random <= keyVal.Value)
                return keyVal.Key;

        throw new ArgumentException("Maybe you've passed only zero values?");
    }

    // This can be optimized for primitive number types
    public T Max<T>(T[] array) where T : IComparable<T>
	{
		if (array == null || array.Length == 0)
			return default(T);
	
		T max = array[0];
		T curVal;
		long arrayLEngth = array.LongLength;
		
		for (long i = 1L; i < arrayLEngth; ++i)
		{
			curVal = array[i];
			if (curVal.CompareTo(max) > 0)
				max = curVal;
		}
		
		return max;
	}
	

    // Here, min x = at least x far from zero (i.e. it works both ways)
    public void AssureMinAbsValueOnEachAxis(ref Vector3 values, Vector3 minValuesToAssureOnEachAxis)
    {
        Vector3 cumulatedValues = values + minValuesToAssureOnEachAxis;
        Vector3 valuesAxesSigns =
            new Vector3(Mathf.Sign(values.x), Mathf.Sign(values.y), Mathf.Sign(values.z));
        Vector3 minVelocityToAssureAbsValues =
            new Vector3(Mathf.Abs(minValuesToAssureOnEachAxis.x), Mathf.Abs(minValuesToAssureOnEachAxis.y), Mathf.Abs(minValuesToAssureOnEachAxis.z));
        Vector3 velocityAbsValues =
            new Vector3(Mathf.Abs(values.x), Mathf.Abs(values.y), Mathf.Abs(values.z));

        values.x =
            Mathf.Sign(minValuesToAssureOnEachAxis.x) == valuesAxesSigns.x ?
                valuesAxesSigns.x * Mathf.Max(velocityAbsValues.x, minVelocityToAssureAbsValues.x)
                : cumulatedValues.x;

        values.y =
            Mathf.Sign(minValuesToAssureOnEachAxis.y) == valuesAxesSigns.y ?
                valuesAxesSigns.y * Mathf.Max(velocityAbsValues.y, minVelocityToAssureAbsValues.y)
                : cumulatedValues.y;

        values.z =
            Mathf.Sign(minValuesToAssureOnEachAxis.z) == valuesAxesSigns.z ?
                valuesAxesSigns.z * Mathf.Max(velocityAbsValues.z, minVelocityToAssureAbsValues.z)
                : cumulatedValues.z;
    }
	
//	public T Min<T>(T[] array)
//	{
//		if (array == null || array.Length == 0)
//			return default(T);
//		
//		T min = array[0];
//		T curVal;
//		long arrayLEngth = array.LongLength;
//		
//		for (long i = 1L; i < arrayLEngth; ++i)
//		{
//			curVal = array[i];
//			if (curVal < min)
//				min = curVal;
//		}
//		
//		return min;
//	}



	#region implemented abstract members of Singleton8
	public override void Init () {}
	#endregion
}

