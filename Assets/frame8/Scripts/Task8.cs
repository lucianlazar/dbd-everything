using UnityEngine;
using System.Collections;


/* Notes: 
 * -the first step is executed immediately
 * -note the delayBetweenExecutions semantic (it's not like InvokeRepeating); 
 * -the minimum delay between two executions always translates in 1 frame, so there won't be a StackOverflowException
 * 
 * 
 * 
 * 
 */

// TODO make a inner clas that inherrits from mono, instead of using Task8 dirrectly for this
using System;


public sealed class Task8 : MonoBehaviour, IDisposable
{
	int _TargetNumberOfExecutions, _NumberOfExecutionsLeft;
	float _DelayBetweenExecutions;
	bool _IsPaused = true, _ExecutionInProgress, _DelayedNaturalEndingWasScheduled;
	
	// These two fields may increase/decrease, depending on how/when the resume/pause methods are being called
	float _CurrentUnconsumedStartDelay;
	float _CurrentUnconsumedEndDelay;
	
	float _TimeWhenStartOrResumeExecutionInternalDelayedCallWasScheduled;
	float _TimeWhenOnTaskEndedNaturallyDelayedCallWasScheduled;
	
  	public bool IsPaused {get{return _IsPaused;}}
  	public bool DontDestroyOnNaturalEnd {get; set;}
  	// TODO find references to this and correct them, since the semantic before 07.05.2015 was "ExecutionEnded"
	public bool ExecutionInProgress {get{return _ExecutionInProgress;}}
	public int TargetNumberOfExecutions {get{return _TargetNumberOfExecutions;} set{_TargetNumberOfExecutions = value;}}
	


	Func<bool> _OnCoroutineExecuteCallBack; // return false if you want the task to be immediately interrupted and _OnTaskEndCallBack to be called
	Action _OnCoroutineEndCallBack;

	// Use only CreateAndStart method to do stuff
	Task8() {}
	
	// Delays are in seconds
	public static Task8 CreateAndStart(float delayBetweenExecutions,
	                                  Func<bool> onCoroutineExecuteCallBack)
	{
		return CreateAndStart(0f, 0, delayBetweenExecutions, 0, onCoroutineExecuteCallBack, null);
	}
	
	// Delays are in seconds
	public static Task8 CreateAndStart(float delayBetweenExecutions,
	                                  Func<bool> onCoroutineExecuteCallBack,
	                                  Action onCoroutineEndCallBack)
	{
		return CreateAndStart(0f, 0, delayBetweenExecutions, 0, onCoroutineExecuteCallBack, onCoroutineEndCallBack);
	}
	
	// Delays are in seconds
	public static Task8 CreateAndStart(float startDelay, 
	                                  float delayBetweenExecutions,
	                                  Func<bool> onCoroutineExecuteCallBack)
	{
		return CreateAndStart(startDelay, 0, delayBetweenExecutions, 0, onCoroutineExecuteCallBack, null);
	}

	// Delays are in seconds
	public static Task8 Create(int times, // 0 = infinite, until end condition method is true
	                          float delayBetweenExecutions, 
	                          float endDelay,
	                          Func<bool> onCoroutineExecuteCallBack,
	                          Action onCoroutineEndCallBack)
	{
		// Instantiate the mono script by attaching it to a new empty gameobject
		Task8 task = new GameObject("Task8").AddComponent<Task8>();
		task.Init(times, delayBetweenExecutions, endDelay, onCoroutineExecuteCallBack, onCoroutineEndCallBack);

		return task;
	}

	public static Task8 CreateAndStart(float startDelay, 
	                                  int times, // 0 = infinite, until end condition method is true
	                                  float delayBetweenExecutions, 
	                                  float endDelay,
	                                  Func<bool> onCoroutineExecuteCallBack,
	                                  Action onCoroutineEndCallBack)
	{
		// Instantiate the mono script by attaching it to a new empty gameobject

		
		// Start execution
		return Create(times,delayBetweenExecutions, endDelay, onCoroutineExecuteCallBack, onCoroutineEndCallBack).StartOrResumeExecution(startDelay);
	}
	
	public void Init(int times, // 0 = infinite, until end condition method is true
	                 float delayBetweenExecutions, 
	                 float endDelay,
	                 Func<bool> onCoroutineExecuteCallBack,
	                 Action onCoroutineEndCallBack)
	{
		if (_ExecutionInProgress)
			throw new UnityException("ExecutionInProgress. First call EndExecution!");

		_TargetNumberOfExecutions = times > 0 ? times : -1; // changing it to -1, because 0 means end of task in OnTaskExecuteCallBackCoRoutine
		_DelayBetweenExecutions = delayBetweenExecutions >= 0f ? delayBetweenExecutions : 0f;
		_CurrentUnconsumedEndDelay = endDelay >= 0f ? endDelay : 0f;
		_OnCoroutineExecuteCallBack = onCoroutineExecuteCallBack;
		_OnCoroutineEndCallBack = onCoroutineEndCallBack;
		_DelayedNaturalEndingWasScheduled = false;
		_CurrentUnconsumedStartDelay = 0f;
	}
	
	public Task8 StartOrResumeExecution()
	{
		return StartOrResumeExecution(0f);
	}
	
	// Note: In the case of resuming, <delayBeforeContinuingExecution> will be added to the previous
	// <delayBeforeContinuingExecution> value subtracting the time 
	// between the last StartOrResumeExecution() and PauseExecution()
	/// <summary>
	/// Starts the or resumes execution.
	/// </summary>
	/// <returns>itself OR null if the OnTaskEndedNaturally callback was being scheduled</returns>
	/// <param name="delayBeforeContinuingExecution">Delay before continuing execution.</param>
	public Task8 StartOrResumeExecution(float delayBeforeContinuingExecution)
	{
        if (GlobalRefs8.Instance.unityThread != null && // this is null if the game is not initialized (most probably, Task8 is used outside frame8, which is fine)
            System.Threading.Thread.CurrentThread != GlobalRefs8.Instance.unityThread)
        {
            LiveDebugger8.logL("StartOrResumeExecution(): Can't use Task8 outside Unity's thread");
            throw new UnityException("StartOrResumeExecution(): Can't use Task8 outside Unity's thread");
        }

        if (!_IsPaused)
			throw new UnityException("Task is not paused, so it can't be resumed! Probably task is still running.");
		
		if (delayBeforeContinuingExecution < 0f)
			throw new UnityException("delayBeforeContinuingExecution must be >= 0");
		
		_IsPaused = false;
		
		// The task was paused after being started/resumed. 
		if (_ExecutionInProgress)
		{
			// case 1: the task ended, but OnTaskNaturallyEnded callback was being delayed and meanwhile
			// PauseExecution was called => we increment _CurrentComputedEndDelay by <delayBeforeContinuingExecution> 
			// and resume the delayed invoke of OnTaskNaturallyEnded
			if (_DelayedNaturalEndingWasScheduled)
			{
				_CurrentUnconsumedEndDelay += delayBeforeContinuingExecution;
				
				if (_CurrentUnconsumedEndDelay > 0f)
				{
					_TimeWhenOnTaskEndedNaturallyDelayedCallWasScheduled = Time.time;
					Invoke("OnTaskEndedNaturally", _CurrentUnconsumedEndDelay);
				}
				else
					OnTaskEndedNaturally();
					
				return null;
			}
			
			// case 2: the task didn't execute its first step yet due to StartOrResumeExecutionInternal
			// call being delayed and meanwhile PauseExecution being called.
			// OR
			// case 3: delay for StartOrResumeExecutionInternal has elapsed and the method was called,
			// so at least one step was executed
			_CurrentUnconsumedStartDelay += delayBeforeContinuingExecution;
		}
		// We're starting a new task and not resumming the current one
		else
		{
			_ExecutionInProgress = true;
			_NumberOfExecutionsLeft = _TargetNumberOfExecutions;
			_CurrentUnconsumedStartDelay = delayBeforeContinuingExecution;
		}
		
		if (_CurrentUnconsumedStartDelay > 0f)
		{
			_TimeWhenStartOrResumeExecutionInternalDelayedCallWasScheduled = Time.time;
			Invoke("StartOrResumeExecutionInternal", _CurrentUnconsumedStartDelay);
		}
		else
			StartOrResumeExecutionInternal();

		return this;
	}

	// Called after the startDelay has elapsed
	void StartOrResumeExecutionInternal()
	{
		_CurrentUnconsumedStartDelay = 0f;
		StartCoroutine(OnCoroutineExecuteCoroutine());
	}

	// Termination by _OnCoroutineExecuteCallBack() returning false is still considered natural
	IEnumerator OnCoroutineExecuteCoroutine()
	{	
        while (true)
        {
            if (_IsPaused)
                yield break;

		    // No more executions left
		    if (_NumberOfExecutionsLeft == 0)
		    {
			    // End execution
			    Invoke("OnTaskEndedNaturally", _CurrentUnconsumedEndDelay);
                yield break;
            }
		    else
		    {
			    // Don't decrease the counter if the executions count must be infinite
			    if (_NumberOfExecutionsLeft != -1) // 1 or bigger
				    --_NumberOfExecutionsLeft;

                // Execute a step and check the result (if the coroutine can continue)
                if (_OnCoroutineExecuteCallBack == null || !_OnCoroutineExecuteCallBack())
                {
                    // End execution
                    if (_CurrentUnconsumedEndDelay > 0f)
                    {
                        _TimeWhenOnTaskEndedNaturallyDelayedCallWasScheduled = Time.time;
                        _DelayedNaturalEndingWasScheduled = true;
                        Invoke("OnTaskEndedNaturally", _CurrentUnconsumedEndDelay);
                    }
                    else
                    {
                        OnTaskEndedNaturally();
                    }
                    yield break;
                }
                // Continue
                else
                {
                    // In editor, yield does not work if not in play mode
#if UNITY_EDITOR
                    if (!Application.isPlaying)
                        throw new UnityException("Can't use Task8 in edit mode!");
#endif
                    if (_DelayBetweenExecutions > 0f)
                        yield return new WaitForSeconds(_DelayBetweenExecutions); // wait some time
                    else
                        yield return null; // wait a frame
                }
		    }
        }
	}
	
	public void PauseExecution()
    {
        if (GlobalRefs8.Instance.unityThread != null && // this is null if the game is not initialized (most probably, Task8 is used outside frame8, which is fine)
            System.Threading.Thread.CurrentThread != GlobalRefs8.Instance.unityThread)
        {
            LiveDebugger8.logL("PauseExecution(): Can't use Task8 outside Unity's thread");
            throw new UnityException("PauseExecution(): Can't use Task8 outside Unity's thread");
        }

        _IsPaused = true;
		
		// Execution not yet started. The first step is being delayed
		// Get the time elapsed since StartOrResumeExecution.
		// Decrement _CurrentComputedStartDelay by that amount, so next time
		// StartOrResumeExecution(newDelay) is called, it'll delay the StartOrResumeExecutionInternal 
		// by <_CurrentComputedStartDelay+newDelay>, as should naturally be expected
		if (IsInvoking("StartOrResumeExecutionInternal"))
		{
			CancelInvoke("StartOrResumeExecutionInternal");
			float consumedDelay = 
				Time.time - _TimeWhenStartOrResumeExecutionInternalDelayedCallWasScheduled;
			if (consumedDelay > 0f)
				_CurrentUnconsumedStartDelay -= consumedDelay;
			if (_CurrentUnconsumedStartDelay < 0f)
				_CurrentUnconsumedStartDelay = 0f;
		}
		// Execution ended, but the end callback is being delayed
		else if (IsInvoking("OnTaskEndedNaturally"))
		{
			CancelInvoke("OnTaskEndedNaturally");
			float consumedDelay = Time.time - _TimeWhenOnTaskEndedNaturallyDelayedCallWasScheduled;
			if (consumedDelay > 0f)
				_CurrentUnconsumedEndDelay -= consumedDelay;
			if (_CurrentUnconsumedEndDelay < 0f)
				_CurrentUnconsumedEndDelay = 0f;
		}
		// Execution steps were in progress
		else
			StopAllCoroutines();
	}
	
	void OnTaskEndedNaturally()
	{
		_CurrentUnconsumedEndDelay = 0f;
		EndExecution(true, !DontDestroyOnNaturalEnd);
	}
	
	// Called after the end condition was met && _EndDelay has elapsed OR the user wants to forcible end it
	public void EndExecution(bool dispatchEndEvent, bool dispose)
    {
        if (GlobalRefs8.Instance.unityThread != null && // this is null if the game is not initialized (most probably, Task8 is used outside frame8, which is fine)
            System.Threading.Thread.CurrentThread != GlobalRefs8.Instance.unityThread)
        {
            LiveDebugger8.logL("EndExecution(): Can't use Task8 outside Unity's thread");
            throw new UnityException("EndExecution(): Can't use Task8 outside Unity's thread");
        }

        _IsPaused = true;
		
		// Already destroyed
		if (_ExecutionInProgress)
		{
			_ExecutionInProgress = false;
			if (IsInvoking())
				CancelInvoke();
			StopAllCoroutines();
		}
		
		if (dispose)
			Dispose();
			
		if (dispatchEndEvent && _OnCoroutineEndCallBack != null)
			_OnCoroutineEndCallBack();
	}
	
	public void Dispose()
    {
        if (GlobalRefs8.Instance.unityThread != null && // this is null if the game is not initialized (most probably, Task8 is used outside frame8, which is fine)
            System.Threading.Thread.CurrentThread != GlobalRefs8.Instance.unityThread)
        {
            LiveDebugger8.logL("Dispose(): Can't use Task8 outside Unity's thread");
            throw new UnityException("Dispose(): Can't use Task8 outside Unity's thread");
        }

        // Done with this MonoBehaviour and this GameObject
#if UNITY_EDITOR
        if (!Application.isPlaying)
			DestroyImmediate(this.gameObject);
		else
#endif
			Destroy(this.gameObject);
	}
}

