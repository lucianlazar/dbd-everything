using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Utility component to forward mouse or touch input to clicked gameobjects.
/// Calls OnPress, OnClick and OnRelease methods on "first" game object.
/// </summary>
/// 
/// 
/// TODO use insead of SendMessage
/// ExecuteEvents.Execute<ICustomMessageTarget>(target, null, (x,y)=>x.Message1());
public class InputToEvent : MonoBehaviour
{
	public static InputToEvent Instance { get; private set; }

    public static Vector3 inputHitPos;

    // De-implemented for now, as we don't need it
    public static GameObject goPointedAt { get; private set; }

    public float doubleClickMaxTimeBetweenClicks = .3f;
    public float longPressMinTime = 1f;
    public bool DetectPointedAtGameObject;
    public LayerMask detectionLayers;
	public int maxPenetratedObjects;

    public bool Dragging { get { return _Dragging; } }
	public Vector2 DragVector { get { return _Dragging ? currentPos - pressedPosition : Vector2.zero; } }
	public Vector2 PressedPosition { get { return pressedPosition; } }
	public Vector2 CurrentPosition { get { return currentPos; } }

	Camera m_Camera;
    List<GameObject> pointedGOs_InLastPress, pointedGOs_InLastPress_Canceled, pointedGOs_InLastRelease;
    Vector2 pressedPosition = Vector2.zero;
    Vector2 currentPos = Vector2.zero;
    float _CurrentPressStartTime, _LastClickTime;
    bool _LongPressExecutedForCurrentPress;
    bool _Dragging;
	RaycastHit[] _CurrentRaycastHits;
	int _NumCurrentlyHitObjects;


    void Awake()
    {
		Instance = this;
		m_Camera = GetComponent<Camera>();
		_CurrentRaycastHits = new RaycastHit[maxPenetratedObjects];
		pointedGOs_InLastPress = new List<GameObject>();
		pointedGOs_InLastPress_Canceled = new List<GameObject>();
		pointedGOs_InLastRelease = new List<GameObject>();
	}


    void Update()
    {
        //if (DetectPointedAtGameObject)
        //{
        //    goPointedAt = RaycastObject(Input.mousePosition);
        //}

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            currentPos = touch.position;

            if (touch.phase == TouchPhase.Began)
            {
                Press(touch.position);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                Release(touch.position);
            }
        }
        else
        {
            currentPos = Input.mousePosition;
            if (Input.GetMouseButtonDown(0))
            {
                Press(Input.mousePosition);
            }
            if (Input.GetMouseButtonUp(0))
            {
                Release(Input.mousePosition);
            }

            if (!Input.GetMouseButton(0))
                    return; // Nothing to process: no touches are pressed and neither the mouse left button is

            //if (Input.GetMouseButtonDown(1))
            //{
            //    pressedPosition = Input.mousePosition;
            //    pointedGO_InLastPress = RaycastObject(pressedPosition);
            //    if (pointedGO_InLastPress)
            //    {
            //        pointedGO_InLastPress.SendMessage("OnRightClick", SendMessageOptions.DontRequireReceiver);
            //    }
            //}
        }

        if (_Dragging)
        {
            if (pointedGOs_InLastPress.Count > 0)
            {
                RaycastObjects(currentPos);

				var toRemove = new List<GameObject>();
				bool longPressNotSentToAll = !_LongPressExecutedForCurrentPress;

				GameObject pointedGO_InLastPress;
				for (int i = 0; i < pointedGOs_InLastPress.Count; ++i)
				{
					pointedGO_InLastPress = pointedGOs_InLastPress[i];
					if (IsGOCurrentlyHit(pointedGO_InLastPress))
					{
						if (longPressNotSentToAll)
						{
							// Long-press time reached => send OnLongPress
							if (Time.time - _CurrentPressStartTime > longPressMinTime)
							{
								_LongPressExecutedForCurrentPress = true;

								ExecuteEvents.Execute<IEventHandler>(pointedGO_InLastPress, null, (handler, eventData) => handler.OnLongPress(eventData));
							}
						}
					}
					else
					{
						// Not pointing on that object anymore => send OnPressCanceled
						ExecuteEvents.Execute<IEventHandler>(pointedGO_InLastPress, null, (handler, eventData) => handler.OnPressCanceled(eventData));
						pointedGOs_InLastPress_Canceled.Add(pointedGO_InLastPress);
						pointedGOs_InLastPress.RemoveAt(i--);
					}
				}

			}
        }
    }

	void OnDestroy()
	{
		Instance = null;
	}

	void Press(Vector2 screenPos)
    {
        pressedPosition = screenPos;
        _Dragging = true;
        _LongPressExecutedForCurrentPress = false;
        _CurrentPressStartTime = Time.time;

        RaycastObjects(screenPos);

		pointedGOs_InLastPress.Clear();
		for (int i = 0; i < _NumCurrentlyHitObjects; ++i)
		{
			var go = GetHitGameObjectAt(i);
			//Debug.Log(go.name);
			pointedGOs_InLastPress.Add(go);
			ExecuteEvents.Execute<IEventHandler>(go, null, (handler, eventData) => handler.OnPress(eventData));
		}
    }

    void Release(Vector2 screenPos)
	{
		// Get the currently pointed objects
		RaycastObjects(screenPos);

		bool isDoubleClick = (Time.time - _LastClickTime) < doubleClickMaxTimeBetweenClicks;
		for (int i = 0; i < pointedGOs_InLastPress.Count; ++i)
		{
			var pointedGO_InLastPress = pointedGOs_InLastPress[i];

			if (IsGOCurrentlyHit(pointedGO_InLastPress))
			{
				ExecuteEvents.Execute<IEventHandler>(pointedGO_InLastPress, null, (handler, eventData) => handler.OnRelease(eventData));
				ExecuteEvents.Execute<IEventHandler>(pointedGO_InLastPress, null, (handler, eventData) => handler.OnClick(eventData));

				if (pointedGOs_InLastRelease.Contains(pointedGO_InLastPress) && isDoubleClick)
					ExecuteEvents.Execute<IEventHandler>(pointedGO_InLastPress, null, (handler, eventData) => handler.OnDoubleClick(eventData));

				_LastClickTime = Time.time;
			}
			else
			{
				// Not pointing anymore on the same game object when the pointer is released
				ExecuteEvents.Execute<IEventHandler>(pointedGO_InLastPress, null, (handler, eventData) => handler.OnPressCanceled(eventData));
				pointedGOs_InLastPress_Canceled.Add(pointedGO_InLastPress);
			}
		}

		foreach (var canceledGO in pointedGOs_InLastPress_Canceled)
			ExecuteEvents.Execute<IEventHandler>(canceledGO, null, (handler, eventData) => handler.OnReleaseAfterCanceled(eventData));


		pointedGOs_InLastPress.Clear();
		pointedGOs_InLastRelease.Clear();
		pointedGOs_InLastPress_Canceled.Clear();

		for (int i = 0; i < _NumCurrentlyHitObjects; ++i)
			pointedGOs_InLastRelease.Add(GetHitGameObjectAt(i));

        pressedPosition = Vector2.zero;
        _Dragging = false;
    }

    void RaycastObjects(Vector2 screenPos)
    { _NumCurrentlyHitObjects = Physics.RaycastNonAlloc(m_Camera.ScreenPointToRay(screenPos), _CurrentRaycastHits, 200, detectionLayers.value); }


	bool IsGOCurrentlyHit(GameObject go)
	{
		for (int i = 0; i < _NumCurrentlyHitObjects; ++i)
			if (GetHitGameObjectAt(i) == go)
				return true;

		return false;
	}

	GameObject GetHitGameObjectAt(int i) { return _CurrentRaycastHits[i].collider.gameObject; }


	public interface IEventHandler : IEventSystemHandler
	{
		void OnPress(BaseEventData eventData);
		void OnLongPress(BaseEventData eventData);
		/// <summary>Called on game objects that aren't anymore pointed at, but the pointer is still pressed</summary>
		void OnPressCanceled(BaseEventData eventData);
		void OnRelease(BaseEventData eventData);
		/// <summary>Only called after the pointer was released and it doesn't point to the original game object anymore</summary>
		void OnReleaseAfterCanceled(BaseEventData eventData);
		/// <summary>Called on game objects that are pointed at when the pointer is realeased. Called after <see cref="OnRelease(BaseEventData)"/></summary>
		void OnClick(BaseEventData eventData);
		void OnDoubleClick(BaseEventData eventData);
	}
}