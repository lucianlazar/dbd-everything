﻿using Com.TheFallenGames.DBD.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI
{
    public class PlayerChoiceUI : MonoBehaviour
    {
		public event Action<int> playerChoiceChanged;
		public Toggle prefab;

		//public int SelectedClass { get; private set; }

		[NonSerialized]
		public ToggleGroup toggleGroup;


		void Awake()
        {
			var players = Design.Instance.players.GetAll();
			int numPlayers = players.Length;
			toggleGroup = GetComponentInChildren<ToggleGroup>();

			for (int i = 0; i < numPlayers; i++)
			{
				var toggle = GameObject.Instantiate(prefab.gameObject, toggleGroup.transform, false).GetComponent<Toggle>();
				toggle.gameObject.SetActive(true);
				toggle.GetComponentInChildren<Text>().text = players[i].name;
				toggle.GetComponent<Image>().color = players[i].color;
				LayoutRebuilder.MarkLayoutForRebuild(toggle.transform as RectTransform);
				toggleGroup.RegisterToggle(toggle);

				int copyOfI = i;
				toggle.onValueChanged.AddListener( isOn =>
				{
					if (isOn)
					{
						//SelectedClass = copyOfI;
						if (playerChoiceChanged != null)
							playerChoiceChanged(copyOfI);
					}
				});
			}

			prefab.transform.SetAsLastSibling();

			var horLayout = toggleGroup.GetComponent<HorizontalLayoutGroup>();
			LayoutRebuilder.MarkLayoutForRebuild(horLayout.transform as RectTransform);
		}
    }
}
