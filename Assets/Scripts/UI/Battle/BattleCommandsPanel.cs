﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Battle
{
    public class BattleCommandsPanel : MonoBehaviour
    {
		public event Action BattleClicked, DoNothingClicked, ReadyClicked;//, CloseClicked;
		public event Action<bool> OnChoseIfToEvade;

		public string DoNothingText { set { doNothingButton.GetComponentInChildren<Text>().text = value; } }


		internal ButtonsState State
		{
			set
			{
				//Debug.Log("command panel State changed to: " + value);
				SetAllActive(false);
				switch (value)
				{
					case ButtonsState.ALL_DISABLED:
						// The buttons are naturally already disabled at this point
						break;

					case ButtonsState.WAITING:
						waitingButton.gameObject.SetActive(true);
						break;

					case ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_CANCEL_BATTLE:
						evadeButton.gameObject.SetActive(true);
						doNotEvadeButton.gameObject.SetActive(true);
						break;

					case ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE:
						battleButton.gameObject.SetActive(true);
						doNothingButton.gameObject.SetActive(true);
						break;

					case ButtonsState.IN_BATTLE:
						readyButton.gameObject.SetActive(true);
						break;

					//case ButtonsState.BATTLE_END:
					//	closeButton.gameObject.SetActive(true);
					//	break;
				}
			}
		}

		//internal bool LocalPlayerCanCancel
		//{
		//	set
		//	{
		//		evadeButton.gameObject.SetActive(value);
		//	}
		//}

		Button waitingButton;
		Button battleButton;
		Button doNothingButton;
		Button evadeButton;
		Button doNotEvadeButton;
		Button readyButton;
		//Button closeButton;
		Button[] allButtons;


		void Awake()
		{
			allButtons = transform.GetComponentsInChildren<Button>();
			waitingButton = allButtons[0];
			battleButton = allButtons[1];
			doNothingButton = allButtons[2];
			evadeButton = allButtons[3];
			doNotEvadeButton = allButtons[4];
			readyButton = allButtons[5];
			//closeButton = allButtons[4];

			battleButton.onClick.AddListener(() => { if (BattleClicked != null) BattleClicked(); });
			doNothingButton.onClick.AddListener(() => { if (DoNothingClicked != null) DoNothingClicked(); });
			evadeButton.onClick.AddListener(() => { if (OnChoseIfToEvade != null) OnChoseIfToEvade(true); });
			doNotEvadeButton.onClick.AddListener(() => { if (OnChoseIfToEvade != null) OnChoseIfToEvade(false); });
			readyButton.onClick.AddListener(() => { if (ReadyClicked != null) ReadyClicked(); });
			//closeButton.onClick.AddListener(() => { if (CloseClicked != null) CloseClicked(); });
		}

		void SetAllActive(bool active)
		{
			foreach (var b in allButtons)
				b.gameObject.SetActive(active);
		}

		public enum ButtonsState
		{
			ALL_DISABLED,
			WAITING,
			LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE,
			LOCAL_PLAYER_CHOOSING_IF_TO_CANCEL_BATTLE,
			IN_BATTLE,
			BATTLE_END
		}

    }
}
