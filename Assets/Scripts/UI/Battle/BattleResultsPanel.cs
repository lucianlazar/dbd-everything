﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Battle
{
    public class BattleResultsPanel : MonoBehaviour
    {
		public TexturedText leftResultText, rightResultText;
		public TexturedText leftATKText, rightATKText;
		public TexturedText leftMAGICText, rightMAGICText;
		public float durationPer10Increments;
		public float minDuration;
		public SpriteRenderer serpentSpriteRenderer;

		TexturedText _CurrentCountingText;
		int _CurrentStartValue, _CurrentTargetValue;
		float _StartTime, _CurrentDuration;
		Action _CurrentOnDoneCallback;


		void Update()
		{
			if (_CurrentCountingText)
			{
				float elapsed01 = (Time.time - _StartTime) / _CurrentDuration;
				if (elapsed01 >= 1f)
				{
					_CurrentCountingText.Text = _CurrentTargetValue + "";
					_CurrentCountingText = null;

					if (_CurrentOnDoneCallback != null)
						_CurrentOnDoneCallback();
				}
				else
				{
					int currentValue = Mathf.RoundToInt(Utils8.Instance.Log01(elapsed01, 1) * _CurrentTargetValue);
					//if (currentValue > _CurrentTargetValue)
					//	currentValue = _CurrentTargetValue;

					_CurrentCountingText.Text = currentValue + "";
				}
			}
		}

		public void Clear() { leftResultText.Text = rightResultText.Text = leftATKText.Text = rightATKText.Text = leftMAGICText.Text = rightMAGICText.Text = ""; HideSerpent(); }
		public void CountLeftTo(int value, Action onDone = null) { CountTo(value, leftResultText, onDone); }
		public void CountRightTo(int value, Action onDone = null) { CountTo(value, rightResultText, onDone); }
		public void CountTo(int value, TexturedText into, Action onDone = null)
		{
			if (!int.TryParse(into.Text, out _CurrentStartValue))
				_CurrentStartValue = 0;
			_CurrentTargetValue = value;
			_CurrentCountingText = into;
			_StartTime = Time.time;
			_CurrentDuration = (durationPer10Increments / 10) * (_CurrentTargetValue - _CurrentStartValue);
			if (_CurrentDuration < minDuration)
				_CurrentDuration = minDuration;

#if UNITY_EDITOR
			if (Launcher.s_debug_FastBattle)
				_CurrentDuration = .1f;
#endif
			_CurrentOnDoneCallback = onDone;
		}

		public void SetDetails(int firstATK, int secondATK, int firstMagic, MagicType firstMagicType, int secondMagic, MagicType secondMagicType)
		{
			leftATKText.Text = firstATK + "";
			rightATKText.Text = secondATK + "";
			leftMAGICText.Text = GetPrefixByMagicType(firstMagicType) + firstMagic;
			rightMAGICText.Text = GetPrefixByMagicType(secondMagicType) + secondMagic;
		}

		public void OverrideResults(int leftScore, int rightScore)
		{
			leftResultText.Text = leftScore + "";
			rightResultText.Text = rightScore + "";
		}

		public void ShowSerpent()
		{
			//serpentSpriteRenderer.CrossFadeAlpha(1f, .4f, false);
			serpentSpriteRenderer.gameObject.SetActive(true);
		}

		public void HideSerpent()
		{
			//serpentSpriteRenderer.CrossFadeAlpha(0f, .01f, false);
			serpentSpriteRenderer.gameObject.SetActive(false);
		}

		string GetPrefixByMagicType(MagicType type) { return (type == MagicType.MULTIPLICATIVE ? "x" : ""); }
	}
}
