﻿
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using frame8.Logic.Misc.Other.Extensions;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using Com.TheFallenGames.DBD.Data;

namespace Com.TheFallenGames.DBD.UI.Battle
{
	public class BattleCharacterInfoPanel : MonoBehaviour
	{
		public Text playerNameText;
		public Material dissolveMaterial;
		//public Text atkMathText, atkText;
		//public Text magicMathText, magicText;
		//[UnityEngine.Serialization.FormerlySerializedAs("finalATKText")]
		//public Text finalBattlePointsText;
		//public Text resultText;
		public BattleHitsPanel battleHitsPanel;


		public Character PresentedCharacter
		{
			get { return _Char; }
			set
			{
				_Char = value;
				_CharDesign = _Char.Design;

				// TODO
				// iconImage.sprite

				//SetWon(false);
				ResetState();

				// Set name, if it's a player; otherwise, hide the name panel
				var charAsPlayer = _Char as Player;
				playerNameText.transform.parent.gameObject.SetActive(charAsPlayer);
				bool playerCardsPanelVisible = false;
				if (charAsPlayer)
				{
					playerNameText.text = charAsPlayer.ShortNickName;
					playerCardsPanelVisible = charAsPlayer.Design.canPickUpItems;
				}
				_PlayerCardsPanel.GetComponent<CanvasGroup>().alpha = playerCardsPanelVisible ? 1f : 0f;

				Potential = new CharacterBattlePotential(_Char);
				// Commented: this will be done after any affects have been applied (i.e. when both players played cards and pressed READY)
				//Debug.Log("TODO take effects into consideration when calculating min/max atk");
				//Potential.CalculateMinMaxFinalScore();
				_CardPanel.Model = _Char.GetCardModel();
				ClearUsedItems();


				//atkMathText.text = atk + " x 1"; // will be modified when more characters will be added
				//atkText.text = "= " + GetMinMax_String(minATK, maxATK);
				//magicMathText.text = GetNMultiMinMax_String(magic, _CharDesign.minMagicPower, _CharDesign.maxMagicPower);
				//magicText.text = "= " + GetMinMax_String(minMagic, maxMagic);
				//finalBattlePointsText.text = GetMinMax_String_2Lines(MinFinalATK, MaxFinalATK);
			}
		}

		public CharacterBattlePotential Potential {get; private set;}

		Character _Char;
        CharacterDesign _CharDesign;
		CardPanel _CardPanel;
        Animation8 _SecondAnimation;
		Image[] _UsedItemsFrames;
		Image[] _UsedItemsIcons;
		Animator _CrossingAnimator, _ShakeAnimator;
		int _NumUsedItems;
		Transform _PlayerCardsPanel;


		void Awake()
		{
			_CardPanel = GetComponentInChildren<CardPanel>();
			_SecondAnimation = new Animation8(InterpolationFunction8.LINEAR);
            _SecondAnimation.AnimationTime = 2f;
            _SecondAnimation.AnimationSmoothness = AnimationSmoothnessLevel8.THREE;
            var props = new AnimatedPropertiesGroup8(OnSecondAnimationStep);
            //props.AnimateVectors3 = true;
            //props.InitialVector3 = _CardPanel.transform.localScale;
            //props.TargetVector3 = props.InitialVector3 * .88f;
            _SecondAnimation.AddAnimatedPropertiesGroup(props);

			_PlayerCardsPanel = transform.Find("PlayerCardsPanel");
			var count = _PlayerCardsPanel.childCount;
			_UsedItemsIcons = new Image[count];
			_UsedItemsFrames = new Image[count];
			for (int i = 0; i < count; ++i)
			{
				_UsedItemsFrames[i] = _PlayerCardsPanel.GetChild(i).GetChild(0).GetComponent<Image>();
				_UsedItemsIcons[i] = _UsedItemsFrames[i].transform.GetChild(0).GetComponent<Image>();
			}

			_ShakeAnimator = GetComponent<Animator>();
			transform.GetComponentAtPath("CardHolder/CardPanel/Holder/DefeatedAnimatedImage", out _CrossingAnimator);
			_CrossingAnimator.gameObject.SetActive(false);

			// Work on a copy of it
			dissolveMaterial = Instantiate(dissolveMaterial);

			// Setting it on any child that accepts a material
			var all = transform.GetDescendants();
			all.Add(transform);
			foreach (var tr in all)
			{
				var c = tr.GetComponent<Graphic>();
				if (c)
					c.material = dissolveMaterial;
			}
		}
		



        bool OnSecondAnimationStep()
        {
			//_CardPanel.transform.localScale = _SecondAnimation.GetAnimatedPropertiesGroup(0).CurrentVector3;
			dissolveMaterial.SetFloat("_Progress", 1f - _SecondAnimation.GetAnimatedPropertiesGroup(0).CurrentAnimationProgress);

			return true;
		}

        string GetNMultiMinMax_String(int n, int min, int max)
        {
            return n + " x " + GetMinMax_String(min, max);
        }

        string GetMinMax_String(int min, int max)
        {
            if (min == max)
                return min + "";

            return "[min " + min + ", max " + max + "]";
        }

        string GetMinMax_String_2Lines(int min, int max)
        {
            if (min == max)
                return min + "";

            return "min " + min + "\nmax " + max;
        }

        //public void SetWon(bool won)
        //{
        //    resultText.enabled = won;
        //}

		public void ClearUsedItems()
		{
			foreach (var v in _UsedItemsIcons)
			{
				v.sprite = null;
				v.color = Color.clear;
				v.transform.parent.gameObject.SetActive(false);
			}

			_NumUsedItems = 0;
		}
		
		public void OnItemUsed(BaseItemDesign design)
		{
			_UsedItemsFrames[_NumUsedItems].gameObject.SetActive(true);
			_UsedItemsFrames[_NumUsedItems].sprite = design.GetCardModel().cardCategoryDesign.cardFrame;
			_UsedItemsIcons[_NumUsedItems].color = Color.white;
			_UsedItemsIcons[_NumUsedItems++].sprite = design.MiniIcon;
		}

        public void ResetState()
        {
            if (_SecondAnimation.IsPlaying)
                _SecondAnimation.Stop(false, false);

			_CrossingAnimator.gameObject.SetActive(false);
			SetTieAnimation(false);

			//_CardPanel.transform.localScale = _SecondAnimation.GetAnimatedPropertiesGroup(0).InitialVector3;
			dissolveMaterial.SetFloat("_Progress", 1f);
        }

		public void SetTieAnimation(bool tieAnimation)
		{
			if (tieAnimation)
			{
				//_ShakeAnimator.enabled = true;
				_ShakeAnimator.SetTrigger("TriggerShake");
			}
			else
			{
				_ShakeAnimator.ResetTrigger("TriggerShake");
				//_ShakeAnimator.enabled = false;
			}
		}

        public void OnDefeated(bool animate)
        {
            if (_SecondAnimation.IsPlaying)
                _SecondAnimation.Stop(false, false);

			_CrossingAnimator.gameObject.SetActive(true);
			if (animate)
			{
				_CrossingAnimator.Play("DefeatedCard", 0, 0f);
				StartCoroutine(
                    ExecuteAfterXFrames(
                        1,
                        () =>
                        {
							
								//if (won)
							_SecondAnimation.Start(_CrossingAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.length, null);
								//else
								//    _CardPanel.transform.localScale = _ScaleAnimation.GetAnimatedPropertiesGroup(0).InitialVector3;
						}
					)
                );
            }
            else
			{
				//_CardPanel.transform.localScale = _SecondAnimation.GetAnimatedPropertiesGroup(0).TargetVector3;
            }
        }
        
        IEnumerator ExecuteAfterXFrames(int x, System.Action action)
        {
            while (x-- > 0)
                yield return null;

            if (action != null)
                action();

            yield break;
        }
    }
}
