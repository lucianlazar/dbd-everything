﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Battle
{
    public class BattlePanel : SlidingPanel
	{
		public BattleCommandsPanel commandsPanel;
		public BattleResultsPanel resultsPanel;
		public Text logText;

		public BattleStats currentBattleStats;

		public Character FirstCharacter { get; private set; }
		public Character SecondCharacter { get; private set; }

		//public int FirstCharacterCurrentBattlePoints { get; private set; }
		//public int SecondCharacterCurrentBattlePoints { get; private set; }

		//public int FirstCharacterCurrentMagic { get { return _FirstCharacterCurrentMagic; } }
		//public int SecondCharacterCurrenMagic { get { return _SecondCharacterCurrenMagic; } }

		//public int FirstCharacterCurrentAtk { get { return _FirstCharacterCurrentAtk; } }
		//public int SecondCharacterCurrenAtk { get { return _SecondCharacterCurrenAtk; } }

		public Character CurrentWinningChar { get; private set; }
		public Character CurrentLosingChar { get; private set; }

		//public bool FinalBattlePointsArePreDetermined { get; private set; }
		//public int NumberOfTiesBeforeFinalOutcome { get; private set; }

		BattleCharacterInfoPanel firstCharInfoPanel, secondCharInfoPanel;
		//int _FirstCharacterCurrentAtk;
		//int _SecondCharacterCurrenAtk;
		//int _FirstCharacterCurrentMagic;
		//int _SecondCharacterCurrenMagic;


		protected override void Awake()
		{
			base.Awake();

			var charInfoPanels = GetComponentsInChildren<BattleCharacterInfoPanel>();
			firstCharInfoPanel = charInfoPanels[0];
			secondCharInfoPanel = charInfoPanels[1];
		}

		protected override void Start()
		{
			base.Start();
			//commandsPanel.CloseClicked += OnCloseClicked;
		}


		public void Show (Character firstCharacter, Character secondCharacter)
		{
			SetCommandingMode(true);
			firstCharacter.Stats.InBattle = secondCharacter.Stats.InBattle = true;

			commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;

			firstCharInfoPanel.PresentedCharacter = FirstCharacter = firstCharacter;
            secondCharInfoPanel.PresentedCharacter = SecondCharacter = secondCharacter;

			resultsPanel.Clear();

			base.Show(false);
        }

		public void OnCardPlayed(Player player, BaseItemDesign item)
		{
			var panel = player == FirstCharacter ? firstCharInfoPanel : secondCharInfoPanel;
			panel.OnItemUsed(item);
		}

		//public void ShowSerpent()
		//{

		//}


		public override void Close(float delay, Action onDone, bool fireClosedEvent = true)
		{
#if UNITY_EDITOR
			if (Launcher.s_debug_FastBattle)
				delay = .2f;
#endif
			commandsPanel.State = BattleCommandsPanel.ButtonsState.ALL_DISABLED;

			base.Close(delay, onDone, fireClosedEvent);
        }

		protected override void Show(bool keepCurrentInitialPosition)
		{
			logText.text = "";

			base.Show(keepCurrentInitialPosition);
		}

		protected override void OnPreAnimStart()
		{
#if UNITY_EDITOR
			if (Launcher.s_debug_FastBattle)
				_ShowHideAnimation.AnimationTime = .1f;
#endif
		}

		public void CalculateBattleOutcome()
		{
			currentBattleStats = new BattleStats();
			currentBattleStats.CalculateBattleOutcome(firstCharInfoPanel, secondCharInfoPanel);
		}

		public void BeginCounting(int first, int second, Action onDone)
		{
			SetCommandingMode(false);
			resultsPanel.Clear();
			firstCharInfoPanel.PresentedCharacter.SetUpBattleHits(secondCharInfoPanel.battleHitsPanel);
			secondCharInfoPanel.PresentedCharacter.SetUpBattleHits(firstCharInfoPanel.battleHitsPanel);
			resultsPanel.CountLeftTo(
				first, 
				() =>
				{
					// Play the hit effect on the affected character (the one to the right), then count the right's atk
					secondCharInfoPanel.battleHitsPanel.PlayHit(
						1.5f,
						() =>
						{
							resultsPanel.CountRightTo(
								second,
								// Play the hit effect on the affected character (the one to the left), then count call onDone
								() => firstCharInfoPanel.battleHitsPanel.PlayHit(1.5f, onDone)
							);
						}
					);
				}
			);
		}

		public void AnimateTie(Action onDone, bool bothPartiesAreEquallyStrong)
		{
			logText.text = "Tie: " + (bothPartiesAreEquallyStrong ? "both parties are equally strong!" : "preparing for the final hit...");
			firstCharInfoPanel.SetTieAnimation(true);
			secondCharInfoPanel.SetTieAnimation(true);

			Debug.Log("TODO: play other animations etc.");
			DoAfterSeconds(2f, () => 
			{
				logText.text = "";
				firstCharInfoPanel.SetTieAnimation(false);
				secondCharInfoPanel.SetTieAnimation(false);
				if (onDone != null)
					onDone();
			});
		}

		public void ActivateBattleResultsPanel()
		{
			SetCommandingMode(false);
		}

		public void SetBattleWinner(Character winningChar)
		{
			FirstCharacter.Stats.InBattle = SecondCharacter.Stats.InBattle = false;
			if (winningChar)
				SetBattleResult(winningChar == FirstCharacter ? firstCharInfoPanel : secondCharInfoPanel);
			else
				SetBattleResult(null);
		}

		public void SetBattleResult(BattleCharacterInfoPanel winningCharPanel)
		{
            if (winningCharPanel)
            {
				//winningCharPanel.SetWon(true);
				CurrentWinningChar = winningCharPanel.PresentedCharacter;
				var losingPanel = winningCharPanel == firstCharInfoPanel ? secondCharInfoPanel : firstCharInfoPanel;
				CurrentLosingChar = losingPanel.PresentedCharacter;
				losingPanel.OnDefeated(true);
            }
            else
				CurrentWinningChar = CurrentLosingChar = null;

			//Hide(autoHideDelayAfterBattle, null);
		}

		void SetCommandingMode(bool active)
		{
			commandsPanel.gameObject.SetActive(active);
			resultsPanel.gameObject.SetActive(!active);
		}

		void DoAfterSeconds(float seconds, Action action) { StartCoroutine(DoAfterSecondsCoroutine(seconds, action)); }

		IEnumerator DoAfterSecondsCoroutine(float seconds, Action action)
		{
			yield return new WaitForSeconds(seconds);
			if (action != null)
				action();
		}
	}
}
