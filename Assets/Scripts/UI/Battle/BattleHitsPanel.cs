﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Battle
{
    public class BattleHitsPanel : MonoBehaviour
    {
		AnimatedSprite _HitAnimatedSprite;
		GameObject _MiscHitGameObject;

		Action _HitEndedAction;
		float _TimeWhenStarted, _Duration = -1f;


		void Start()
		{
			_HitAnimatedSprite = transform.GetChild(0).GetComponent<AnimatedSprite>();
			_HitAnimatedSprite.gameObject.SetActive(false);
		}

		void Update()
		{
			if (_Duration <= 0f)
				return;

			float elapsed = Time.time - _TimeWhenStarted;
			if (elapsed >= _Duration)
			{
				_Duration = -1f;
				if (_MiscHitGameObject)
				{
					_MiscHitGameObject.SetActive(false);
					_MiscHitGameObject = null;
				}
				else
				{
					_HitAnimatedSprite.sprites = null;
					_HitAnimatedSprite.gameObject.SetActive(false);
				}
				if (_HitEndedAction != null)
					_HitEndedAction();

				enabled = false; // do not receive unnecessary Update calls
			}
		}

		public void SetUpHitEffectWithMiscGameObject(GameObject miscHitGameObject)
		{
			_MiscHitGameObject = miscHitGameObject;
		}

		public void SetUpHitEffectWithSpriteFrames(Sprite[] frames)
		{
			_HitAnimatedSprite.Initialize(frames);
		}

		public void SetUpNoHitEffect()
		{

		}

		public void PlayHit(float duration, Action onDone)
		{
			if (_MiscHitGameObject)
			{
				_MiscHitGameObject.transform.position = transform.position - transform.forward * .5f; // bringing it closer to the camera
				_MiscHitGameObject.SetActive(true);
			}
			else if (_HitAnimatedSprite.sprites != null && _HitAnimatedSprite.sprites.Length > 0)
			{
				_HitAnimatedSprite.gameObject.SetActive(true);
			}

			_TimeWhenStarted = Time.time;
			_Duration = duration;
			_HitEndedAction = onDone;

			enabled = true;
		}
    }
}
