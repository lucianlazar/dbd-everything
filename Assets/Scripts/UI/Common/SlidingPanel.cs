﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Common
{
    public class SlidingPanel : MonoBehaviour
    {
		public event Action Showing, Closed;

		public float showHideDuration;
		public float autoHideDelay;
		public HiddenPosition hiddenPosition;

		public bool IsShownOrShowing { get { return IsShowing || IsShown; } }
		public bool IsShowing { get; private set; }
		public bool IsShown { get; private set; }
		public bool IsClosing { get; private set; }
		public float HiddenVerticalNormalizedPosition { get { return hiddenPosition == HiddenPosition.TOP ? 0f : 1f; } }
		public virtual bool Interactable { get { return _Interactable; } set { _Interactable = _ScrollRect.enabled = value; } }

		protected ScrollRect _ScrollRect;
        protected Animation8 _ShowHideAnimation;


		[FormerlySerializedAs("interactable")]
		bool _Interactable;


		protected virtual void Awake()
		{
			_ScrollRect = GetComponent<ScrollRect>();
			_ScrollRect.verticalNormalizedPosition = HiddenVerticalNormalizedPosition;
			_ScrollRect.enabled = _Interactable;

			_ShowHideAnimation = new Animation8(InterpolationFunction8.SIN_SLOWOUT);
			_ShowHideAnimation.AnimationTime = showHideDuration;
			_ShowHideAnimation.AnimationSmoothness = AnimationSmoothnessLevel8.THREE;
			var props = new AnimatedPropertiesGroup8(OnShowHideAnimationStep);
			props.AnimateFloats = true;
			_ShowHideAnimation.AddAnimatedPropertiesGroup(props);
		}

		protected virtual void Start() { }


		protected virtual void Show(bool keepCurrentInitialPosition)
		{
			float initialFirstFloat = keepCurrentInitialPosition ? _ScrollRect.verticalNormalizedPosition : HiddenVerticalNormalizedPosition;
			if (IsClosing)
			{
				initialFirstFloat = _ScrollRect.verticalNormalizedPosition; // continue back, but from the current position
				_ShowHideAnimation.Stop(false, false);
				IsClosing = false;
			}

			if (Showing != null)
				Showing();
			IsShowing = true;

			_ShowHideAnimation.GetAnimatedPropertiesGroup(0).InitialFirstFloat = initialFirstFloat;
			_ShowHideAnimation.GetAnimatedPropertiesGroup(0).TargetFirstFloat = 1f - HiddenVerticalNormalizedPosition;
			OnPreAnimStart();
			_ShowHideAnimation.Start(() => { IsShown = true; IsShowing = false; });
		}

		public void Close(Action onDone) { Close(autoHideDelay, onDone); }

		public virtual void Close(float delay, Action onDone, bool fireClosedEvent = true)
		{
			if (IsShowing && !IsShown) // opening anim not ended
			{
				_ShowHideAnimation.Stop(false, false);
				IsShowing = false;
			}
			else
			{
				// Already clicked
				if (_ShowHideAnimation.IsPlaying)
					return;
			}
			
			IsShown = false;
			IsClosing = true;

			_ShowHideAnimation.GetAnimatedPropertiesGroup(0).InitialFirstFloat = _ScrollRect.verticalNormalizedPosition;
            _ShowHideAnimation.GetAnimatedPropertiesGroup(0).TargetFirstFloat = HiddenVerticalNormalizedPosition;
			OnPreAnimStart();
			_ShowHideAnimation.Start(delay, () => { IsClosing = false; if (onDone != null) onDone(); if (fireClosedEvent && Closed != null) Closed(); });
        }

		protected virtual void OnPreAnimStart() { }

		bool OnShowHideAnimationStep()
        { _ScrollRect.verticalNormalizedPosition = _ShowHideAnimation.GetAnimatedPropertiesGroup(0).CurrentFirstFloat; return true; }

		protected virtual void OnCloseClicked()
		{
			Close(autoHideDelay, null);
		}

		public enum HiddenPosition
		{
			TOP,
			BOTTOM
		}

    }
}
