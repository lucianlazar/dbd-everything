﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Core;
using System;
using System.Collections.Generic;

using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Common
{
    public class PlayerNavigationPanel : MonoBehaviour
    {
		public event Action<int> NavigateClicked;

		public bool Interactable
		{
			set
			{
				leftButton.interactable = rightButton.interactable = value;
			}
		}

        Image bgImage;
        Text inspectedPlayerText;
		Button leftButton, rightButton;

		void Awake()
		{
			bgImage = GetComponent<Image>();
			inspectedPlayerText = transform.Find("Text").GetComponent<Text>();

			var buttons = GetComponentsInChildren<Button>();
			leftButton = buttons[0];
			rightButton = buttons[1];

			leftButton.onClick.AddListener(() => { if (NavigateClicked != null) NavigateClicked(-1); });
			rightButton.onClick.AddListener(() => { if (NavigateClicked != null) NavigateClicked(1); });
		}

		public void UpdateViews(Player player)
		{
			// Background color
			float alpha = bgImage.color.a;
			Color c = player.Design.color;
			c.a = alpha;
			bgImage.CrossFadeColor(c, .4f, false, false);

			//// Player's class
			//inspectedPlayerText.text = player.Design.name;
			inspectedPlayerText.text = player.ShortNickName;
		}

	}
}
