﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.UI.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI
{
    public class StatsPanel : MonoBehaviour
    {
		public Text statsText;

		public PlayerNavigationPanel NavigationPanel { get; private set; }
		public Player InspectedPlayer
		{
			get { return _InspectedPlayer; }
			set
			{
				if (_InspectedPlayer != value)
				{
					_InspectedPlayer = value;
					UpdateFromInspectedPlayer();
				}
			}
		}

		Player _InspectedPlayer;


		void Awake()
		{
			NavigationPanel = GetComponentInChildren<PlayerNavigationPanel>();
			NavigationPanel.NavigateClicked += NavigationPanel_NavigateClicked;
		}


		public void UpdateFromInspectedPlayer()
		{
			NavigationPanel.UpdateViews(_InspectedPlayer);

			string text = _InspectedPlayer.Stats.ToString();
			int substrIdx = text.IndexOf(' ') + 1;
			if (substrIdx > text.Length)
				return;
			statsText.text = text.Substring(substrIdx);
		}

		void NavigationPanel_NavigateClicked(int direction)
		{
			if (!_InspectedPlayer)
				return; // wait for InspectedPlayer to be set externally

			InspectedPlayer = SpawnManager.Instance.GetPlayerInstanceAtRelativePosition(_InspectedPlayer, direction);
		}
	}
}
