﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Inventory
{
	public class CardPanel : MonoBehaviour
	{
		[SerializeField]
		public Image _BorderImage;

		[SerializeField]
		public Image _FrameImage;

		[SerializeField]
		public Image _Image;

		[SerializeField]
		public Image _DescriptionBackgroundImage;

		[SerializeField]
		Text _TitleText;

		[SerializeField]
		Text _DescriptionText;

		[SerializeField]
		Toggle _Toggle;

		[SerializeField]
		CardInteractionPanel _CardInteraction;

		public CardInteractionPanel CardInteraction { get { return _CardInteraction; } }

		public bool Interactable
		{
			set
			{
				_Toggle.isOn = false; // don't preserve the 'toggled' state
				_Toggle.gameObject.SetActive(value);
			}
		}

		public bool IsSelected { get { return _Toggle.isOn; } set { _Toggle.isOn = value; } }

		public CardModel Model
		{
			get { return _Model; }
			set
			{
				//if (value != _Model)
				//{
					_Model = value;

					if (_Model != null)
					{
						SetSpriteAndWhiteOrClear(_BorderImage, _Model.cardCategoryDesign.cardBorder);
						SetSpriteAndWhiteOrClear(_FrameImage, _Model.cardCategoryDesign.cardFrame);
						var descriptionBackground = _Model.cardCategoryDesign.cardDescriptionBackgroundOverride;
						if (!descriptionBackground)
							descriptionBackground = Design.Instance.general.generalCardsDesign.cardDescriptionDefaultBackground;
						SetSpriteAndWhiteOrClear(_DescriptionBackgroundImage, descriptionBackground);
						SetSpriteAndWhiteOrClear(_Image, _Model.design.icon);

						_Image.transform.localScale = Vector3.one * _Model.cardCategoryDesign.mainImageScaleFactor;

						_TitleText.text = _Model.title;
						_DescriptionText.text = _Model.description;

						_TitleText.enabled = _DescriptionText.enabled = _Model.cardCategoryDesign.showDescriptionText;

						var asBaseItemDesign = _Model.design as BaseItemDesign;
						_CardInteraction.IsPlayable = asBaseItemDesign != null && asBaseItemDesign.cardType != CardType.NOT_PLAYABLE;
					}
					gameObject.SetActive(_Model != null);

					// De-select if previously selected
					if (IsSelected)
						IsSelected = false;
				}
			//}
		}

		CardModel _Model;


		void SetSpriteAndWhiteOrClear(Image image, Sprite sprite)
		{
			image.sprite = sprite;

			if (sprite)
				image.color = Color.white;
			else
				image.color = Color.clear;
		}
	}
}
