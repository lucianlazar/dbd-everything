﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Common;
using frame8.Logic.Misc.Visual.UI.MonoBehaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Inventory
{
    public class InventoryPanel : SlidingPanel
	{
		public CardPanel cardPanelPrefab;

		public event Action<CardModel> PlayClick, DiscardClick, ChooseClick, ExamineClick, RequestTradeClick;

		public override bool Interactable
		{
			get
			{
				return base.Interactable;
			}

			set
			{
				base.Interactable = NavigationPanel.Interactable = value;
			}
		}

		public PlayerNavigationPanel NavigationPanel { get; private set; }
		public Player InspectedPlayer
		{
			get { return _InspectedPlayer; }
			set
			{
				if (_InspectedPlayer != value)
				{
					if (_InspectedPlayer)
					{
						_InspectedPlayer.Stats.ItemsRemoved -= OnItemsRemoved;
						_InspectedPlayer.Stats.ItemsAdded -= OnItemsAdded;
					}

					_InspectedPlayer = value;

					if (_InspectedPlayer)
					{
						_InspectedPlayer.Stats.ItemsRemoved += OnItemsRemoved;
						_InspectedPlayer.Stats.ItemsAdded += OnItemsAdded;
					}

					UpdateViews();
				}
			}
		}

		public bool CardsChoosable
		{
			set
			{
				//string s = "";
				foreach (var c in _Cards)
				{
					//s += (c.Model == null ? "(null)" : c.Model.design.name) + ": cm=" + value + ", ";
					c.CardInteraction.IsChooseable = value;
				}
				//Debug.Log(s);
			}
		}

		public bool CardsTradeable
		{
			set
			{
				foreach (var c in _Cards)
					c.CardInteraction.IsTradeable = value;
			}
		}

		public bool CardsInteractable
		{
			set
			{
				//string s = "";
				foreach (var c in _Cards)
				{
					//s += (c.Model == null ? "(null)" : c.Model.design.name) + ": inter=" + value + ", ";
					c.Interactable = value;
				}
				//Debug.Log(s);
			}
		}

		public bool CanDiscardCards
		{
			set
			{
				foreach (var c in _Cards)
					c.CardInteraction.CanDiscard = value;
			}
		}

		public bool CardsDiscardable
		{
			set
			{
				foreach (var c in _Cards)
					c.CardInteraction.IsDiscardable = value;
			}
		}

		public bool CardsExaminableAndCanExamine
		{
			set
			{
				foreach (var c in _Cards)
					c.CardInteraction.CanExamine = c.CardInteraction.IsExaminable = value;
			}
		}

		public string PlayButtonText
		{
			set
			{
				foreach (var c in _Cards)
					c.CardInteraction.PlayButtonText = value;
			}
		}

		//public object TreasuresDiscardable { get; internal set; }

		CardPanel[] _Cards;
		int _InventoryCapacity;
		Player _InspectedPlayer;
		HorizontalLayoutGroup _Content;


		protected override void Awake()
        {
			base.Awake();

            _InventoryCapacity = Design.Instance.general.inventoryCapacity;

			_Content = transform.GetChild(0).GetChild(0).GetComponent<HorizontalLayoutGroup>();

			_Cards = new CardPanel[_InventoryCapacity];
			var contentRadioToggleGroup = _Content.GetComponent<RadioToggleGroup>();
            for (int i = 0; i < _InventoryCapacity; ++i)
			{
                _Cards[i] = Instantiate(cardPanelPrefab.gameObject, _Content.transform, false).GetComponent<CardPanel>();
				contentRadioToggleGroup.RegisterRadioToggle(_Cards[i].transform.GetChild(0).GetChild(3).GetComponent<Toggle>());
			}

			NavigationPanel = GetComponentInChildren<PlayerNavigationPanel>();
        }

		protected override void Start()
		{
			base.Start();

			for (int i = 0; i < _InventoryCapacity; ++i)
			{
				var cardCopy = _Cards[i];
				cardCopy.CardInteraction.DiscardClick += () => { if (DiscardClick != null) DiscardClick(cardCopy.Model); };
				cardCopy.CardInteraction.PlayClick += () => { if (PlayClick != null) PlayClick(cardCopy.Model); };
				cardCopy.CardInteraction.ChooseClick += () => { if (ChooseClick != null) ChooseClick(cardCopy.Model); };
				cardCopy.CardInteraction.ExamineClick += () => { if (ExamineClick != null) ExamineClick(cardCopy.Model); };
				cardCopy.CardInteraction.RequestTradeClick += () => { if (RequestTradeClick != null) RequestTradeClick(cardCopy.Model); };
			}
		}

		public void SetCardsCanPlay(bool canPlay, params CardType[] cardTypes)
		{
			foreach (var c in _Cards)
			{
				var model = c.Model;
				if (model == null)
				{
					c.CardInteraction.CanPlay = false;
					continue;
				}
				c.CardInteraction.CanPlay = canPlay && cardTypes != null && Array.IndexOf(cardTypes, (model.design as BaseItemDesign).cardType) != -1;
			}
		}

		public void MakeAllNotPlayable()
		{
			foreach (var c in _Cards)
				c.CardInteraction.IsPlayable = false;
		}

		public void MakeAllPlayableIfCanPlayThem()
		{
			foreach (var c in _Cards)
				c.CardInteraction.IsPlayable = c.CardInteraction.CanPlay;
		}

		public new virtual void Show(bool keepCurrentInitialPosition) { base.Show(keepCurrentInitialPosition); }

		public void SelectFirst()
		{
			if (_InspectedPlayer && _InspectedPlayer.Stats.ItemsInInventory.Length > 0)
				_Cards[0].IsSelected = true;
		}

		internal void SelectNone()
		{
			foreach (var c in _Cards)
				c.IsSelected = false;
		}

		void OnItemsAdded(params BaseItem[] items) { Add(items); }
		void OnItemsRemoved(params BaseItem[] items) { Remove(items); }

		void Add(params BaseItem[] items)
        {
            foreach (var item in items)
            {
                bool added = false;

                foreach (var card in _Cards)
                    if (card.Model == null)
                    {
                        card.Model = item.GetCardModel();
                        added = true;
                        break;
                    }

                if (!added)
                    Debug.LogError("InventoryPanel.Add: " + item.name + ": failed: inventory full");
			}
			RebuildLayoutAndUpdateCanvases();
		}

        void Remove(params BaseItem[] items)
        {
            foreach (var item in items)
            {
                bool found = false;
                foreach (var card in _Cards)
                    if (card.Model != null && card.Model.design == item.Design)
                    {
                        card.Model = null;
                        found = true;
                        break;
                    }

                if (!found)
                    throw new UnityException("InventoryPanel.Remove: " + item.name + ": failed: didn't find it");
			}
			RebuildLayoutAndUpdateCanvases();
		}

		void UpdateViews()
		{
			foreach (var card in _Cards)
				card.Model = null;

			for (int i = 0; i < _InspectedPlayer.Stats.ItemsInInventory.Length; ++i)
				_Cards[i].Model = _InspectedPlayer.Stats.ItemsInInventory[i].GetCardModel();

			NavigationPanel.UpdateViews(_InspectedPlayer);
			RebuildLayoutAndUpdateCanvases();
		}

		internal void RebuildLayoutAndUpdateCanvases()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(_Content.transform as RectTransform);

			// Pseudo-bugfix: after being enabled, some UI elements have their visual representation 
			// positioned at the same place they were before being disabled, even if their bounds have been properly adjusted.
			// changing the scrollrect's scroll pos refreshes the layout. Other options, like MarkForRebuild, ForceRebuild, ForceUpdateCanvases don't work
			var orig = _ScrollRect.normalizedPosition;
			_ScrollRect.normalizedPosition = Vector2.one - _ScrollRect.normalizedPosition;
			_ScrollRect.normalizedPosition = orig;
		}

		//void RebuildRec(RectTransform rt)
		//{
		//	foreach (RectTransform rc in rt)
		//		RebuildRec(rc);

		//	LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
		//}

		//public void RemoveAnyWithDesignType<TDesign>(int count = 1) where TDesign : EntityDesign
		//{
		//    bool found;
		//    while (--count >= 0)
		//    {
		//        found = false;
		//        foreach (var card in _Cards)
		//            if (card.Model != null && card.Model.itemDesign is TDesign)
		//            {
		//                card.Model = null;
		//                found = true;
		//                break;
		//            }

		//        if (!found)
		//            Debug.LogError("InventoryPanel.RemoveAnyOfType: " + typeof(TDesign).Name + "; count-i="+ count + ": failed: didn't find it");
		//    }
		//}
	}
}
