﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using frame8.Logic.Misc.Other.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Inventory
{
	public class CardInteractionPanel : MonoBehaviour
	{
		public event Action PlayClick, DiscardClick, ChooseClick, ExamineClick, RequestTradeClick;

		public bool IsChooseable { set { _ChooseButton.gameObject.SetActive(value); } }
		public bool IsTradeable { set { _RequestTradeButton.gameObject.SetActive(value); } }
		public bool CanPlay { get { return _PlayButton.interactable; } set { _PlayButton.interactable = value; } }
		public bool IsPlayable { set { _PlayButton.gameObject.SetActive(value); } }
		public bool CanDiscard { set { _DiscardButton.interactable = value; } }
		public bool IsDiscardable { set { _DiscardButton.gameObject.SetActive(value); }}
		public bool CanExamine { set { _ExamineButton.interactable = value; } }
		public bool IsExaminable { set { _ExamineButton.gameObject.SetActive(value); } }
		public string PlayButtonText
		{
			set
			{
				if (!_PlayButtonText)
					_PlayButton.transform.GetComponentAtPath("Text", out _PlayButtonText);

				_PlayButtonText.text = value;
			}
		}

		[SerializeField]
		Button _PlayButton;
		[SerializeField]
		Button _DiscardButton;
		[SerializeField]
		Button _ChooseButton;
		[SerializeField]
		Button _ExamineButton;
		[SerializeField]
		Button _RequestTradeButton;

		Text _PlayButtonText;


		void Awake()
		{
			//_PlayButton = transform.GetChild(0).GetComponent<Button>();
			//_DiscardButton = transform.GetChild(1).GetComponent<Button>();
			//ChooseMode = false;

			_PlayButton.onClick.AddListener(() => { if (PlayClick != null) PlayClick(); });
			_DiscardButton.onClick.AddListener(() => { if (DiscardClick != null) DiscardClick(); });
			_ChooseButton.onClick.AddListener(() => { if (ChooseClick != null) ChooseClick(); });
			_ExamineButton.onClick.AddListener(() => { if (ExamineClick != null) ExamineClick(); });
			_RequestTradeButton.onClick.AddListener(() => { if (RequestTradeClick != null) RequestTradeClick(); });
		}
	}
}
