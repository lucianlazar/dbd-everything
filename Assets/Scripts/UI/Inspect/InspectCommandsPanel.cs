﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Inspect
{
    public class InspectCommandsPanel : MonoBehaviour
    {
		public event Action NextEntityClicked, RangedBattleClicked, MiscClicked, ChooseClicked, LootClicked, CloseClicked;

		internal string MiscButtonText { set { miscButton.transform.Find("Text").GetComponent<Text>().text = value; } }
		internal string NextButtonText { set { nextEntityButton.gameObject.SetActive(value != null); nextEntityText.text = value; } }
		internal bool LootingActive { get; set; }

		internal ButtonsState State
		{
			set
			{
				//Debug.Log("command panel State changed to: " + value);
				SetAllActive(false);
				closeButton.gameObject.SetActive(true);
				nextEntityButton.gameObject.SetActive(!string.IsNullOrEmpty(nextEntityText.text));

				switch (value)
				{
					//case ButtonsState.ALL_DISABLED:
					//	closeButton.gameObject.SetActive(false);
					//	break;

					case ButtonsState.RANGED_BATTLE:
						rangedBattleButton.gameObject.SetActive(true);
						LootingActive = false;
						break;

					case ButtonsState.CHOOSER:
						chooseButton.gameObject.SetActive(true);
						LootingActive = false;
						break;

					case ButtonsState.MISC:
						miscButton.gameObject.SetActive(true);
						break;
				}
				lootButton.gameObject.SetActive(LootingActive);
			}
		}

		Button nextEntityButton;
		Button rangedBattleButton;
		Button chooseButton;
		Button miscButton;
		Button lootButton;
		Button closeButton;
		Button[] allButtons;
		Text nextEntityText;


		void Awake()
		{
			allButtons = transform.GetComponentsInChildren<Button>();
			nextEntityButton = allButtons[0];
			rangedBattleButton = allButtons[1];
			chooseButton = allButtons[2];
			miscButton = allButtons[3];
			lootButton = allButtons[4];
			closeButton = allButtons[5];

			nextEntityText = nextEntityButton.transform.Find("Text").GetComponent<Text>();

			nextEntityButton.onClick.AddListener(() => { if (NextEntityClicked != null) NextEntityClicked(); });
			rangedBattleButton.onClick.AddListener(() => { if (RangedBattleClicked != null) RangedBattleClicked(); });
			chooseButton.onClick.AddListener(() => { if (ChooseClicked != null) ChooseClicked(); });
			miscButton.onClick.AddListener(() => { if (MiscClicked != null) MiscClicked(); });
			lootButton.onClick.AddListener(() => { if (LootClicked != null) LootClicked(); });
			closeButton.onClick.AddListener(() => { if (CloseClicked != null) CloseClicked(); });
		}

		void SetAllActive(bool active)
		{
			foreach (var b in allButtons)
				b.gameObject.SetActive(active);
		}

		public enum ButtonsState
		{
			//ALL_DISABLED,
			RANGED_BATTLE,
			CHOOSER,
			MISC,
			CLOSE_ONLY
		}

    }
}
