﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Common;
using Com.TheFallenGames.DBD.UI.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Inspect
{
    public class InspectPanel : SlidingPanel
	{
		public Text longDescriptionText;

		public event Action<Entity> InspectedEntityChanged;

		public InspectCommandsPanel commandsPanel;

		public Entity InspectedEntity
		{
			get { return _InspectedEntity; }
			set
			{

				_InspectedEntity = value;
				string longDescription = "";
				if (_InspectedEntity)
				{
					entityCardPanel.Model = _InspectedEntity.GetCardModel();

					if (_InspectedEntity.GetParentAsBlock() && GetAllChildrenOfTheCurrentEntityBlock().Count > 1)
						commandsPanel.NextButtonText = "Next >>>";
					else
						commandsPanel.NextButtonText = null;

					longDescription = _InspectedEntity.Design.longDescription;
				}
				else
				{
					entityCardPanel.Model = null;
				}

				longDescriptionText.text = longDescription;

				if (InspectedEntityChanged != null)
					InspectedEntityChanged(_InspectedEntity);
			}
		}
		Entity _InspectedEntity;

		public CardPanel entityCardPanel;


		public void Show(Entity entity)
		{
			InspectedEntity = entity;

			Show(false);
		}

		protected override void Awake()
		{
			base.Awake();

			//_ScrollRect.onValueChanged.AddListener(OnScroll);
			entityCardPanel = GetComponentInChildren<CardPanel>();
		}

		protected override void Start()
		{
			base.Start();

			commandsPanel.CloseClicked += OnCloseClicked;
			commandsPanel.NextEntityClicked += OnNextEntityClicked;
			// The Strike animation is not used in the inspect panel
			GetComponentInChildren<Animator>().gameObject.SetActive(false);
		}

		//void OnScroll(Vector2 v)
		//{

		//}

		List<Entity> GetAllChildrenOfTheCurrentEntityBlock()
		{
			var block = _InspectedEntity.GetParentAsBlock();
			var result = new List<Entity>();
			result.AddRange(block.GetChildrenOfType<Character>().ToArray());
			result.AddRange(block.GetChildrenOfType<CommonItem>().ToArray());
			result.AddRange(block.GetChildrenOfType<EscapeItem>().ToArray());
			result.AddRange(block.GetChildrenOfType<MonsterItem>().ToArray());
			result.AddRange(block.GetChildrenOfType<Treasure>().ToArray());

			return result;
		}

		void OnNextEntityClicked()
		{
			var all = GetAllChildrenOfTheCurrentEntityBlock();
			int indexOfCurrent = all.IndexOf(_InspectedEntity);
			int indexOfNext = (indexOfCurrent + 1) % all.Count;
			InspectedEntity = all[indexOfNext];
		}

	}
}
