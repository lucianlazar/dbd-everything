﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Battle
{
    public class GameResultPanel : MonoBehaviour
    {
		public TexturedText titleText, centerText, subText;
		public float animationDuration;

		public bool IsShowing { get; private set;}

		bool _IsAnimating;
		float _CurrentAnimationStartValue, _CurrentAnimationEndValue, _CurrentSignedDistance;
		float _StartTime;
		Action _CurrentOnDoneCallback;
		RectTransform _RectTransform;
		float _OriginalWidth;

		void Awake()
		{
			Initialize();
		}

		void Initialize()
		{
			_RectTransform = transform as RectTransform;
			_OriginalWidth = _RectTransform.rect.width;
		}

		void Update()
		{
			if (_IsAnimating)
			{
				float elapsed01 = (Time.time - _StartTime) / animationDuration;
				bool done;
				if (done = elapsed01 >= 1f)
					elapsed01 = 1f;
				
				float width = _CurrentAnimationStartValue + Utils8.Instance.Log01(elapsed01, 1) * _CurrentSignedDistance;
				_RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);

				if (done)
				{
					_IsAnimating = false;
					if (_CurrentOnDoneCallback != null)
						_CurrentOnDoneCallback();
				}
			}
		}

		public void Show(string title, string center, string sub, Action onDone = null)
		{
			//if (!_RectTransform)
			//{
			//	Initialize();
			//}
			gameObject.SetActive(true);

			titleText.Text = title;
			centerText.Text = center;
			subText.Text = sub;
			IsShowing = _IsAnimating = true;
			_CurrentAnimationStartValue = 0f;
			_CurrentAnimationEndValue = _OriginalWidth;
			_CurrentSignedDistance = _CurrentAnimationEndValue - _CurrentAnimationStartValue;
			_StartTime = Time.time;
			_CurrentOnDoneCallback = onDone;

			// Just so it can start from the beginning immediately, not after one frame (while being shown as full-size)
			Update();
		}

		public void Close()
		{
			gameObject.SetActive(false);
		}
	}
}
