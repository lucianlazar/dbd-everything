﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI
{
    [RequireComponent(typeof(InputField))]
    public class NameInputUI : MonoBehaviour
    {
        const string KEY_PLAYER_NAME = "k_PlayerName";

        InputField _InputField;

        void Start()
        {
            _InputField = GetComponent<InputField>();
            string playerName = PlayerPrefs.GetString(KEY_PLAYER_NAME);
            if (string.IsNullOrEmpty(playerName))
            {
                playerName = GetNewName();
                PersistPlayerName(playerName);
            }
            PhotonNetwork.playerName = playerName;
            _InputField.text = playerName;
        }

        public void OnSetPlayerNameClicked(string playerName)
        {
            if (playerName.Length == 0)
            {
                playerName = GetNewName();
                _InputField.text = playerName;
            }
            PersistPlayerName(playerName);
            PhotonNetwork.playerName = playerName;
        }

        void PersistPlayerName(string playerName)
        {
            PlayerPrefs.SetString(KEY_PLAYER_NAME, playerName);
        }

        string GetNewName() { return "Player" + UnityEngine.Random.Range(1, 10000); }
    }
}
