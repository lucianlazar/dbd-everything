﻿using Com.TheFallenGames.DBD.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI
{
    public class GameModeUI : MonoBehaviour
    {
		public event Action<GameModeEnum> modeChanged;

		//public GameModeEnum SelectedGameMode { get { return (GameModeEnum)_SelectedToggleIndex; } }

		[NonSerialized]
		public ToggleGroup toggleGroup;
		//int _SelectedToggleIndex;


		void Awake()
		{
			toggleGroup = GetComponentInChildren<ToggleGroup>();

			var toggles = GetComponentsInChildren<Toggle>();
			//_SelectedToggleIndex = Array.FindIndex(toggles, t => t.isOn);

			for (int i = 0; i < toggles.Length; ++i)
			{
				int copyOfI = i;
				toggles[i].onValueChanged.AddListener(isOn => { if (isOn) { /*_SelectedToggleIndex = copyOfI;*/ if (modeChanged != null) modeChanged.Invoke((GameModeEnum)copyOfI); } });
			}
		}
	}
}
