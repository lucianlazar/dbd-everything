﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI.Totems
{
    public class TotemsPanel : MonoBehaviour
    {
        Button[] _AllButtons;
        //void Awake()
        //{
        //}
        //PlayerManager _PlayerManager;

        public void Initialize(Action<TotemType> onBuildCallback)
        {
            if (_AllButtons == null)
                _AllButtons = GetComponentsInChildren<Button>();

            for (int i = 0; i < _AllButtons.Length; ++i)
            {
                int copyOfI = i;
                _AllButtons[i].onClick.AddListener(() => onBuildCallback((TotemType)copyOfI));
                _AllButtons[i].GetComponentInChildren<Text>().text = Design.Instance.totems.GetAll()[i].name;
                _AllButtons[i].interactable = false;
            }
        }

        public void SetButtonInteractable(TotemType type, bool interactable)
        {
            _AllButtons[(int)type].interactable = interactable;
        }

        public void SetAllButtonsInteractable(bool interactable)
        {
            foreach (var bt in _AllButtons)
                bt.interactable = interactable;
        }

        internal void UpdateBuildOptions(Player player)
        {
			gameObject.SetActive(player.Design.canBuildTotems);
			if (!player.Design.canBuildTotems)
				return;

            var playerStats = player.Stats;
            //// One totem per turn
            //if (playerStats.AcquiredTotem)
            //{
            //    SetAllButtonsInteractable(false);
            //    return;
            //}

            // One totem per block
            var totemsAtCurrentBlock = player.GetParentAsBlock().GetChildrenOfType<Totem>();
            if (totemsAtCurrentBlock.Count > 0)
            {
                SetAllButtonsInteractable(false);
                return;
            }

            // No totem can be built on top a monster
            var caracters = player.GetParentAsBlock().GetChildrenOfType<Character>();
            if (caracters.Count > 1)
            {
                SetAllButtonsInteractable(false);
                return;
            }

            // Check for cost
            if (playerStats.Ore < Design.Instance.totems.costPerTotem)
			{
				SetAllButtonsInteractable(false);
				return;
			}

            // Enable building for types for which the number of instances didn't reach max
            foreach (var totem in Design.Instance.totems.GetAll())
            {
                var type = totem.GetTotemType();
                int rem = Design.Instance.totems.maxInstancesPerTotemType - playerStats.GetOwnedTotemsOfType(type).Length;
                SetButtonInteractable(type, rem > 0);
            }
		}
    }
}
