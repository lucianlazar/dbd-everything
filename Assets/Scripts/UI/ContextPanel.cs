﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.UI
{
    public class ContextPanel : MonoBehaviour
    {
		public const int RESULT_CANCELLED = -1;
		public const int RESULT_DEFAULT_ON_TIMEOUT = -2;


		//public UnityEngine.UI.Button prefab;
		public bool IsShown { get; private set; }

        Text _TitleText;
        Button[] _Buttons;
        Action<int> _CurrentCallback;
        RectTransform _RectTransform;
		RectTransform _RootCanvasRT;
		float _TimeWhenShown, _CurrentTimeout;
		int _CurrentDefaultOption;
		string _CurrentTitle;


        void Awake()
        {
            _TitleText = transform.GetChild(0).GetComponent<Text>();
            _Buttons = GetComponentsInChildren<Button>();
			_RectTransform = transform as RectTransform;
			_RootCanvasRT = _RectTransform.root.GetComponent<RectTransform>();

			for (int i = 0; i < _Buttons.Length; ++i)
            {
                int copyOfI = i;
                _Buttons[i].onClick.AddListener(() => OnOptionSelected(copyOfI));
            }

			gameObject.SetActive(false);
        }

        void OnOptionSelected(int optionIndex)
        {
            CloseInternal(); // calling close here, in case the dialog is immediately opened in _CurrentCallback()
			if (_CurrentCallback != null)
				ExecuteCallbackAndSetToNull(optionIndex);
		}

		void ExecuteCallbackAndSetToNull(int optionIndex)
		{
			var callback = _CurrentCallback;
			_CurrentCallback = null; // setting this to null here, in case the dialog is immediately opened in _CurrentCallback()
			callback(optionIndex);
		}

        void Update()
        {
			if (!IsShown || _CurrentTimeout == 0f)
				return;

			float rem = _CurrentTimeout - (Time.time - _TimeWhenShown);
			if (rem < 0f)
			{
				Cancel(_CurrentDefaultOption);
			}
			else
			{
				_TitleText.text = _CurrentTitle + "\n[" + rem.ToString("00") + "]";
			}
        }


        public void Show(Vector3 worldPosition, string title, string[] options, Action<int> callback) { Show(worldPosition, title, options, null, callback); }

		public void Show(Vector3 worldPosition, string title, string[] options, bool[] enabledOptions, Action<int> callback)
		{
			//Show((Vector2)Camera.main.WorldToScreenPoint(worldPosition), title, options, enabledOptions, callback);
			Show((Vector2)_RootCanvasRT.InverseTransformPoint(worldPosition), title, options, enabledOptions, callback); // zero Z component
		}

		/// <summary>
		/// Showing it in the middle of the canvas
		/// </summary>
		/// <param name="title"></param>
		/// <param name="options"></param>
		/// <param name="enabledOptions"></param>
		/// <param name="callback"></param>
		public void Show(string title, string[] options, bool[] enabledOptions, Action<int> callback, float timeout = 0f, int defaultOption = RESULT_DEFAULT_ON_TIMEOUT)
		{
			//Vector2 screenPos = Camera.main.ViewportToScreenPoint(Vector3.one * .5f);
			//screenPos += new Vector2(-_RectTransform.rect.width, _RectTransform.rect.height) / 2f;
			Show(_RootCanvasRT.rect.size / 2, title, options, enabledOptions, callback, timeout, defaultOption);
		}

		public void Show(Vector2 posInCanvas, string title, string[] options, bool[] enabledOptions, Action<int> callback, float timeout = 0f, int defaultOption = RESULT_DEFAULT_ON_TIMEOUT)
		{
			if (IsShown)
				Cancel();
			IsShown = true;
			gameObject.SetActive(true);

            _TitleText.text = _CurrentTitle = title;
            _CurrentCallback = callback;
			_CurrentTimeout = timeout;
			_CurrentDefaultOption = defaultOption;
			_TimeWhenShown = Time.time;

			int i = 0;
            // Activate the needed buttons
            for (; i < options.Length; ++i)
            {
                _Buttons[i].gameObject.SetActive(true);
                _Buttons[i].GetComponentInChildren<Text>().text = options[i];
                _Buttons[i].interactable = enabledOptions == null || enabledOptions.Length <= i || enabledOptions[i];
            }

            // De-activate the rest
            for (; i < _Buttons.Length; ++i)
                _Buttons[i].gameObject.SetActive(false);

            //Canvas.ForceUpdateCanvases();
            //float y = screenPos.y;
            //float dialogHeight = _RectTransform.rect.height;
            //y = Mathf.Clamp(y, dialogHeight * .7f, GetComponentInParent<Canvas>().GetComponent<RectTransform>().rect.height - dialogHeight * .7f);
            posInCanvas.y = 0; // always in the middle
			float w = _RectTransform.rect.width;
			//posInCanvas.x = Mathf.Clamp(posInCanvas.x, _RootCanvasRT.rect.size.x / 4, 3 * _RootCanvasRT.rect.size.x / 4 - w);
			posInCanvas.x = 0; // hotfix. TODO set X to be near the relevant entity

			//_RectTransform.anchoredPosition3D = screenPos;
			//_RectTransform.anchoredPosition = screenPos;
			//_RectTransform.localPosition = screenPos;
			_RectTransform.localPosition = posInCanvas;
        }

		void CloseInternal()
		{
			IsShown = false;
			gameObject.SetActive(false);
		}

		public void Cancel(int valueToSendAsChoice = RESULT_CANCELLED)
		{
			CloseInternal();

			// No response (canceled)
			if (_CurrentCallback != null)
				ExecuteCallbackAndSetToNull(valueToSendAsChoice);
		}
	}
}
