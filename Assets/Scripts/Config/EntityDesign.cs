﻿using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public abstract class EntityDesign
    {
        public string name;
		public Sprite icon;

		[TextArea(2, 30)]
		public string longDescription;

		CardModel _CardModel;

        public virtual CardModel GetCardModel()
        { return _CardModel ?? (_CardModel = new CardModel() { design = this }); }
    }
}
