﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config
{
    [Serializable]
    public class GridDesign
    {
        public int numRows;
        public int numColumns;
        public int numBlocksWithDoubleWalls;
        public float distanceBetweenCells;
		public int numSpawnPoints;

		public int numCells { get { return numRows * numColumns; } }
		public int numCellsIncludingOuterCell { get { return numCells + 1; } }
    }
}
