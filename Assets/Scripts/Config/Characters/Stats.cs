﻿using Com.TheFallenGames.DBD.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Characters
{
    [Serializable]
    public class Stats
    {
        public int atk;
        public int magic;

        public override string ToString()
        {
            return "ATK: " + atk +
                   "\nMAGIC: " + magic;
        }

        public virtual string ToStringRichWithBonuses(int bonusATK, int bonusMAGIC)
        {
            return Utils.GetStatsRichTextWithBonus(Data.Stat.ATK, atk, bonusATK, true) +
                   Utils.GetStatsRichTextWithBonus(Data.Stat.SKILL, magic, bonusMAGIC, false);
        }
    }
}
