﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.HitAssets;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;

namespace Com.TheFallenGames.DBD.Config.Characters
{
    [Serializable]
    public abstract class CharacterDesign : EntityDesign
    {
        public Stats[] statsPerLevel;
		[UnityEngine.Tooltip("If -1, his current level will be given as xp reward")]
        public int xpGivenOnDeath = -1;
		[FormerlySerializedAs("minMagicPower")]
		[SerializeField]
		public int minSkillPower = 1;
		[FormerlySerializedAs("maxMagicPower")]
		[SerializeField]
		public int maxSkillPower = 6;
		public MagicType magicType;
		public Effect[] initialEffects;
		[FormerlySerializedAs("battleEffects")]
		[SerializeField]
		Effect[] _BaseBattleEffects;
		public CharacterHitAssets hitAssets;


		public List<Effect> GetBaseBattleEffects()
		{
			return new List<Effect>(_BaseBattleEffects);
		}

		public virtual List<Effect> GetAllBattleEffects(Character self, Character enemy, Cell.Descriptor selfCellOnBattle = null, Cell.Descriptor enemyCellOnBattle = null) { return GetBaseBattleEffects(); }
	}
}
