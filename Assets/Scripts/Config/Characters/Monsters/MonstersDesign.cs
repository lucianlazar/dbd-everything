﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;

namespace Com.TheFallenGames.DBD.Config.Characters.Monsters
{
    [Serializable]
    public class MonstersDesign : DesignItemsCollection<MonsterDesign>
	{
		public EntityCategoryCardDesign cardCategoryDesign;
		public BordersPerLevel bordersPerLevel;

		public MonsterDesign SLUG;
        public MonsterDesign RAT;
        public MonsterDesign HAWK;
		public MonsterDesign SERPENT;
        public MonsterDesign HORNET;
        public MonsterDesign SKELETON;
        public MonsterDesign JELLYFISH;
        public MonsterDesign SCORPION;
        public MonsterDesign OGRE;
        public MonsterDesign SIREN;
        public MonsterDesign GOLEM;
        public MonsterDesign BAT;
        public MonsterDesign GHOST;
        public MonsterDesign SPIDER;
        public MonsterDesign WITCH;
        public MonsterDesign HYDRA;
        public MonsterDesign WRAITH;
        public MonsterDesign ARMOURED_GHOST;
		public DragonDesign DRAGON;


		public List<MonsterDesign> GetAllExceptDragon()
		{
			var allWoDragon = new List<MonsterDesign>(base.GetAll());
			allWoDragon.Remove(DRAGON);

			return allWoDragon;
		}

		[Serializable]
		public class BorderPerLevel
		{
			public int maxLevel;
			public Sprite border;
		}

		[Serializable]
		public class BordersPerLevel
		{
			public BorderPerLevel[] list;


			public Sprite this[int level]
			{
				get
				{
					foreach (var bpl in list)
						if (bpl.maxLevel >= level)
							return bpl.border;

					return null;
				}
			}
		}
    }
}
