﻿using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Characters.Monsters
{
    [Serializable]
    public class MonsterDesign : CharacterDesign
    {
        public int level;
        public MonsterItemDesign rewardItem;
    }
}
