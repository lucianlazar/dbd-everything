﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Data.Inventory;
using UnityEngine;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Actors;
using UnityEngine.Serialization;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Pathfinding;

namespace Com.TheFallenGames.DBD.Config.Characters.Players
{
    [Serializable]
    public class RogueDesign : PlayerDesign
	{
		public override int ComparePathfindingCurrencies_SpecialMoves(PathfindingCurrency a, PathfindingCurrency b)
		{
			//var result = a.TotalSpecialMovesWithoutPassingsThroughEnemies - b.TotalSpecialMovesWithoutPassingsThroughEnemies;
			//if (result == 0)
			//{
			//	result = a.ghostMoves - b.ghostMoves;
			//	if (result == 0)
			//	{
			//		result = a.diagonalMoves - b.diagonalMoves;
			//		if (result == 0)
			//			result = a.passingsThroughWalls - b.passingsThroughWalls;
			//	}
			//}

			//return result;

			return  GHOST_MOVE_DEFAULT_COMPARING_WEIGHT * (a.ghostMoves - b.ghostMoves) +
					//ENEMY_PASS_MOVE_DEFAULT_COMPARING_WEIGHT * (a.passingsThroughEnemies - b.passingsThroughEnemies) +
					DIAGONAL_MOVE_DEFAULT_COMPARING_WEIGHT * (a.diagonalMoves - b.diagonalMoves) +
					WALL_PASS_MOVE_DEFAULT_COMPARING_WEIGHT * (a.passingsThroughWalls - b.passingsThroughWalls);
		}
	}
}
