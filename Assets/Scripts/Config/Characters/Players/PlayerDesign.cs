﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Data.Inventory;
using UnityEngine;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Actors;
using UnityEngine.Serialization;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Pathfinding;

namespace Com.TheFallenGames.DBD.Config.Characters.Players
{
    [Serializable]
    public class PlayerDesign : CharacterDesign
    {
		public Sprite cardBorder;
        public Color color;
        public bool canBuildTotems;
        public bool canPickUpItems;
        public int numDungeonPushes;
        public int numMoves;
		[FormerlySerializedAs("rangedBattleEffects")]
		[SerializeField]
		Effect[] _RangedBattleEffects;

		public const int TOTAL_MOVES_DEFAULT_WEIGHT_WHEN_COMPARING_FOR_FEWER_TOTAL_MOVES_WHEN_RESOURCES_KNOWN = 1000;
		//public const int REGULAR_MOVES_DEFAULT_WEIGHT_WHEN_COMPARING_FOR_FEWER_SPECIAL_MOVES_WHEN_RESOURCES_UNKNOWN = 1000;
		protected const int GHOST_MOVE_DEFAULT_COMPARING_WEIGHT = 30;
		protected const int ENEMY_PASS_MOVE_DEFAULT_COMPARING_WEIGHT = 15;
		protected const int DIAGONAL_MOVE_DEFAULT_COMPARING_WEIGHT = 10;
		protected const int WALL_PASS_MOVE_DEFAULT_COMPARING_WEIGHT = 5;


		public override CardModel GetCardModel()
		{
			var card = base.GetCardModel();
			card.cardCategoryDesign.cardBorder = cardBorder;

			return card;
		}

		public override List<Effect> GetAllBattleEffects(Character self, Character enemy, Cell.Descriptor selfCellOnBattle = null, Cell.Descriptor enemyCellOnBattle = null)
		{
			var results = base.GetAllBattleEffects(self, enemy);

			if (selfCellOnBattle == null)
			{
				if (!self)
					return results;

				selfCellOnBattle = self.GetParentCell().Desc;
			}

			if (enemyCellOnBattle == null)
			{
				if (!enemy)
					return results;

				enemyCellOnBattle = enemy.GetParentCell().Desc;
			}

			if (selfCellOnBattle.RelativePosSimpleTo(enemyCellOnBattle) == Data.RelativePositionSimple.NEIGHBOR_DIRECT)
			{
				results = new List<Effect>(results); // create a new list, bcause otherwise we'd override the config
				results.AddRange(_RangedBattleEffects);
			}

			return results;
		}


		public virtual int ComparePathfindingCurrencies_SpecialMoves(PathfindingCurrency a, PathfindingCurrency b)
		{
			//var result = a.TotalSpecialMoves - b.TotalSpecialMoves;
			//if (result == 0)
			//{
			//	result = 1000 * (a.ghostMoves - b.ghostMoves);
			//	if (result == 0)
			//	{
			//		result = 100 * (a.passingsThroughEnemies - b.passingsThroughEnemies);
			//		if (result == 0)
			//		{
			//			result = 10 * (a.diagonalMoves - b.diagonalMoves);
			//			if (result == 0)
			//				result = a.passingsThroughWalls - b.passingsThroughWalls;
			//		}
			//	}
			//}
			//return result;

			return  GHOST_MOVE_DEFAULT_COMPARING_WEIGHT * (a.ghostMoves - b.ghostMoves) +
					ENEMY_PASS_MOVE_DEFAULT_COMPARING_WEIGHT * (a.passingsThroughEnemies - b.passingsThroughEnemies) +
					DIAGONAL_MOVE_DEFAULT_COMPARING_WEIGHT * (a.diagonalMoves - b.diagonalMoves) +
					WALL_PASS_MOVE_DEFAULT_COMPARING_WEIGHT * (a.passingsThroughWalls - b.passingsThroughWalls);
		}
	}
}
