﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Characters.Players
{
    [Serializable]
    public class PlayersDesign : DesignItemsCollection<PlayerDesign>
	{
		public EntityCategoryCardDesign cardCategoryDesign;

		public PlayerDesign KNIGHT;
		public PlayerDesign ARCHER;
		public PlayerDesign MAGE;
		public RogueDesign ROGUE;
		public PlayerDesign BEAST;
		public EagleDesign EAGLE;
	}
}
