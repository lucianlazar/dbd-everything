﻿using Com.TheFallenGames.DBD.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Characters.Totems
{
    [Serializable]
    public class TotemsDesign : DesignItemsCollection<TotemDesign>
    {
        public int costPerTotem;
        public int maxInstancesPerTotemType;

		public EntityCategoryCardDesign cardCategoryDesign;

		public TotemDesign ATK;
        public TotemDesign MOVE;
        public TotemDesign XP;


        public TotemDesign this[TotemType type]
        {
            get
            {
                return GetAll()[(int)type];
            }
        }
    }
}
