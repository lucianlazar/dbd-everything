﻿using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config.Characters.Totems
{
    [Serializable]
    public class TotemDesign : CharacterDesign
    {
        public Effect effect;

		[NonSerialized]
        string _ShortName;

        public TotemType GetTotemType() { return (TotemType) Array.IndexOf(Design.Instance.totems.GetAll(), this); }
        public string GetShortName() { return _ShortName ?? (_ShortName = Enum.GetName(typeof(TotemType), GetTotemType())); }
    }
}
