﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config
{
    [Serializable]
    public class GeneralDesign
	{
		public int inventoryCapacity;
		public int maxCardsPlayedPerBattle;
//#if UNITY_EDITOR
		public int debug_CellPathTextSize;
		public bool debug_CellPathActive;
		//#endif
		public float battlePanelAdditionalHideDelayWhenSpider;
		public float battlePanelAdditionalHideDelayWhenSerpent;
		public float battlePanelTradeAutoDecisionTimeout;

		public GeneralCardsDesign generalCardsDesign;
	}
}
