﻿using Com.TheFallenGames.DBD.Common;
using System;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config
{
    public class DesignItemsCollection<T> where T : class
	{
		[NonSerialized]
		protected T[] _All;

        public virtual  T[] GetAll()
        {
            if (_All == null)
            {
                _All = Utils.GetAllFieldsOfType<T>(this);
            }

            return _All;
        }
    }
}
