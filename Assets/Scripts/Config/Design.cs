﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if UNITY_EDITOR
using UnityEditor.Callbacks;
#endif
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config
{
    public class Design : MonoBehaviour
    {
        public static Design Instance
        {
            get
            {
                if (_Instance == null && !_GameQuitting)
				{
                    var res = Resources.Load<Design>("Design");
					// Instantiating it, so that we'd modify a copy of the design, not the asset itself (the file)
					_Instance = Instantiate(res.gameObject).GetComponent<Design>();
					_Instance.transform.position = Vector3.one * 1000f;
				}

                return _Instance;
            }
        }

        static Design _Instance;

		static bool _GameQuitting;

        public GeneralDesign general;
		//public AIDesign ai;
		public GridDesign grid;
        public PlayersDesign players;
        public MonstersDesign monsters;
        public TotemsDesign totems; 
        public AllItems items;
		public Shortcuts shortcut = new Shortcuts();

#if UNITY_EDITOR
		[DidReloadScripts]
		static void ScriptsReloaded() { _GameQuitting = false; }
#endif

		void Awake()
		{
			//_Instance = this;
			DontDestroyOnLoad(this);
		}

		void OnDestroy()
		{
			_GameQuitting = true;
			_Instance = null;
		}


		public class Shortcuts
		{
			public List<Effect> GetSerpentItemEffects() { return Instance.monsters.SERPENT.rewardItem.GetEffects(null, null); }
			public int GetSerpentItemEffectAmount() { return GetSerpentItemEffects()[0].amount; }
		}
	}
}
