﻿using Com.TheFallenGames.DBD.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config
{
	public class ClassicBoardSetup : IDisposable
	{
		public const string SERIALIZED_FILENAME_WITHOUT_EXT = "ClassicBoardSetup";
		public const string SERIALIZED_FILENAME_WITH_EXT = SERIALIZED_FILENAME_WITHOUT_EXT + ".txt";

		public List<int> ore = new List<int>();
		public List<int> items = new List<int>();
		public List<int> monsters = new List<int>();
		public List<int> spawnPoints = new List<int>();
		public List<int> leftWalls = new List<int>();
		public List<int> topWalls = new List<int>();
		public List<int> rightWalls = new List<int>();
		public List<int> bottomWalls = new List<int>();

		public int ValidEscapeItems { get { return GetEscapeItems().FindAll(i => i != -1).Count; } }
		public int ValidCommonItems { get { return GetCommonItems().FindAll(i => i != -1).Count; } }
		public int ValidTreasures { get { return GetTreasures().FindAll(i => i != -1).Count; } }
		public int ValidMonsters { get { return monsters.FindAll(i => i != -1).Count; } }
		public int ValidSpawnPoints { get { return spawnPoints.FindAll(i => i != -1).Count; } }

		public string FilePathRelativeToAssets { get { return "/Resources/" + SERIALIZED_FILENAME_WITH_EXT; } }
		public string FileAbsolutePath { get { return Application.dataPath + FilePathRelativeToAssets; } }

		const string COUNT_HEADER = "Count ";
		const string VALUES_HEADER = "Values ";


		public ClassicBoardSetup()
		{
			int len = Design.Instance.items.GetAll().Length;
			for (int i = 0; i < len; ++i)
				items.Add(-1);

			len = Design.Instance.monsters.GetAll().Length;
			for (int i = 0; i < len; ++i)
				monsters.Add(-1);
		}

		public void RandomizeEntities()
		{
			int numAllCells = Design.Instance.grid.numCellsIncludingOuterCell;
			ore = new List<byte>(Utils.ChooseRandomAsBytes(0, numAllCells, Design.Instance.items.ore.numOrePieces)).ConvertAll(i => (int)i);
			items = new List<byte>(Utils.ChooseRandomAsBytes(0, numAllCells, items.Count)).ConvertAll(i => (int)i);
			monsters = new List<byte>(Utils.ChooseRandomAsBytes(0, numAllCells, monsters.Count)).ConvertAll(i => (int)i);
			spawnPoints = new List<byte>(Utils.ChooseRandomAsBytes(0, numAllCells, Design.Instance.grid.numSpawnPoints)).ConvertAll(i => (int)i);
		}

		public void ToggleWall(List<int> wallsCollection, int i)
		{
			if (wallsCollection.Contains(i))
				wallsCollection.Remove(i);
			else
				wallsCollection.Add(i);
		}

		public void AddOre(int i)
		{
			ore.Add(i);
			while (ore.Count > Design.Instance.items.ore.numOrePieces)
				ore.RemoveAt(0);
		}

		public void AddSpawnPoint(int i)
		{
			spawnPoints.Add(i);
			while (spawnPoints.Count > Design.Instance.grid.numSpawnPoints)
				spawnPoints.RemoveAt(0);
		}

		// Returns an array with 0=left..3=bottom
		public List<byte> GetWalls(int i)
		{
			var result = new List<byte>();
			if (leftWalls.Contains(i))
				result.Add(0);
			if (topWalls.Contains(i))
				result.Add(1);
			if (rightWalls.Contains(i))
				result.Add(2);
			if (bottomWalls.Contains(i))
				result.Add(3);

			return result;
		}

		public List<int> GetEscapeItems()
		{
			var itemsFromBoard = new List<int>(items);
			var itemsFromDesign = Design.Instance.items.escapeItems.GetAll();
			int numItemsFromDesign = itemsFromDesign.Length;
			itemsFromBoard.RemoveRange(numItemsFromDesign, itemsFromBoard.Count - numItemsFromDesign);

			return itemsFromBoard;
		}

		public List<int> GetCommonItems()
		{
			var itemsFromBoard = new List<int>(items);
			var itemsFromDesign = Design.Instance.items.commonItems.GetAll();
			int numItemsFromDesign = itemsFromDesign.Length;
			// Remove escape items
			itemsFromBoard.RemoveRange(0, Design.Instance.items.escapeItems.GetAll().Length);
			// Remove all after common items
			itemsFromBoard.RemoveRange(numItemsFromDesign, itemsFromBoard.Count - numItemsFromDesign);

			return itemsFromBoard;
		}

		public List<int> GetTreasures()
		{
			var itemsFromBoard = new List<int>(items);
			// Remove all until treasures
			itemsFromBoard.RemoveRange(0, itemsFromBoard.Count - Design.Instance.items.treasures.GetAll().Length);

			return itemsFromBoard;
		}

		public void Dispose()
		{
		}


		/// <summary>
		/// Format:
		/// PropName
		/// Count [count]
		/// Values [values (ex 1 22 -1 22)]
		/// </summary>
		/// <returns></returns>
		public string ConvertToText()
		{
			string str = "";

			var fields = GetFields();
			int numFields = fields.Length;

			for (int i = 0; i < numFields; ++i)
			{
				var fi = fields[i];

				str += fi.Name + ":\n";
				var val = fi.GetValue(this) as List<int>;

				str += COUNT_HEADER + val.Count + "\n";

				str += VALUES_HEADER;
				foreach (var v in val)
					str += " " + v;

				str += "\n\n";
			}

			return str;
		}
#if UNITY_EDITOR
		public void WriteAsset()
		{
			System.IO.File.WriteAllText(FileAbsolutePath, ConvertToText());
			//string filePathRelativeToProject = "Assets/" + FilePathRelativeToAssets;
			//AssetDatabase.ImportAsset(filePathRelativeToProject);
			UnityEditor.AssetDatabase.Refresh();
			//AssetDatabase.SaveAssets();
		}
#endif

		public void ReadAsset()
		{
			var classicBoardSetupText = Resources.Load<TextAsset>(SERIALIZED_FILENAME_WITHOUT_EXT);
			if (classicBoardSetupText)
			{
				try
				{
					UpdateFromText(classicBoardSetupText.text);
				}
				catch (Exception e)
				{
					Debug.LogError("ClassicBoardSetup.UpdateFromText failed: Unrecognized format");
					Debug.LogException(e);
				}
				Resources.UnloadAsset(classicBoardSetupText);
			}
		}

		public void UpdateFromText(string input)
		{
			var fields = GetFields();
			int numFields = fields.Length;

			string newLine = "\n";
			if (!input.Contains("\n\n"))
			{
				if (!input.Contains("\r\n\r\n"))
					throw new UnityException("ClassicBoardSetup: Input file - unknown format");
				newLine = "\r\n";
			}

			string[] sections = input.Split(new string[] { newLine + newLine }, StringSplitOptions.RemoveEmptyEntries);
			//Debug.Log("Sections " + sections.Length);
			
			var toBeAdded = new List<List<int>>();

			for (int i = 0; i < numFields; ++i)
			{
				string section = sections[i];
				string[] subSections = section.Split(new string[] { newLine }, StringSplitOptions.RemoveEmptyEntries);

				//Debug.Log("subSections " + subSections.Length);
				int count = int.Parse(subSections[1].Replace(COUNT_HEADER, ""));
				string[] values = subSections[2].Replace(VALUES_HEADER, "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

				var list = new List<int>();

				string str = "";
				for (int j = 0; j < count; ++j)
				{
					var val = int.Parse(values[j]);
					str += val + " ";
					list.Add(val);
				}

				//Debug.Log("values = " + str);

				toBeAdded.Add(list);
			}

			// Only add all of them at once after the parsing went well. If an exception was thrown, the object will remain intact
			for (int i = 0; i < numFields; ++i)
			{
				var fi = fields[i];
				var val = fi.GetValue(this) as List<int>;
				val.Clear();
				val.AddRange(toBeAdded[i]);
			}
		}


		System.Reflection.FieldInfo[] GetFields()
		{
			return GetType().GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
		}
	}
}
