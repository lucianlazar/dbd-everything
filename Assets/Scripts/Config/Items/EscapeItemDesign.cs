﻿using System;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.World;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class EscapeItemDesign : BaseItemDesign, IEffectsItem
    {
        [UnityEngine.SerializeField]
        List<Effect> _Effects;


		public List<Effect> GetEffects(Character caster, Character enemy, Cell.Descriptor casterCellOnCast = null, Cell.Descriptor enemyCellOnCast = null) { return new List<Effect>(_Effects); }
	}
}
