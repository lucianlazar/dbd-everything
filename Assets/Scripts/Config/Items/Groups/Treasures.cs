﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class Treasures : DesignItemsCollection<TreasureDesign>
    {
		public EntityCategoryCardDesign cardCategoryDesign;

		public TreasureDesign A;
        public TreasureDesign B;
        public TreasureDesign C;
        public TreasureDesign D;
        public TreasureDesign E;
	}
}
