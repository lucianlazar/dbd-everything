﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Serialization;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class CommonItems : DesignItemsCollection<CommonItemDesign>
    {
		public EntityCategoryCardDesign utilityCardCategoryDesign;
		public EntityCategoryCardDesign battleCardCategoryDesign;

		public CommonItemDesign COURAGE_POTION;
		public CommonItemDesign SPELL_BOOK;
        public CommonItemDesign SHIELD;
		public ThrowingKnifeDesign THROWING_DAGGER;
		public CommonItemDesign FIRE_GLOVE;
		public CommonItemDesign MAGIC_SWORD;
	}
}
