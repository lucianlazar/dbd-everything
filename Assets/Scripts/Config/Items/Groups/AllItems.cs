﻿using Com.TheFallenGames.DBD.Common;
using System;
using System.Collections.Generic;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class AllItems : DesignItemsCollection<BaseItemDesign>
    {
        public OreDesign ore;

        public EscapeItems escapeItems;
        public CommonItems commonItems;
        public Treasures treasures;

        public override BaseItemDesign[] GetAll()
        {
            if (_All == null)
            {
                var list = new List<BaseItemDesign>(escapeItems.GetAll());
                list.AddRange(commonItems.GetAll());
                list.AddRange(treasures.GetAll());
                _All = list.ToArray();
            }

            return _All;
        }
    }
}
