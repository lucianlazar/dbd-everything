﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class EscapeItems : DesignItemsCollection<EscapeItemDesign>
	{
		public EntityCategoryCardDesign cardCategoryDesign;

		public EscapeItemDesign MAP;
        public EscapeItemDesign KEYS;
        public EscapeItemDesign TORCH;


    }
}
