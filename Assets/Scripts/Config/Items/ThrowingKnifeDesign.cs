﻿using Com.TheFallenGames.DBD.Config.Effects;
using System;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.World;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class ThrowingKnifeDesign : CommonItemDesign
	{
		//[UnityEngine.SerializeField]
		//int _RangedBonusDistance;

		[UnityEngine.SerializeField]
		List<Effect> _RangedBonuses;


		public override List<Effect> GetEffects(Character caster, Character enemy, Cell.Descriptor casterCellOnCast = null, Cell.Descriptor enemyCellOnCast = null)
		{
			var results = base.GetEffects(caster, enemy, casterCellOnCast, enemyCellOnCast);

			if (casterCellOnCast == null)
			{
				if (!caster)
					return results;

				casterCellOnCast = caster.GetParentCell().Desc;
			}

			if (enemyCellOnCast == null)
			{
				if (!enemy)
					return results;

				enemyCellOnCast = enemy.GetParentCell().Desc;
			}

			if (casterCellOnCast.RelativePosSimpleTo(enemyCellOnCast) == Data.RelativePositionSimple.NEIGHBOR_DIRECT)
			{
				results = new List<Effect>(results); // create a new list, bcause otherwise we'd override the config
				results.AddRange(_RangedBonuses);
			}

			return results;
		}
	}
}
