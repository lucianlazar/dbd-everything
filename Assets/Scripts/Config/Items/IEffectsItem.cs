﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Items
{
    public interface IEffectsItem
    {
		List<Effect> GetEffects(Character caster, Character enemy, Cell.Descriptor casterCellOnCast = null, Cell.Descriptor enemyCellOnCast = null);
    }
}
