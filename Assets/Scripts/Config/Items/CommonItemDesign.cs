﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class CommonItemDesign : BaseItemDesign, ICostItem, IEffectsItem
    {
        public int Cost { get { return _Cost; } }

        [UnityEngine.SerializeField]
        int _Cost;

		[UnityEngine.SerializeField]
		List<Effect> _Effects;

		[UnityEngine.SerializeField]
		List<Effect> _PassiveEffects;


		public virtual List<Effect> GetEffects(Character caster, Character enemy, Cell.Descriptor casterCellOnCast = null, Cell.Descriptor enemyCellOnCast = null) { return new List<Effect>(_Effects); }

		public virtual List<Effect> GetPassiveEffects() { return new List<Effect>(_PassiveEffects); }
	}
}
