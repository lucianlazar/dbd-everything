﻿using Com.TheFallenGames.DBD.Data;
using System;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config.Items
{
    [Serializable]
    public class BaseItemDesign : EntityDesign
    {
		public CardType cardType;
		[SerializeField]
		[Tooltip("If not assigned, the default icon will be used")]
		Sprite _MiniIcon;

		/// <summary> Initially, neede for the L & R images in battle</summary>
		public Sprite MiniIcon { get { return _MiniIcon == null ? icon : _MiniIcon; } }
    }
}
