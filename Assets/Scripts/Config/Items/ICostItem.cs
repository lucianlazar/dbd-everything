﻿using Com.TheFallenGames.DBD.Config.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Config.Items
{
    public interface ICostItem
    {
        int Cost { get; }
    }
}
