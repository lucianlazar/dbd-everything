﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config
{
	[Serializable]
	public class EntityCategoryCardDesign
	{
		public Sprite cardBorder;
		public Sprite cardFrame;
		public Sprite cardDescriptionBackgroundOverride;
		public float mainImageScaleFactor = 1f;
		public bool showDescriptionText = true;


		public void CopyFrom(EntityCategoryCardDesign other)
		{
			cardBorder = other.cardBorder;
			cardFrame = other.cardFrame;
			cardDescriptionBackgroundOverride = other.cardDescriptionBackgroundOverride;
			mainImageScaleFactor = other.mainImageScaleFactor;
			showDescriptionText = other.showDescriptionText;
		}
	}
}
