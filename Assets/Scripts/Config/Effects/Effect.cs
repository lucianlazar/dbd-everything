﻿using Com.TheFallenGames.DBD.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Config.Effects
{
	// TODO add "Effect source" (item)
    [Serializable]
    public class Effect
    {
        public Stat type;
        public int amount;
		public OwnerEnum owner = OwnerEnum.SELF;
		public DurationEnum duration = DurationEnum.UNDEFINED;

		//public bool DynamicallyCreated { get { return dynamicallyCreated; } }

		[NonSerialized]
		public bool dynamicallyCreated;


		public static Effect CreateDinamically(Effect other)
		{
			return new Effect(other) { dynamicallyCreated = true };
		}

		public static Effect CreateDinamically(Stat type, int amount, OwnerEnum owner = OwnerEnum.SELF, DurationEnum duration = DurationEnum.UNDEFINED)
		{
			return new Effect() { type = type, amount = amount, owner = owner, duration = duration, dynamicallyCreated = true };
		}


		public Effect() { }

		private Effect(Effect other)
		{
			type = other.type;
			amount = other.amount;
			owner = other.owner;
			duration = other.duration;
		}


		public string ToShortString() { return type + " " + amount + (dynamicallyCreated ? " (dyn.)" : ""); }


		public enum OwnerEnum
		{
			SELF,
			ENEMY
		}

		public enum DurationEnum
		{
			UNTIL_BATTLE_END,
			UNTIL_TURN_END,
			UNDEFINED
		}
	}
}
