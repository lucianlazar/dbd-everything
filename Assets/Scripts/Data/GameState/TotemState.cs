﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Data.GameState
{
    public class TotemState
	{
		public byte position;
		public byte type;
	}
}
