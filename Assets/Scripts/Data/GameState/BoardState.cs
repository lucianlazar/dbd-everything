﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Data.GameState
{
	public class BoardState
	{
		internal byte[] ore; // size = <numOrePieces>; values = from 0 to <numRows*numCols-1>
		internal byte[] walls; // size = <numRows*numCols>; values = first 2 bytes are the number of walls (1 or 2); the following 2 represent the position of first wall; the following 2 represent the position of the second wall (but only if there ARE 2 walls)
		internal byte[] items; // size = <numItems>; values = positions of items ordered by the <Design.Instance.Items.GetAll()> array;
		internal byte[] monsters; // size = <numMonsters>; values = positions of monsters ordered by the <Design.Instance.monsters.GetAll()> array;
		internal byte dragon;
		internal byte[] players; // size = <PhotonNetwork.room.PlayerCount>; values = positions of players ordered by their ids;
		internal byte outerCell;

		internal const byte ORE_KEY = 0;
		internal const byte WALLS_KEY = 1;
		internal const byte ITEMS_KEY = 2;
		internal const byte MONSTERS_KEY = 3;
		internal const byte DRAGON_KEY = 4;
		internal const byte PLAYERS_KEY = 5;
		internal const byte OUTER_CELL_KEY = 7;


		public BoardState()
		{
			Init();
		}

		public BoardState(Hashtable data)
		{
			Init();

			ore = data[ORE_KEY] as byte[];
			walls = data[WALLS_KEY] as byte[];
			items = data[ITEMS_KEY] as byte[];
			monsters = data[MONSTERS_KEY] as byte[];
			dragon = (byte)data[DRAGON_KEY];
			players = data[PLAYERS_KEY] as byte[];
			outerCell = (byte)data[OUTER_CELL_KEY];
		}

		protected virtual void Init()
		{
		}

		public void UpdateFromClassicBoardSetup(ClassicBoardSetup classicSetup)
		{
			// Ore
			ore = classicSetup.ore.ConvertAll(i => (byte)i).ToArray();

			// Walls
			walls = GetEncodedWallsPositionsFromClassicBoardSetup(classicSetup);

			// Items
			items = classicSetup.items.ConvertAll(i => (byte)i).ToArray();

			// Monsters & Dragon
			var setupMonsters = classicSetup.monsters.ConvertAll(i => (byte)i);
			dragon = setupMonsters[setupMonsters.Count - 1];
			setupMonsters.RemoveAt(setupMonsters.Count - 1);
			monsters = setupMonsters.ToArray();
		}

		public virtual Hashtable ToPhotonHashable()
		{
			var data = new Hashtable();
			data[ORE_KEY] = ore;
			data[WALLS_KEY] = walls;
			data[ITEMS_KEY] = items;
			data[MONSTERS_KEY] = monsters;
			data[DRAGON_KEY] = dragon;
			data[PLAYERS_KEY] = players;
			data[OUTER_CELL_KEY] = outerCell;

			return data;
		}

		internal List<byte[]> GetDecodedWalls()
		{
			return DecodeWallsPositions(walls);
		}

		internal byte[] GetEncodedWallsPositionsFromClassicBoardSetup(ClassicBoardSetup setup)
		{
			int numBlocks = Design.Instance.grid.numCellsIncludingOuterCell;
			byte[] in_out_wallsForEachBlock = new byte[numBlocks];
			byte[][] wallPositionsForEachBlock = new byte[numBlocks][];

			for (int i = 0; i < numBlocks; ++i)
			{
				var walls = setup.GetWalls(i);
				in_out_wallsForEachBlock[i] = (byte)walls.Count;
				wallPositionsForEachBlock[i] = walls.ToArray();
			}

			EncodeWallsPositions(in_out_wallsForEachBlock, wallPositionsForEachBlock);

			return in_out_wallsForEachBlock;
		}

		internal void EncodeWallsPositions(byte[] in_out_wallsForEachBlock, byte[][] wallPositionsForEachBlock)
		{
			// Transform to binary representation: xx[=numwalls] xx[=firstwallIndex] xx[=secondwallIndex] 00;
			for (int i = 0; i < in_out_wallsForEachBlock.Length; ++i)
			{
				int numWalls = in_out_wallsForEachBlock[i];
				int binary = numWalls << 6; // 000000xx to xx000000

				byte[] positions = wallPositionsForEachBlock[i];
				if (numWalls > 0)
				{
					binary |= positions[0] << 4; // position: 000000xx to 00xx0000

					if (numWalls > 1)
						binary |= positions[1] << 2; // position: 000000xx to 0000xx00
				}

				in_out_wallsForEachBlock[i] = (byte)binary;
			}
		}

		internal List<byte[]> DecodeWallsPositions(byte[] walls)
		{
			var wallsAndTheirPositions = new List<byte[]>();

			for (int i = 0; i < walls.Length; ++i)
			{
				byte numWallsMask = 1 << 7 | 1 << 6; // 11000000
				byte firstWallMask = 1 << 5 | 1 << 4; // 00110000
				byte secondWallMask = 1 << 3 | 1 << 2; // 00001100

				byte wallInfo = walls[i];
				int numWalls = (wallInfo & numWallsMask) >> 6;

				byte[] wallsPositions = new byte[numWalls];

				if (numWalls > 0)
				{
					wallsPositions[0] = (byte)((wallInfo & firstWallMask) >> 4); // 00xx0000 => 000000xx

					if (numWalls > 1)
						wallsPositions[1] = (byte)((wallInfo & secondWallMask) >> 2); // 0000xx00 => 000000xx
				}
				wallsAndTheirPositions.Add(wallsPositions);
			}

			return wallsAndTheirPositions;
		}
	}
}
