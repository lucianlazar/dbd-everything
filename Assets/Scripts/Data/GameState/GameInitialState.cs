﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Data.GameState
{
    public class GameInitialState : BoardState
    {
        internal int[] playerActorIDs; // size = <PhotonNetwork.room.PlayerCount>; values = actor ids of players ordered asc;
        internal byte[] playerChoices; // size = <PhotonNetwork.room.PlayerCount>; values = indices of player types from Design.Instance.players ordered by their ids;

		internal const byte PLAYER_ACTOR_IDS_KEY = 51;
		internal const byte PLAYER_CHOICES_KEY = 52;


		public GameInitialState()
			: base()
        {
            Init();
        }

        public GameInitialState(Hashtable data)
			: base(data)
        {
			playerActorIDs = data[PLAYER_ACTOR_IDS_KEY] as int[];
			playerChoices = data[PLAYER_CHOICES_KEY] as byte[];
		}


		public override Hashtable ToPhotonHashable()
		{
			var data = base.ToPhotonHashable();

			data[PLAYER_ACTOR_IDS_KEY] = playerActorIDs;
			data[PLAYER_CHOICES_KEY] = playerChoices;

			return data;
		}
	}
}
