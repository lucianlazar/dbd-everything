﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Data.GameState
{
	public class PlayerState
	{
		public int actorID;
		public byte chosenClass;
		public byte level;
		public byte[] itemsInInventory; // values = positions of items ordered by the <Design.Instance.Items.GetAll()> array
		public bool knowsDragonsVoice;
		public bool hasSoul;

		public byte xp;
		public byte ore;
		public TotemState[] ownedTotems;
		public EffectState[] appliedEffects;
	}
}
