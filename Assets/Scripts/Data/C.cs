﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Data
{
	public static class SceneNames
	{
		public const string _MAIN = "_Main";
		public const string GAME = "Game";
		public const string LOADING = "Loading";
	}

	public static class DefaultPlayerIDs
	{
		public const int CPUPlayerID = byte.MaxValue / 2 + 23;
	}

	public static class DBDStrings
	{
	}

	public static class C
	{
		public const float MAX_DRAG_SCREEN_DISTANCE_ALLOWED_TO_STILL_CONSIDER_LONG_CLICK_ON_ENTITY = 10f;
	}

	public enum TotemType : byte
    {
        ATK,
        MOVE,
        XP,
        COUNT_
    }

    public enum Stat
    {
        DUNGEON_PUSH,
        ATK,
        SKILL,
        MOVES,
        MOVES_THROUGH_WALLS,
		XP_PER_TURN,
        GOD_MOVES,
		DIAGONAL_MOVES,
		BATTLE_EVADING,
		ATTRACT_ENEMY,
		CARD_STEALING,
		RANGED_BATTLE,
		GHOST_MOVES, // battle-evading & wall passing
	}

    public enum WallSide
    {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
	}
	
	public enum RelativePositionSimple
	{
		IDENTICAL,
		NEIGHBOR_DIRECT, // neighbor, verically or horizontally
		NEIGHBOR_DIAGONAL, // neighbor
		DISTANT // farther than all of the 9 possible surrounding neighors
	}

	[Flags]
	public enum CellRelativePosition
	{
		LEFT = 1,
		TOP = 2,
		RIGHT = 4,
		BOTTOM = 8,

	}

	public enum CardType
	{
		NOT_PLAYABLE,
		UTILITY,
		BATTLE
	}

	public enum MagicType
	{
		/// <summary>Adds to the total Battle Points (on top of ATK)</summary>
		ADDITIVE,

		/// <summary>Multiplies the Final ATK to obtain the total Battle Points</summary>
		MULTIPLICATIVE
	}

	public enum SingleCellMoveCost
	{
		SIMPLE_MOVE,
		//THROUGH_ENEMY_MOVE,
		THROUGH_WALL_MOVE,
		DIAGONAL_MOVE,
	}

	public enum GameModeEnum
	{
		TOURNAMENT,
		MULTIPLAYER,
		TRAINING
	}

	public enum BoardSetupEnum
	{
		CLASSIC,
		RANDOMIZED
	}

	public enum GameStateEnum
	{
		NONE,
		INITIALIZING_WAITING_FOR_OTHER_PLAYERS_TO_CONNECT,
		GAMEPLAY,
		GAMEPLAY_PAUSED_WAITING_FOR_PLAYER_TO_RECONNECT
	}
}
