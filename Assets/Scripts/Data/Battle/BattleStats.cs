﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Com.TheFallenGames.DBD.UI.Battle;

namespace Com.TheFallenGames.DBD.Data
{
    public class BattleStats
    {
		public bool finalBattlePointsArePreDetermined;
		public int  //numberOfTiesBeforeFinalOutcome,
					firstPlayerScore,
					secondPlayerScore,
					firstPlayerScoreFromAtk,
					secondPlayerScoreFromAtk,
					firstPlayerScoreFromMagic,
					secondPlayerScoreFromMagic;
		
		/// <summary>Null if the battle was pre-determined or there was no tie</summary>
		public BattleStats statsOfFirstTieBeforeFinalRound;


		public BattleStats() { }

		public BattleStats(byte[] asByteArray)
		{
			int i = 0;
			finalBattlePointsArePreDetermined = asByteArray[i++] == 1;
			//numberOfTiesBeforeFinalOutcome = asByteArray[i++];
			firstPlayerScore = asByteArray[i++];
			secondPlayerScore = asByteArray[i++];
			firstPlayerScoreFromAtk = asByteArray[i++];
			secondPlayerScoreFromAtk = asByteArray[i++];
			firstPlayerScoreFromMagic = asByteArray[i++];
			secondPlayerScoreFromMagic = asByteArray[i++];
			byte[] tieStatsAsByteArray = new byte[asByteArray.Length - i];
			Array.Copy(asByteArray, i, tieStatsAsByteArray, 0, tieStatsAsByteArray.Length);
			if (asByteArray.Length > i)
				statsOfFirstTieBeforeFinalRound = new BattleStats(tieStatsAsByteArray);
		}


		internal byte[] ConvertToByteArray()
		{
			var result = 
				new List<byte>
				(
					new byte[]
					{
						(byte)(finalBattlePointsArePreDetermined ? 1 : 0),
						//(byte)numberOfTiesBeforeFinalOutcome,
						(byte)firstPlayerScore,
						(byte)secondPlayerScore,
						(byte)firstPlayerScoreFromAtk,
						(byte)secondPlayerScoreFromAtk,
						(byte)firstPlayerScoreFromMagic,
						(byte)secondPlayerScoreFromMagic
					}
				);

			if (statsOfFirstTieBeforeFinalRound != null)
				result.AddRange(statsOfFirstTieBeforeFinalRound.ConvertToByteArray());

			return result.ToArray();
		}

		internal void CalculateBattleOutcome(BattleCharacterInfoPanel firstCharInfoPanel, BattleCharacterInfoPanel secondCharInfoPanel)
		{
			firstCharInfoPanel.Potential.ComputeSelfWithoutOpponentOrSpecialEffects();

			finalBattlePointsArePreDetermined = firstCharInfoPanel.Potential.MinFinalScore == firstCharInfoPanel.Potential.MaxFinalScore
										 && secondCharInfoPanel.Potential.MinFinalScore == secondCharInfoPanel.Potential.MaxFinalScore;

			// Skip the fight
			if (finalBattlePointsArePreDetermined && firstCharInfoPanel.Potential.MinFinalScore == secondCharInfoPanel.Potential.MinFinalScore)
			{
				firstPlayerScore =
					secondPlayerScore =
					firstPlayerScoreFromAtk =
					secondPlayerScoreFromAtk =
					firstCharInfoPanel.Potential.MinFinalScore;
				//numberOfTiesBeforeFinalOutcome = 1;

				//SetBattleResult(null);

				// TODO show on UI

				return;
			}

			//int first;// = firstCharInfoPanel.CalculateAndShowFinalBattlePoints();
			//int second;// = secondCharInfoPanel.CalculateAndShowFinalBattlePoints();

			//if (first != second)
			//{
			//    _OnBattleOver(first > second ? firstCharInfoPanel.PresentedCharacter : secondCharInfoPanel.PresentedCharacter);

			//    return;
			//}
			//numberOfTiesBeforeFinalOutcome = 0;
			do
			{
				firstPlayerScore =
					firstCharInfoPanel.Potential.ComputeFinalBattlePoints(out firstPlayerScoreFromAtk, out firstPlayerScoreFromMagic);
				secondPlayerScore =
					secondCharInfoPanel.Potential.ComputeFinalBattlePoints(out secondPlayerScoreFromAtk, out secondPlayerScoreFromMagic);

				if (firstPlayerScore == secondPlayerScore)
				{
					//numberOfTiesBeforeFinalOutcome++;

					if (statsOfFirstTieBeforeFinalRound == null)
						statsOfFirstTieBeforeFinalRound = new BattleStats(this.ConvertToByteArray());

					continue; // until there's no tie
				}

				break; // no tie => found a winner
			} while (true);
		}
	}
}
