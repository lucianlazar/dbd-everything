﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.World;

namespace Com.TheFallenGames.DBD.Data
{
    public class CharacterBattlePotential
	{
		public int MinFinalScore { get; private set; }
		public int MaxFinalScore { get; private set; }

		public Character MeAsCharacter { get { return _Char; } }
		public Player MeAsPlayer { get { return _CharAsPlayer; } }

		//public float AverageFinalScore { get { return (MinFinalScore + MaxFinalScore) / 2f; } }

		Character _Char;
		Player _CharAsPlayer;
		CharacterDesign _CharDesign;


		public CharacterBattlePotential(Character character)
		{
			_Char = character;
			_CharDesign = _Char.Design;
			_CharAsPlayer = _Char as Player;
		}


		public float GetChanceToWinAgainst(CharacterBattlePotential otherPotential)
		{
			//Debug.Log(otherPotential._CharDesign.name + ": " + MinFinalScore + ", " + MaxFinalScore + ", " + otherPotential.MinFinalScore + ", " + otherPotential.MaxFinalScore + ": " + Utils8.Instance.GetChanceSampleABiggerThanSampleB(MinFinalScore, MaxFinalScore, otherPotential.MinFinalScore, otherPotential.MaxFinalScore));
			return Utils8.Instance.GetChanceSampleABiggerThanSampleB(MinFinalScore, MaxFinalScore, otherPotential.MinFinalScore, otherPotential.MaxFinalScore);
		}


		public float ComputeWithOpponentAndGetWinChance(
			Character other,
			Cell.Descriptor myCellOnBattle = null, Cell.Descriptor otherCellOnBattle = null)
		{
			ComputeWithOpponent(other, myCellOnBattle, otherCellOnBattle);
			var otherPotential = new CharacterBattlePotential(other);
			otherPotential.ComputeWithOpponent(_Char, otherCellOnBattle, myCellOnBattle);

			return GetChanceToWinAgainst(otherPotential);
		}

		public void ComputeWithOpponent(
			Character other,
			Cell.Descriptor myCellOnBattle = null, Cell.Descriptor otherCellOnBattle = null)
		{
			int allowedNumberOfPlayedCardsForSelf = _CharAsPlayer == null ? 0 : _CharAsPlayer.Stats.RemainingBattleCardsToPlay;
			ComputeWithOpponent(other, allowedNumberOfPlayedCardsForSelf, myCellOnBattle, otherCellOnBattle);
		}

		public void ComputeWithOpponent(
			Character other, 
			int allowedNumberOfPlayedCardsForSelf,
			Cell.Descriptor myCellOnBattle = null, Cell.Descriptor otherCellOnBattle = null)
		{
			var otherCharAsPlayer = other as Player;
			int maxCardsToPlayByOther = otherCharAsPlayer == null ? 0 : otherCharAsPlayer.Stats.RemainingBattleCardsToPlay;
			ComputeWithOpponent(other, allowedNumberOfPlayedCardsForSelf, maxCardsToPlayByOther, myCellOnBattle, otherCellOnBattle);
		}

		public void ComputeWithOpponent(
			Character other, 
			int allowedNumberOfPlayedCards, int enemyAllowedNumberOfPlayedCards,
			Cell.Descriptor myCellOnBattle = null, Cell.Descriptor otherCellOnBattle = null)
		{
			CalculateMinMaxFinalScore(other, false, allowedNumberOfPlayedCards, enemyAllowedNumberOfPlayedCards, myCellOnBattle, otherCellOnBattle);
		}

		/// <summary> Useful when no effects can be further applied. Save computation time </summary>
		public void ComputeSelfWithoutOpponentOrSpecialEffects(Cell.Descriptor myCellOnBattle = null, Cell.Descriptor otherCellOnBattle = null)
		{
			CalculateMinMaxFinalScore(null, false, -1, -1, myCellOnBattle, otherCellOnBattle);
		}

		public void ComputeSelfOnlyWithOpponentSerpentIfAny(Character other, Cell.Descriptor myCellOnBattle = null, Cell.Descriptor otherCellOnBattle = null)
		{
			CalculateMinMaxFinalScore(other, true, -1, -1, myCellOnBattle, otherCellOnBattle);
		}

		public int ComputeFinalBattlePoints(out int atk, out int magic)
		{
			int finalBattlePoints = 0;
			atk = _Char.Stats[Data.Stat.ATK];
			int finalATK = atk; // this will change in the future
								//string magicMathString = "";

			//atkText.text = "= " + finalATK;

			if (_Char.Design.magicType == Data.MagicType.ADDITIVE)
			{
				magic = 0;
				int magicInstances = _Char.Stats[Data.Stat.SKILL];
				for (int i = 0, currentATKFromMagic; i < magicInstances; ++i)
				{
					currentATKFromMagic = GetOneRandomMagicInstance();
					//magicMathString += currentATKFromMagic;

					//if (i != magicInstances - 1)
					//	magicMathString += " + ";

					magic += currentATKFromMagic;
				}
			}
			else
			{
				magic = GetOneRandomMagicInstance();
			}


			//magicMathText.text = magicMathString;
			//magicText.text = "= " +  finalATKFromMagic;

			finalBattlePoints = ComposeFinalScore(finalATK, magic);
			//finalBattlePointsText.text = finalBattlePoints + "";

			return finalBattlePoints;
		}

		void CalculateMinMaxFinalScore(
			Character other,
			bool applySerpentOnly, 
			int allowedNumberOfPlayedCards, int enemyAllowedNumberOfPlayedCards,
			Cell.Descriptor myCellOnBattle, Cell.Descriptor otherCellOnBattle)
		{
			int atk = _Char.Stats[Data.Stat.ATK];
			int skill = _Char.Stats[Data.Stat.SKILL];

			if (other)
				ModifyByAllEffects(ref atk, ref skill, other, applySerpentOnly, allowedNumberOfPlayedCards, enemyAllowedNumberOfPlayedCards, myCellOnBattle, otherCellOnBattle);

			int minSkill = skill * _CharDesign.minSkillPower;
			int maxSkill = skill * _CharDesign.maxSkillPower;
			int minATK = atk; // this will change in the future
			int maxATK = atk; // this will change in the future
			MinFinalScore = ComposeFinalScore(minSkill, minATK);
			MaxFinalScore = ComposeFinalScore(maxSkill, maxATK);
		}

		void Select(List<Effect> effects, Effect.OwnerEnum effectDesiredOwner) { effects.RemoveAll(e => e.owner != effectDesiredOwner || (e.type != Stat.ATK && e.type != Stat.SKILL)); }

		void SelectMostPowerfulEffectsGroups(int maxCount, List<List<Effect>> effectGroups, CharacterDesign ownerDesign)
		{
			// The most powerful at end
			effectGroups.Sort((a, b) =>
			{
				var res = GetEffectGroupAveragePower(a, ownerDesign) - GetEffectGroupAveragePower(b, ownerDesign);
				if (res != 0f)
					return (int)(res * 1000); // dissolving the floating point small, but important differences (1.4 and 1.8 would've been considered equal if casted to int without first multiplying them with a big number)

				return 0;
			});

			while (effectGroups.Count > maxCount)
				effectGroups.RemoveAt(0);
		}

		float GetEffectGroupAveragePower(List<Effect> effectGroup, CharacterDesign ownerDesign)
		{
			float power = 0f;
			foreach (var eff in effectGroup)
			{
				if (eff.type == Stat.ATK)
					power += eff.amount;
				else if (eff.type == Stat.SKILL)
					power += ((ownerDesign.minSkillPower + ownerDesign.maxSkillPower) / 2f) * eff.amount;
			}

			return power;
		}

		void AddMostPowerfulInventoryItemsEffects(
			List<Effect> results, 
			Player itemsOwner, Character enemyOfItemsOwner, 
			Effect.OwnerEnum itemsDesiredOwner, 
			int allowedNumberOfPlayedCards,
			Cell.Descriptor itemsOwnerCellOnBattle, Cell.Descriptor itemsOwnerEnemyCellOnBattle)
		{
			var potentialEffectGroups = new List<List<Effect>>();
			foreach (var item in itemsOwner.Stats.ItemsInInventory)
			{
				var itemDesignAsEffectsItem = item.Design as IEffectsItem;
				if (itemDesignAsEffectsItem != null && item.Design.cardType == CardType.BATTLE)
				{
					var effects = itemDesignAsEffectsItem.GetEffects(itemsOwner, enemyOfItemsOwner, itemsOwnerCellOnBattle, itemsOwnerEnemyCellOnBattle);
					Select(effects, itemsDesiredOwner);
					potentialEffectGroups.Add(effects);
				}
			}

			// Only select as much groups as the number of cards that can be played, and the most powerful ones
			SelectMostPowerfulEffectsGroups(allowedNumberOfPlayedCards, potentialEffectGroups, itemsOwner.Design);
			foreach (var group in potentialEffectGroups)
				results.AddRange(group);
		}

		void ModifyByAllEffects(
			ref int atk, ref int skill, 
			Character other,
			bool applySerpentOnly, 
			int selfAllowedNumberOfPlayedCards, int enemyAllowedNumberOfPlayedCards,
			Cell.Descriptor myCellOnBattle, Cell.Descriptor otherCellOnBattle)
		{
			// Apply all battle effects applicable to self
			var allEffectsToApplyToSelf = new List<Effect>();
			List<Effect> effects;
			if (!applySerpentOnly)
			{
				effects = _CharDesign.GetAllBattleEffects(_Char, other, myCellOnBattle, otherCellOnBattle);
				Select(effects, Effect.OwnerEnum.SELF);
				allEffectsToApplyToSelf.AddRange(effects);


				// Apply enemy's battle effects applicable to his enemy (us)
				effects = other.Design.GetAllBattleEffects(other, _Char, otherCellOnBattle, myCellOnBattle);
				Select(effects, Effect.OwnerEnum.ENEMY);
				allEffectsToApplyToSelf.AddRange(effects);


				// Apply the atk/skill effects of all cards which are applicable to self IF we can play cards
				if (_CharAsPlayer != null)
					AddMostPowerfulInventoryItemsEffects(allEffectsToApplyToSelf, _CharAsPlayer, other, Effect.OwnerEnum.SELF, selfAllowedNumberOfPlayedCards, myCellOnBattle, otherCellOnBattle);
			}

			Player _OtherAsPlayer = other as Player;
			if (_OtherAsPlayer != null)
			{
				if (!applySerpentOnly)
				{
					// Apply the atk/skill effects of all enemy's cards which are applicable to his enemy (us)
					AddMostPowerfulInventoryItemsEffects(allEffectsToApplyToSelf, _OtherAsPlayer, _Char, Effect.OwnerEnum.ENEMY, enemyAllowedNumberOfPlayedCards, otherCellOnBattle, myCellOnBattle);
				}

				// Apply enemy's serpent, if any
				var serpentItemDesign = Design.Instance.monsters.SERPENT.rewardItem;
				if (_OtherAsPlayer.Stats.GetIndexOfItemWithDesign(serpentItemDesign) != -1)
				{
					effects = serpentItemDesign.GetEffects(_OtherAsPlayer, _Char, otherCellOnBattle, myCellOnBattle);
					Select(effects, Effect.OwnerEnum.ENEMY);
					allEffectsToApplyToSelf.AddRange(effects);
				}
			}

			// Finally, modify the atk and skill with all the collected effects
			ModifyByEffects(ref atk, ref skill, allEffectsToApplyToSelf);
		}

		void ModifyByEffects(ref int atk, ref int skill, List<Effect> effects)
		{
			foreach (var eff in effects)
				if (eff.type == Stat.ATK)
					atk += eff.amount;
				else if (eff.type == Stat.SKILL)
					skill += eff.amount;
		}


		int ComposeFinalScore(int atk, int magic)
		{
			if (_Char.Design.magicType == Data.MagicType.ADDITIVE)
				return atk + magic;

			return atk * magic;
		}

		int GetOneRandomMagicInstance()
		{
			return UnityEngine.Random.Range(_CharDesign.minSkillPower, _CharDesign.maxSkillPower + 1);
		}
	}
}
