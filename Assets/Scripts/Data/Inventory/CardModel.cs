﻿using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Data.Inventory
{
    public class CardModel
    {
        public EntityDesign design;
        public string title { get { return design.name; } }
        public string description;

		// Filled manually
		//[NonSerialized] // sometimes this 
		public EntityCategoryCardDesign cardCategoryDesign = new EntityCategoryCardDesign();
	}
}
