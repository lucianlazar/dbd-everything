﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Data
{
    public class MonsterStats : CharacterStats
    {
		public new Monster StatsOwner { get { return _StatsOwner == null ? (_StatsOwner = base.StatsOwner as Monster) : _StatsOwner; } }
		Monster _StatsOwner; // for caching only;


        public MonsterStats(Monster statsOwner) : base(statsOwner, statsOwner.Design.level)
        {
        }
	}
}
