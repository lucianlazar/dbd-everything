﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Com.TheFallenGames.DBD.Pathfinding;

namespace Com.TheFallenGames.DBD.Data
{
    public class PlayerStats : CharacterStats
	{
		public const int MAX_XP = 55;

		public new Player StatsOwner { get { return _StatsOwner == null ? (_StatsOwner = base.StatsOwner as Player) : _StatsOwner; } }
		Player _StatsOwner; // for caching only;

		public int XP { get; private set; } // 1...55
        public int Ore { get; set; }
        public int Treasures { get { return GetInventoryItemsOfType<Treasure>().Length; } }
        public Totem[] Totems { get { return _Totems; } }

        #region Per-turn stats
        public int NumMovesConsumed { get; set; }
        public int NumDungeonPushesConsumed { get; set; }
        public int NumBattleCardsPlayed { get; set; }
		public bool AcquiredTotem { get; set; }
        public int RemainingDungeonPushes { get { return this[Stat.DUNGEON_PUSH] - NumDungeonPushesConsumed; } }
        public int RemainingMoves { get { return this[Stat.MOVES] - NumMovesConsumed; } }
		public int RemainingBattleCardsToPlay { get { return _StatsOwner.Design.canPickUpItems ? (Design.Instance.general.maxCardsPlayedPerBattle - NumBattleCardsPlayed) : 0; } }
		#endregion
		
        Totem[] _Totems = new Totem[0];

        public PlayerStats(Player statsOwner) : base(statsOwner)
        {
            XP = 1;

			_BaseStats[Stat.MOVES] = StatsOwner.Design.numMoves;
			_BaseStats[Stat.DUNGEON_PUSH] = StatsOwner.Design.numDungeonPushes;
        }

        protected override string GetStringBeforeEffects()
        {
            return "XP: " + XP +
                   "\nMoves: " + NumMovesConsumed + " of " + Utils.GetStatsRichTextWithBonus("", _BaseStats[Stat.MOVES], this[Stat.MOVES] - _BaseStats[Stat.MOVES], true) +
                   "Dungeon pushes: " + NumDungeonPushesConsumed + " of " + Utils.GetStatsRichTextWithBonus("", _BaseStats[Stat.DUNGEON_PUSH], this[Stat.DUNGEON_PUSH] - _BaseStats[Stat.DUNGEON_PUSH], true) +
                   "Treasures: " + Treasures +
                   "\nOre: <color=yellow>" + Ore + "</color>";
        }

		public void AqcuireTotem(Totem totem)
        {
            var newList = new List<Totem>(_Totems);
            newList.Add(totem);
            _Totems = newList.ToArray();

            AddEffect(totem.Design.effect);
            AcquiredTotem = true;
        }

        public void ReleaseTotem(Totem totem)
        {
            RemoveEffect(totem.Design.effect);

            var newList = new List<Totem>(_Totems);
            newList.Remove(totem);
            _Totems = newList.ToArray();
        }

        public Totem[] GetOwnedTotemsOfType(TotemType type)
        {
            var design = Design.Instance.totems[type];
            return GetOwnedTotemsOfType(design);
        }

        public Totem[] GetOwnedTotemsOfType(TotemDesign design)
        {
            return Array.FindAll(_Totems, t => t.Design == design);
        }

        public void UpdateStatsOnTurnStart()
        {
			NumDungeonPushesConsumed = 0;
            NumMovesConsumed = 0;
			NumBattleCardsPlayed = 0;
			AcquiredTotem = false;
        }

		public void UpdateStatsOnBattleTurnStart()
		{
			NumBattleCardsPlayed = 0;
		}

        /// <summary> Returns the number of levels that the character has advanced after the xp has increased </summary>
        public void UpdateStatsOnEnemyKilled(Character losingCharacter)
        {
			Player losingCharAsPlayer = losingCharacter as Player;

			// XP
			//int xpToGive = losingCharAsPlayer == null ? losingCharacter.Design.xpGivenOnDeath : losingCharacter.Stats.Level;
			int xpToGive = losingCharacter.Design.xpGivenOnDeath;
			if (xpToGive == -1) // increase by the defeated player's level
				xpToGive = losingCharacter.Stats.Level;
			IncreaseXP(xpToGive);

			bool canPickUpItems = StatsOwner.Design.canPickUpItems;
			if (losingCharAsPlayer)
			{
				// Ore
				Ore += losingCharAsPlayer.Stats.Ore;
				losingCharAsPlayer.Stats.Ore = 0;

				if (canPickUpItems)
				{
					// Transfer all items for which the winner has available slots
					while (FreeInventorySlots > 0)
					{
						if (losingCharAsPlayer.Stats.ItemsInInventory.Length == 0)
							break;

						losingCharAsPlayer.Stats.ItemsInInventory[0].Owner = StatsOwner;
					}
				}
				else
				{
					// If we are an animal, the items became owned by no one (i.e. are put into the discard pile and will be respawned in the next turn)
					while (losingCharAsPlayer.Stats.ItemsInInventory.Length > 0)
						losingCharAsPlayer.Stats.ItemsInInventory[0].Owner = null;
				}
			}
		}

		/// <summary> Returns the number of levels that the character has advanced after the xp has increased </summary>
		public int UpdateStatsOnTurnEnd()
		{
			RemoveEffectsByDuration(Effect.DurationEnum.UNTIL_TURN_END);

			int xp = 0;
            foreach (var effect in _Effects)
            {
                if (effect.type == Stat.XP_PER_TURN)
                {
                    xp += effect.amount;
                }
            }
            return IncreaseXP(xp);
        }


		//internal void ConsumeItem(int itemIndexInInventory) { ConsumeItem(ItemsInInventory[itemIndexInInventory]); }

		//internal void ConsumeItem(BaseItem item)
		//{
		//	if (RemainingBattleCardsToPlay < 1)
		//		throw new UnityException("RemainingBattleCardsToPlay < 1");

		//	if (item.Design.cardType == CardType.BATTLE)
		//		++NumBattleCardsPlayed;

		//	// Commented: the item will remove itself
		//	//RemoveItemsFromInventory(item);
		//	item.ConsumeByCurrentOwner();
		//}


		Effect GetFirstEffectForStat(Stat stat, bool preferDynamicallyCreated)
		{
			Effect first = null;
			foreach (var eff in _Effects)
				if (eff.type == stat)
				{
					if (first == null)
						first = eff;

					if (!preferDynamicallyCreated)
						break;

					if (eff.dynamicallyCreated)
						return eff;
				}

			return first;
		}

		/// <summary>returns true if the effect was found</summary>
		public void ChangeTemporaryStat(Stat stat, int amountChange/*, bool removeIfReachedZeroOrNegative*/, bool createIfNotFound)
		{
			var eff = GetFirstEffectForStat(stat, true);

			if (eff == null && !createIfNotFound)
				return;

			if (eff == null || !eff.dynamicallyCreated)
			{
				eff = Effect.CreateDinamically(stat, 0, Effect.OwnerEnum.SELF, Effect.DurationEnum.UNTIL_TURN_END);
				AddEffect(eff);
			}
			eff.amount += amountChange;

			//if (removeIfReachedZeroOrNegative && eff.amount <= 0)
			//	RemoveEffect(eff);
		}

		internal void ReleaseTreasures(int amount, bool temporary)
        {
            var treasures = GetInventoryItemsOfType<Treasure>();
            if (treasures.Length < amount)
                throw new UnityException("ReleaseTreasures: (requested) " + amount + " > (having)" + treasures.Length);

            for (int i = 0; i < amount; ++i)
				treasures[i].Owner = null;
        }

        //protected override string GetStatsPerLevelString()
        //{

        //}

        /// <summary> Returns the number of levels that the character has advanced after the xp has increased </summary>
        int IncreaseXP(int xp)
        {
            int newXP = Math.Min(MAX_XP, XP + xp);
            if (newXP == XP)
                return 0;

            XP = newXP;
            return UpdateLevel();
        }

        int UpdateLevel()
        {
            int oldLevel = Level;
            Level = (int)(Math.Sqrt(XP * 8 + 1) - 1) / 2;

            int diff = Level - oldLevel;

            // Update base stats
            if (diff != 0)
                UpdateBaseState();

            return diff;
        }

		internal void RemoveOwnershipFromAllItems()
		{
			while (_ItemsInInventory.Length > 0)
				_ItemsInInventory[0].Owner = null;
		}

		internal void SubtractMovementCosts(PathfindingCurrency cost)
		{
			NumMovesConsumed += cost.moves;

			if (cost.passingsThroughWalls > 0)
				ChangeTemporaryStat(Stat.MOVES_THROUGH_WALLS, -cost.passingsThroughWalls, false);
			if (cost.passingsThroughEnemies > 0)
				ChangeTemporaryStat(Stat.BATTLE_EVADING, -cost.passingsThroughEnemies, false);
			if (cost.diagonalMoves > 0)
				ChangeTemporaryStat(Stat.DIAGONAL_MOVES, -cost.diagonalMoves, false);
			if (cost.ghostMoves > 0)
				ChangeTemporaryStat(Stat.GHOST_MOVES, -cost.ghostMoves, false);
		}

		//int GetTotalXPNeededForNextLevel()
		//{

		//} 
	}
}
