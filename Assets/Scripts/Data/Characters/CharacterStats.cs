﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Data
{
    public class CharacterStats
    {
		public event Action<BaseItem[]> ItemsRemoved, ItemsAdded;

		public Character StatsOwner { get; internal set; }

		public int Level { get; protected set; } // 1..10
        public int NumEffects { get { return _Effects.Count; } }
        public BaseItem[] ItemsInInventory { get { return _ItemsInInventory; } }
        public int FreeInventorySlots { get { return Design.Instance.general.inventoryCapacity - ItemsInInventory.Length; } }
		public int NumBattleItemsInInventory { get { return Array.FindAll(ItemsInInventory, item => item.Design.cardType == CardType.BATTLE).Length; } }
		public bool InBattle { get; set; }
		public bool KnowsDragonsVoice { get; set; }
		public bool HasSoul { get; set; }
		public bool HasSpider { get { return GetIndexOfItemWithDesign(Design.Instance.monsters.SPIDER.rewardItem) != -1; } }

		protected Dictionary<Stat, int> _BaseStats = new Dictionary<Stat, int>();
        protected List<Effect> _Effects = new List<Effect>();
        protected BaseItem[] _ItemsInInventory = new BaseItem[0];
        protected CharacterDesign _CharDesign;


        public CharacterStats(Character statsOwner, int startingLevel = 1)
        {
			StatsOwner = statsOwner;
            _CharDesign = StatsOwner.Design;
			Level = startingLevel;

			foreach (var eff in _CharDesign.initialEffects)
				AddEffect(eff);

            UpdateBaseState();
        }


        public int this[Stat stat]
        {
            get
            {
                int value = 0;
                if (_BaseStats.ContainsKey(stat))
                    value += _BaseStats[stat];

                foreach (var effect in _Effects)
                    if (effect.type == stat)
                        value += effect.amount;

				// Never should ATK drop below 0
				if (stat == Stat.ATK && value < 0)
					value = 0;

				// Never should MAGIC drop below 0
				if (stat == Stat.SKILL && value < 0)
					value = 0;

				return value;
            }
            //set
            //{

            //}
        }

        public override string ToString()
        {
			return
				_CharDesign.name + " Level " + Level + "\n" +
				GetStatsPerLevelString() + "\n" +
				(KnowsDragonsVoice ? "<color=red>Learned Dragon's Voice</color>\n" : "") +
				GetStringBeforeEffects() + "\n" +
				EffectsToString();
        }

		protected virtual string GetStringBeforeEffects()
		{
			return "";
		}

		public string EffectsToString()
		{
			string s = " Effects: <i>";
			if (NumEffects == 0)
				s += "none";
			else
			{
				s += "->\n";
				for (int i = 0; i < NumEffects; ++i)
					s += "  " + GetEffect(i).ToShortString() + (i % 2 == 1 ? "\n" : ", ");
			}

			return s + "</i>";
		}

        public void AddEffect(Effect effect)
        {
            _Effects.Add(effect);
        }

        public void RemoveEffect(Effect effect)
        {
            _Effects.Remove(effect);
        }

		internal void RemoveEffectsForStat(Stat statType)
		{
			_Effects.RemoveAll(e => e.type == statType);
		}

		public Effect GetEffect(int index)
        {
            return _Effects[index];
        }

        public T[] GetInventoryItemsOfType<T>() where T : BaseItem
        {
            return Array.ConvertAll(Array.FindAll(_ItemsInInventory, t => typeof(T).IsAssignableFrom(t.GetType())), t => t as T);
        }

        public void AddItemsToInventory(params BaseItem[] items)
        {
            var newList = new List<BaseItem>(_ItemsInInventory);
            newList.AddRange(items);
            _ItemsInInventory = newList.ToArray();

			// Add the passive effects
			foreach (var item in items)
			{
				var it = item.Design as CommonItemDesign;
				if (it != null)
					foreach (var eff in it.GetPassiveEffects())
						AddEffect(eff);
			}

			if (ItemsAdded != null)
				ItemsAdded(items);

		}

        public void RemoveItemsFromInventory(params BaseItem[] items)
        {
            var newList = new List<BaseItem>(_ItemsInInventory);
			foreach (var v in items)
				newList.Remove(v);
            _ItemsInInventory = newList.ToArray();

			// Remove any passive effects
			foreach (var item in items)
			{
				var it = item.Design as CommonItemDesign;
				if (it != null)
					foreach (var eff in it.GetPassiveEffects())
						RemoveEffect(eff);
			}

			if (ItemsRemoved != null)
				ItemsRemoved(items);
		}

		public int GetIndexOfItem(BaseItem item) { return GetIndexOfItemWithDesign(item.Design); }
		public int GetIndexOfItemWithDesign(BaseItemDesign itemDesign)
		{
			int index = 0;
			foreach (var v in _ItemsInInventory)
			{
				if (v.Design == itemDesign)
					return index;
				++index;
			}

			return -1;
		}


		// Called for both parties
		internal virtual void UpdateStatsOnBattleStarted(Character enemy)
		{
			EffectManager.Instance.CastEffects(StatsOwner, enemy, StatsOwner.name + "'s Battle effects", _CharDesign.GetAllBattleEffects(StatsOwner, enemy).ToArray());
		}

		internal virtual void UpdateStatsOnBattleEnded()
		{
			RemoveEffectsByDuration(Effect.DurationEnum.UNTIL_BATTLE_END);
		}

		internal virtual void RemoveEffectsByDuration(Effect.DurationEnum duration)
		{
			_Effects.RemoveAll(e => {
				/*Debug.Log("Remove " + e.type + ", " + e.duration + ", " + duration + "? " + (e.duration == duration));*/
				 return e.duration == duration; });
		}

		protected void UpdateBaseState()
        {
			var stats = GetStatsForCurrentLevel();

			_BaseStats[Stat.ATK] = stats.atk;
            _BaseStats[Stat.SKILL] = stats.magic;
        }

        protected virtual string GetStatsPerLevelString()
        {
            return GetStatsForCurrentLevel()
					.ToStringRichWithBonuses(
                        this[Stat.ATK] - _BaseStats[Stat.ATK], 
                        this[Stat.SKILL] - _BaseStats[Stat.SKILL]
                    );
        }

		int GetNumLevels() { return _CharDesign.statsPerLevel.Length; }
		int GetIndexOfStatsForCurrentLevel() { return Mathf.Max(Mathf.Min(GetNumLevels(), Level) - 1); }
		Stats GetStatsForCurrentLevel() { return _CharDesign.statsPerLevel[GetIndexOfStatsForCurrentLevel()]; }
	}
}
