﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Data.HitAssets
{
	[Serializable]
	public class CharacterHitAsset
	{
		public Sprite[] animationFrames;
		public GameObject[] miscGameObjects;

		public GameObject GetRandomMiscGameObject() { return miscGameObjects[UnityEngine.Random.Range(0, miscGameObjects.Length)]; }
	}
}
