﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Data.HitAssets
{
	[Serializable]
    public class CharacterHitAssets
	{
		public CharacterHitAsset[] assets;

		public bool AssetsLoaded { get { return assets != null && assets.Length > 0; } }

		public void LoadSprites(string parentFolderPathInResources, params string[] hitNames)
		{
			assets = new CharacterHitAsset[hitNames.Length];
			for (int i = 0; i < hitNames.Length; ++i)
			{
				assets[i] = new CharacterHitAsset();
				var sprites = Resources.LoadAll<Sprite>(parentFolderPathInResources + "/" + hitNames[i]);
				assets[i].animationFrames = sprites;
			}
		}

		public void LoadGameObjects(string parentFolderPathInResources, params string[] hitNames)
		{
			assets = new CharacterHitAsset[hitNames.Length];
			for (int i = 0; i < hitNames.Length; ++i)
			{
				assets[i] = new CharacterHitAsset();
				var gos = Resources.LoadAll<GameObject>(parentFolderPathInResources + "/" + hitNames[i]);
				assets[i].miscGameObjects = new GameObject[gos.Length];
				for (int j = 0; j < gos.Length; j++)
				{
					assets[i].miscGameObjects[j] = GameObject.Instantiate<GameObject>(gos[j]);
					assets[i].miscGameObjects[j].SetActive(false);
					assets[i].miscGameObjects[j].transform.SetParent(HitEffectsHolder.Instance.transform);
				}
			}
		}

		public CharacterHitAsset GetRandomHit() { return assets[UnityEngine.Random.Range(0, assets.Length)]; }

	}
}
