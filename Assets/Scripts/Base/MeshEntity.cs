﻿using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Base
{
    public abstract class MeshEntity : Entity, IMeshHolder
    {
        public Transform MeshHolder
        {
            get
            {
                if (!_MeshHolder)
                    _MeshHolder = transform.Find("MeshHolder");
                return _MeshHolder;
            }
        }

        public GameObject MeshGameObject { get; protected set; }

        public virtual string MeshPathInResources { get { return MainAssetParentFolderPathInResources + "/" + MeshNameInResources; } }
        public virtual string MeshNameInResources { get { return name; } }

        Transform _MeshHolder;


        protected override void Initialize()
        {
            base.Initialize();

            if (!string.IsNullOrEmpty(MeshPathInResources))
			{
                MeshGameObject = MeshLoader.LoadMesh(this, true);

				// Initialize the animated sprite component, if any
				var animatedSprite = MeshGameObject.GetComponent<AnimatedSprite>();
				if (animatedSprite)
				{
					animatedSprite.Initialize(MainAssetParentFolderPathInResources + "/IdleAnimation");

					// Setting the first frame to also be the icon, if none was specified
					if (!Design.icon)
						Design.icon = animatedSprite.sprites[0];
				}
			}
        }
    }
}
