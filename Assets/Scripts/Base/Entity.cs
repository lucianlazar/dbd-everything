﻿using Assets.Scripts.Common.Interfaces;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Base
{
    public abstract class Entity : DBDObject, IWorldObjectWithTitle, InputToEvent.IEventHandler
	{
		public static event Action<Entity> EntityLongPressed;

        public static T InstantiateEntity<T>(T entityScriptOnPrefab, EntityDesign design) where T : Entity
        {
            var instance = GameObject.Instantiate(entityScriptOnPrefab.gameObject) as GameObject;
            var scriptInstance = instance.GetComponent<T>();
            (scriptInstance as Entity).Design = design; // using (x as Entity) because the derived types hide the base's design property, but they rely on it in Initialize()
            scriptInstance.Initialize();

            return scriptInstance;
        }

        public EntityDesign Design { get; private set; }

		public abstract string EntityCategory { get; }
		public virtual string MainAssetParentFolderPathInResources { get { return EntityCategory + "s/" + GetType().Name + "s"; } }

		public Text TitleText { get; set; }
		string _DisplayName = "";

		Block _ParentAsBlock;

		protected virtual void Awake()
		{
			UpdateParentAsBlock();
			UIUtils.UnifyCanvasAndInitTitle(this, "NameText");
			//Debug.Log(name + ": " + TitleText);
			if (TitleText)
			{
				// Temporary, to avoid overlapping with other texts
				TitleText.transform.Rotate(0, 0, 15);
			}
		}

		void OnTransformParentChanged() { UpdateParentAsBlock(); }

		//public Block GetParentAsBlock() { return transform.parent == null ? null : transform.parent.GetComponent<Block>(); }
		//public Cell GetParentCell() { return GetParentAsBlock().GetParentAsCell(); }
		public Block GetParentAsBlock() { return _ParentAsBlock; }
		public Cell GetParentCell() { return _ParentAsBlock.GetParentAsCell(); }

		public RelativePositionSimple RelativePosSimpleTo(Entity other) { return GetParentCell().Desc.RelativePosSimpleTo(other.GetParentCell().Desc); }

		public virtual void RespawnOnBlock(Block newParent, Vector3? positionDelta = null, bool setRotation = false)
		{
			SetParentBlock(newParent, positionDelta, setRotation);
			OnRespawned();
		}

		public virtual void SetParentBlock(Block newParent, Vector3? positionDelta = null, bool setRotation = false)
		{
            var oldParent = GetParentAsBlock();
            if (oldParent)
			{
				if (oldParent == newParent)
					return;

				RemoveChildFromParent(oldParent, this);
			}
            if (newParent)
				AddChildToParent(newParent, this, (positionDelta ?? Vector3.zero), setRotation);
            gameObject.SetActive(newParent);
		}

		#region implementation for InputToEvent.IEventHandler
		public void OnPress(BaseEventData eventData) { }
		public virtual void OnLongPress(BaseEventData eventData)
		{
			// Won't be considered a long-click on Entity, because it moved too much
			if (InputToEvent.Instance.DragVector.magnitude > C.MAX_DRAG_SCREEN_DISTANCE_ALLOWED_TO_STILL_CONSIDER_LONG_CLICK_ON_ENTITY)
				return;

			if (EntityLongPressed != null)
				EntityLongPressed(this);
		}
		public void OnPressCanceled(BaseEventData eventData) { }
		public void OnRelease(BaseEventData eventData) { }
		public void OnReleaseAfterCanceled(BaseEventData eventData) { }
		public void OnClick(BaseEventData eventData) { }
		public void OnDoubleClick(BaseEventData eventData) { }
		#endregion

		internal virtual CardModel GetCardModel() { return Design.GetCardModel(); }

		protected virtual void Initialize()
        {
            name = Design.name;
            SetDisplayName(Design.name);
        }

		public string GetDisplayName() { return _DisplayName; }

        public virtual void SetDisplayName(string displayName)
        {
			_DisplayName = displayName;
            if (TitleText)
			{
				bool valid = !string.IsNullOrEmpty(displayName);
				TitleText.enabled = valid;
				if (valid)
					TitleText.text = displayName;
            }
		}

		protected virtual void OnRespawned()
		{
		}

		void UpdateParentAsBlock() { _ParentAsBlock = transform.parent == null ? null : transform.parent.GetComponent<Block>(); }
	}
}
