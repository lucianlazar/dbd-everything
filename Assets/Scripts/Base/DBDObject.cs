﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Base
{
	public class DBDObject : MonoBehaviour
	{
		protected List<Entity> _Children = new List<Entity>();


		// Hackish way of calling AddChild and RemoveChild on a Block from the Entity.SetParentBlock callback
		// The standard way doesn't work because protected members can only be called inside the class they are declared in or inside a derived class of that
		protected static void AddChildToParent(DBDObject parent, Entity entity, Vector3 positionDelta, bool setRotation)
		{ parent.AddChild(entity, positionDelta, setRotation); }

		protected static void RemoveChildFromParent(DBDObject parent, Entity entity)
		{ parent.RemoveChild(entity); }


		void AddChild(Entity entity, Vector3 positionDelta, bool setRotation)
		{
			entity.transform.position = transform.position + positionDelta;
			if (setRotation)
				entity.transform.rotation = transform.rotation;
			entity.transform.parent = transform;
			_Children.Add(entity);
		}

		void RemoveChild(Entity entity)
		{
			_Children.Remove(entity);
			entity.transform.parent = null;
		}

		public Entity GetChild(int index) { return _Children[index]; }

		public int GetIndexOfChild(Entity entity) { return _Children.IndexOf(entity); }

		public List<T> GetChildrenOfType<T>() where T : Entity
		{
			var typeOfT = typeof(T);
			var results = new List<T>();
			foreach (var e in _Children)
				if (typeOfT.IsAssignableFrom(e.GetType()))
					results.Add(e as T);

			return results;
		}
	}
}
