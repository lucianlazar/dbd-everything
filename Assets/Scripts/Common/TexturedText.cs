﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Common
{
	//[ExecuteInEditMode]
	public class TexturedText : MonoBehaviour
	{
		[SerializeField]
		Text _MaskText;

		[SerializeField]
		Text _EffectsText;

		[SerializeField]
		string _Text;

		[SerializeField]
		bool updateInEditMode;

		public string Text
		{
			get { return _Text; }
			set
			{
				_Text = value;
				OnValueChanged();
			}
		}



		void Awake()
		{
			if (_MaskText)
			{
				if (_MaskText.GetComponent<Mask>() == null)
					_MaskText.gameObject.AddComponent<Mask>();
			}
		}

#if UNITY_EDITOR
		void Update()
		{
			OnValueChanged();
		}
#endif

		void OnValueChanged()
		{
			UpdateMaskText();
			UpdateEffectsText();
		}

		void UpdateMaskText()
		{
			if (_MaskText)
				_MaskText.text = _Text;
		}

		void UpdateEffectsText()
		{
			if (_EffectsText)
				_EffectsText.text = _Text;
		}
	}
}
