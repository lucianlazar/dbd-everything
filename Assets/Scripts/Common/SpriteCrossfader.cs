﻿using Com.TheFallenGames.DBD.Common.Interfaces;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Common
{
    internal class SpriteCrossfader
	{
		public Sprite first
		{
			set
			{
				firstRenderer.sprite = value;
			}
		}

		public Sprite second
		{
			set
			{
				secondRenderer.sprite = value;
			}
		}


		public float CrossfadeAmount01
		{
			set
			{
				Color firstColor = firstRenderer.color, secondColor = secondRenderer.color;
				if (value < secondSpriteFadeInStartMoment01)
				{
					// No crossfading yet
					firstColor.a = 1f;
					secondColor.a = 0f;
				}
				else
				{
					if (value < firstSpriteFadeOutStartMoment01) // fadein the second, while keeping the first fully visible
					{
						firstColor.a = 1f;
						secondColor.a = (value - secondSpriteFadeInStartMoment01) / fadeInOutDuration01;
					}
					else // once the second is fully visible; fade out the first
					{
						firstColor.a = (1f - value) / fadeInOutDuration01;
						secondColor.a = 1f;
					}
				}

				firstRenderer.color = firstColor;
				secondRenderer.color = secondColor;
			}
		}

		SpriteRenderer firstRenderer, secondRenderer;
		float fadeInOutDuration01, secondSpriteFadeInStartMoment01, firstSpriteFadeOutStartMoment01;

		public SpriteCrossfader (SpriteRenderer firstRenderer, SpriteRenderer secondRenderer, float fadingStartMoment01 = 0f)
		{
			this.firstRenderer = firstRenderer;
			this.secondRenderer = secondRenderer;
			fadingStartMoment01 = Mathf.Clamp(fadingStartMoment01, .001f, .99f);
			fadeInOutDuration01 = (1f - fadingStartMoment01) / 2;
			secondSpriteFadeInStartMoment01 = fadingStartMoment01;
			firstSpriteFadeOutStartMoment01 = secondSpriteFadeInStartMoment01 + fadeInOutDuration01;
		}
    }
}
