﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common.Interfaces
{
	public interface IWorldObjectWithTitle
	{
		Text TitleText { get; set; }
		Transform transform { get; }
	}
}
