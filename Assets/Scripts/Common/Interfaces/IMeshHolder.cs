﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Common.Interfaces
{
    interface IMeshHolder
    {
        string MeshPathInResources { get; }
        UnityEngine.Transform MeshHolder { get; }
    }
}
