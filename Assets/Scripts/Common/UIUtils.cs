﻿using Assets.Scripts.Common.Interfaces;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Common
{
    public static class UIUtils
	{
		public static void UnifyCanvasAndInitTitle(IWorldObjectWithTitle @this, string titlePathRelativeToCanvas)
		{
			var canvas = @this.transform.Find("Canvas");
			if (canvas)
			{
				if (!string.IsNullOrEmpty(titlePathRelativeToCanvas))
				{
					var goText = canvas.Find(titlePathRelativeToCanvas);
					if (goText)
						@this.TitleText = goText.GetComponent<Text>();
				}

				UnifiedWorldCanvas.Instance.SwallowCanvas(canvas);
			}

		}
	}
}
