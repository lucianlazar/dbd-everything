﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Pathfinding;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Common
{
	public class BoardHelper
	{
		Board _Board;
		int _NumRows, _NumColumns;
		Cell[][] _Cells;
		OuterCell _OuterCell;


		public BoardHelper(Board board)
		{
			_Board = board;
			_NumRows = _Board.NumRows;
			_NumColumns = _Board.NumColumns;
			_Cells = _Board.Cells;
			_OuterCell = _Board.OuterCell;
		}

	}
}
