﻿using UnityEngine;
using System.Collections;

namespace Com.TheFallenGames.DBD.Common
{
	public class SpriteSwitcher : MonoBehaviour
	{
		public Sprite[] sprites;

		protected SpriteRenderer _SpriteRenderer;
		protected int _CurrentSpriteIndex = -1;


		protected virtual void Awake()
		{
			_SpriteRenderer = GetComponent<SpriteRenderer>();
		}


		public virtual void CycleSprite() { SwitchSprite((_CurrentSpriteIndex + 1) % sprites.Length); }

		public virtual void SwitchSprite(int index)
		{
			_CurrentSpriteIndex = index;
			SwitchSprite(sprites[_CurrentSpriteIndex]);
		}

		public virtual void SwitchSprite(Sprite sprite) { _SpriteRenderer.sprite = sprite; }
	}
}
