﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Config.Characters;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Pathfinding;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Common
{
	public static class Utils
	{
		/// <summary>
		/// end=exclusively
		/// </summary>
		/// <param name="start"></param>
		/// <param name="endExcl"></param>
		/// <param name="num"></param>
		/// <returns></returns>
		public static byte[] ChooseRandomAsBytes(int start, int endExcl, int num) { return ChooseRandomAsBytes(start, endExcl, num, new byte[0]); }
		public static byte[] ChooseRandomAsBytes(int start, int endExcl, int num, byte[] ignoredValues)
		{
			//string s = "";
			//foreach (var v in ignoredValues)
			//	s += " " + v;

			//UnityEngine.Debug.Log(start + "-" + end + "-" + num + ": [" + s + "]");

			int numAll = endExcl - start;
			int numIgnored = ignoredValues.Length;
			//int numAllWithoutIgnored = numAll - numIgnored;
			var indices = new List<byte>(numAll);
			byte[] result = new byte[num];

			// Add all
			for (byte i = 0; i < numAll; ++i)
				indices.Add(i);
			// Remove ignored
			for (byte i = 0; i < numIgnored; ++i)
				indices.Remove(ignoredValues[i]);

			int ranIdx;
			for (int i = 0; i < num; ++i)
			{
				ranIdx = UnityEngine.Random.Range(0, indices.Count);
				result[i] = indices[ranIdx];
				indices.RemoveAt(ranIdx);
			}

			//s = "";
			//foreach (var v in result)
			//	s += " " + v;
			//UnityEngine.Debug.Log("result("+result.Length+"): " + s);

			return result;
		}

		public static T[] GetAllFieldsOfType<T>(object instance) where T : class
		{
			var fields = instance.GetType().GetFields();
			var allTs = Array.FindAll(fields, f => typeof(T).IsAssignableFrom(f.FieldType));

			return Array.ConvertAll(allTs, tFieldInfo => tFieldInfo.GetValue(instance) as T);
		}

		public static string GetStatsRichTextWithBonus(Stat stat, int value, int bonus, bool appendNewLine = false)
		{ return GetStatsRichTextWithBonus(Enum.GetName(typeof(Stat), stat), value, bonus, appendNewLine); }
		public static string GetStatsRichTextWithBonus(string name, int value, int bonus, bool appendNewLine = false)
		{ return name + " " + value + (bonus == 0 ? "" : ("<color=lime> + " + bonus + "</color>")) + (appendNewLine ? "\n" : ""); }



		public static void RandomizeList<T>(List<T> list)
		{
			int n = list.Count;
			while (n > 1)
			{
				int k = UnityEngine.Random.Range(0, n--);
				var t = list[n];
				list[n] = list[k];
				list[k] = t;
			}
		}

		public static bool IsTrueOrThrow(bool b) { if (!b) throw new InvalidOperationException(); return true; }

		/// <summary>
		/// Returns wether the out cell is directly reachable. 
		/// </summary>
		public static ClosestCellResult SortCellsByClosestAndGetClosest(
			Player actor,
			Board.Descriptor boardDescriptor,
			List<Cell.Descriptor> cells,
			ref List<Cell.Descriptor> directlyReachable,
			ref List<Cell.Descriptor> indirectlyReachable,
			Cell.Descriptor originOverride = null
		)
		{
			//bool directNull = directlyReachable == null;
			bool indirectNull = indirectlyReachable == null;

			directlyReachable = new List<Cell.Descriptor>(cells);
			indirectlyReachable = new List<Cell.Descriptor>();

			var actorCellDesc = boardDescriptor.GetExistingCellDesc(actor.GetParentCell());
			var actorCellDescToUseInPFC = originOverride == null ? actorCellDesc : originOverride;

//#if UNITY_EDITOR
//			if (actorCellDesc == actorCellDescToUseInPFC && cells.Contains(actorCellDesc))
//				throw new UnityException("SortCellsByClosestAndGetClosest: 'cells' is not allowed to include the actor's cell, IF originOverride is null or the same as the actor's ");
//#endif

			var real_budget = actor.GetPathfindingBudget(false);
			var enemiesInfo = new EnemiesInfo(actor);
			var real_comparer = new PathfindingCurrency.Comparer_PrioritizeConsumingFewerTotalMovesWhenResourcesKnown(actor.Design);
			var real_pfc = new PathfindingContext(/*actor,*/ actorCellDescToUseInPFC, real_budget, real_comparer, enemiesInfo);
			boardDescriptor.ComputeBestPaths(real_pfc);
			bool addActorCellUsedInPFC = false;
			for (int i = 0; i < directlyReachable.Count;)
			{
				var c = directlyReachable[i];
				if (c == actorCellDescToUseInPFC)
				{
					addActorCellUsedInPFC = true;
					directlyReachable.RemoveAt(i);
					continue;
				}

				// asd IsTrueOrThrow(originOverride != null && originOverride != c)
				//else 
				if (
					//// return own cell, since it was provided to us. also doing a debug check for the moment
					//c == actorCellDesc && IsTrueOrThrow(originOverride != null && originOverride != c) 
					//|| 
					real_pfc.mapDestCellToBestSolution.ContainsKey(c))
				{
					++i;
					continue;
				}

				indirectlyReachable.Add(c);
				directlyReachable.RemoveAt(i);
			}

			directlyReachable.Sort((a, b) => real_comparer.Compare(real_pfc.mapDestCellToBestSolution[a].cost, real_pfc.mapDestCellToBestSolution[b].cost));
			if (addActorCellUsedInPFC)
				directlyReachable.Insert(0, actorCellDescToUseInPFC);

			if (directlyReachable.Count < cells.Count) // not all are reachable
			{
				if (!indirectNull || directlyReachable.Count == 0)
				{
					// There are cells that can be indirectly reached => find & sort them
					var unlimited_budget = PathfindingCurrency.CreateUnlimited(actor.Stats[Stat.GHOST_MOVES]);
					var unlimited_comparer = new PathfindingCurrency.Comparer_PrioritizeConsumingFewerSpecialMovesWhenResourcesUnknown(actor.Design);
					var unlimited_pfc = new PathfindingContext(/*actor,*/ actorCellDescToUseInPFC, unlimited_budget, unlimited_comparer, enemiesInfo);
					boardDescriptor.ComputeBestPaths(unlimited_pfc);

					if (indirectlyReachable.Exists(c => !unlimited_pfc.mapDestCellToBestSolution.ContainsKey(c)))
					{
						string s = "actorCellDesc=" + actorCellDesc;
						s += "; cells=";
						foreach (var c in cells)
							s += c + ", ";
						s += "; dCells=";
						foreach (var c in directlyReachable)
							s += c + ", ";
						s += "; iCells=";
						foreach (var c in indirectlyReachable)
							s += c + ", ";
						s += "; originOverride=" + originOverride;

						throw new UnityException("not reachable with unlimited budget?? Debug info: " + s);
					}

					indirectlyReachable.Sort((a, b) => unlimited_comparer.Compare(unlimited_pfc.mapDestCellToBestSolution[a].cost, unlimited_pfc.mapDestCellToBestSolution[b].cost));
				}
			}

			if (directlyReachable.Count > 0)
				return new ClosestCellResult(directlyReachable[0], true);
			if (indirectlyReachable.Count > 0)
				return new ClosestCellResult(indirectlyReachable[0], false);

			throw new UnityException("impossible");
		}


		public static float GetWinChanceAgainst(
			Board.Descriptor boardDescriptor, 
			CharacterBattlePotential myPotential, 
			float minRequiredChance, 
			Character enemy, 
			bool atRange, 
			out Cell.Descriptor closestCellToMoveTo)
		{
			Cell.Descriptor myCellOnBattle = null;
			var enemyCell = boardDescriptor.allCellsIncludingOuter[enemy.GetParentCell().indexInCellsArray];
			closestCellToMoveTo = null;
			if (atRange)
			{
				var reachableCellsToEnemy = new List<OneCellMovementSolution>();
				boardDescriptor.GetSurroundingCells(enemyCell, false, false, reachableCellsToEnemy, null);
				if (reachableCellsToEnemy.Count > 0)
				{
					reachableCellsToEnemy.RemoveAll(c => myPotential.MeAsPlayer.GetEnemiesInBlock(c.cell.block).Count > 0);

					if (reachableCellsToEnemy.Count > 0) // sort by proximity
					{
						var cells = reachableCellsToEnemy.ConvertAll(sol => sol.cell);
						var myCellDesc = boardDescriptor.GetExistingCellDesc(myPotential.MeAsCharacter.GetParentCell());
						//if (cells.Contains(myCellDesc)) 
						//	myCellOnBattle = myCellDesc; // if already positioned at a cell from which the ranged battle can start, return it
						//else
						//{
							List<Cell.Descriptor> _ = null, __ = new List<Cell.Descriptor>();
							var result = SortCellsByClosestAndGetClosest(myPotential.MeAsPlayer, boardDescriptor, cells, ref _, ref __);
							myCellOnBattle = result.cell;
						//}
					}
				}
			}

			if (myCellOnBattle == null)
				myCellOnBattle = enemyCell;

			float winChance = myPotential.ComputeWithOpponentAndGetWinChance(enemy, myCellOnBattle);
			if (winChance >= minRequiredChance)
				closestCellToMoveTo = myCellOnBattle;

			return winChance;
		}

		//public static void SelectMostPowerfulEffectsGroupsToBeUsedByOwner(int maxCount, List<List<Effect>> effectGroups, CharacterDesign ownerDesign, CharacterDesign enemyOfOwnerDesign)
		//{
		//	// The most powerful at end
		//	effectGroups.Sort((a, b) =>
		//	{
		//		var res = GetEffectGroupAveragePower(a, ownerDesign, enemyOfOwnerDesign) - GetEffectGroupAveragePower(b, ownerDesign, enemyOfOwnerDesign);
		//		if (res != 0f)
		//			return (int)(res * 1000); // dissolving the floating point small, but important differences (1.4 and 1.8 would've been considered equal if casted to int without first multiplying them with a big number)

		//		return 0;
		//	});

		//	while (effectGroups.Count > maxCount)
		//		effectGroups.RemoveAt(0);
		//}

		//// Counting negative stats for enemies as positive stats for owner
		//public static float GetEffectGroupAveragePotential(List<Effect> effectGroup, CharacterDesign ownerDesign, CharacterDesign enemyOfOwnerDesign)
		//{
		//	float power = 0f;
		//	foreach (var eff in effectGroup)
		//	{
		//		bool selfIsOwner = eff.owner == Effect.OwnerEnum.SELF;
		//		if (eff.type == Stat.ATK)
		//		{
		//			power += selfIsOwner ? eff.amount : -eff.amount;
		//		}
		//		else if (eff.type == Stat.SKILL)
		//		{
		//			var affectedDesign = selfIsOwner ? ownerDesign : enemyOfOwnerDesign;
		//			float skillPower = (affectedDesign.minSkillPower + affectedDesign.maxSkillPower) / 2f;
		//			if (!selfIsOwner)
		//				skillPower = -skillPower;
		//			power += skillPower;
		//		}
		//	}

		//		return power;
		//	}
	}
}
