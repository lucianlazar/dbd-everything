﻿using Com.TheFallenGames.DBD.Common.Interfaces;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Common
{
    internal static class MeshLoader
    {
        public static GameObject LoadMesh(IMeshHolder parent, bool preserveRotationFromPrefab)
        {
            //Debug.Log("LoadMesh: TODO: do it more efficient, as for now a prefab is loaded and destroyed each time we instantiate");
            var meshGORes = Resources.Load<GameObject>(parent.MeshPathInResources);
			if (!meshGORes)
				Debug.LogError("Doesn't exist:" + parent.MeshPathInResources);
            //meshGORes.SetActive(false);
            //meshGORes.hideFlags = HideFlags.HideInHierarchy; // will be destroyed soon

            var meshHolderChild = parent.MeshHolder;
            GameObject meshGOInstance = GameObject.Instantiate(meshGORes, meshHolderChild);
            meshGOInstance.transform.position = meshHolderChild.position;
            if (!preserveRotationFromPrefab)
                meshGOInstance.transform.rotation = meshHolderChild.rotation;
            meshGOInstance.SetActive(true);

            return meshGOInstance;
            //// Commented: GO's cannot be unloaded. They are destroyed instead
            //Resources.UnloadAsset(meshGORes); 
            //GameObject.Destroy(meshGORes, true);
        }
    }
}
