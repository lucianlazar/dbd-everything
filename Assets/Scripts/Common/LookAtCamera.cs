﻿using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Data;
using System;

namespace Com.TheFallenGames.DBD.Common
{
    public class LookAtCamera : MonoBehaviour
    {
		public bool preserveZ;
		public bool oppositeDirection;
		

		void Update()
		{
			float eulerZBefore = transform.eulerAngles.z;
			var vec = Camera.main.transform.position - transform.position;
			if (oppositeDirection)
				vec = -vec;

			transform.LookAt(transform.position + vec);
			if (preserveZ)
			{
				var rot = transform.eulerAngles;
				rot.z = eulerZBefore;
				transform.eulerAngles = rot;
			}
			transform.up = Camera.main.transform.up;
		}

	}
}
