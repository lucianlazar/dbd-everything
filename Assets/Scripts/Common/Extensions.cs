﻿using Assets.Scripts.Common.Interfaces;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace Assets.Scripts.Common
{
	static class Extensions
	{
		public static void UnifyCanvasAndInitTitle(this IWorldObjectWithTitle @this, string titlePathRelativeToCanvas)
		{
			var canvas = @this.transform.Find("Canvas");
			if (canvas)
			{
				if (!string.IsNullOrEmpty(titlePathRelativeToCanvas))
				{
					var goText = canvas.Find("Panel/Text");
					if (goText)
						@this.TitleText = goText.GetComponent<Text>();
				}

				UnifiedWorldCanvas.Instance.SwallowCanvas(canvas);
			}

		}

	}
}
