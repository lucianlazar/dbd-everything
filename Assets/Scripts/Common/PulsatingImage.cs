﻿using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Data;
using System;

namespace Com.TheFallenGames.DBD.Common
{
    public class PulsatingImage : MonoBehaviour
    {
		public Color secondColor;
		public float interval;

		Image _Image;
		Color _StartColor;

		void Awake()
		{
			_Image = GetComponent<Image>();
			_StartColor = _Image.color;
			if (interval == 0)
				interval = 1f;

			enabled = false;
		}

		void OnDisable()
		{
			_Image.enabled = false;
		}

		void OnEnable()
		{
			_Image.enabled = true;
		}

		void Update()
		{
			_Image.color = Color.Lerp(_StartColor, secondColor, Mathf.Abs(Mathf.Sin((Time.time / interval))));
		}
	}
}
