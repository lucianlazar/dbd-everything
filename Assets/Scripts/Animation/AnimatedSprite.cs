﻿using UnityEngine;
using System.Collections;
using frame8.Logic.Misc.Other.Extensions;
using System;

namespace Com.TheFallenGames.DBD.Common
{
	/// <summary>
	/// Should be started as disabled. Calling Initialize will enable the component
	/// </summary>
	public class AnimatedSprite : SpriteSwitcher
	{
		[Range(.01f, 10f)]
		public float frameDuration;
		public bool slightlyDelayFirstFrame = true;
		public bool crossfadeFrames = true;
		[Range(.01f, 1f)]
		[Tooltip("Must be LESS than frameDuration")]
		public float crossfadeDuration;
		[Tooltip("If this is on, crossfading frames is not supported")]
		public bool oneShot;
		public float oneShotFadeOutTime = .6f;

		float _LastFrameSwitchTime;
		int _GameFramesToSkip;
		int _SkippedGameFrames;
		float _CrossfadeMoment01;
		float _TimeWhenStartedToFadeOutInOneShot;
		bool _IsOneShotAndFadingOut;

		SpriteCrossfader _SpriteCrossfader;
		SpriteRenderer _TwinSpriteRenderer;


		void Update()
		{
			// Avoid starting multiple animations in sync
			if (_SkippedGameFrames < _GameFramesToSkip)
			{
				++_SkippedGameFrames;
				return;
			}

			if (_IsOneShotAndFadingOut)
			{
				float elapsedFadeOut = Time.time - _TimeWhenStartedToFadeOutInOneShot;
				float elapsedFadeOut01 = elapsedFadeOut / oneShotFadeOutTime;
				if (elapsedFadeOut01 > 1f)
				{
					// Done fading out, which also means done the one-shot
					_IsOneShotAndFadingOut = false;
					gameObject.SetActive(false);
					return;
				}

				SetAlphaForAllRenderers(1f - elapsedFadeOut01);

				return;
			}

			float elapsed = Time.time - _LastFrameSwitchTime;
			float elapsed01 = elapsed / frameDuration;
			if (elapsed01 >= 1f)
			{
				if (oneShot)
				{
					if (_CurrentSpriteIndex == sprites.Length - 1)
					{
						_TimeWhenStartedToFadeOutInOneShot = Time.time;
						if (oneShotFadeOutTime > 0f)
							_IsOneShotAndFadingOut = true;
						else
							// Done one shot
							gameObject.SetActive(false);
						return;
					}

				}

				CycleSprite();
			}
			else
			{
				if (crossfadeFrames)
				{
					//float crossfadeValue;
					//if (elapsed01 >= _CrossfadeMoment01)
					//	crossfadeValue = (elapsed01 - _CrossfadeMoment01) / crossfadeDuration;
					//else
					//	crossfadeValue = 0f;
					//_SpriteCrossfader.CrossfadeAmount01 = crossfadeValue;
					_SpriteCrossfader.CrossfadeAmount01 = elapsed01;
				}
			}
		}

		void SetAlphaForAllRenderers(float alpha)
		{
			var c = _SpriteRenderer.color;
			c.a = alpha;
			_SpriteRenderer.color = c;
			if (_TwinSpriteRenderer)
			{
				c = _TwinSpriteRenderer.color;
				c.a = alpha;
				_TwinSpriteRenderer.color = c;
			}
		}

		// Testing 
		//void OnEnable()
		//{
		//	if (!transform.parent || transform.parent.GetComponent<AnimatedSprite>())
		//		return;

		//	Initialize(null);
		//}

		public void Initialize(string animationSpritesFolderPathInResources)
		{
			// Only load them if they weren't assigned in inspector
			if (sprites == null || sprites.Length == 0)
				sprites = Resources.LoadAll<Sprite>(animationSpritesFolderPathInResources);
			Initialize(sprites);
		}

		public void Initialize(Sprite[] spritesToUse)
		{
			ResetPlayback();

			sprites = spritesToUse;

			crossfadeDuration = Mathf.Clamp(crossfadeDuration, 0f, frameDuration);
			_CrossfadeMoment01 = 1f - crossfadeDuration / frameDuration;

			if (crossfadeFrames)
			{
				var twin = Instantiate(gameObject, transform, true);
				var allComponents = twin.transform.GetComponents<Component>();
				foreach (var component in allComponents)
				{
					if (component is Transform)
						continue;

					var spriteRend = component as SpriteRenderer;
					if (spriteRend)
					{
						_TwinSpriteRenderer = spriteRend;
						continue;
					}

					Destroy(component);
				}
				_SpriteCrossfader = new SpriteCrossfader(_SpriteRenderer, _TwinSpriteRenderer, _CrossfadeMoment01);
			}

			base.SwitchSprite(0); // initially show first frame

			// Enable animation only if there's more than 1 frame OR the animation is oneshot
			if (sprites.Length > 1 || oneShot)
			{
				if (slightlyDelayFirstFrame && sprites.Length > 1)
					_GameFramesToSkip = UnityEngine.Random.Range(0, 120);
				enabled = true;
			}
		}

		internal void ResetPlayback()
		{
			_LastFrameSwitchTime = 0;
			_GameFramesToSkip = 0;
			_SkippedGameFrames = 0;
			_CrossfadeMoment01 = 0;
			SetAlphaForAllRenderers(1f);
			_IsOneShotAndFadingOut = false;
			if (_SpriteCrossfader != null)
				_SpriteCrossfader.CrossfadeAmount01 = 0;
		}

		public override void SwitchSprite(int index)
		{
			base.SwitchSprite(index);

			// Every time the frame advances, the sprites are swapped and the first one become totally visible while the second totally invisible
			if (crossfadeFrames)
			{
				_SpriteCrossfader.second = sprites[(index + 1) % sprites.Length];
				_SpriteCrossfader.CrossfadeAmount01 = 0f;
			}
		}

		public override void SwitchSprite(Sprite sprite)
		{
			base.SwitchSprite(sprite);

			_LastFrameSwitchTime = Time.time;
		}
	}
}
