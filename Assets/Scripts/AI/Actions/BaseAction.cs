﻿using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Data;
using System;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;

namespace Com.TheFallenGames.DBD.Actions
{
    public abstract class BaseAction
	{
		public abstract void Do();
	}
}
