﻿using UnityEngine;
using System.Collections;
using System;
using Photon;
using UnityEngine.EventSystems;

namespace Com.TheFallenGames.DBD.World
{
    public class AllowedArea : PunBehaviour, InputToEvent.IEventHandler
	{
        public Action<AllowedArea> onSelected;

        ParticleSystem _ParticleSystem;
        Color _OriginalStartColor, _OriginalEndColor;
        Vector3 _ParticleSystemOriginalScale;
		bool _Interractable;


		void Awake()
        {
            _ParticleSystem = GetComponentInChildren<ParticleSystem>();
            _ParticleSystemOriginalScale = _ParticleSystem.transform.localScale;
        }

		#region implementation for InputToEvent.IEventHandler
		public void OnPress(BaseEventData eventData) { }
		public virtual void OnLongPress(BaseEventData eventData) { }
		public void OnPressCanceled(BaseEventData eventData) { }
		public void OnRelease(BaseEventData eventData) { }
		public void OnReleaseAfterCanceled(BaseEventData eventData) { }
		public void OnClick(BaseEventData eventData) { }
		public void OnDoubleClick(BaseEventData eventData)
		{
			if (_Interractable && onSelected != null)
			{
				if (onSelected != null)
					onSelected(this);
			}
		}
		#endregion

        public Cell GetParentAsCell() { return transform.parent == null ? null : transform.parent.GetComponent<Cell>(); }

        public void SetHighlight(int highlighNumber, bool visualOnly = false)
        {
            gameObject.SetActive(highlighNumber > 0);
            if (highlighNumber > 0)
            {
                Vector3 scale = _ParticleSystemOriginalScale;
                if (highlighNumber > 1) // not directly-reachable
                    scale *= .4f;
                _ParticleSystem.transform.localScale = scale;
            }
            _Interractable = !visualOnly && highlighNumber == 1;
        }

        //[PunRPC]
        //void Select()
        //{
        //    if (onSelected != null)
        //        onSelected(this, true);
        //}

    }
}
