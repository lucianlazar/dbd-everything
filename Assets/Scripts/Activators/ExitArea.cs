﻿using UnityEngine;
using System.Collections;
using System;
using Photon;

namespace Com.TheFallenGames.DBD.World
{
    public class ExitArea : PunBehaviour
    {
		public Block ParentBlock { get { return transform.parent.GetComponent<Block>(); } set { transform.parent = value.transform; } }

        ParticleSystem _ParticleSystem;

		public bool IsOpen { get { return _IsOpen; }
			set
			{
				_IsOpen = value;
				var e = _ParticleSystem.emission;
				e.enabled = value;
			}
		}


		bool _IsOpen;

		void Awake()
        {
            _ParticleSystem = GetComponentInChildren<ParticleSystem>();
			IsOpen = _IsOpen;
        }


    }
}
