﻿using Com.TheFallenGames.DBD.Core;
using Com.TheFallenGames.DBD.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Network.Data
{
    public class GameLoadingManager : Photon.PunBehaviour
    {
        void Start()
        {
            CheckPlayerCount();
        }

        void CheckPlayerCount()
        {
            if (GetNeededPlayers() == 0)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    //Debug.Log("Ready to game; loading Game scene as Master Client");
                    PhotonNetwork.LoadLevel(SceneNames.GAME);
                }
                else
                {
                    //Debug.Log("Ready to game; Waiting fot the Master Client to load the Game scene");
                }
            }
            else
            {
                var msg = "Connected players: " + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers;
                GameObject.Find("WaitingForOtherPlayersText").GetComponent<Text>().text = msg;
                //Debug.Log(msg);
            }
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            //Debug.Log("Player connected: " + newPlayer.NickName + "; " + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers + " slots occupied");
			CheckPlayerCount();
        }

        int GetNeededPlayers()
        {
            return PhotonNetwork.room.MaxPlayers - PhotonNetwork.room.PlayerCount;
        }
    }
}