﻿using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.Core
{
    public class Launcher : Photon.PunBehaviour
    {
        [Tooltip("The Ui Panel to let the user enter name, connect and play")]
        public GameObject controlPanel;
        [Tooltip("The UI Label to inform the user that the connection is in progress")]
        public GameObject progressLabel;

        public GameModeUI gameModeUI;
        public ToggleGroup desiredMaxPlayersToggleGroup;
		public ToggleGroup boardSetupToggleGroup;
		public PlayerChoiceUI playerChoiceUI;

		//[SerializeField]
		//PhotonLogLevel _PhotonLogLevel;
		//[SerializeField]
        bool _OfflineMode;

#if UNITY_EDITOR
		[SerializeField]
		bool _FastBattle; public static bool s_debug_FastBattle;

		[SerializeField]
		bool _DebugHumanEndsTurnImmediately; public static bool s_debug_HumanEndsTurnImmediately;

		[SerializeField]
		bool _AutoPlay;

		[SerializeField]
		bool _EnableOnePlayerInMultiplayer;
#endif
		/// <summary>
		/// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
		/// </summary>   
		//[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
		//[SerializeField]
		//[Range(2, 4)]
		byte _MaxPlayersPerRoom;

		//[SerializeField]
		BoardSetupEnum _BoardSetup;

		[SerializeField]
		GameModeEnum _GameMode;

		byte _PlayerClassToUse;

		string _GameVersion;

        /// <summary>
        /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
        /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
        /// Typically this is used for the OnConnectedToMaster() callback.
        /// </summary>
        bool _JoinRoomRequestPending;

		Button _PlayButton;


		void Awake()
		{
			// Clear previous resources, if went back from the game screen
			Resources.UnloadUnusedAssets();
			GC.Collect();

			GlobalRefs8.Instance.OnGameInitialized();

			// TODO use a dedicated class for this. maybe it will change in the future, like also include the platform number
			_GameVersion = Application.version;

			// Won't list rooms yet
			PhotonNetwork.autoJoinLobby = false;

			// PhotonNetwork.LoadLevel on master will also trigger LoadLevel on clients
			PhotonNetwork.automaticallySyncScene = true;

			//// Set the desired photon log level
			//PhotonNetwork.logLevel = _PhotonLogLevel;

			PhotonNetwork.QuickResends = 3;
			PhotonNetwork.networkingPeer.SentCountAllowance = 7;
			PhotonNetwork.MaxResendsBeforeDisconnect = 6;
			if (!PhotonNetwork.connected)
				PhotonNetwork.CrcCheckEnabled = true;

#if UNITY_EDITOR
			s_debug_FastBattle = _FastBattle;
			s_debug_HumanEndsTurnImmediately = _DebugHumanEndsTurnImmediately;
#endif

			// Unity one-time per runtime initialization
			UnityEngine.Random.InitState((int)DateTime.UtcNow.Ticks);
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			Application.targetFrameRate = 60;
		}

        void Start()
        {
            controlPanel.SetActive(true);
            progressLabel.SetActive(false);

            for (int i = 0; i < desiredMaxPlayersToggleGroup.transform.childCount; ++i)
            {
                int copyOfI = i;
                desiredMaxPlayersToggleGroup.transform.GetChild(i).GetComponent<Toggle>().onValueChanged.AddListener(isOn =>
                {
                    if (isOn)
                        _MaxPlayersPerRoom = (byte)(copyOfI + 1);
                });
            }

#if UNITY_EDITOR
			if (_EnableOnePlayerInMultiplayer)
				desiredMaxPlayersToggleGroup.transform.GetChild(0).gameObject.SetActive(true);
#endif

			// Board setup
			for (int i = 0; i < 2; i++)
			{
				var toggle = boardSetupToggleGroup.transform.GetChild(i).GetComponent<Toggle>();
				int copyOfI = i;
				toggle.onValueChanged.AddListener(isOn =>
				{
					if (isOn)
						_BoardSetup = (BoardSetupEnum)copyOfI;
				});
			}

			playerChoiceUI.playerChoiceChanged += choice => { _PlayerClassToUse = (byte)choice; };
			gameModeUI.modeChanged += OnGameModeChanged;

			ForceSetPlayerChoice(0);
			ForceSetBoardSetup(BoardSetupEnum.CLASSIC);
			ForceSetNumberOfPlayers(2);
			ForceSetGameMode(_GameMode);

			_PlayButton = GameObject.Find("PlayButton").GetComponent<Button>();
			_PlayButton.onClick.AddListener(OnPlayClicked);
#if UNITY_EDITOR
			if (_AutoPlay)
            {
				_PlayButton.interactable = false;
                Invoke("OnPlayClicked", 1f);
            }
#endif
		}

		void ForceSetGameMode(GameModeEnum gameMode)
		{
			var toggle = gameModeUI.toggleGroup.transform.GetChild((int)gameMode).GetComponent<Toggle>();
			toggle.isOn = true;
			//gameModeUI.toggleGroup.NotifyToggleOn(toggle);
			_GameMode = gameMode;
		}

		void ForceSetNumberOfPlayers(byte num)
		{
			var toggle = desiredMaxPlayersToggleGroup.transform.GetChild(num - 1).GetComponent<Toggle>();
			toggle.isOn = true;
			//if (desiredMaxPlayersToggleGroup.gameObject.activeInHierarchy)
			//	desiredMaxPlayersToggleGroup.NotifyToggleOn(toggle);
			_MaxPlayersPerRoom = num;
		}

		void ForceSetBoardSetup(BoardSetupEnum boardSetup)
		{
			var toggle = boardSetupToggleGroup.transform.GetChild((int)boardSetup).GetComponent<Toggle>();
			toggle.isOn = true;
			//if (boardSetupToggleGroup.gameObject.activeInHierarchy)
			//	boardSetupToggleGroup.NotifyToggleOn(toggle);
			_BoardSetup = boardSetup;
		}

		void ForceSetPlayerChoice(byte classIndex)
		{
			var toggle = playerChoiceUI.toggleGroup.transform.GetChild(classIndex).GetComponent<Toggle>();
			toggle.isOn = true;
			//if (playerChoiceUI.toggleGroup.gameObject.activeInHierarchy)
			//	playerChoiceUI.toggleGroup.NotifyToggleOn(toggle);
			_PlayerClassToUse = classIndex;
		}

		void OnGameModeChanged(GameModeEnum gameMode)
		{
			_GameMode = gameMode;

			var maxPl = desiredMaxPlayersToggleGroup.transform.parent.gameObject;
			var boardSetup = boardSetupToggleGroup.transform.parent.gameObject;
			var plChoice = playerChoiceUI.gameObject;

			maxPl.SetActive(true);
			boardSetup.SetActive(true);
			plChoice.SetActive(true);

			//Debug.Log("TODO remove offline=true & set back 2 players to tournament mode & remove 'return' & uncomment last line in method");
			//_OfflineMode = true;
			switch (gameMode)
			{
				case GameModeEnum.TOURNAMENT:
					ForceSetNumberOfPlayers(1);
					ForceSetBoardSetup(BoardSetupEnum.CLASSIC);
					ForceSetPlayerChoice(0);
					maxPl.SetActive(false);
					boardSetup.SetActive(false);
					//plChoice.SetActive(false);
					break;

				case GameModeEnum.MULTIPLAYER:
					ForceSetNumberOfPlayers(2);
					break;

				case GameModeEnum.TRAINING:
					ForceSetNumberOfPlayers(1);
					maxPl.SetActive(false);
					break;
			}

			_OfflineMode = gameMode != GameModeEnum.MULTIPLAYER;
		}

//		void Update()
//		{
//#if UNITY_EDITOR
//			// Keep the play button active only if selected 2 different classes
//			if (!_AutoPlay)
//#endif
//				_PlayButton.interactable = PlayerChoiceUI.ChosenClass != PlayerChoiceUI.ChosenClass2;
//		}


		void OnPlayClicked()
        {
            controlPanel.SetActive(false);
            progressLabel.SetActive(true);

			PhotonNetwork.player.SetClassChoice(_PlayerClassToUse);

			if (PhotonNetwork.offlineMode == _OfflineMode)
			{
				if (_OfflineMode)
					CreateAndJoinOwnRoom();
				else
					ConnectOrJoinOnline();
			}
			else
			{
				if (_OfflineMode)
				{
					if (PhotonNetwork.connected)
					{
						// Need to disconnect first
						PhotonNetwork.Disconnect(); // will call OnDisconnectedFromPhoton below
					}
					else
					{
						PhotonNetwork.offlineMode = true; // will call OnConnectedToMaster below
					}
				}
				else
				{
					PhotonNetwork.offlineMode = false;
					ConnectOrJoinOnline();
				}
			}
        }

		void ConnectOrJoinOnline()
		{
			if (PhotonNetwork.connected)
			{
				JoinRandomRoomWithCurrentSettings();
			}
			else // online mode
			{
				_JoinRoomRequestPending = true;
				PhotonNetwork.ConnectUsingSettings(_GameVersion);
			}
		}

		void JoinRandomRoomWithCurrentSettings()
		{
			PhotonNetwork.JoinRandomRoom(GetRoomProperties(), _MaxPlayersPerRoom);
		}

		void CreateAndJoinOwnRoom()
		{
			var roomOptions = new RoomOptions();
			roomOptions.MaxPlayers = _MaxPlayersPerRoom;
			var roomProps = GetRoomProperties();
			roomOptions.CustomRoomProperties = roomProps;

			// All props are visible in the lobby for matchmaking
			var keys = new List<string>();
			foreach (var k in roomProps.Keys)
				keys.Add(k.ToString());
			roomOptions.CustomRoomPropertiesForLobby = keys.ToArray();

			PhotonNetwork.CreateRoom(PhotonNetwork.playerName + "'s room", roomOptions, null);
		}

        #region Photon.PunBehaviour callbacks
        public override void OnConnectedToMaster()
        {
			Debug.Log("Launcher: OnConnectedToMaster()");
			if (PhotonNetwork.offlineMode)
				CreateAndJoinOwnRoom();
			else if (_JoinRoomRequestPending)
				JoinRandomRoomWithCurrentSettings();
		}

		public override void OnDisconnectedFromPhoton()
		{
			Debug.Log("Launcher: OnDisconnectedFromPhoton()");
			if (_OfflineMode && !PhotonNetwork.offlineMode) // manually disconnected to enter offline mode
			{
				PhotonNetwork.offlineMode = true; // will call OnConnectedToMaster
			}
			else
			{
				//Debug.Log("Launcher: OnDisconnectedFromPhoton(); returning to control panel...");
				progressLabel.SetActive(false);
				controlPanel.SetActive(true);
			}
		}

		public override void OnJoinedRoom()
        {
            _JoinRoomRequestPending = false;
            //Debug.Log("Launcher: OnJoinedRoom()");

            // We only load if we are the first player, else we rely on  PhotonNetwork.automaticallySyncScene to sync our instance scene.
            if (PhotonNetwork.room.PlayerCount == 1)
            {
                //Debug.Log("Loading scene via network: '" + Data.SceneNames.LOADING + "' ");
                PhotonNetwork.LoadLevel(Data.SceneNames.LOADING);
            }
        }

        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
			Debug.Log("Launcher:OnPhotonRandomJoinFailed(); No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = " + _MaxPlayersPerRoom + "}, null);");
			CreateAndJoinOwnRoom();
        }

		public override void OnPhotonCreateRoomFailed(object[] codeAndMsg)
		{
			if (PhotonNetwork.offlineMode)
				throw new UnityException("Cannot create room in offline mode???");

			Debug.Log("Trying again to join a room..");
			JoinRandomRoomWithCurrentSettings();
		}
		#endregion

		ExitGames.Client.Photon.Hashtable GetRoomProperties()
		{
			var ht = new ExitGames.Client.Photon.Hashtable();
			ht[RoomProps.BOARD_SETUP] = _BoardSetup.ToIntString();
			ht[RoomProps.PREV_GAME_STATE] = GameStateEnum.NONE.ToIntString();
			ht[RoomProps.GAME_STATE] = GameStateEnum.INITIALIZING_WAITING_FOR_OTHER_PLAYERS_TO_CONNECT.ToIntString();
			ht[RoomProps.GAME_MODE] = _GameMode.ToIntString();

			return ht;
		}
    }

}