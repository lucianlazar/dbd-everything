﻿using Com.TheFallenGames.DBD.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Core
{
	public class Visuals : MonoBehaviour
	{
		public static Visuals Instance { get; private set; }

		ParticleSystem _OreTakenParticleSystemEffect;




		void Awake()
		{
			Instance = this;
			_OreTakenParticleSystemEffect = transform.Find("OreTakenParticleSystemEffect").GetComponent<ParticleSystem>();
		}

		void OnDestroy()
		{
			Instance = null;	
		}


		public void PlayOreTakenEffect(Vector3 pos)
		{
			_OreTakenParticleSystemEffect.transform.position = pos + Vector3.up * .2f;
			_OreTakenParticleSystemEffect.gameObject.SetActive(true);
			StartCoroutine(DisableGO(_OreTakenParticleSystemEffect.gameObject));
		}

		IEnumerator DisableGO(GameObject go)
		{
			yield return new WaitForSeconds(2f);

			if (go)
				go.SetActive(false);
		}
	}
}
