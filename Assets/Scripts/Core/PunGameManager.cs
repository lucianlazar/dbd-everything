﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Network.Data;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;
using System;
using Com.TheFallenGames.DBD.Common;
using System.Collections.Generic;
using System.Collections;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Core
{
	public class PunGameManager : Photon.PunBehaviour
    {
        public static PunGameManager Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GameObject("PunGameManager").AddComponent<PunGameManager>();
                }

                return _Instance;
            }
        }

        #region Callbacks from master
        public event System.Action<GameInitializationData> Ev_GameInitializing;
        public event System.Action Ev_MasterReadyForNewRequest;
        public event System.Action Ev_MasterRejectedRequest;
        public event System.Action Ev_Master_PreNewTurn;
		public event System.Action<NewTurnRespawningData> Ev_RespawnEntitiesPreNewTurn;
		public event System.Action Ev_NewTurn;
		public event System.Action Ev_PlayerEndedTurn;
		public event System.Action<int> Ev_PlayerMoveToCellAccepted;
		public event System.Action<int, int, int> Ev_OtherMoveToCellAccepted;
		public event System.Action<int> Ev_PlayerShiftDungeonAccepted;
        public event System.Action<int> Ev_PlayerGetOreAccepted;
        public event System.Action<int> Ev_PlayerDiscardAccepted; // param = item's index in player's inventory (PlayerStats)
        public event System.Action<int> Ev_PlayerPickUpItemAccepted; // param = item's index in block's ContainedEntities array; buy or simply pick up
        public event System.Action<TotemType> Ev_PlayerBuildTotemAccepted; // param = totem type
		public event System.Action<int, int> Ev_PlayerSetupBattleAccepted; // param = enemy's cell index; param2 = character's index in block's ContainedEntities array
        public event System.Action<int> Ev_PlayerPlayCardAccepted; // param = enemy's cell index; param2 = character's index in block's ContainedEntities array
		public event System.Action Ev_Battle_PlayerStaysIdle;
		public event System.Action Ev_Battle_PlayerInitiates;
		public event System.Action<bool> Ev_Battle_ApproachedPlayerChoseIfToCancelBattle;
		public event System.Action Ev_Battle_ApproachedPlayerAlsoStaysIdle;
		public event System.Action Ev_Battle_ApproachedPlayerInitiates;
		public event System.Action<int> Ev_PlayerReadyForBattle; // the payer's id
		public event System.Action<int> Ev_PlayerBattleTurnStarted; // the payer's id
		public event System.Action<BattleStats> Ev_BattleResults;
		public event System.Action<int/*, int*/> Ev_Master_PlayerEliminated; // called on master. first the loser's id//, second the killer's id (-1 if killed by the dragon)
		public event System.Action<int/*, int*/> Ev_Client_PlayerEliminated; // called on clients. first the loser's id//, second the killer's id (-1 if killed by the dragon)
		public event System.Action Ev_PostBattleWinnerDoLooting; // Either this (potentially followed by a Ev_PostBattleWinnerDoneLooting) or Ev_GameResult will be called
		public event System.Action Ev_PostBattleWinnerDoneLooting;
		public event System.Action<int, int, int, object[]> Ev_ItemTransfer;
		public event System.Action<int> Ev_GameResult; // the winner's id, or -1, if dead due to elimination in single player
		#endregion


		public PhotonPlayer CPUPhotonPlayer { get; private set; }
		public PhotonPlayer[] PlayerListWithCPUIfAny { get { var l = new List<PhotonPlayer>(PhotonNetwork.playerList); if (CPUPhotonPlayer != null) l.Add(CPUPhotonPlayer); return l.ToArray(); } }
		public int PlayerCountIncludingCPUIfAny { get { return PlayerListWithCPUIfAny.Length; } }

		/// <summary> The player id that executes the current turn </summary>
		public int CurrentPlayerID { get; private set; }
        public int CurrentTurnNumber { get; private set; }
		
		internal int PlayersReadyForBattle { get; set; } // Used in master client

		#region Used if Master Client
        DBDEvent CurrentRequestedEvent { get; set; }
		bool CurrentRequestDispatchingInProgress { get; set; } // Used in master client
        int ExecutionConfirmationsForCurrentRequest { get; set; } // Used in master client
		internal int CurrentRequestSenderID { get; private set; }

		// This is true when, for example, a player sends the "ready" signal to battle, but it shouldn't expect 
		// a "Ready for new request" event, as the master will either publish a "on player battle-turn started" 
		// event (if the sender was first in the battle) or an "on battle results" event (if the sender was the second)
		RequestExecutedConfirmationType CurrentRequestExecuteConfirmationType { get; set; }
		#endregion

		static PunGameManager _Instance;

		//List<Player> _EliminatedPlayers = new List<Player>();


		enum RequestExecutedConfirmationType
		{
			INFORM_MASTER_AND_SENDER,
			INFORM_MASTER_ONLY,
			INFORM_EVERYONE
		}


        #region MonoBehaviour CallBack
        void Awake()
		{
			if (PhotonNetwork.room.GetGameMode() == GameModeEnum.TOURNAMENT)
				CPUPhotonPlayer = new PhotonPlayer(true, DefaultPlayerIDs.CPUPlayerID, "CPU");

			PhotonNetwork.OnEventCall += OnEvent;
        }

        void OnDestroy()
        {
            PhotonNetwork.OnEventCall -= OnEvent;
			_Instance = null;
        }
        #endregion

        #region Used if Master Client
        public void DispatchGameInitializing(Hashtable initialGameState)
        {
            DispatchEvent(DBDEvent.GAME_INITIALIZING, initialGameState);
        }

        public void DispatchNewTurnEvent(int playerID, Hashtable respawnData = null, bool incrementTurn = true)
        {
            int newTurnNumber = incrementTurn ? (CurrentTurnNumber + 1) : CurrentTurnNumber;
            object content1 = new int[] { playerID, newTurnNumber };
			object[] content = new object[] { content1, respawnData };
            DispatchEvent(DBDEvent.NEW_TURN, content);
		}

		public void DispatchBattleTurnStated(int playerId)
		{
			DispatchEvent(DBDEvent.BATTLE__PLAYER_BATTLETURN_STARTED, playerId);
		}

		public void DispatchBattleResult(BattleStats stats)
		{
			DispatchEvent(DBDEvent.BATTLE_RESULT, stats.ConvertToByteArray());
		}

		internal void DispatchNewTurnEvent(Hashtable respawnData = null)
        {
            int currentPlayerID = CurrentPlayerID;
            if (currentPlayerID == -1)
                throw new System.InvalidOperationException();

			// New turn for the next player that has a soul
			int nextPlayerID = CurrentPlayerID;
			int soullessPlayers = 0;
			do
			{
				nextPlayerID = GetNextPlayerIDFor(nextPlayerID); // get the next if not null. else, get the 'first' player
			}
			while (!SpawnManager.Instance.GetPlayerInstanceByActorID(nextPlayerID).Stats.HasSoul && ++soullessPlayers < PlayerCountIncludingCPUIfAny);
			
			if (soullessPlayers == PhotonNetwork.room.PlayerCount)
			{
				Debug.LogError("FATAL: Couldn't find a next player with a soul (???). PlayerCount=" + PlayerCountIncludingCPUIfAny + ". Aborting...");
				return;
			}

            DispatchNewTurnEvent(nextPlayerID, respawnData);
		}

		internal void DispatchPlayerEliminatedEvent_Client(int playerID)
		{
			DispatchEvent(DBDEvent.PLAYER_ELIMINATED, playerID);
		}

		internal void DispatchPlayerEliminatedEvent_Master(int playerID)
		{
			if (Ev_Master_PlayerEliminated != null)
				Ev_Master_PlayerEliminated(playerID);
			//DispatchEvent(DBDEvent.PLAYER_ELIMINATED, playerID);
		}

		internal void DispatchPostBattleEvent()
		{
			DispatchEvent(DBDEvent.POST_BATTLE_WINNER_DO_LOOTING, null);
		}

		internal void DispatchGameResultEvent(int playerIDOrMinusOne)
		{
			DispatchEvent(DBDEvent.GAME_RESULT, playerIDOrMinusOne);
		}

		void DispatchEvent(DBDEvent eventCode, object content, bool reliable = true)
		{
			ExecuteAfterOneFrame(() => OnDBDEvent(eventCode, content, PhotonNetwork.player.ID));
			//PhotonNetwork.RaiseEvent(eventCode, content, true, new RaiseEventOptions { Receivers = ReceiverGroup.Others });
		}

		void OnAdditionalClientConfirmedExecution()
		{
			++ExecutionConfirmationsForCurrentRequest;
			//Debug.Log("OnAdditionalClientConfirmedExecution: ExecutionConfirmationsForCurrentRequest="+ ExecutionConfirmationsForCurrentRequest);

			//Debug.Log("ExecutionConfirmationsForCurrentRequest=" + ExecutionConfirmationsForCurrentRequest + "; room players=" + PhotonNetwork.room.PlayerCount);

			bool currentSenderIsMaster = CurrentRequestSenderID == PhotonNetwork.player.ID;
			int numConfirmationsNeededBeforeAcceptingNewRequests = PhotonNetwork.room.PlayerCount;

			// Everyone confirmed the execution => ready for the next request
			if (ExecutionConfirmationsForCurrentRequest == numConfirmationsNeededBeforeAcceptingNewRequests)
            {
                CurrentRequestDispatchingInProgress = false;

				List<int> receivers = new List<int>();
				switch (CurrentRequestExecuteConfirmationType)
				{
					case RequestExecutedConfirmationType.INFORM_MASTER_AND_SENDER:
						if (!currentSenderIsMaster)
							receivers.Add(CurrentRequestSenderID);
						break;

					case RequestExecutedConfirmationType.INFORM_EVERYONE:
						foreach (var player in PhotonNetwork.playerList) // not adding the CPU player, as that case is treated separately by GameplayManager
							if (player.ID != PhotonNetwork.player.ID)
								receivers.Add(player.ID);
						break;
				}


				string rec = "";
				foreach (var r in receivers)
					rec += r + " ";
				//Debug.Log("OGM: Conf receivers: " + rec + " -- " + CurrentRequestExecuteConfirmationType);
				//receivers.Add(PhotonNetwork.player.ID);

				// Notify the receivers, if any
				if (receivers.Count > 0)
					RaiseDBDEvent(DBDEvent.MASTER_READY_FOR_NEW_REQUEST, null, true, new RaiseEventOptions { TargetActors = receivers.ToArray() });

				// The master should always know when he's ready for a new request; waiting one frame to prevent long-chained calls 
				ExecuteAfterOneFrame(() => OnDBDEvent(DBDEvent.MASTER_READY_FOR_NEW_REQUEST, null, CurrentRequestSenderID));
			}
        }
		#endregion

		#region Client interface
		public void RequestMoveOther(int startCellIndex, int indexInBlock, int cellIndex)
		{
			RaiseDBDEventToMaster(DBDEvent.OTHER_MOVE_TO_CELL, new byte[] { (byte)startCellIndex, (byte)indexInBlock, (byte)cellIndex });
		}

		public void RequestMove(int cellIndex)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_MOVE_TO_CELL, (byte)cellIndex);
		}

		public void RequestShiftDungeon(int shifterIndex)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_SHIFT_DUNGEON, (byte)shifterIndex);
		}

		public void RequestGetOre(int cellIndex)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_GET_ORE, (byte)cellIndex);
		}

		public void RequestDiscard(int itemIndexInInventory)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_DISCARD, (byte)itemIndexInInventory);
		}

		public void RequestPickUpItem(int itemIndexInBlock)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_PICKUP_ITEM, (byte)itemIndexInBlock);
		}

		public void RequestBuildTotem(TotemType totemType)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_BUILD_TOTEM, (byte)totemType);
		}

		public void RequestSetupBattle(int cellIndex, int characterIndexInBlock)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_SETUP_BATTLE, new byte[] { (byte)cellIndex, (byte)characterIndexInBlock });
		}

		public void RequestPlayCard(int itemIndexInInventory)
		{
			RaiseDBDEventToMaster(DBDEvent.PLAYER_USE_ITEM, (byte)itemIndexInInventory);
		}

		public void RequestInitiateBattleFirst() { RaiseDBDEventToMaster(DBDEvent.BATTLE__PLAYER_INITIATES); }
		public void RequestInitiateBattleAfterCurrentPlayerPrefersNotTo() { RaiseDBDEventToMaster(DBDEvent.BATTLE__APPROACHED_PLAYER_INITIATES); }
		public void RequestStayIdleInBattleFirst() { RaiseDBDEventToMaster(DBDEvent.BATTLE__PLAYER_STAYS_IDLE); }
		public void RequestStayIdleInBattleAfterCurrentPlayerAlsoStays() { RaiseDBDEventToMaster(DBDEvent.BATTLE__APPROACHED_PLAYER_ALSO_STAYS_IDLE); }
		public void RequestChooseIfToCancelBattle(bool cancel)
		{
			RaiseDBDEventToMaster(DBDEvent.BATTLE__APPROACHED_PLAYER_CHOSE_IF_TO_CANCEL_BATTLE, cancel);
		}

		public void RequestReadyToBattle()
		{
			RaiseDBDEventToMaster(DBDEvent.BATTLE__PLAYER_READY, PhotonNetwork.player.ID);
		}

		public void RequestPostBattleWinnerDoneLooting()
		{
			RaiseDBDEventToMaster(DBDEvent.POST_BATTLE__WINNER_DONE_LOOTING, PhotonNetwork.player.ID);
		}

		public void RequestTransferItem(
			int donorPlayerID,
			int itemIndexInInventory,
			int receiverPlayerID,
			int receiverTradedItemIndexInInventory = -1 // only available if trading
		)
		{
			RaiseDBDEventToMaster(DBDEvent.TRANSFER_ITEM,
				new byte[] {
					(byte)donorPlayerID,
					(byte)itemIndexInInventory,
					(byte)receiverPlayerID,
					(byte)(receiverTradedItemIndexInInventory == -1 ? byte.MaxValue : receiverTradedItemIndexInInventory), // -1 doesn't exist in byte space
					byte.MaxValue // N/A
				}
			);
		}

		public void RequestAnswerTradeProposal(
			int donorPlayerID,
			int itemIndexInInventory,
			int receiverPlayerID,
			int receiverTradedItemIndexInInventory,
			bool accepted
		)
		{
			RaiseDBDEventToMaster(DBDEvent.TRANSFER_ITEM,
				new byte[] {
					(byte)donorPlayerID,
					(byte)itemIndexInInventory,
					(byte)receiverPlayerID,
					(byte)receiverTradedItemIndexInInventory,
					(byte)(accepted ? 1 : 0)
				}
			);
		}

		public void OnCurrentActionExecuted()
		{
			//Debug.Log("PGM: OnCurrentActionExecuted");
			RaiseDBDEventToMaster(DBDEvent.PLAYER_CONFIRMED_EXECUTION, PhotonNetwork.player.ID);
        }

        public void RequestEndMyTurn()
        {
			RaiseDBDEventToMaster(DBDEvent.PLAYER_ENDED_TURN, PhotonNetwork.player.ID);
        }

		public void DoAfterMasterReady(Action action)
		{
			//int i = UnityEngine.Random.Range(0, 10000);
			//Debug.Log("Schedule " + i);
			Action actionWithUnsubscribe = null;
			actionWithUnsubscribe = () =>
			{
				//Debug.Log("PreExecute " + i);
				if (action != null)
					action();
				//Debug.Log("PostExecute " + i);

				Ev_MasterReadyForNewRequest -= actionWithUnsubscribe;
			};
			Ev_MasterReadyForNewRequest += actionWithUnsubscribe;
		}
		#endregion

		#region other
		public PhotonPlayer GetPlayerWithActorID(int actorID) { return Array.Find(PlayerListWithCPUIfAny, p => p.ID == actorID); }
		public PhotonPlayer GetNextPlayerFor(PhotonPlayer player)
		{
			var pList = PlayerListWithCPUIfAny;
			return pList[(Array.IndexOf(pList, player) + 1) % pList.Length];
		}
		public int GetNextPlayerIDFor(PhotonPlayer player) { return GetNextPlayerFor(player).ID; }
		public int GetNextPlayerIDFor(int playerID) { return GetNextPlayerFor(Array.Find(PlayerListWithCPUIfAny, p => p.ID == playerID)).ID; }
		#endregion


		void NotifySenderRequestRejected(object content, DBDEvent eventCode, int senderId)
		{
			Debug.LogError("NotifySenderRequestRejected by master (id=" + PhotonNetwork.masterClient.ID + "): senderID=" + senderId + ", req=" + eventCode + ", result=DBDEvent.MASTER_REJECTED_REQUEST");

			if (senderId == PhotonNetwork.player.ID)
				// We're the master & the sender => call the event locally afte 1 frame
				ExecuteAfterOneFrame(() => OnDBDEvent(DBDEvent.MASTER_REJECTED_REQUEST, content, senderId));
			else
				// Inform the sender that his request was rejected
				RaiseDBDEvent(DBDEvent.MASTER_REJECTED_REQUEST, content, true, new RaiseEventOptions { TargetActors = new int[] { senderId } });
		}

		void RaiseDBDEventToMaster(DBDEvent eventCode, object eventContent = null)
		{
			if (PhotonNetwork.isMasterClient)
				// We're the master & the sender => call the event locally after 1 frame
				ExecuteAfterOneFrame(() => OnDBDEvent(eventCode, eventContent, PhotonNetwork.player.ID));
			else
			{
				// Notify the master, which will further inform everyone (including us)
				RaiseDBDEvent(eventCode, eventContent, true, new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient });
			}
		}

		void RaiseDBDEvent(DBDEvent eventCode, object eventContent, bool sendReliable, RaiseEventOptions options)
		{ PhotonNetwork.RaiseEvent((byte)eventCode, eventContent, sendReliable, options); }

		void OnDBDEvent(DBDEvent eventCode, object content, int senderId)
		{
			// The player was disconnected meanwhile
			if (PhotonNetwork.room == null)
				return;

			//Debug.Log("OnDBDEvent: " + eventCode + ", senderId= " + senderId + ", content= " + content);
			//PhotonPlayer sender = PhotonPlayer.Find(senderId);
			//CurrentEventContent = content;

			// Pre-checks by Master Client, dispatching events, forwarding events, inform the sender of a possible reject etc.
			if (PhotonNetwork.isMasterClient)
			{
				// For each request we set INFORM_MASTER_AND_SENDER as default, but the confirmation requests aren't dispatched to anyone. 
				// We'll just call OnAdditionalClientConfirmedExecution() for each of them
				if (eventCode != DBDEvent.PLAYER_CONFIRMED_EXECUTION)
					CurrentRequestExecuteConfirmationType = RequestExecutedConfirmationType.INFORM_MASTER_AND_SENDER;

				switch (eventCode)
				{
					// Added here to prevent control from falling into the 'default' case, as they'll be handled in the second switch below
					case DBDEvent.MASTER_REJECTED_REQUEST:

					case DBDEvent.MASTER_READY_FOR_NEW_REQUEST:
						//Debug.Log("PGM: Event: MASTER_READY_FOR_NEW_REQUEST");

						// If PLAYER_ENDED_TURN completed, the master will prepare the entities for spawning & dispatch the respawn pre-newturn event
						// In case a player is eliminated, the respective event is called first
						if (CurrentRequestedEvent == DBDEvent.PLAYER_ENDED_TURN)
						{
							//var eliminatedPlayer = UpdateEliminatedPlayersList();
							//if (eliminatedPlayer)
							//{
							//	if (Ev_Master_PlayerEliminated != null)
							//		Ev_Master_PlayerEliminated(eliminatedPlayer.ActorID);
							//}
							//else
							//{
								if (Ev_Master_PreNewTurn != null)
									Ev_Master_PreNewTurn();
							//}
						}

						break;

					case DBDEvent.BATTLE__PLAYER_READY:
						// The master (we) will either publish a "on player battle-turn started"
						// event (if the sender was first in the battle) or an "on battle results" event (if the sender was the second)
						CurrentRequestExecuteConfirmationType = RequestExecutedConfirmationType.INFORM_MASTER_ONLY;

						goto default; // the event should still be dispatched to others, as usual

						// After a battle-turn has started, the player whose battle-turn is should receive a OnReadyforNewRequest, after which his battle buttons will be enabled 
					case DBDEvent.BATTLE__PLAYER_BATTLETURN_STARTED:
						// When the first player stays idle, the next one should also be informed when is it allowed to activate the battle buttons and choose if to also stay idle or initiate
					case DBDEvent.BATTLE__PLAYER_STAYS_IDLE:
						// The sender is the one that "also stays idle", but the other player should also know
					case DBDEvent.BATTLE__APPROACHED_PLAYER_ALSO_STAYS_IDLE:
						// The sender is the one that cancelled it, but the other player should also know
					case DBDEvent.BATTLE__APPROACHED_PLAYER_CHOSE_IF_TO_CANCEL_BATTLE:
					case DBDEvent.BATTLE__APPROACHED_PLAYER_INITIATES:
					case DBDEvent.BATTLE__PLAYER_INITIATES:
					case DBDEvent.BATTLE_RESULT:
					case DBDEvent.POST_BATTLE__WINNER_DONE_LOOTING:
					case DBDEvent.GAME_INITIALIZING:
						// The other player must be informed
					case DBDEvent.TRANSFER_ITEM:
						CurrentRequestExecuteConfirmationType = RequestExecutedConfirmationType.INFORM_EVERYONE;

						goto default; // the event should still be dispatched to others, as usual

					// Received only by Master Client
					// A client has finished executing a command
					case DBDEvent.PLAYER_CONFIRMED_EXECUTION:
						{
							// In case someone sends this event and !CurrentRequestDispatchingInProgress, we simply ignore it
							if (CurrentRequestDispatchingInProgress)
								OnAdditionalClientConfirmedExecution();

							return;
						}

					//// Received if we are Master Client; 
					//// Not forwarded to other clients
					//// NEW_TURN will be instead (as long as no other request is already executing)
					//case DBDEvent.PLAYER_ENDED_TURN:
					//	{
					//		if (CurrentRequestDispatchingInProgress)
					//			// Notify the sender that the specified action is not allowed at this state/moment
					//			NotifySenderRequestRejected(content, eventCode, senderId);
					//		else
					//		{
					//			Ev_Master_PrepareRespawningEntitiesForNewTurn();
					//			//// Inform all that a new turn began
					//			//DispatchNewTurnEvent();
					//		}
					//		return;
					//	}

					default:
						{
							// Any other request can be accepted only if !CurrentRequestDispatchingInProgress
							if (CurrentRequestDispatchingInProgress)
							{
								// Notify the sender that the specified action is not allowed at this state/moment
								NotifySenderRequestRejected(content, eventCode, senderId);
								return;
							}

							CurrentRequestedEvent = eventCode;
							CurrentRequestDispatchingInProgress = true;
							CurrentRequestSenderID = senderId;
							ExecutionConfirmationsForCurrentRequest = 0;

							//if (eventCode == DBDEvent.PLAYER_SETUP_BATTLE)
							//	PlayersReadyForBattle = 0;
							//else if (eventCode == DBDEvent.BATTLE__PLAYER_READY)
							//	++PlayersReadyForBattle;

							// Notify all the other players on this action, including the sender, which will let him know the request was accepted
							RaiseDBDEvent(eventCode, content, true, new RaiseEventOptions { Receivers = ReceiverGroup.Others });
							break;
						}
				}
			}

			// Execute the request locally; these events are received by anyone, master or not
			switch (eventCode)
			{
				case DBDEvent.MASTER_READY_FOR_NEW_REQUEST:
					{
						if (Ev_MasterReadyForNewRequest != null)
							Ev_MasterReadyForNewRequest();

						break;
					}

				case DBDEvent.GAME_INITIALIZING:
					{
						if (Ev_GameInitializing != null)
							Ev_GameInitializing(new GameInitializationData(content as Hashtable));

						break;
					}

				case DBDEvent.MASTER_REJECTED_REQUEST:
					{
						if (Ev_MasterRejectedRequest != null)
							Ev_MasterRejectedRequest();
						break;
					}

				case DBDEvent.NEW_TURN:
					{
						object[] asObjArr = content as object[];

						var asIntArr = asObjArr[0] as int[];
						CurrentPlayerID = asIntArr[0];
						CurrentTurnNumber = asIntArr[1];
						var respawnDataHash = asObjArr[1] as Hashtable;
						NewTurnRespawningData respawnData = null;
						if (respawnDataHash != null)
							respawnData = new NewTurnRespawningData(respawnDataHash);

						if (respawnData != null)
							if (Ev_RespawnEntitiesPreNewTurn != null)
								Ev_RespawnEntitiesPreNewTurn(respawnData);

						if (Ev_NewTurn != null)
							Ev_NewTurn();

						break;
					}

				case DBDEvent.PLAYER_ENDED_TURN:
					if (Ev_PlayerEndedTurn != null)
						Ev_PlayerEndedTurn();
					break;

				case DBDEvent.PLAYER_MOVE_TO_CELL:
					{
						byte cellIndex = (byte)content;

						if (Ev_PlayerMoveToCellAccepted != null)
							Ev_PlayerMoveToCellAccepted(cellIndex);

						break;
					}

				case DBDEvent.OTHER_MOVE_TO_CELL:
					{
						byte startCellIndex = ((byte[])content)[0];
						byte indexInBlock = ((byte[])content)[1];
						byte cellIndex = ((byte[])content)[2];

						if (Ev_OtherMoveToCellAccepted != null)
							Ev_OtherMoveToCellAccepted(startCellIndex, indexInBlock, cellIndex);

						break;
					}

				case DBDEvent.TRANSFER_ITEM:
					{
						byte donor = ((byte[])content)[0];
						byte itemIndexInInventory = ((byte[])content)[1];
						byte receiver = ((byte[])content)[2];
						int receiverTradedItemIndexInInventory = ((byte[])content)[3]; // != byte.MaxValue only if trading
						byte acceptedByte = ((byte[])content)[4]; // != byte.MaxValue only if trading

						if (receiverTradedItemIndexInInventory == byte.MaxValue)
							receiverTradedItemIndexInInventory = -1;

						bool? accepted = null;
						if (acceptedByte != byte.MaxValue) // valid value => the other player responded to trade
							accepted = acceptedByte == 1;

						if (Ev_ItemTransfer != null)
							Ev_ItemTransfer(donor, itemIndexInInventory, receiver, new object[] { receiverTradedItemIndexInInventory, accepted });

						break;
					}

				case DBDEvent.PLAYER_SHIFT_DUNGEON:
					{
						byte shifterIndex = (byte)content;

						if (Ev_PlayerShiftDungeonAccepted != null)
							Ev_PlayerShiftDungeonAccepted(shifterIndex);

						break;
					}

				case DBDEvent.PLAYER_GET_ORE:
					{
						byte cellIndex = (byte)content;

						if (Ev_PlayerGetOreAccepted != null)
							Ev_PlayerGetOreAccepted(cellIndex);

						break;
					}

				case DBDEvent.PLAYER_DISCARD:
					{
						byte itemIndexInInventory = (byte)content;

						if (Ev_PlayerDiscardAccepted != null)
							Ev_PlayerDiscardAccepted(itemIndexInInventory);

						break;
					}
					
				case DBDEvent.PLAYER_PICKUP_ITEM:
					{
						byte itemIndexInBlock = (byte)content;

						if (Ev_PlayerPickUpItemAccepted != null)
							Ev_PlayerPickUpItemAccepted(itemIndexInBlock);

						break;
					}
					
				case DBDEvent.PLAYER_BUILD_TOTEM:
					{
						var totemType = (TotemType)content;

						if (Ev_PlayerBuildTotemAccepted != null)
							Ev_PlayerBuildTotemAccepted(totemType);

						break;
					}

				case DBDEvent.PLAYER_SETUP_BATTLE:
					{
						PlayersReadyForBattle = 0;

						byte[] data = (byte[])content;
						int cellIndesx = data[0];
						int characterIndexInBlock = data[1];

						if (Ev_PlayerSetupBattleAccepted != null)
							Ev_PlayerSetupBattleAccepted(cellIndesx, characterIndexInBlock);

						break;
					}

				case DBDEvent.PLAYER_USE_ITEM:
					{
						byte itemIndexInInventory = (byte)content;

						if (Ev_PlayerPlayCardAccepted != null)
							Ev_PlayerPlayCardAccepted(itemIndexInInventory);

						break;
					}

				#region Battle events
				case DBDEvent.BATTLE__PLAYER_INITIATES:
					if (Ev_Battle_PlayerInitiates != null)
						Ev_Battle_PlayerInitiates();
					break;

				case DBDEvent.BATTLE__PLAYER_STAYS_IDLE:
					if (Ev_Battle_PlayerStaysIdle != null)
						Ev_Battle_PlayerStaysIdle();
					break;

				case DBDEvent.BATTLE__APPROACHED_PLAYER_INITIATES:
					if (Ev_Battle_ApproachedPlayerInitiates != null)
						Ev_Battle_ApproachedPlayerInitiates();
					break;

				case DBDEvent.BATTLE__APPROACHED_PLAYER_CHOSE_IF_TO_CANCEL_BATTLE:
					if (Ev_Battle_ApproachedPlayerChoseIfToCancelBattle != null)
						Ev_Battle_ApproachedPlayerChoseIfToCancelBattle((bool)content);
					break;

				case DBDEvent.BATTLE__APPROACHED_PLAYER_ALSO_STAYS_IDLE:
					if (Ev_Battle_ApproachedPlayerAlsoStaysIdle != null)
						Ev_Battle_ApproachedPlayerAlsoStaysIdle();
					break;

				case DBDEvent.BATTLE__PLAYER_BATTLETURN_STARTED:
					{
						int playerID = (int)content;

						if (Ev_PlayerBattleTurnStarted != null)
							Ev_PlayerBattleTurnStarted(playerID);

						break;
					}

				case DBDEvent.BATTLE__PLAYER_READY:
					{
						++PlayersReadyForBattle;

						int playerID = (int)content;

						if (Ev_PlayerReadyForBattle != null)
							Ev_PlayerReadyForBattle(playerID);

						break;
					}

				case DBDEvent.BATTLE_RESULT:
					{
						PlayersReadyForBattle = 0;
						if (Ev_BattleResults != null)
							Ev_BattleResults(new BattleStats((byte[])content));

						break;
					}

				case DBDEvent.PLAYER_ELIMINATED:
					{
						int playerID = (int)content;

						if (Ev_Client_PlayerEliminated != null)
							Ev_Client_PlayerEliminated(playerID);

						break;
					}

				case DBDEvent.POST_BATTLE_WINNER_DO_LOOTING:
					{
						if (Ev_PostBattleWinnerDoLooting != null)
							Ev_PostBattleWinnerDoLooting();

						break;
					}

				case DBDEvent.POST_BATTLE__WINNER_DONE_LOOTING:
					{
						if (Ev_PostBattleWinnerDoneLooting != null)
							Ev_PostBattleWinnerDoneLooting();

						break;
					}

				case DBDEvent.GAME_RESULT:
					{
						int playerIDOrMinusOne = (int)content;
						if (Ev_GameResult != null)
							Ev_GameResult(playerIDOrMinusOne);

						break;
					}
				#endregion

				default:
					{
						Debug.Log("OnEvent: not handled: " + eventCode);
						break;
					}
			}
		}

		#region Callbacks from PUN
		void OnEvent(byte dbdEvent, object content, int senderId)
		{
			if (Enum.IsDefined(typeof(DBDEvent), dbdEvent))
			{
				// Will be logged inside it
				OnDBDEvent((DBDEvent)dbdEvent, content, senderId);
			}
			else
				Debug.Log("OnNonDBEvent: " + dbdEvent + ", senderId= " + senderId + ", content= " + content);
		}
		#endregion

		void ExecuteAfterOneFrame(Action a) { StartCoroutine(ExecuteAfterOneFrameCoroutine(a)); }
		IEnumerator ExecuteAfterOneFrameCoroutine(Action a)
		{
			yield return null;
			a();
		}
	}



    public static class DBDTurnExtensions
    {
		static void CheckForValidity(RoomInfo room, string key)
		{
			if (room == null)
				throw new UnityException("room == null");
			if (room.CustomProperties == null)
				throw new UnityException("room.CustomProperties == null");
			if (!room.CustomProperties.ContainsKey(key))
				throw new UnityException(key + " key not found");
		}

		public static BoardSetupEnum GetBoardSetup(this RoomInfo room)
		{
			CheckForValidity(room, RoomProps.BOARD_SETUP);

			return (BoardSetupEnum)int.Parse(room.CustomProperties[RoomProps.BOARD_SETUP].ToString());
		}

		public static GameStateEnum GetPrevGameState(this RoomInfo room)
		{
			CheckForValidity(room, RoomProps.PREV_GAME_STATE);
			return (GameStateEnum)int.Parse(room.CustomProperties[RoomProps.PREV_GAME_STATE].ToString());
		}

		public static GameStateEnum GetGameState(this RoomInfo room)
		{
			CheckForValidity(room, RoomProps.GAME_STATE);
			return (GameStateEnum)int.Parse(room.CustomProperties[RoomProps.GAME_STATE].ToString());
		}

		public static GameModeEnum GetGameMode(this RoomInfo room)
		{
			CheckForValidity(room, RoomProps.GAME_MODE);
			return (GameModeEnum)int.Parse(room.CustomProperties[RoomProps.GAME_MODE].ToString());
		}

		public static string ToIntString(this BoardSetupEnum th) { return ((int)th) + ""; }
		public static string ToIntString(this GameStateEnum th) { return ((int)th) + ""; }
		public static string ToIntString(this GameModeEnum th) { return ((int)th) + ""; }

		// This locally sets the class and will sync it in-game asap. 
		public static void SetClassChoice(this PhotonPlayer player, byte classChoice) { player.SetCustomProperties(new Hashtable() { { PlayerProps.PLAYER_CLASS_CHOICE, classChoice } }); }

		public static byte GetClassChoice(this PhotonPlayer player)
		{
			object choice = 0;
			player.CustomProperties.TryGetValue(PlayerProps.PLAYER_CLASS_CHOICE, out choice);

			return (byte)choice;
		}
	}
}
