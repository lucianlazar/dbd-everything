﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.UI;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.World;
using Photon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;

namespace Com.TheFallenGames.DBD.Core
{
    public class GameStateManager : PunBehaviour
    {
        public Board board;
        public SpawnManager spawnManager;
        public GameplayManager gameplayManager;
		public GameResultPanel gameResultPanel;
		//Design _Design;
		//internal Player[] EliminatedPlayers { get { return _EliminatedPlayers.ToArray(); } }
		//List<Player> _EliminatedPlayers = new List<Player>();
		PunGameManager _PGM;

		void Awake()
        {
			_PGM = PunGameManager.Instance;
			//_Design = Design.Instance;

			// Subscribe to GameStarted event. The Master Client will fire it when ready
			//if (!PhotonNetwork.isMasterClient)
			_PGM.Ev_GameInitializing += OnGameInitializing;
			_PGM.Ev_Master_PreNewTurn += OnPreNewTurn;
			_PGM.Ev_Master_PlayerEliminated += Master_OnPlayerEliminated;
			_PGM.Ev_RespawnEntitiesPreNewTurn += spawnManager.RespawnAll;
			_PGM.Ev_GameResult += OnGameResult;
		}

		IEnumerator Start()
        {
            // Waiting for other scripts' Start() methods to be called
            yield return null;
            yield return null;
            yield return null;

            if (PhotonNetwork.isMasterClient)
            {
                // Wait until all players connected, just in case they meanwhile disconnected
                do yield return new WaitForSeconds(.2f);
                while (PhotonNetwork.playerList.Length < PhotonNetwork.room.MaxPlayers);

				// Set the game state in room properties and wait for OnPhotonCustomRoomPropertiesChanged before dispatching the game initialization event
				PhotonNetwork.room.SetCustomProperties(
					new Hashtable(2)
					{
							{ RoomProps.PREV_GAME_STATE, GameStateEnum.INITIALIZING_WAITING_FOR_OTHER_PLAYERS_TO_CONNECT.ToIntString() },
							{ RoomProps.GAME_STATE, GameStateEnum.GAMEPLAY.ToIntString() }
					},
					new Hashtable(2)
					{
							{ RoomProps.PREV_GAME_STATE, GameStateEnum.NONE.ToIntString() },
							{ RoomProps.GAME_STATE, GameStateEnum.INITIALIZING_WAITING_FOR_OTHER_PLAYERS_TO_CONNECT.ToIntString() }
					}
				);
			}
            else
            {
                // Waiting for the Master Client to generate the state and send it to us
            }

            //StartCoroutine(DispatchGameInitializingAfter2FramesWhenAllPlayersConnected());
        }

		#region PunBehaviour events
		public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
		{
			// The property suggesting that all the players connected at the start of the game was successfully set
			if (propertiesThatChanged != null
				&& propertiesThatChanged.ContainsKey(RoomProps.PREV_GAME_STATE)
				&& propertiesThatChanged.ContainsKey(RoomProps.GAME_STATE)
				&& PhotonNetwork.room.GetPrevGameState() == GameStateEnum.INITIALIZING_WAITING_FOR_OTHER_PLAYERS_TO_CONNECT
				&& PhotonNetwork.room.GetGameState() == GameStateEnum.GAMEPLAY
				)
			{
				if (PhotonNetwork.isMasterClient)
				{
					// We generate the state and send it to the others
					var gameState = InitializeGameState();
					_PGM.DispatchGameInitializing(gameState.ToPhotonHashable());

					//// Call this manually for us, as the photon event will only be sent to others
					//OnGameStarted(gameState);
				}
			}
		}
		#endregion



		#region PunGameManager callbacks & related
		void OnGameInitializing(GameInitializationData initialState)
		{
			//Debug.Log("GameStateManager: OnGameInitializing");

			// Unsubscribe, since this it is(should be) only called once
			_PGM.Ev_GameInitializing -= OnGameInitializing;

            board.BuildWalls(initialState.GetDecodedWalls());
            spawnManager.SpawnAll(initialState);

            foreach (var cell in board.AllCellsIncludingOuterCell)
                spawnManager.SpawnAllowedArea(cell);


			spawnManager.SpawnExitArea(board.AllCellsIncludingOuterCell[initialState.dragon]);

			gameplayManager.enabled = true;

			// Each Player script will know its ID
			bool isTournament = PhotonNetwork.room.GetGameMode() == GameModeEnum.TOURNAMENT;
			for (int i = 0; i < spawnManager.playerInstances.Length; ++i)
			{
				int id = initialState.playerActorIDs[i];
				spawnManager.playerInstances[i].ActorID = id;

				// Passing the local player script to the LocalHumanPlayerManager
				if (id == PhotonNetwork.player.ID)
					gameplayManager.localHumanPlayerManager.Me = spawnManager.playerInstances[i];
				// Passing the local cpu player script, if any, to the LocalCPUPlayerManager
				else if (isTournament && id == DefaultPlayerIDs.CPUPlayerID)
					gameplayManager.localCPUPlayerManager.Me = spawnManager.playerInstances[i];
			}

			// Preventing any interaction wiht the cpu manager when it's not needed
			if (gameplayManager.localCPUPlayerManager && !gameplayManager.localCPUPlayerManager.Me)
				gameplayManager.localCPUPlayerManager = null;

			// Be sure that all scripts' Start are called and set this player as ready
			_PGM.Ev_MasterReadyForNewRequest += OnGameInitializedInAllClients;
			StartCoroutine(ExecuteAfterFrames_Coroutine(3, _PGM.OnCurrentActionExecuted));
        }

        void OnGameInitializedInAllClients()
		{
			//Debug.Log("GameStateManager: OnGameInitializedInAllClients");

			// Unsubscribe, since this it is(should be) only called once
			_PGM.Ev_MasterReadyForNewRequest -= OnGameInitializedInAllClients;

			// We, as the master client, choose the random player for the first turn
			if (PhotonNetwork.isMasterClient)
			{
				// Notify every one of the new turn
				var playerList = PhotonNetwork.playerList;
				int ran = UnityEngine.Random.Range(0, playerList.Length);
				_PGM.DispatchNewTurnEvent(playerList[ran].ID); // in case of local vsCPU mode, we'll be the first player
				// Wait for OnPhotonCustomRoomPropertiesChanged to be called
			}
		}

		void OnPreNewTurn()
		{
			var r = spawnManager.GetRespawnableObjects();

			int numPlayers = r.players.Count;
			int numMonsters = r.monsters.Count;
			int numDragon = r.dragon == null ? 0 : 1;
			int numCommonItems = r.commonItems.Count;
			int numEscapeItems = r.escapeItems.Count;
			int numTreasures = r.treasures.Count;

			// Nothing to respawn => dispatch the new turn event directly
			int numEntitiesToSpawn = numPlayers + numMonsters + numDragon + numCommonItems + numEscapeItems + numTreasures;
			Hashtable dataAsHashtable = null;
			if (numEntitiesToSpawn > 0)
			{
				var data = new NewTurnRespawningData()
				{
					players = new byte[numPlayers],
					monsters = new byte[numMonsters],
					dragon = new byte[numDragon],
					commonItems = new byte[numCommonItems],
					escapeItems = new byte[numEscapeItems],
					treasures = new byte[numTreasures]
				};

				var candidateCells = GetBestCellsToRespawnOnRandomized();
				for (int j = 0; j < numPlayers; ++j)
				{
					data.players[j] = (byte)candidateCells[0].indexInCellsArray;
					candidateCells.RemoveAt(0);
				}

				for (int j = 0; j < numMonsters; ++j)
				{
					data.monsters[j] = (byte)candidateCells[0].indexInCellsArray;
					candidateCells.RemoveAt(0);
				}

				if (numDragon == 1)
				{
					data.dragon[0] = (byte)candidateCells[0].indexInCellsArray;
					candidateCells.RemoveAt(0);
				}

				SelectCellsPositionsToRespawnItems(data.commonItems, candidateCells);
				SelectCellsPositionsToRespawnItems(data.escapeItems, candidateCells);
				SelectCellsPositionsToRespawnItems(data.treasures, candidateCells);

				dataAsHashtable = data.ToPhotonHashable();
			}

			_PGM.DispatchNewTurnEvent(dataAsHashtable);
		}

		void SelectCellsPositionsToRespawnItems(byte[] positions, List<Cell> candidateCells)
		{
			for (int i = 0, j = 0; i < candidateCells.Count && j < positions.Length; ++i)
			{
				var cell = candidateCells[i];
				int maxAllowedItems = cell.ContainedBlock.ContainsOre() ? 1 : 0;
				if (cell.ContainedBlock.GetChildrenOfType<BaseItem>().Count > maxAllowedItems)
					continue;

				positions[j++] = (byte)candidateCells[0].indexInCellsArray;
				candidateCells.RemoveAt(i);
			}
		}

		void Master_OnPlayerEliminated(int playerID)
		{
			_PGM.DoAfterMasterReady(() => OnAllClientsReceivedEliminationEvent(playerID));

			// First, dispatch the client-specific elimination event
			_PGM.DispatchPlayerEliminatedEvent_Client(playerID);
		}

		void OnAllClientsReceivedEliminationEvent(int eliminatedPlayerID)
		{
			if (
				PhotonNetwork.room.GetGameMode() == GameModeEnum.TOURNAMENT // Tournament: one of them was killed
				|| _PGM.PlayerCountIncludingCPUIfAny == 1 // Singleplayer: the only one was killed => game end
				)
			{
				_PGM.DispatchGameResultEvent(-1);
				return;
			}

			var allEliminated = SpawnManager.Instance.GetPlayersByEliminatedState(true);

			// Multiplayer: If only one left => game end
			if (allEliminated.Count == _PGM.PlayerCountIncludingCPUIfAny - 1)
			{
				var nonEliminated = SpawnManager.Instance.GetPlayersByEliminatedState(false);

				if (nonEliminated.Count != 1)
				{
					Debug.LogError("FATAL: nonEliminatedPlayers.Count=" + nonEliminated.Count);
					foreach (var p in nonEliminated)
						Debug.LogError("--- found as non eliminated: " + p.Design.name);
				}

				var nonEliminatedPlayer = nonEliminated[0];

				_PGM.DispatchGameResultEvent(nonEliminatedPlayer.ActorID);
				return;
			}

			// The game can continue => Dispatch the post battle event

			_PGM.DispatchPostBattleEvent();
		}

		void OnGameResult(int winnerPlayerID)
		{
			string title, center, sub;
			if (winnerPlayerID == -1)
			{
				title = "Game Over";
				center = "You've lost";
				sub = "";
			}
			else
			{
				title = "Winner:";
				center = spawnManager.GetPlayerInstanceByActorID(winnerPlayerID).Design.name;
				sub = "(" + _PGM.GetPlayerWithActorID(winnerPlayerID).NickName + ")";
			}

			gameResultPanel.Show(title, center, sub, null);
			// Not calling OnCurrentRequestExecuting, because at this point no other request should be
		}
		#endregion

		IEnumerator ExecuteAfterFrames_Coroutine(int frames, Action action)
        {
            while (frames-- > 0)
                yield return null;

            action();

            yield break;
        }

        //IEnumerator DispatchGameInitializingAfter2FramesWhenAllPlayersConnected()
        //{
        //    yield return null;
        //    yield return null;

        //    if (PhotonNetwork.isMasterClient)
        //    {
        //        // Wait until all players connected
        //        do yield return new WaitForSeconds(.5f);
        //        while (PhotonNetwork.playerList.Length < PhotonNetwork.room.MaxPlayers);

        //        // We generate the state and send it to the others
        //        var gameState = InitializeGameState();
        //        PunGameManager.Instance.DispatchEvent_GameInitializing(gameState.ToPhotonHashable());

        //        //// Call this manually for us, as the photon event will only be sent to others
        //        //OnGameStarted(gameState);
        //    }
        //    else
        //    {
        //        // Waiting for the Master Client to generate the state and send it to us
        //    }
        //}

        /// <summary> Called if we're the master</summary>
        GameInitializationData InitializeGameState()
        {
            GameInitializationData data = new GameInitializationData();

			// Player photon IDs
            data.playerActorIDs = RetrievePlayersActorIDsSorted();

			// Board setup
			//Debug.Log(PhotonNetwork.room.GetBoardSetup());
			if (PhotonNetwork.room.GetBoardSetup() == Data.BoardSetupEnum.CLASSIC)
			{
				var classicSetup = new ClassicBoardSetup();
				classicSetup.ReadAsset();

				data.UpdateFromClassicBoardSetup(classicSetup);

				var chosenSpawnPositionsIndices = Utils.ChooseRandomAsBytes(0, classicSetup.spawnPoints.Count, _PGM.PlayerCountIncludingCPUIfAny);
				var chosenSpawnPositions = Array.ConvertAll(chosenSpawnPositionsIndices, i => (byte)classicSetup.spawnPoints[i]);
				data.players = chosenSpawnPositions;
			}
			else
			{
				data.GenerateRandom(board.AllCellsIncludingOuterCell.Length, _PGM.PlayerCountIncludingCPUIfAny);
			}

			// Player choices
			data.playerChoices = new byte[_PGM.PlayerCountIncludingCPUIfAny];
			for (byte i = 0; i < _PGM.PlayerCountIncludingCPUIfAny; ++i)
			{
				if (_PGM.PlayerListWithCPUIfAny[i].ID == DefaultPlayerIDs.CPUPlayerID)
				{
					// Hardcoding archer for CPU for now
					Debug.Log("TODO class choice for CPU");
					data.playerChoices[i] = 1;
					continue;
				}
				data.playerChoices[i] = _PGM.PlayerListWithCPUIfAny[i].GetClassChoice();
			}

			return data;
        }

		#region calculations & queries
		int[] RetrievePlayersActorIDsSorted()
        {
            PhotonPlayer currentPlayerWithMinID = null;
            var playerListWithCPUIfAny = new List<PhotonPlayer>(_PGM.PlayerListWithCPUIfAny);

			var sortedIDsList = new List<int>();
            do
            {
                int min = int.MaxValue;
                for (int i = 0; i < playerListWithCPUIfAny.Count; ++i)
                {
                    if (playerListWithCPUIfAny[i].ID <= min)
                    {
                        currentPlayerWithMinID = playerListWithCPUIfAny[i];
                        min = currentPlayerWithMinID.ID;
                    }
                }
                playerListWithCPUIfAny.Remove(currentPlayerWithMinID);
                sortedIDsList.Add(min);
            } while (playerListWithCPUIfAny.Count > 0);


			return sortedIDsList.ToArray();
        }



		List<Cell> GetBestCellsToRespawnOnRandomized()
		{
			var allCells = board.AllCellsIncludingOuterCell;

			//var freeCells = new List<Cell>();
			var highPriorityCells = new List<Cell>();
			//var oreOnlyCells = new List<Cell>();
			//var itemOnlyCells = new List<Cell>();
			//var itemAndOreOnlyCells = new List<Cell>();
			//var multipleItemsOnlyCells = new List<Cell>();
			var totemOnlyCells = new List<Cell>();
			var mixedTotemWithItem = new List<Cell>();
			var monsterOrPlayerOnlyCells = new List<Cell>();
			var mixedMonsterOrPlayerWithWithItem = new List<Cell>();
			var lowPCharWith2OrMoreItems = new List<Cell>();

			var allLists =
			new List<Cell>[]
			{
				highPriorityCells, /*oreOnlyCells, itemOnlyCells, itemAndOreOnlyCells, multipleItemsOnlyCells,*/ totemOnlyCells,
				mixedTotemWithItem, monsterOrPlayerOnlyCells, mixedMonsterOrPlayerWithWithItem, lowPCharWith2OrMoreItems
			};

			foreach (var cell in allCells)
			{
				var block = cell.ContainedBlock;
				int numEntities = block.NumContainedEntities;
				int itemsCount = block.GetChildrenOfType<BaseItem>().Count;
				int totemsCount = block.GetChildrenOfType<Totem>().Count;
				if (numEntities == 0)
				{
					highPriorityCells.Add(cell);
				}
				else if (numEntities == 1)
				{
					if (block.ContainsOre())
					{
						//oreOnlyCells.Add(cell);
						highPriorityCells.Add(cell);
					}
					else if (itemsCount == 2) // 1st=ore, 2nd=item
					{
						//itemOnlyCells.Add(cell);
						highPriorityCells.Add(cell);
					}
					else if (totemsCount == 1)
					{
						totemOnlyCells.Add(cell);
					}
					else
					{
						monsterOrPlayerOnlyCells.Add(cell);
					}
				}
				else if (numEntities == 2)
				{
					bool hasOre = block.ContainsOre();
					if (itemsCount == 2)
					{
						//if (hasOre)
						//	itemAndOreOnlyCells.Add(cell);
						//else
						//	multipleItemsOnlyCells.Add(cell);
						highPriorityCells.Add(cell);
					}
					else
					{
						if (totemsCount == 1)
							mixedTotemWithItem.Add(cell);
						else
							mixedMonsterOrPlayerWithWithItem.Add(cell);
					}
				}
				else
				{
					if (itemsCount == numEntities)
					{
						//multipleItemsOnlyCells.Add(cell);
						highPriorityCells.Add(cell);
					}
					else
					{
						lowPCharWith2OrMoreItems.Add(cell);
					}
				}
			}

			var finalResults = new List<Cell>();
			foreach (var list in allLists)
			{
				Utils.RandomizeList(list);
				finalResults.AddRange(list);
			}

			return finalResults;
		}
		#endregion
	}
}
