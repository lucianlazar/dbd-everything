﻿using System;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.World;
using UnityEngine;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Data.Inventory;

namespace Com.TheFallenGames.DBD.Core
{
    public class SpawnManager : MonoBehaviour
    {
        public static SpawnManager Instance { get; private set; }

		public int PlayerCount { get { return playerInstances.Length; } }

        public Board board;
        public Ore orePrefab;
        public CommonItem commonItemPrefab;
        public MonsterItem monsterItemPrefab;
        public EscapeItem escapeItemPrefab;
        public Treasure treasurePrefab;
        public Monster monsterPrefab;
        public Dragon dragonPrefab;
		public Player playerPrefab;
        public Totem totemPrefab;
		public AllowedArea allowedAreaPrefab;
		public ExitArea exitAreaPrefab;

		[NonSerialized] public Ore[] oreInstances;
		[NonSerialized] public CommonItem[] commonItemInstances;
		[NonSerialized] public EscapeItem[] escapeItemInstances;
		[NonSerialized] public Treasure[] treasureInstances;
		[NonSerialized] public Monster[] monsterInstances;
		[NonSerialized] public Dragon dragonInstance;
		[NonSerialized] public Player[] playerInstances; // ordered by actor id asc
		[NonSerialized] public Totem[] totemInstances = new Totem[0]; // need to have the list initialized beforehand, since no totems are initially spawned
		[NonSerialized] public ExitArea exitAreaInstance;
		//public List<Entity> graveyard = new List<Entity>();
		[NonSerialized] public List<Entity> allEntities = new List<Entity>();
		
		Design _Design;

		public static readonly Vector3 allEntitiesSpawnPositionDelta = Vector3.up * .3f;


		void Awake()
        {
            Instance = this;
            _Design = Design.Instance;
        }

		void OnDestroy()
		{
			Instance = null;	
		}

		internal Player GetPlayerInstanceByActorID(int actorID)
		{
			foreach (var player in playerInstances)
				if (player.ActorID == actorID)
					return player;

			return null;
		}

		// Updates the list & returns the newly eliminated player, if any
		internal List<Player> GetPlayersByEliminatedState(bool eliminated)
		{
			var r = new List<Player>();
			foreach (var p in playerInstances)
				if (p.Stats.HasSoul != eliminated)
					r.Add(p);

			return r;
		}

		internal Player GetPlayerInstanceAtRelativePosition(Player relativeTo, int relativePosition)
		{
			int indexOfWanted = Array.IndexOf(playerInstances, relativeTo) + relativePosition;
			indexOfWanted += playerInstances.Length; // in case it's -1, loop to the last one
			indexOfWanted %= playerInstances.Length;

			return playerInstances[indexOfWanted];
		}

		internal List<Character> GetAllCharacters(bool includeTotems)
		{
			var l = new List<Character>(playerInstances);
			l.AddRange(monsterInstances);
			l.Add(dragonInstance);
			if (includeTotems)
				l.AddRange(totemInstances);

			return l;
		}

		internal void ReLinkNonPickedUpMonsterItems()
		{
			foreach (var m in monsterInstances)
			{
				if (m.RewardItem.OwnerAsBlock != null) // monster is dead and item left on the spot
					m.RewardItem.GiveToOriginalOwner();
			}
		}

		internal RespawnableObjectsHolder GetRespawnableObjects()
		{
			var r = new RespawnableObjectsHolder();
			r.commonItems = GetItemsWithNoParentBlockOrOwner(commonItemInstances);
			r.escapeItems = GetItemsWithNoParentBlockOrOwner(escapeItemInstances);
			r.treasures = GetItemsWithNoParentBlockOrOwner(treasureInstances);
			r.monsters = GetEntitiesWithNoParentBlock(monsterInstances);
			r.monsters.RemoveAll(m => m.RewardItem.OwnerAsPlayer != null); // only respawn dead monsters if their RewardItem is not owned by a player
			// Same for the dragon
			r.dragon = (dragonInstance.GetParentAsBlock() == null && dragonInstance.RewardItem.OwnerAsPlayer == null) ? dragonInstance : null;
			r.players = GetEntitiesWithNoParentBlock(playerInstances);
			r.players.RemoveAll(p => !p.Stats.HasSoul); // only respawning players with a soul

			return r;
		}

		internal List<T> GetItemsWithNoParentBlockOrOwner<T>(T[] inputArray) where T : BaseItem { return GetItemsWithNoOwner(GetEntitiesWithNoParentBlock(inputArray).ToArray()); }

		internal List<T> GetEntitiesWithNoParentBlock<T>(T[] inputArray) where T : Entity
		{
			var results = new List<T>();
			foreach (var v in inputArray)
				if (v.GetParentAsBlock() == null)
					results.Add(v);

			return results;
		}

		internal List<T> GetItemsWithNoOwner<T>(T[] inputArray) where T : BaseItem
		{
			var results = new List<T>();
			foreach (var v in inputArray)
				if (v.Owner == null)
					results.Add(v);

			return results;
		}

		internal List<CommonItem> GetCommonItemsSortedByDesignCostDesc()
		{
			var list = new List<CommonItem>(commonItemInstances);
			list.Sort((a, b) => b.Design.Cost - a.Design.Cost);
			return list;
		}

		//internal Entity GetEntityByCardModel(CardModel cardModel)
		//{
		//	return allEntities.Find(e => e.GetCardModel() == cardModel);
		//}

		/// <summary>
		/// Doesn't work for players, because the same class can be present twice
		/// </summary>
		/// <param name="design"></param>
		/// <returns></returns>
		internal Entity GetEntityByDesign(EntityDesign design)
		{
			return allEntities.Find(e => e.Design == design);
		}

		internal void SpawnAll(GameInitializationData initialState)
		{
			SpawnEntities(out oreInstances, initialState.ore, 0, initialState.ore.Length, orePrefab, _Design.items.ore);
			SpawnItems(initialState);
			SpawnEntities(out monsterInstances, initialState.monsters, 0, initialState.monsters.Length, monsterPrefab, _Design.monsters.GetAllExceptDragon().ToArray());
			dragonInstance = SpawnEntity(initialState.dragon, dragonPrefab, _Design.monsters.DRAGON);
			SpawnMonsterItems();
			SpawnPlayers(initialState);
		}

		internal void SpawnExitArea(Cell parentCell)
		{
			exitAreaInstance = GameObject.Instantiate(exitAreaPrefab.gameObject, parentCell.ContainedBlock.transform).GetComponent<ExitArea>();
			exitAreaInstance.transform.position = parentCell.transform.position;
			exitAreaInstance.transform.position += allEntitiesSpawnPositionDelta;
			exitAreaInstance.name = typeof(ExitArea).Name;
			exitAreaInstance.IsOpen = false;		}

		internal AllowedArea SpawnAllowedArea(Cell parentCell)
        {
            var instance = GameObject.Instantiate(allowedAreaPrefab.gameObject, parentCell.transform).GetComponent<AllowedArea>();
            instance.transform.position = parentCell.transform.position;
            instance.transform.position += allEntitiesSpawnPositionDelta;
            instance.gameObject.SetActive(false);
            instance.name = typeof(AllowedArea).Name;
            parentCell.AllowedAreaHighlight = instance;

            return instance;
        }

		internal Totem SpawnTotem(Cell parentCell, Player owner, TotemDesign design)
		{
			var totemInstance = InstantiateAndStoreEntity(totemPrefab, design);
			totemInstance.Owner = owner;
			totemInstance.SetParentBlock(parentCell.ContainedBlock);
			var newList = new List<Totem>(totemInstances);
			newList.Add(totemInstance);
			totemInstances = newList.ToArray();

			return totemInstance;
		}

		internal void DestroyTotem(Totem totem)
		{
			totem.Owner = null;
			var newList = new List<Totem>(totemInstances);
			newList.Remove(totem);
			GameObject.Destroy(totem.gameObject);
			totemInstances = newList.ToArray();
		}

		internal void RespawnEntity(Entity entity, params Type[] allowedEntityTypesOnRespawnPosition)
        {
            List<int> indicesOfCandidateCells = new List<int>();
            for (int i = 0; i < board.AllCellsIncludingOuterCell.Length; ++i)
            {
                var block = board.AllCellsIncludingOuterCell[i].ContainedBlock;

                bool allowed = true;
                foreach (var entityInBlock in block)
                {
                    foreach (var allowedType in allowedEntityTypesOnRespawnPosition)
                    {
                        allowed = allowedType.IsAssignableFrom(entityInBlock.GetType());

                        // Found an entity that prevents the current entity to spawn on this block 
                        if (!allowed)
                            break;
                    }

                    // Block not availabe for respawn => go to next one
                    if (!allowed)
                        break;
                }

                if (allowed)
                    indicesOfCandidateCells.Add(i);
            }

            int respawnCellIndex = UnityEngine.Random.Range(0, indicesOfCandidateCells.Count);
            Block respawnBlock = board.AllCellsIncludingOuterCell[indicesOfCandidateCells[respawnCellIndex]].ContainedBlock;

            entity.RespawnOnBlock(respawnBlock, allEntitiesSpawnPositionDelta);
		}


		internal void RespawnAll(NewTurnRespawningData respawnData)
		{
			var r = GetRespawnableObjects();

			// Commented: this is done immediately after the item is picked up
			//// Any item that was temporarily free, won't be anymore
			//foreach (var commonItem in r.commonItems)
			//	commonItem.IsTemporarilyFree = false;

			//for (int i = 0; i < r.players.Count; ++i)
			//	r.players[i].RespawnOnBlock(board.AllCellsIncludingOuterCell[respawnData.players[i]].ContainedBlock, allEntitiesSpawnPositionDelta);

			//for (int i = 0; i < r.monsters.Count; ++i)
			//	r.monsters[i].RespawnOnBlock(board.AllCellsIncludingOuterCell[respawnData.monsters[i]].ContainedBlock, allEntitiesSpawnPositionDelta);

			//for (int i = 0; i < r.commonItems.Count; ++i)
			//	r.commonItems[i].RespawnOnBlock(board.AllCellsIncludingOuterCell[respawnData.commonItems[i]].ContainedBlock, allEntitiesSpawnPositionDelta);

			//for (int i = 0; i < r.escapeItems.Count; ++i)
			//	r.escapeItems[i].RespawnOnBlock(board.AllCellsIncludingOuterCell[respawnData.escapeItems[i]].ContainedBlock, allEntitiesSpawnPositionDelta);

			//for (int i = 0; i < r.treasures.Count; ++i)
			//	r.treasures[i].RespawnOnBlock(board.AllCellsIncludingOuterCell[respawnData.treasures[i]].ContainedBlock, allEntitiesSpawnPositionDelta);
			var allEntities = r.All;
			var allPositions = respawnData.All;


			for (int i = 0; i < allEntities.Count; ++i)
			{
				try
				{
					allEntities[i].RespawnOnBlock(board.AllCellsIncludingOuterCell[allPositions[i]].ContainedBlock, allEntitiesSpawnPositionDelta);
				}
				catch
				{
					Debug.Log("RespawnAll error: allEntities=" + allEntities.Count + ", allPositions=" + allPositions.Count);
					Debug.Log("RespawnAll error: allPositions[i]=" + allPositions[i]);

					foreach (var r2 in allEntities)
						Debug.Log(r2.name);

					throw;
				}

			}

		}

		internal Ore SpawnOre(Block block, int amount)
		{
			int prevCount = oreInstances.Length;
			int newCount = prevCount + 1;
			oreInstances = new Ore[newCount];
			var newInstance = InstantiateAndStoreEntity(orePrefab, _Design.items.ore);
			newInstance.SetParentBlock(block, allEntitiesSpawnPositionDelta);
			newInstance.Amount = amount;

			return newInstance;
		}

		internal void DestroyOre(Ore ore)
		{
			var l = new List<Ore>(oreInstances);
			l.Remove(ore);
			oreInstances = l.ToArray();
			ore.SetParentBlock(null);
			ore.DissolveAndDestroySelf();
		}

		internal void SpawnMonsterItem(Monster monster)
        {
            monster.RewardItem = InstantiateAndStoreEntity(monsterItemPrefab, monster.Design.rewardItem);
        }

        void SpawnItems(GameInitializationData initialState)
        {
            BaseItemDesign[] currentItems = _Design.items.commonItems.GetAll();
            SpawnEntities(out commonItemInstances, initialState.items, 0, currentItems.Length, commonItemPrefab, currentItems);
            currentItems = _Design.items.escapeItems.GetAll();
            SpawnEntities(out escapeItemInstances, initialState.items, commonItemInstances.Length, currentItems.Length, escapeItemPrefab, currentItems);
            currentItems = _Design.items.treasures.GetAll();
            SpawnEntities(out treasureInstances, initialState.items, commonItemInstances.Length + escapeItemInstances.Length, currentItems.Length, treasurePrefab, currentItems);
        }

        //internal void SpawnOre(byte[] ore)
        //{
        //    oreInstances = new Ore[ore.Length];
        //    for (int i = 0; i < ore.Length; ++i)
        //    {
        //        var instance = Instantiate(orePrefab.gameObject, board.AllCellsIncludingOuterCell[ore[i]].ContainedBlock.transform).GetComponent<Ore>();
        //        instance.transform.position = instance.transform.parent.position + Vector3.up * .2f;
        //        oreInstances[i] = instance;
        //    }
        //}

        //internal void SpawnItems(byte[] items)
        //{
        //    var all = _Design.items.GetAll();
        //    foreach (var index in items)
        //    {
        //        Entity.InstantiateEntity(itemPrefab, all[index]);
        //        var instance = Instantiate(orePrefab.gameObject, board.AllCellsIncludingOuterCell[index].ContainedBlock.transform).GetComponent<Ore>();
        //        instance.transform.position = instance.transform.parent.position + Vector3.up * .2f;
        //    }
        //}

        //internal void SpawnMonsters(byte[] monsters)
        //{
        //    foreach (var index in monsters)
        //    {
        //        Entity.InstantiateEntity<Monster>()
        //        var instance = Instantiate(orePrefab.gameObject, board.AllCellsIncludingOuterCell[index].ContainedBlock.transform).GetComponent<Ore>();
        //        instance.transform.position = instance.transform.parent.position + Vector3.up * .2f;
        //    }
        //}

        void SpawnPlayers(GameInitializationData initialState)
        {
            var allPlayerDesigns = _Design.players.GetAll();
            var chosenPlayerDesignsOrderedByPlayerIndex = new PlayerDesign[initialState.playerChoices.Length];
            for (int i = 0; i < initialState.playerChoices.Length; ++i)
                chosenPlayerDesignsOrderedByPlayerIndex[i] = allPlayerDesigns[initialState.playerChoices[i]];
            SpawnEntities(out playerInstances, initialState.players, 0, initialState.players.Length, playerPrefab, chosenPlayerDesignsOrderedByPlayerIndex);
        }

        void SpawnEntities<T>(out T[] arrayOfInstances, byte[] positions, int firstPosIndex, int count, T prefab, params EntityDesign[] designs) where T : Entity
        {
            bool onePrefab = designs.Length == 1;
            EntityDesign theOnlyDesign = designs[0];
            arrayOfInstances = new T[count];
            for (int i = firstPosIndex, instanceIndex; i < firstPosIndex + count; ++i)
            {
                //try
                //{
                    instanceIndex = i - firstPosIndex;
                    arrayOfInstances[instanceIndex] = SpawnEntity(positions[i], prefab, onePrefab ? theOnlyDesign : designs[instanceIndex]);
    //            }
				//catch (Exception e)
    //            {
    //                Debug.LogException(e);
    //            }
            }
        }

		T SpawnEntity<T>(byte position, T prefab, EntityDesign design) where T : Entity
		{
			var instance = InstantiateAndStoreEntity(prefab, design);
			instance.SetParentBlock(board.AllCellsIncludingOuterCell[position].ContainedBlock, allEntitiesSpawnPositionDelta);

			return instance;
		}


		T InstantiateAndStoreEntity<T>(T prefab, EntityDesign design) where T : Entity
		{
			var instance = Entity.InstantiateEntity(prefab, design);
			allEntities.Add(instance);

			return instance;
		}

		void SpawnMonsterItems()
        {
            foreach (var monster in monsterInstances)
                SpawnMonsterItem(monster);

			SpawnMonsterItem(dragonInstance);
		}
	}
}
