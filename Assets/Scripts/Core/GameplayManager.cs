﻿using UnityEngine;
using System.Collections;
using Photon;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Core;
using UnityEngine.SceneManagement;
using System;
using Com.TheFallenGames.DBD.World;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.UI.Totems;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.UI;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.UI.Inspect;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Core.PlayerManagers;

namespace Com.TheFallenGames.DBD.Core
{
	public class GameplayManager : PunBehaviour
	{
		public HumanPlayerManager localHumanPlayerManager;
		public CPUPlayerManager localCPUPlayerManager;

		// UI stuff
		public Button endTurnButton;
		public Button leaveRoomButton;
		public TotemsPanel totemsPanel;
		public ContextPanel contextPanel;
		public InventoryPanel inventoryPanel;
		public BattlePanel battlePanel;
		public InspectPanel inspectPanel;
		public StatsPanel statsPanel;
		public Text turnText;
		public Board board;

		// Null if the turn is neither local CPU's nor local player's 
		public BasePlayerManager CurrentLocalPlayerManager
		{
			get
			{
				if (localHumanPlayerManager.IsMyTurn)
					return localHumanPlayerManager;
				if (localCPUPlayerManager && localCPUPlayerManager.IsMyTurn)
					return localCPUPlayerManager;
				return null;
			}
		}
		// Null if the battle turn is neither local CPU's nor local player's 
		public BasePlayerManager CurrentLocalPlayerManagerInBattle
		{
			get
			{
				if (localHumanPlayerManager.IsMyTurnInBattle)
					return localHumanPlayerManager;
				if (localCPUPlayerManager && localCPUPlayerManager.IsMyTurnInBattle)
					return localCPUPlayerManager;
				return null;
			}
		}
		// Null if the battle turn is neither local CPU's nor local player's OR is post battle
		public BasePlayerManager CurrentLocalPlayerManagerInBattleIfNotEnded
		{
			get
			{
				if (localHumanPlayerManager.IsMyTurnInBattleAndBattleNotEnded)
					return localHumanPlayerManager;
				if (localCPUPlayerManager && localCPUPlayerManager.IsMyTurnInBattleAndBattleNotEnded)
					return localCPUPlayerManager;
				return null;
			}
		}
		// Null if neither the local CPU nor the local player if the opponent of the current player
		public BasePlayerManager LocalPlayerManagerIfOpponentOfCurrentPlayer
		{
			get
			{
				if (localHumanPlayerManager.AmIBattlingCurrentPlayer)
					return localHumanPlayerManager;
				if (localCPUPlayerManager && localCPUPlayerManager.AmIBattlingCurrentPlayer)
					return localCPUPlayerManager;
				return null;
			}
		}
		// The current local player or the winner of the current battle, if local (human or cpu). Null, else
		public BasePlayerManager WinnerLocalPMOfCurrentBattleOrCurrentLocalPlayerPM
		{
			get
			{
				if (WinnerOfCurrentBattleOrCurrentPlayer == localHumanPlayerManager.Me)
					return localHumanPlayerManager;
				if (localCPUPlayerManager && WinnerOfCurrentBattleOrCurrentPlayer == localCPUPlayerManager.Me)
					return localCPUPlayerManager;
				return null;
			}
		}
		// The winner of the current battle, if local (human or cpu). Null, else (or if there's no winner or battle)
		public BasePlayerManager WinnerLocalPlayerPMOfCurrentBattle
		{
			get
			{
				if (WinnerOfCurrentBattle == localHumanPlayerManager.Me)
					return localHumanPlayerManager;
				if (localCPUPlayerManager && WinnerOfCurrentBattle == localCPUPlayerManager.Me)
					return localCPUPlayerManager;
				return null;
			}
		}

		// The Player whose turn is
		public Player CurrentPlayer { get; set; }
		// The Monster/Player whom CurrentPlayer is battling
		public Character OpponentOfCurrentPlayer { get; set; }
		public Character WinnerOfCurrentBattle { get; set; }
		public Player WinnerOfCurrentBattleAsPlayer { get { return WinnerOfCurrentBattle as Player; } }
		// The player whose turn is to play cards in battle
		public Player CurrentPlayerInBattle
		{
			get { return _CurrentPlayerInBattle; }
			set
			{
				_CurrentPlayerInBattle = value;
				UpdateInventoryInteractability();
			}
		}
		// Returns the current player whose battle-turn is, if there's a battle; Else, the current player
		public Player CurrentPlayerInBattleOrCurrentPlayer { get { return CurrentPlayerInBattle == null ? CurrentPlayer : CurrentPlayerInBattle; } }
		public Player WinnerOfCurrentBattleOrCurrentPlayer { get { return WinnerOfCurrentBattleAsPlayer == null ? CurrentPlayer : WinnerOfCurrentBattleAsPlayer; } }
		public Block LosingBlockOfCurrentBattle { get; set; }
		public Character LosingCharacterOfCurrentBattle { get { return WinnerOfCurrentBattle == null ? null : (CurrentPlayer == WinnerOfCurrentBattle ? OpponentOfCurrentPlayer : CurrentPlayer); } }
		public bool IsPreBattle { get { return CurrentPlayer.Stats.InBattle && CurrentPlayerInBattle == null; } }
		public bool IsIntraBattle { get { return CurrentPlayerInBattle != null && !IsPostBattle; } }
		public bool IsPostBattle { get { return WinnerOfCurrentBattleAsPlayer != null; } }
		public bool IsPreIntraOrPostBattle { get { return IsPreBattle || IsIntraBattle || IsPostBattle; } }
		public bool IsPostBattleAndCurrentPlayerWon { get { return IsPostBattle && WinnerOfCurrentBattleAsPlayer == CurrentPlayer; } }

		internal List<Cell> HighlightedCells { get { return _LastHighLightedCells; } }

		Design _Design;
		SpawnManager _SpawnManager;
		PunGameManager _PGM;
		Player _CurrentPlayerInBattle;
		List<Cell> _LastHighLightedCells = new List<Cell>();


		#region Unity methods
		void Awake()
		{
			_Design = Design.Instance;
			_SpawnManager = FindObjectOfType<SpawnManager>();
			board = FindObjectOfType<Board>();

			inspectPanel.gameObject.SetActive(true);
			battlePanel.gameObject.SetActive(true);

			_PGM = PunGameManager.Instance;

			//_PGM.GameStarted += OnGameStarted;
			_PGM.Ev_NewTurn += OnNewTurn;
			_PGM.Ev_PlayerEndedTurn += OnPlayerEndedTurn;
			_PGM.Ev_PlayerMoveToCellAccepted += OnMoveToCellAccepted;
			_PGM.Ev_OtherMoveToCellAccepted += OnOtherMoveToCellAccepted;
			_PGM.Ev_PlayerShiftDungeonAccepted += OnDungeonPushAccepted;
			_PGM.Ev_PlayerGetOreAccepted += OnGetOreAccepted;
			_PGM.Ev_PlayerDiscardAccepted += OnDiscardAccepted;
			_PGM.Ev_PlayerPickUpItemAccepted += OnPickUpItemAccepted;
			_PGM.Ev_PlayerBuildTotemAccepted += OnPlayerBuildTotemAccepted;
			_PGM.Ev_PlayerSetupBattleAccepted += OnSetupBattleAccepted;
			_PGM.Ev_PlayerPlayCardAccepted += OnPlayCardAccepted;
			_PGM.Ev_Battle_PlayerInitiates += OnBattlePlayerInitiates;
			_PGM.Ev_Battle_PlayerStaysIdle += OnBattlePlayerStaysIdle;
			_PGM.Ev_Battle_ApproachedPlayerChoseIfToCancelBattle += OnBattleApproachedPlayerChoseIfToCancelBattle;
			_PGM.Ev_Battle_ApproachedPlayerInitiates += OnBattleApproachedPlayerInitiates;
			_PGM.Ev_Battle_ApproachedPlayerAlsoStaysIdle += OnBattleApproachedPlayerAlsoStaysIdle;
			_PGM.Ev_PlayerReadyForBattle += OnPlayerReadyForBattleAccepted;
			_PGM.Ev_PlayerBattleTurnStarted += OnPlayerBattleTurnStarted;
			_PGM.Ev_BattleResults += OnBattleResultsObtainedFromMaster;
			_PGM.Ev_Client_PlayerEliminated += Client_OnPlayerEliminated;
			_PGM.Ev_PostBattleWinnerDoLooting += PostBattleWinnerDoLooting;
			_PGM.Ev_PostBattleWinnerDoneLooting += OnPostBattleWinnerDoneLooting;
			_PGM.Ev_ItemTransfer += OnItemTransferRequestAccepted;
			_PGM.Ev_GameResult += OnGameResult;
		}

		void Start()
		{
			localHumanPlayerManager.enabled = true;
			if (PhotonNetwork.room.GetGameMode() == GameModeEnum.TOURNAMENT)
				localCPUPlayerManager.enabled = true;
		}

#if UNITY_EDITOR
		void Update()
		{
			if (Time.frameCount % (60 * 2) == 0 && CurrentPlayer)
				UpdateWinChances();
		}
#endif

		void OnDestroy()
		{

			//Debug.Log("Destr GpM");
		}
		#endregion


		#region Commands triggered by UI events (local player) OR by CPUPlayerManager
		public void Submit_MoveToCell(Cell cell)
		{
			_PGM.RequestMove(cell.indexInCellsArray);
		}

		public void Submit_MoveOther(Character otherCharacter, Cell targetCell)
		{
			Cell startCell = otherCharacter.GetParentCell();
			_PGM.RequestMoveOther(startCell.indexInCellsArray, startCell.ContainedBlock.GetIndexOfChild(otherCharacter), targetCell.indexInCellsArray);
		}

		public void Submit_StartBattleWith(Character otherChar)
		{
			var cell = otherChar.GetParentCell();
			_PGM.RequestSetupBattle(cell.indexInCellsArray, cell.ContainedBlock.GetIndexOfChild(otherChar));
			UnHighlightLastReachableCells();
			if (inspectPanel.IsShownOrShowing) // if it's CPU's turn, this should still be closed, if opened, because the battle panel has a higher priority of being shown
				inspectPanel.Close(0f, null);
		}

		// Sender is local player or local CPU
		public void Submit_InitiateBattle(Player sender)
		{
			if (CurrentPlayer == sender)
				_PGM.RequestInitiateBattleFirst();
			else
				_PGM.RequestInitiateBattleAfterCurrentPlayerPrefersNotTo();
		}

		// Sender is local player or local CPU
		public void Submit_StayIdleInBattle(Player sender)
		{
			if (CurrentPlayer == sender)
				_PGM.RequestStayIdleInBattleFirst();
			else
				_PGM.RequestStayIdleInBattleAfterCurrentPlayerAlsoStays();
		}

		public void Submit_ChooseIfToCancelBattle(bool cancel) { _PGM.RequestChooseIfToCancelBattle(cancel); }

		public void Submit_GetOre(Cell cell)
		{
			UnHighlightLastReachableCells();
			_PGM.RequestGetOre(cell.indexInCellsArray);
		}

		public void Submit_PickUpItem(BaseItem item)
		{
			_PGM.RequestPickUpItem(item.GetParentAsBlock().GetIndexOfChild(item));
		}

		/// <summary>Cards can only be discarded outside battle or by the winninng player in post-battle</summary>
		public void Submit_DiscardItem(BaseItemDesign itemDesign)
		{
			_PGM.RequestDiscard(WinnerOfCurrentBattleOrCurrentPlayer.Stats.GetIndexOfItemWithDesign(itemDesign));
		}

		public void Submit_UseItem(BaseItemDesign baseItemDesign)
		{
			_PGM.RequestPlayCard(CurrentPlayerInBattleOrCurrentPlayer.Stats.GetIndexOfItemWithDesign(baseItemDesign));
		}

		public void Submit_ReadyToBattle()
		{
			_PGM.RequestReadyToBattle();
		}

		public void Submit_PostBattleWinnerDoneLooting()
		{
			_PGM.RequestPostBattleWinnerDoneLooting();
		}

		//public void Submit_AnswerTradeProposal(int donorPlayerID, int itemIndexInInventory, int receiverPlayerID, int receiverTradedItemIndexInInventory, bool agreed)
		public void Submit_AnswerTradeProposal(Player donor, BaseItem donorItem, Player receiver, BaseItem receiverTradedItem, bool agreed)
		{ _PGM.RequestAnswerTradeProposal(donor.ActorID, donor.Stats.GetIndexOfItem(donorItem), receiver.ActorID, receiver.Stats.GetIndexOfItem(receiverTradedItem), agreed); }

		public void Submit_TransferItem(Player donor, BaseItemDesign donorItemDesign, Player receiver, BaseItemDesign receiverTradedItemDesign)
		{
			_PGM.RequestTransferItem(
				donor.ActorID,
				donor.Stats.GetIndexOfItemWithDesign(donorItemDesign),
				receiver.ActorID,
				receiverTradedItemDesign == null ? -1 /*stealing*/ : receiver.Stats.GetIndexOfItemWithDesign(receiverTradedItemDesign)
			);
		}

		//public void Submit_ReadyToAct()
		//{
		//	if (localHumanPlayerManager.IsMyTurn)
		//		UpdateInteractivityForCurrentPlayer(true);
		//	else if (cpu)
		//}

		//public void Submit_ShiftDungeon(Shifter shifter) { _PGM.RequestShiftDungeon(board.GetShiterIndex(shifter)); }
		public void Submit_ShiftDungeon(Shifter shifter) { _PGM.RequestShiftDungeon(shifter.IndexInShiftersArray); }

		public void Submit_BuildTotem(TotemType totemType) { _PGM.RequestBuildTotem(totemType); }

		public void Submit_EndMyTurn()
		{
			UpdateInteractivityForCurrentPlayer(false);
			_PGM.RequestEndMyTurn();
		}
		#endregion


		#region PunBehaviour events
		public override void OnLeftRoom()
		{
			Debug.Log("PM: OnLeftRoom");
			//Task8.CreateAndStart(3f, 0, 0f, 0f, null, () => SceneManager.LoadScene(0));
			//SceneManager.LoadScene(0);
			StartCoroutine(LoadMainMenuDelayed());
			//PhotonNetwork.Disconnect(); // will callOnDisconnectedFromPhoton below
		}

		public override void OnDisconnectedFromPhoton()
		{
			Debug.Log("PM: OnDisconnectedFromPhoton");
			StartCoroutine(LoadMainMenuDelayed());
		}
		#endregion


		#region PunGameManager events
		void OnNewTurn()
		{
			int playerID = _PGM.CurrentPlayerID;
			CurrentPlayer = _SpawnManager.GetPlayerInstanceByActorID(playerID);
			turnText.text = "<color=silver>Turn #" + _PGM.CurrentTurnNumber + "</color>\n" + CurrentPlayer.ShortNickNameWithColoredClass;

			OpponentOfCurrentPlayer = WinnerOfCurrentBattle = CurrentPlayerInBattle = null;
			LosingBlockOfCurrentBattle = null;


			//Debug.Log("PlayerManager: OnNewTurn; playerID=" + playerID + "; isOurTurn=" + (playerID == PhotonNetwork.player.ID));

			// Make sure the inventory represents the current player's cards
			InspectPlayerInventory(CurrentPlayer, false);

			CurrentPlayer.OnStartedTurn();
			CurrentPlayer.Stats.UpdateStatsOnTurnStart();

			UpdateInteractivityForCurrentPlayer(true);
			if (!statsPanel.InspectedPlayer)
				statsPanel.InspectedPlayer = CurrentPlayer;
			UpdateStatsUI();

			if (CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(CurrentLocalPlayerManager.OnNewTurn);

			UpdateWinChances();

			// Notify the Master Client we received/executed the command, so it can be ready to accept new ones (from us, if it's our turn)
			_PGM.OnCurrentActionExecuted();
		}

		void OnPlayerEndedTurn()
		{
			UpdateInteractivityForCurrentPlayer(false);

			int levelsAchieved = CurrentPlayer.Stats.UpdateStatsOnTurnEnd();
			//if (levelsAchieved > 0)
			//	Debug.Log("PlayerManager: current player gained " + levelsAchieved + " levels. Current level: " + CurrentPlayer.Stats.Level);


			if (LosingBlockOfCurrentBattle)
			{
				// Make the left items not free anymore. The winner had a chance to pick them up for free, but since he declined them, they are now like any other cost item
				foreach (var temporarilyFreeItem in LosingBlockOfCurrentBattle.GetChildrenOfType<CommonItem>())
					temporarilyFreeItem.IsFreeUntilNextPickUp = false;
			}

			_SpawnManager.ReLinkNonPickedUpMonsterItems();

			CurrentPlayer.OnEndedTurn();
			if (CurrentLocalPlayerManager)
				CurrentLocalPlayerManager.OnTurnEnded();

			UpdateStatsUI();

			_PGM.OnCurrentActionExecuted();
		}

		void OnMoveToCellAccepted(int cellIndex)
		{
			//if (localPlayerManager.IsMyTurn)
			//	_PGM.Ev_PlayerMoved -= PlayerMoveToCellAccepted;
			UpdateInteractivityForCurrentPlayer(false);

			MoveCurrentPlayerToCell(board.AllCellsIncludingOuterCell[cellIndex]);
		}

		void OnOtherMoveToCellAccepted(int startCellIndex, int indexInBlock, int cellIndex)
		{
			UpdateInteractivityForCurrentPlayer(false);

			var character = board.AllCellsIncludingOuterCell[startCellIndex].ContainedBlock.GetChild(indexInBlock) as Character;
			var destCell = board.AllCellsIncludingOuterCell[cellIndex];

			CurrentPlayer.Stats.ChangeTemporaryStat(Stat.ATTRACT_ENEMY, -1, false);

			board.IsAnimatingMove = true;
			character.Anim_MoveTo(destCell.ContainedBlock, () => OnOtherPlayerMoveToCellAnimationEnded(character, destCell));
		}

		void OnDungeonPushAccepted(int shifterIndex)
		{
			ShiftDungeon(board.AllShiftPoints[shifterIndex]);
		}

		// Case 1(most common): current player moved to a cell
		// Case 2: a battle just happened and the winning player has found ore in his current cell (this won't be the case for the range attacks)
		void OnGetOreAccepted(int cellIndex)
		{
			var cell = board.AllCellsIncludingOuterCell[cellIndex];
			var ore = cell.ContainedBlock.PopOre();

			WinnerOfCurrentBattleOrCurrentPlayer.Stats.Ore += ore.Amount;
			_SpawnManager.DestroyOre(ore);
			UpdateStatsUI();

			if (WinnerLocalPMOfCurrentBattleOrCurrentLocalPlayerPM)
				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell); // continue interracting with the cell
			else if (!IsPostBattle)
				HighlightReachableCells(); // for others

			_PGM.OnCurrentActionExecuted();
		}

		// Case 1: current player discarded an item outside battle
		// Case 2: a battle happened the and winning player discarded something to make room for any item on the ground
		void OnDiscardAccepted(int itemIndexInInventory)
		{
			WinnerOfCurrentBattleOrCurrentPlayer.Stats.ItemsInInventory[itemIndexInInventory].Owner = null;

			//// Restore the battle panel state, so "READY" can be pressed
			//// TODO see if this is still needed, as it doesn't really make sense to discard while in battle
			//if (localHumanPlayerManager.IsMyTurnInBattleAndBattleNotEnded)
			//	battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.IN_BATTLE;

			if (localHumanPlayerManager.IsMyTurnOutsideBattleOrIsPostBattleAndIWon || localCPUPlayerManager && localCPUPlayerManager.IsMyTurnOutsideBattleOrIsPostBattleAndIWon)
			{
				//// We won and we've just discarded something => close the context menu, if it's opened
				//if (IsPostBattleAndIWon)
				//{
				//if (contextPanel.IsShown)
				//	contextPanel.Cancel();
				//}

				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell); // continue interracting with the cell
			}
			_PGM.OnCurrentActionExecuted();
		}

		// Case 1: current player picked an item outside battle
		// Case 2: a battle happened and the winning player picked up something from the current block (where loser dropped it) after it discarded something to make room
		void OnPickUpItemAccepted(int itemIndexInBlock)
		{
			var item = WinnerOfCurrentBattleOrCurrentPlayer.GetParentAsBlock().GetChild(itemIndexInBlock) as BaseItem;

			// In case it needs to be bought, release the needed amount of treasures
			var itemAsCommonItem = item as CommonItem;
			if (itemAsCommonItem != null && itemAsCommonItem.CurrentCost > 0)
			{
				WinnerOfCurrentBattleOrCurrentPlayer.Stats.ReleaseTreasures(itemAsCommonItem.CurrentCost, false);
			}

			item.Owner = WinnerOfCurrentBattleOrCurrentPlayer;

			//if (IsPostBattleAndCurrentPlayerWon || Ism)
			UpdateStatsUI();

			if (localHumanPlayerManager.IsMyTurnOutsideBattleOrIsPostBattleAndIWon || localCPUPlayerManager && localCPUPlayerManager.IsMyTurnOutsideBattleOrIsPostBattleAndIWon)
				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);// the local player/cpu's turn: continue interracting with the cell
			else if (!IsPostBattle)
				HighlightReachableCells(); // for others
			_PGM.OnCurrentActionExecuted();
		}

		void OnPlayerBuildTotemAccepted(TotemType totemType)
		{
			_SpawnManager.SpawnTotem(CurrentPlayer.GetParentCell(), CurrentPlayer, _Design.totems[totemType]);
			CurrentPlayer.Stats.Ore -= _Design.totems.costPerTotem;
			// Re-calculate the reachable cells, since player has one more move now
			UpdateInteractivityForCurrentPlayer(true);
			UpdateStatsUI();

			if (CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(CurrentLocalPlayerManager.OnReadyToAct);

			_PGM.OnCurrentActionExecuted();
		}

		void OnSetupBattleAccepted(int cellIndex, int characterIndexInBlock)
		{
			//Debug.Log("OnSetupBattleAccepted localPlayerManager.IsMyTurnInBattle=" + localPlayerManager.IsMyTurnInBattle);
			OpponentOfCurrentPlayer = board.AllCellsIncludingOuterCell[cellIndex].ContainedBlock.GetChild(characterIndexInBlock) as Character;
			endTurnButton.interactable = false;
			battlePanel.Show(CurrentPlayer, OpponentOfCurrentPlayer);

			if (CurrentPlayer.RelativePosSimpleTo(OpponentOfCurrentPlayer) != RelativePositionSimple.IDENTICAL) // ranged battle => subtract the ranged effect
				CurrentPlayer.Stats.ChangeTemporaryStat(Stat.RANGED_BATTLE, -1, false);

			bool pvp = OpponentOfCurrentPlayer is Player;
			if (pvp)
			{
				bool opponentHasSpider = OpponentOfCurrentPlayer.Stats.HasSpider;
				if (LocalPlayerManagerIfOpponentOfCurrentPlayer && opponentHasSpider)
				{
					_PGM.DoAfterMasterReady(() => LocalPlayerManagerIfOpponentOfCurrentPlayer.OnChooseIfToCancelBattle());
					// no need to show "WAITING.." log in case of the CPU being opponent and having spider, because he takes decisions quickly
				}
				else
				{
					if (CurrentLocalPlayerManager) // is the turn of local player or local cpu
					{
						if (CurrentLocalPlayerManager == localHumanPlayerManager && opponentHasSpider)
							// Player's turn & the opponent has spider => waiting to see if opponent will evade with spider
							SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState.WAITING); 
						else
							_PGM.DoAfterMasterReady(() => CurrentLocalPlayerManager.OnChooseFirstIfToBattle());
					}
				}
			}
			else
			{
				if (PhotonNetwork.isMasterClient)
					_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated(CurrentPlayer.ActorID));
			}

			if (!localHumanPlayerManager.Me.Stats.InBattle)
				// We're a spectator => see the empty results panel for now
				battlePanel.ActivateBattleResultsPanel();

			UpdateInteractivityForCurrentPlayer(false);
			UpdateInventoryInteractability(true); // but the cards in inventory should be visible

			_PGM.OnCurrentActionExecuted();
		}

		void ConsumeItem(Player playerToConsumeItem, BaseItem item)
		{
			var itemDesignAsEffectsItem = item.Design as IEffectsItem;
			Character opponentOfPlayerThatConsumesTheItem;
			if (IsIntraBattle)
				opponentOfPlayerThatConsumesTheItem = playerToConsumeItem == CurrentPlayer ? OpponentOfCurrentPlayer : CurrentPlayer;
			else
				opponentOfPlayerThatConsumesTheItem = OpponentOfCurrentPlayer;

			EffectManager.Instance.CastItem(playerToConsumeItem, itemDesignAsEffectsItem, opponentOfPlayerThatConsumesTheItem);
			item.Owner = null;

			bool updateInventoryInteractabilityAfter = true;
			if (item.Design.cardType == CardType.BATTLE)
			{
				playerToConsumeItem.Stats.NumBattleCardsPlayed++;

				battlePanel.OnCardPlayed(playerToConsumeItem, item.Design);
			}
			else if (item.Design.cardType == CardType.UTILITY)
			{
				var effects = itemDesignAsEffectsItem.GetEffects(playerToConsumeItem, null);
				foreach (var eff in effects)
				{
					if (eff.type == Stat.MOVES
						|| eff.type == Stat.MOVES_THROUGH_WALLS
						|| eff.type == Stat.DIAGONAL_MOVES
						|| eff.type == Stat.BATTLE_EVADING
						|| eff.type == Stat.GOD_MOVES
						|| eff.type == Stat.GHOST_MOVES)
					{
						RefreshReachableCells();
						continue;
					}

					if (eff.type == Stat.DUNGEON_PUSH)
					{
						UpdateShiftersInteractability();
						continue;
					}
					if (eff.type == Stat.CARD_STEALING)
					{
						if (CurrentLocalPlayerManager)
						{
							CurrentLocalPlayerManager.OnPreStealCard();
							updateInventoryInteractabilityAfter = false;
							break; // shouldn't have more effects if the stealing effect is present
						}
					}
				}
				//foreach (var eff in items)
				//{
				//	if (eff.type == Stat.ATTRACT_ENEMY)
				//	{

				//		break;
				//	}
				//}
			}

			//playerToConsumeItem.Stats.ConsumeItem(item);
			//_SpawnManager.graveyard.Add(item);
			// TODO show on UI

			if (updateInventoryInteractabilityAfter)
			{
				// Update inventory interactivity if there's a battle and we were the ones that playerd the card OR it's our turn (case in which we just played a utility card)
				if (localHumanPlayerManager.IsMyTurnInBattle || localHumanPlayerManager.IsMyTurnOutsideBattle)
				{
					//inventoryPanel.CardsInteractable = true;
					UpdateInventoryInteractability();
				}
			}

			UpdateStatsUI();
		}

		void OnPlayCardAccepted(int itemIndexInInventory)
		{
			var playerToConsumeItem = CurrentPlayerInBattleOrCurrentPlayer;
			var item = playerToConsumeItem.Stats.ItemsInInventory[itemIndexInInventory];

			ConsumeItem(playerToConsumeItem, item);

			//playerToConsumeItem.Stats.ConsumeItem(item);
			//_SpawnManager.graveyard.Add(item);
			// TODO show on UI
			if (CurrentLocalPlayerManagerInBattleIfNotEnded)
				_PGM.DoAfterMasterReady(CurrentLocalPlayerManagerInBattleIfNotEnded.OnReadyToActInBattle);
			else if (CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(() => CurrentLocalPlayerManager.OnReadyToAct(DBDEvent.PLAYER_USE_ITEM, item));

			_PGM.OnCurrentActionExecuted();
		}

		void OnBattlePlayerInitiates()
		{
			battlePanel.logText.text = CurrentPlayer.ShortNickNameWithColoredClass + " initiates!";

			if (PhotonNetwork.isMasterClient)
				_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated(CurrentPlayer.ActorID));

			_PGM.OnCurrentActionExecuted();
		}

		void OnBattlePlayerStaysIdle()
		{
			battlePanel.logText.text = CurrentPlayer.ShortNickNameWithColoredClass + " prefers not to battle...";

			if (LocalPlayerManagerIfOpponentOfCurrentPlayer)
				_PGM.DoAfterMasterReady(LocalPlayerManagerIfOpponentOfCurrentPlayer.OnChooseIfToBattleAfterCurrentPlayerPrefersNotTo);

			_PGM.OnCurrentActionExecuted();
		}

		void OnBattleApproachedPlayerChoseIfToCancelBattle(bool cancel)
		{
			string logText;
			if (cancel)
			{
				logText = OpponentOfCurrentPlayer.Design.name + " evaded using The Spider!";

				// Moving the spider card into the discard pile
				var opponentStats = (OpponentOfCurrentPlayer as Player).Stats;
				opponentStats.ItemsInInventory[opponentStats.GetIndexOfItemWithDesign(_Design.monsters.SPIDER.rewardItem)].Owner = null;

				// After the master confirmed everyone executed the action, close the panel as if it was a tie (enter the same state as if in a tie) + end turn
				// _PGM.OnCurrentActionExecuted will be called in OnBattlePanelHiddenAfterBattle 
				battlePanel.Close(battlePanel.autoHideDelay + _Design.general.battlePanelAdditionalHideDelayWhenSpider, () => OnBattlePanelHiddenAfterBattle(true));
			}
			else
			{
				logText = OpponentOfCurrentPlayer.Design.name + " chose not to evade with The Spider";

				// The normal flow (same state as if the opponent haven't had the spider)
				if (CurrentLocalPlayerManager)
					_PGM.DoAfterMasterReady(CurrentLocalPlayerManager.OnChooseFirstIfToBattle);
				_PGM.OnCurrentActionExecuted();
			}

			battlePanel.logText.text = logText;
		}

		void OnBattleApproachedPlayerInitiates()
		{
			battlePanel.logText.text = ".. but " + (OpponentOfCurrentPlayer as Player).ShortNickNameWithColoredClass + " initiates!";

			if (PhotonNetwork.isMasterClient)
				_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated((OpponentOfCurrentPlayer as Player).ActorID));

			_PGM.OnCurrentActionExecuted();
		}

		void OnBattleApproachedPlayerAlsoStaysIdle()
		{
			battlePanel.logText.text = (OpponentOfCurrentPlayer as Player).ShortNickNameWithColoredClass + " agrees not to battle";
			battlePanel.SetBattleWinner(null);
			battlePanel.Close(() =>
			{
				//if (localPlayerManager.IsMyTurn)
				//	_PGM.DoOnceAfterMasterReady(_PGM.RequestEndMyTurn);
				_PGM.DoAfterMasterReady(() =>
				{
					UpdateInteractivityForCurrentPlayer(true);
					if (CurrentLocalPlayerManager)
						CurrentLocalPlayerManager.OnReadyToAct();
				});

				_PGM.OnCurrentActionExecuted();
			});
		}

		void OnPlayerBattleTurnStarted(int playerID)
		{
			CurrentPlayerInBattle = _SpawnManager.GetPlayerInstanceByActorID(playerID);
			battlePanel.logText.text = CurrentPlayerInBattle.ShortNickNameWithColoredClass + " is preparing to attack !";

			// When no one has pressed READY yet, this is the first turn
			// The first battle turn marks the beginning of the battle
			if (_PGM.PlayersReadyForBattle == 0)
			{
				CurrentPlayer.Stats.UpdateStatsOnBattleStarted(OpponentOfCurrentPlayer);
				OpponentOfCurrentPlayer.Stats.UpdateStatsOnBattleStarted(CurrentPlayer);

				// Play the knife automatically, if the current player is a non-archer and it has it in inventory and it's one space away (i.e. has used "Ranged Battle")
				if (CurrentPlayer.Design != _Design.players.ARCHER)
				{
					if (CurrentPlayer.RelativePosSimpleTo(OpponentOfCurrentPlayer) == RelativePositionSimple.NEIGHBOR_DIRECT)
					{
						int index = CurrentPlayer.Stats.GetIndexOfItemWithDesign(_Design.items.commonItems.THROWING_DAGGER);
						if (index != -1) // at the moment of the implementation the index can't be -1, but maybe in the future you can have the effect without having the knife
							ConsumeItem(CurrentPlayer, CurrentPlayer.Stats.ItemsInInventory[index]);
					}
				}
			}

			CurrentPlayerInBattle.Stats.UpdateStatsOnBattleTurnStart();


			//Debug.Log("OnPlayerBattleTurnStarted localPlayerManager.IsMyTurnInBattle=" + localPlayerManager.IsMyTurnInBattle + "; CurrentPlayerInBattle=" + CurrentPlayerInBattle.name);
			if (CurrentLocalPlayerManagerInBattle)
			{
				_PGM.DoAfterMasterReady(CurrentLocalPlayerManagerInBattle.OnReadyToActInBattle);
			}
			else
				UpdateStatsUI();

			_PGM.OnCurrentActionExecuted();
		}

		void OnPlayerReadyForBattleAccepted(int playerID)
		{
			battlePanel.logText.text = _SpawnManager.GetPlayerInstanceByActorID(playerID).ShortNickNameWithColoredClass + " is ready !";

			if (PhotonNetwork.isMasterClient)
			{
				bool pvp = battlePanel.SecondCharacter is Player;
				bool battleCanBegin = true;

				if (pvp)
				{
					// The first one ended his battle-turn => now it's the next one's battle-turn
					if (_PGM.PlayersReadyForBattle == 1)
					{
						battleCanBegin = false; // the other one also needs to send the "ready" signal

						var nextPlayer = CurrentPlayer.ActorID == playerID ? (OpponentOfCurrentPlayer as Player) : CurrentPlayer;
						_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated(nextPlayer.ActorID));
					}
				}

				if (battleCanBegin)
					_PGM.DoAfterMasterReady(DispatchBattleOutcome);
			}

			_PGM.OnCurrentActionExecuted();
		}

		void OnBattleResultsObtainedFromMaster(BattleStats battleStats)
		{
			battlePanel.logText.text = "";

			Action endResultCountingAction = () =>
			{
				// _PGM.OnCurrentActionExecuted() will be called at the end of the animation
				battlePanel.BeginCounting(battleStats.firstPlayerScore, battleStats.secondPlayerScore, () => OnBattleResultsShown(battleStats));
			};

			// In order to prevent a more complicated battle flow, only show the intermeriary tie if there's no serpent present in either of the players' inventory
			var serpentItemDesign = _Design.monsters.SERPENT.rewardItem;
			if (CurrentPlayer.Stats.GetIndexOfItemWithDesign(serpentItemDesign) == -1 && OpponentOfCurrentPlayer.Stats.GetIndexOfItemWithDesign(serpentItemDesign) == -1)
			{
				var tieStats = battleStats.statsOfFirstTieBeforeFinalRound;
				// If the battle is not pre-determined, then the final outcome is definitely not a tie.
				// And if the tieStats are not null, then at least 1 tie happened before the final outcome => first, show how a tie is happening, then the actual outcome
				if (!battleStats.finalBattlePointsArePreDetermined && tieStats != null)
				{
					battlePanel.BeginCounting(
						tieStats.firstPlayerScore,
						tieStats.secondPlayerScore,
						() =>
						{
							// Set the tie stats
							battlePanel.resultsPanel.SetDetails(
								tieStats.firstPlayerScoreFromAtk,
								tieStats.secondPlayerScoreFromAtk,
								tieStats.firstPlayerScoreFromMagic, battlePanel.FirstCharacter.Design.magicType,
								tieStats.secondPlayerScoreFromMagic, battlePanel.SecondCharacter.Design.magicType
							);

							battlePanel.AnimateTie(endResultCountingAction, false);
						}
					);

					// The endResultCountingAction will be executed after the tie animation ends
					return;
				}
			}

			// Not showing the animation for the tie. Directly play the final counting animation and end the battle
			endResultCountingAction();
		}

		void Client_OnPlayerEliminated(int playerId)
		{

			_PGM.OnCurrentActionExecuted();
		}

		void OnPostBattleWinnerDoneLooting()
		{
			// End our turn after the looting is done (we or the other player(if he won) did the looting)
			//Debug.Log("OnPostBattleWinnerDoneLooting localPlayerManager.IsMyTurn="+localPlayerManager.IsMyTurn);
			if (CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(_PGM.RequestEndMyTurn);

			_PGM.OnCurrentActionExecuted();
		}

		void OnItemTransferRequestAccepted(int donorPlayerID, int itemIndexInInventory, int receiverPlayerID, object[] receiverTradedItemIndexInInventoryAndAcceptedStatus)
		{
			var donor = _SpawnManager.GetPlayerInstanceByActorID(donorPlayerID);
			var donorItem = donor.Stats.ItemsInInventory[itemIndexInInventory];
			var receiver = _SpawnManager.GetPlayerInstanceByActorID(receiverPlayerID);
			int receiverTradedItemIndexInInventory = (int)receiverTradedItemIndexInInventoryAndAcceptedStatus[0];
			bool updateInteractivityToTrueAfterMasterReadyIfItsMyTurn = false;

			if (receiverTradedItemIndexInInventory == -1) // stealing
			{
				donorItem.Owner = receiver;
				CurrentPlayer.Stats.ChangeTemporaryStat(Stat.CARD_STEALING, -1, false);
				updateInteractivityToTrueAfterMasterReadyIfItsMyTurn = true;
			}
			else // trading
			{
				var receiverTradedItem = receiver.Stats.ItemsInInventory[receiverTradedItemIndexInInventory];
				bool? accepted = (bool?)receiverTradedItemIndexInInventoryAndAcceptedStatus[1];

				if (accepted == null) // not asked yet. need to first ask the potential donor. if the local player is the donor, then submit the answer
				{
					_PGM.DoAfterMasterReady(() =>
					{
						if (donor == localHumanPlayerManager.Me)
							localHumanPlayerManager.OnAnswerTradeProposal(receiver, donorItem, receiverTradedItem);
						else if (localCPUPlayerManager && donor == localCPUPlayerManager.Me)
							localCPUPlayerManager.OnAnswerTradeProposal(receiver, donorItem, receiverTradedItem);
					});
				}
				else
				{
					// The other player was asked and it answered
					Debug.Log("Trade accepted = " + accepted.Value);

					if (CurrentLocalPlayerManager)
						CurrentLocalPlayerManager.Trading_MyDesiredCard = null;

					if (accepted.Value)
					{
						// In case one of them has a full inventory, first free one slot and then add them one at a time
						donorItem.Owner = receiverTradedItem.Owner = null;
						donorItem.Owner = receiver;
						receiverTradedItem.Owner = donor;
					}

					updateInteractivityToTrueAfterMasterReadyIfItsMyTurn = true;
				}
			}

			if (updateInteractivityToTrueAfterMasterReadyIfItsMyTurn && CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(CurrentLocalPlayerManager.OnPostStealCard);

			_PGM.OnCurrentActionExecuted();
		}

		void OnGameResult(int _)
		{
			totemsPanel.gameObject.SetActive(false);
			inventoryPanel.gameObject.SetActive(false);
			// Not calling OnCurrentRequestExecuting, because at this point no other request should be
		}
		#endregion


		#region callbacks from Board
		void OnDungeonShifted()
		{
			//Debug.Log("GM: OnDungeonShifted");
			UpdateInteractivityForCurrentPlayer(true);
			if (CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(CurrentLocalPlayerManager.OnReadyToAct);

			_PGM.OnCurrentActionExecuted();
		}
		#endregion


		#region callbacks from Player
		void OnPlayerMoveToCellAnimationEnded(Cell cell)
		{
			//Debug.Log("PlayerManager.OnPlayerMoveToCellAnimationEnded " + cell.name);
			CurrentPlayer.SetParentBlock(cell.ContainedBlock, SpawnManager.allEntitiesSpawnPositionDelta);
			board.IsAnimatingMove = false;

			if (CurrentLocalPlayerManager)
				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);
			else
			{
				HighlightReachableCells(); // for others
				UpdateInventoryInteractability();
			}

			_PGM.OnCurrentActionExecuted();
		}

		void OnOtherPlayerMoveToCellAnimationEnded(Character character, Cell cell)
		{
			//Debug.Log("PlayerManager.OnPlayerMoveToCellAnimationEnded " + cell.name);
			character.SetParentBlock(cell.ContainedBlock, SpawnManager.allEntitiesSpawnPositionDelta);
			board.IsAnimatingMove = false;

			_PGM.DoAfterMasterReady(() =>
			{
				if (CurrentLocalPlayerManager)
					Submit_StartBattleWith(character);
			});

			_PGM.OnCurrentActionExecuted();
		}

		// Only called on our OR CPU's turn outside battle or on post battle when we/CPU won and need to do looting
		void OnPlayerReadyToInteractWithCell() { OnPlayerReadyToInteractWithCell(false); }

		void OnPlayerReadyToInteractWithCell(bool onlyToPickUpItems)
		{
			//if (localPlayerManager.IsMyTurn) // commented: this will only be called on our turn

			if (!onlyToPickUpItems)
			{
				var enemies = WinnerOfCurrentBattleOrCurrentPlayer.GetEnemiesInCell();
				if (enemies.Count > 0) // Someone to fight
				{
					var otherChar = enemies[0];

					Submit_StartBattleWith(otherChar);
					return;

				}

				// No one to fight


				// If the cell is the Exit cell && the player in it has all the 3 escape items, the game will end, and the master will notify all clients. No additional looting will be made
				// The winner can be either the current player, or (in some rare cases), the opponent of the current player, if they're in the same spot
				if (DidPlayerEscape(WinnerOfCurrentBattleOrCurrentPlayer))
				{
					UnHighlightLastReachableCells();

					if (PhotonNetwork.isMasterClient)
						_PGM.DispatchGameResultEvent(WinnerOfCurrentBattleOrCurrentPlayer.ActorID);

					return;
				}
			}

			// Clearly not null, since OnPlayerReadyToInteractWithCell is only called if a local player is winner of a battle or it's his turn
			WinnerLocalPMOfCurrentBattleOrCurrentLocalPlayerPM.OnLootCell(WinnerOfCurrentBattleOrCurrentPlayer.GetParentCell());
		}
		#endregion


		#region callbacks related to Battle
		// Only called on master
		void DispatchBattleOutcome()
		{
			_PGM.Ev_MasterReadyForNewRequest -= DispatchBattleOutcome;

			battlePanel.CalculateBattleOutcome();
			_PGM.DispatchBattleResult(battlePanel.currentBattleStats);
		}

		void OnBattleResultsShown(BattleStats battleStats)
		{
			CurrentPlayer.Stats.UpdateStatsOnBattleEnded();
			OpponentOfCurrentPlayer.Stats.UpdateStatsOnBattleEnded();

			UpdateStatsUI();

			// For debugging the serpent
			//int firstResultPostBattle = battlePanel.SecondCharacter.Design == _Design.monsters.SERPENT ? 100 : 9;
			//int secondResultPostBattle = 10;
			//int firstATKPostBattle = 5;
			//int secondATKPostBattle = 5;

			int firstResultPostBattle = battleStats.firstPlayerScore;
			int secondResultPostBattle = battleStats.secondPlayerScore;
			int firstATKPostBattle = battleStats.firstPlayerScoreFromAtk;
			int secondATKPostBattle = battleStats.secondPlayerScoreFromAtk;

			bool serpentShown = false;

			WinnerOfCurrentBattle = firstResultPostBattle < secondResultPostBattle ? battlePanel.SecondCharacter : (firstResultPostBattle > secondResultPostBattle ? battlePanel.FirstCharacter : null);

			if (WinnerOfCurrentBattle)
			{
				var loserAsPlayer = (WinnerOfCurrentBattle == CurrentPlayer ? OpponentOfCurrentPlayer : CurrentPlayer) as Player;
				if (loserAsPlayer)
				{
					int loserScore = Mathf.Min(firstResultPostBattle, secondResultPostBattle);
					int winnerScore = Mathf.Max(firstResultPostBattle, secondResultPostBattle);

					//var serpentAmount = _Design.shortcut.GetSerpentItemEffectAmount(loserAsPlayer, WinnerOfCurrentBattleAsPlayer);
					var serpentAmount = _Design.shortcut.GetSerpentItemEffectAmount();
					int winnerScoreAfterSerpent = winnerScore + serpentAmount;
					if (winnerScoreAfterSerpent < 0)
						winnerScoreAfterSerpent = 0;

					// The serpent can appear, if it exist in the loser's inventory
					if (winnerScoreAfterSerpent <= loserScore)
					{
						int serpentIndex = loserAsPlayer.Stats.GetIndexOfItemWithDesign(_Design.monsters.SERPENT.rewardItem);
						if (serpentIndex != -1)
						{
							if (loserAsPlayer == battlePanel.FirstCharacter)
							{
								secondATKPostBattle += serpentAmount;
								secondResultPostBattle += serpentAmount;
							}
							else
							{
								firstATKPostBattle += serpentAmount;
								firstResultPostBattle += serpentAmount;
							}

							// Choose the new winner or the tie
							if (firstResultPostBattle == secondResultPostBattle)
								WinnerOfCurrentBattle = null; // tie
							else
								WinnerOfCurrentBattle = loserAsPlayer; // the loser actually won in the end

							// Visually show the serpent & override final scores
							battlePanel.resultsPanel.OverrideResults(firstResultPostBattle, secondResultPostBattle);
							battlePanel.resultsPanel.ShowSerpent();

							// Discard the item
							loserAsPlayer.Stats.ItemsInInventory[serpentIndex].Owner = null;

							serpentShown = true;
						}
					}
				}
			}

			battlePanel.SetBattleWinner(WinnerOfCurrentBattle);
			battlePanel.resultsPanel.SetDetails(
				firstATKPostBattle,
				secondATKPostBattle,
				battleStats.firstPlayerScoreFromMagic, battlePanel.FirstCharacter.Design.magicType,
				battleStats.secondPlayerScoreFromMagic, battlePanel.SecondCharacter.Design.magicType
			);

			// _PGM.OnCurrentActionExecuted will be called in OnBattlePanelHiddenAfterBattle 
			Action closeAction = () => battlePanel.Close(battlePanel.autoHideDelay + (serpentShown ? _Design.general.battlePanelAdditionalHideDelayWhenSpider : 0f), () => OnBattlePanelHiddenAfterBattle(false));

			if (!WinnerOfCurrentBattle) // first, show the tie animation, then close the panel
				battlePanel.AnimateTie(closeAction, true);
			else
				closeAction();
		}

		void OnBattlePanelHiddenAfterBattle(bool battleCancelled)
		{
			//Debug.Log("Winning char: " + (winningChar == null ? "none" : winningChar.name));

			//WinnerOfCurrentBattleAsPlayer = WinnerOfCurrentBattle as Player; // may be null, of course, if the monster won

			if (WinnerOfCurrentBattle)
			{
				CurrentPlayer.Stats.NumDungeonPushesConsumed += CurrentPlayer.Stats.RemainingDungeonPushes; //consume all dungeon pushes
				CurrentPlayer.Stats.NumMovesConsumed += CurrentPlayer.Stats.RemainingMoves; //consume all moves

				LosingBlockOfCurrentBattle = LosingCharacterOfCurrentBattle.GetParentAsBlock();
				// CurrentPlayer vs a Player
				Player losingCharAsPlayer = LosingCharacterOfCurrentBattle as Player;

				WinnerOfCurrentBattle.OnWonBattle(LosingCharacterOfCurrentBattle);

				// The loosing char is a player and it was eliminated => the master will handle this
				if (losingCharAsPlayer && !losingCharAsPlayer.Stats.HasSoul)
				{
					if (PhotonNetwork.isMasterClient)
					{
						_PGM.DoAfterMasterReady(() => _PGM.DispatchPlayerEliminatedEvent_Master(losingCharAsPlayer.ActorID));
					}
					//int eliminated = _SpawnManager.GetEliminatedPlayers().Count;
					//int current = PhotonNetwork.room.PlayerCount;
					//if (eliminated >= current)
					//// Game will end => don't re-enter this method for item pick-up
					//{

					//}
					//else
					//// Game won't end => dispatch the elimination event & execute PostBattle after
					//{

					//}
				}
				else
				{
					PostBattleWinnerDoLooting();
					return; // already calls OnCurrentActionExecuted
				}

				//if (WinnerOfCurrentBattleAsPlayer)
				//{
				//	WinnerOfCurrentBattleAsPlayer
				//}
				//	// Get XP + all ore + all items for which the winner has free slots
				//	WinnerOfCurrentBattleAsPlayer.Stats.UpdateStatsOnEnemyKilled(losingChar);



			}
			// It was a tie or the battle was cancelled
			else
			{
				if (CurrentLocalPlayerManager)
				{
					_PGM.DoAfterMasterReady(() => {
						//if (battleCancelled)
						//	_PGM.RequestEndMyTurn();
						//else
						//	UpdateInteractivityForCurrentPlayer(true);

						// Update: actually, once a battle has started, the turn should end afterwards, regardless of the outcome
						_PGM.RequestEndMyTurn();
					});
				}
			}

			_PGM.OnCurrentActionExecuted();
		}

		void PostBattleWinnerDoLooting()
		{
			var opponentAsPlayer = OpponentOfCurrentPlayer as Player;
			bool pvp = opponentAsPlayer != null;
			bool currentPlayerWon = WinnerOfCurrentBattle == CurrentPlayer;
			var winningBlock = WinnerOfCurrentBattle.GetParentAsBlock();
			// CurrentPlayer vs a Player
			Player losingCharAsPlayer = LosingCharacterOfCurrentBattle as Player;

			bool winnerCanPickUpItems = WinnerOfCurrentBattleAsPlayer && WinnerOfCurrentBattleAsPlayer.Design.canPickUpItems;

			if (pvp)
			{
				// Drop the remaining items on the losing block spot; the winning player cannot pick up automatically them due to full inventory; will do it manually, if it's also on the same spot
				// Cost items are also pickable without the need to buy them (only until the end of the turn or until the player picks them)
				// BUT, if the winner cannot pick up items (animal class), they are discarded
				while (losingCharAsPlayer.Stats.ItemsInInventory.Length > 0)
				{
					var item = losingCharAsPlayer.Stats.ItemsInInventory[0];
					var itemAsCommonItem = item as CommonItem;
					if (itemAsCommonItem && winnerCanPickUpItems)
						itemAsCommonItem.IsFreeUntilNextPickUp = true;

					item.Owner = winnerCanPickUpItems ? LosingBlockOfCurrentBattle : null;
				}

				bool winnerCanCollectMoreItemsBeforeTurnEnds = winnerCanPickUpItems && LosingBlockOfCurrentBattle == winningBlock && LosingBlockOfCurrentBattle.GetChildrenOfType<BaseItem>().Count > 0;

				if (winnerCanCollectMoreItemsBeforeTurnEnds) // The turn doesn't end at the moment; only after the winner picks up / leaves loot
				{
					if (WinnerLocalPlayerPMOfCurrentBattle)
						_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);
				}
				else // nothing to loot => end turn normally, if it's our turn
				{
					//Debug.Log("Will end my turn because there's no loot localPlayerManager.IsMyTurn="+ localPlayerManager.IsMyTurn);
					if (CurrentLocalPlayerManager)
						_PGM.DoAfterMasterReady(_PGM.RequestEndMyTurn);
				}
			}
			else
			{
				// CurrentPlayer vs a Monster/Totem
				var losingCharAsMonster = LosingCharacterOfCurrentBattle as Monster;
				var losingCharAsTotem = LosingCharacterOfCurrentBattle as Totem;
				if (currentPlayerWon)
				{
					if (losingCharAsMonster)
					{
						// Item left on spot of the winning char, so it can pick it, if it's a human
						// If it's a beast, the item gets no owner (i.e. the monster will be respawned in the next turn)
						losingCharAsMonster.RewardItem.Owner = winnerCanPickUpItems ? WinnerOfCurrentBattle.GetParentAsBlock() : null;
					}
					else if (losingCharAsTotem)
					{
						_SpawnManager.DestroyTotem(losingCharAsTotem);
					}

					if (CurrentLocalPlayerManager)
						_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);
				}
				// Monster/Totem defeated the current player
				else
				{
					// Drop all items into the graveyard
					CurrentPlayer.Stats.RemoveOwnershipFromAllItems();

					// Drop the ore on the opponnent's spot (for example, in the ranged attack case, the ore would remain unprotected if would've been dropped on the loser's spot)
					int oreDropped = CurrentPlayer.Stats.Ore;
					//Debug.Log(oreDropped);
					CurrentPlayer.Stats.Ore = 0;
					var existingOreInBlock = winningBlock.PeekOre();
					//Debug.Log("existingOreInBlock="+ existingOreInBlock);

					// Increase the amount for the existing ore instance in block
					if (existingOreInBlock)
						existingOreInBlock.Amount += oreDropped;
					// Spawn a new ore instance with the player's lost amount
					else if (oreDropped > 0)
						_SpawnManager.SpawnOre(winningBlock, oreDropped);

					if (CurrentLocalPlayerManager)
						_PGM.DoAfterMasterReady(_PGM.RequestEndMyTurn);
				}
			}

			_PGM.OnCurrentActionExecuted();
		}
		#endregion




		public void UpdateInteractivityForCurrentPlayer(bool active)
		{
			endTurnButton.interactable = active && localHumanPlayerManager.IsMyTurn;
			if (active)
			{
				UpdateShiftersInteractability();

				if (localHumanPlayerManager.IsMyTurnOutsideBattle)
				{
					RefreshReachableCells();
					totemsPanel.UpdateBuildOptions(CurrentPlayer);
				}
				else
					totemsPanel.SetAllButtonsInteractable(false);
			}
			else
			{
				UnHighlightLastReachableCells();
				board.SetShiftingEnabled(false);
				totemsPanel.SetAllButtonsInteractable(false);
			}
			UpdateInventoryInteractability(active);
		}

		void MoveCurrentPlayerToCell(Cell cell)
		{
			var path = new List<Cell.Descriptor>();
			Cell.Descriptor cellDesc = null;
			// God moves don't use path movement; they jump directly and only use simpe moves
			if (CurrentPlayer.Stats[Stat.GOD_MOVES] > 0)
			{
				CurrentPlayer.Stats.ChangeTemporaryStat(Stat.GOD_MOVES, -1, false);
				++CurrentPlayer.Stats.NumMovesConsumed;
				cellDesc = cell.Desc;
			}
			else
			{
				var boardDescriptor = board.CreateDescriptor();
				var pathfindingContext = PathfindingCalculateBestPathsUsingAsMuchSpecialMovesAsPossible(boardDescriptor);
				cellDesc = boardDescriptor.GetExistingCellDesc(cell);
				if (pathfindingContext.mapDestCellToBestSolution.ContainsKey(cellDesc)) // this will be false only when using the debugging shortcut for moving
				{
					var solution = pathfindingContext.mapDestCellToBestSolution[cellDesc];
					path = solution.path;
					CurrentPlayer.Stats.SubtractMovementCosts(solution.cost);
				}
			}

			//Debug.Log("PlayerManager.MoveToCell " + cell.name);
			if (path.Count == 0) // direct movement
				path.Add(cellDesc);

			//EnumEffects(CurrentPlayer); // debug
			UnHighlightLastReachableCells();
			UpdateStatsUI();
			board.IsAnimatingMove = true;
			CurrentPlayer.Anim_MoveOnPath(path.ConvertAll(cd => cd.describedCell), () => OnPlayerMoveToCellAnimationEnded(cell));
		}

		void ShiftDungeon(Shifter shifter)
		{
			//Debug.Log("GM: ShiftDungeon");
			//// Preventing further selections
			//shifter.gameObject.SetActive(false);
			CurrentPlayer.Stats.NumDungeonPushesConsumed++;
			UpdateInteractivityForCurrentPlayer(false);
			UpdateStatsUI();

			// Move to it and then place the cell
			board.Anim_ShiftDungeon(shifter, OnDungeonShifted);
		}

		void EnumEffects(Character character) { Debug.Log(character + " " + character.Stats.EffectsToString()); }


		#region Cell reachability highlights
		void HighlightReachableCells()
		{
			if (CurrentPlayer.Stats[Stat.GOD_MOVES] > 0)
			{
				foreach (var cell in board.AllCellsIncludingOuterCell)
					cell.AllowedAreaHighlight.SetHighlight(1, !localHumanPlayerManager.IsMyTurn);

				_LastHighLightedCells.AddRange(board.AllCellsIncludingOuterCell);

				return;
			}

			int remainingMoves = CurrentPlayer.Stats.RemainingMoves;
			var playerCell = CurrentPlayer.GetParentCell();

			//        var reachableCellsPerNumberOfMoves = 
			//_BoardHelper.GetReachableCells(playerCell, CurrentPlayer, remainingMoves, 
			//	Mathf.Min(remainingMoves, CurrentPlayer.Stats[Stat.MOVES_THROUGH_WALLS]), // can move through walls as long as <remainingMoves> is bigger than 0
			//	Mathf.Min(remainingMoves, CurrentPlayer.Stats[Stat.DIAGONAL_MOVES]), // can move diagonally as long as <remainingMoves> is bigger than 0
			//	Mathf.Min(remainingMoves, CurrentPlayer.Stats[Stat.BATTLE_EVADING]) // can move through enemies as long as <remainingMoves> is bigger than 0
			//);

			var boardDescriptor = board.CreateDescriptor();
			var pathfindingContext = PathfindingCalculateBestPathsUsingAsMuchSpecialMovesAsPossible(boardDescriptor);

			//Debug.Log("Found " + pathfindingContext.mapDestCellToBestSolution.Count + " possible moves");
			foreach (var kv in pathfindingContext.mapDestCellToBestSolution)
			{
				var cell = kv.Key;
				var solution = kv.Value;

				_LastHighLightedCells.Add(cell.describedCell);
				cell.describedCell.AllowedAreaHighlight.SetHighlight(1, !localHumanPlayerManager.IsMyTurn);
				cell.describedCell.SetDebugInfo(solution.ToString( _Design.general.debug_CellPathTextSize));
			}

			//for (int i = 0; i < reachableCellsPerNumberOfMoves.Count; ++i)
			//{
			//    _LastHighLightedCells.AddRange(reachableCellsPerNumberOfMoves[i]);

			//    foreach (var nearCell in reachableCellsPerNumberOfMoves[i])
			//        nearCell.AllowedAreaHighlight.SetHighlight(i+1, !localPlayerManager.IsMyTurn);
			//}
		}

		public void UnHighlightLastReachableCells()
		{
			foreach (var lastHighlighted in _LastHighLightedCells)
			{
				lastHighlighted.SetDebugInfo("");
				lastHighlighted.AllowedAreaHighlight.SetHighlight(0);
			}
			_LastHighLightedCells.Clear();
		}

		void RefreshReachableCells()
		{
			UnHighlightLastReachableCells();
			HighlightReachableCells();
		}
		#endregion


		#region other methods
		public void UpdateStatsUI() { statsPanel.UpdateFromInspectedPlayer(); }

		public Pathfinding.PathfindingContext PathfindingCalculateBestPathsUsingAsMuchSpecialMovesAsPossible(Board.Descriptor boardDescriptor, bool addOneMove = false)
		{
			var pathfindingContext = 
				new Pathfinding.PathfindingContext(
					//CurrentPlayer, 
					boardDescriptor.GetExistingCellDesc(CurrentPlayer.GetParentCell()),
					CurrentPlayer.GetPathfindingBudget(addOneMove),
					new Pathfinding.PathfindingCurrency.Comparer_PrioritizeConsumingFewerTotalMovesWhenResourcesKnown(CurrentPlayer.Design),
					new Pathfinding.EnemiesInfo(CurrentPlayer)
				);
			boardDescriptor.ComputeBestPaths(pathfindingContext);

			return pathfindingContext;
		}

		public void InspectPlayerInventory(Player player, bool forceRedraw, bool? interactabilityOverride = null)
		{
			bool redraw = inventoryPanel.InspectedPlayer != player && (forceRedraw);// || (Array.FindAll(player.Stats.ItemsInInventory, i => i.Design.cardType != CardType.NOT_PLAYABLE).Length > 0 && inventoryPanel.InspectedPlayer != player));
			Action inspectAndUpdateAction = () =>
			{
				inventoryPanel.InspectedPlayer = player;
				UpdateInventoryInteractability(interactabilityOverride);
			};

			inspectAndUpdateAction();
			if (redraw)
				inventoryPanel.Show(false);
		}

		void UpdateShiftersInteractability()
		{
			bool shiftersCanBePushedByCurrentPlayer = CurrentPlayer.Stats.RemainingDungeonPushes > 0;
			bool shiftersEnabledForMe = localHumanPlayerManager.IsMyTurnOutsideBattle && shiftersCanBePushedByCurrentPlayer;
			board.SetShiftingEnabled(shiftersEnabledForMe);
		}

		public void UpdateInventoryInteractability(bool? interactability = null)
		{
			bool cardsChooseable = false, canPlayCards = false, canDiscardCards = false, examinableAndCanExamine = false, cardsTradeable = false;
			CardType[] playableCardTypes = null;

			var myInventoryInspected = inventoryPanel.InspectedPlayer == localHumanPlayerManager.Me;
			bool canEndTurn = endTurnButton.interactable;
			bool interactableByOverrideOrTrue = interactability ?? true;

			bool cardsInteractable =
					interactableByOverrideOrTrue
					&& myInventoryInspected
					&& !inspectPanel.IsShownOrShowing
					&& (localHumanPlayerManager.IsMyTurnInBattleAndCanPlayCards && localHumanPlayerManager.Me.Stats.RemainingBattleCardsToPlay > 0 // playing battle cards during our battle-turn
						|| localHumanPlayerManager.IsMyTurnOutsideBattle  // playing utility cards during our turn & ready to do the next thing (end turn is only activated then)
						|| localHumanPlayerManager.IsPostBattleAndIWon); // discarding cards after a battle that we won

			if (cardsInteractable)
			{
				if (localHumanPlayerManager.Trading_MyDesiredCard == null)
				{
					canPlayCards = (canEndTurn || localHumanPlayerManager.IsMyTurnInBattleAndCanPlayCards) // in battle can't end turn, but should be able to play cards
						&& !localHumanPlayerManager.IsPostBattleAndIWon && !contextPanel.IsShown;
					playableCardTypes = new CardType[] { localHumanPlayerManager.Me.Stats.InBattle ? CardType.BATTLE : CardType.UTILITY };
					canDiscardCards = !IsPreIntraOrPostBattle || localHumanPlayerManager.IsPostBattleAndIWon;
				}
				else
					cardsChooseable = true;
				//inventoryPanel.TreasuresDiscardable = 
			}
			else if (interactableByOverrideOrTrue)
			{
				if (myInventoryInspected)
				{

				}
				else
				{
					if (canEndTurn)
					{
						if (localHumanPlayerManager.IsMyTurnOutsideBattle && !inspectPanel.IsShownOrShowing && !contextPanel.IsShown)
						{
							cardsInteractable = true;
							if (localHumanPlayerManager.Me.Stats[Stat.CARD_STEALING] > 0 && localHumanPlayerManager.Me.Stats.FreeInventorySlots > 0)
							{
								//Debug.Log("Stealing mode");
								cardsChooseable = true;
							}
							else if (localHumanPlayerManager.Me.Stats.ItemsInInventory.Length > 0)
							{
								cardsTradeable = true;
							}
						}
					}
				}
			}
			examinableAndCanExamine = !inspectPanel.IsShownOrShowing;
			bool chooseableOrTradeable = cardsChooseable || cardsTradeable;
			bool meInspectedAndNotChooseableOrTradeable = myInventoryInspected && !chooseableOrTradeable;

			inventoryPanel.SetCardsCanPlay(canPlayCards, playableCardTypes);
			if (meInspectedAndNotChooseableOrTradeable)
				inventoryPanel.MakeAllPlayableIfCanPlayThem();
			else
				inventoryPanel.MakeAllNotPlayable();

			inventoryPanel.CardsInteractable = cardsInteractable;
			inventoryPanel.CardsChoosable = cardsChooseable;
			inventoryPanel.CardsTradeable = cardsTradeable;
			inventoryPanel.CanDiscardCards = canDiscardCards;
			inventoryPanel.CardsDiscardable = meInspectedAndNotChooseableOrTradeable;
			inventoryPanel.CardsExaminableAndCanExamine = examinableAndCanExamine;
			inventoryPanel.PlayButtonText = IsIntraBattle ? "Equip" : "Activate";
			inventoryPanel.RebuildLayoutAndUpdateCanvases();

			inventoryPanel.Interactable = interactableByOverrideOrTrue;
			if (!interactableByOverrideOrTrue && localHumanPlayerManager.IsMyTurn)
				inventoryPanel.Close(0f, null);
		}

		public void SetBattleCommandPanelStateNow(BattleCommandsPanel.ButtonsState state, Action optionalAction = null)
		{
			battlePanel.commandsPanel.State = state;
			if (state == BattleCommandsPanel.ButtonsState.IN_BATTLE)
				InspectPlayerInventory(localHumanPlayerManager.Me, false);
			else if (state == BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE)
				battlePanel.commandsPanel.DoNothingText = localHumanPlayerManager.IsMyTurn ? "Let me pass!" : "Allow to pass";

			if (optionalAction != null)
				optionalAction();
		}

		void SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState state, Action optionalAction = null)
		{ _PGM.DoAfterMasterReady(() => SetBattleCommandPanelStateNow(state, optionalAction)); }

		void UpdateWinChances()
		{
			if (CurrentPlayer.GetParentAsBlock() == null || board.IsAnimatingDungeonShift || board.IsAnimatingMove || localCPUPlayerManager && CurrentPlayer == localCPUPlayerManager.Me && localCPUPlayerManager.IsThinking)
				return;

			//Debug.Log(_Design.items.commonItems.THROWING_DAGGER.GetEffects(null, null) == null ? 0 : _Design.items.commonItems.THROWING_DAGGER.GetEffects(null, null).Count);
			//return;
			var currentPlayerPotential = new CharacterBattlePotential(CurrentPlayer);
			//if (CurrentPlayer != localCPUPlayerManager.Me || !localCPUPlayerManager.IsThinking)
			CurrentPlayer.SetDisplayName(CurrentPlayer.NickName);

			foreach (var otherChar in _SpawnManager.GetAllCharacters(true))
			{
				if (otherChar.GetParentAsBlock() == null) // dead
					continue;

				if (otherChar != CurrentPlayer)
				{
					//currentPlayerPotential.ComputeWithOpponent(otherChar);
					//var otherPotential = new CharacterBattlePotential(otherChar);
					//otherPotential.ComputeWithOpponent(CurrentPlayer);

					//float winChance = currentPlayerPotential.GetChanceToWinAgainst(otherPotential);
					//otherChar.SetDisplayName(String.Format("{0:P2}", winChance));
					Cell.Descriptor _;
					var bDesc = board.CreateDescriptor();
					float nonRangedChance = Utils.GetWinChanceAgainst(bDesc, currentPlayerPotential, -1f, otherChar, false, out _);
					string app = ""
;					if (CurrentPlayer.Stats[Stat.RANGED_BATTLE] > 0)
						app = "\n(R: " + String.Format("{0:P0}", Utils.GetWinChanceAgainst(bDesc, currentPlayerPotential, -1f, otherChar, true, out _)) + ")";
					var nam = "<size=10><color=white>" + String.Format("{0:P0}", nonRangedChance) + app + "</color></size>";
					if (otherChar is Player)
					{
						nam = (otherChar as Player).ShortNickName + "\n" + nam;
					}

					otherChar.SetDisplayName(nam);
				}
			}
		}

		bool DidPlayerEscape(Player player)
		{
			// If the cell is the Exit cell, this means the game can end. (if all escape items are in inventory)
			if (_SpawnManager.exitAreaInstance.ParentBlock == player.GetParentAsBlock())
			{
				if (player.Stats.GetInventoryItemsOfType<EscapeItem>().Length == 3) // game ended; the master will notify all clients
				{
					return true;
				}
			}
			return false;
		}

		IEnumerator LoadMainMenuDelayed()
		{
			try { UpdateInteractivityForCurrentPlayer(false); } catch { }
			float secsToWait = 2f;
			Debug.Log("Loading main menu after " + secsToWait + " seconds - otherwise OnConnectedToPhoton is called (possibly PUN bug)");
			yield return new WaitForSeconds(secsToWait);
			SceneManager.LoadScene(0);
		}
		#endregion
	}

}
