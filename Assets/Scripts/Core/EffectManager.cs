﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Core
{
	public class EffectManager
	{
		public static EffectManager Instance
		{
			get
			{
				if (_Instance == null)
					_Instance = new EffectManager();

				return _Instance;
			}
		}

		static EffectManager _Instance;


		public void CastItem(Character caster, IEffectsItem effectsItem, Character enemy)
		{ CastEffects(caster, enemy, (effectsItem as BaseItemDesign).name, effectsItem.GetEffects(caster, enemy).ToArray()); }

		public void CastEffects(Character caster, Character enemy, string sourceItemName, params Config.Effects.Effect[] effects)
		{
			foreach (var effect in effects)
			{
				Character owner;
				string s;
				if (effect.owner == Config.Effects.Effect.OwnerEnum.SELF)
				{
					owner = caster;
					s = "his ";
				}
				else
				{
					owner = enemy;
					s = caster + "'s ";
				}

				Debug.Log(owner.name + " affected by " + s + sourceItemName + ": Gets " + effect.type + ", amount=" + effect.amount + ", duration=" + effect.duration);
				owner.Stats.AddEffect(effect);
			}
		}
	}
}
