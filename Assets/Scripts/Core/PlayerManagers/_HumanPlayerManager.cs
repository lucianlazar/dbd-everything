﻿//using UnityEngine;
//using System.Collections;
//using Photon;
//using UnityEngine.UI;
//using Com.TheFallenGames.DBD.Core;
//using UnityEngine.SceneManagement;
//using System;
//using Com.TheFallenGames.DBD.World;
//using System.Collections.Generic;
//using Com.TheFallenGames.DBD.Common;
//using Com.TheFallenGames.DBD.Config;
//using Com.TheFallenGames.DBD.UI.Totems;
//using Com.TheFallenGames.DBD.Data;
//using Com.TheFallenGames.DBD.UI;
//using Com.TheFallenGames.DBD.Actors.Items;
//using Com.TheFallenGames.DBD.Config.Items;
//using Com.TheFallenGames.DBD.UI.Inventory;
//using Com.TheFallenGames.DBD.UI.Battle;
//using Com.TheFallenGames.DBD.Items;
//using Com.TheFallenGames.DBD.Data.Inventory;
//using Com.TheFallenGames.DBD.Network.Data;
//using Com.TheFallenGames.DBD.UI.Inspect;
//using Com.TheFallenGames.DBD.Base;

//namespace Com.TheFallenGames.DBD.Actors
//{
//    public class HumanPlayerManager : BasePlayerManager
//    {

//		protected override void Start()
//		{
//			base.Start();

//			// Board initializes in Awake, so its info is available only in our Start()
//			foreach (var cell in _Board.AllCellsIncludingOuterCell)
//                cell.AllowedAreaHighlight.onSelected = OnAllowedAreaSelected;

//			_Board.OuterCell.onPlaced += OnShifterSelected;

//			//_Board.onDungeonShifted = OnDungeonShifted;

//			leaveRoomButton.onClick.AddListener(OnLeaveRoomClicked);
//			endTurnButton.onClick.AddListener(OnEndTurnClicked);

//			totemsPanel.Initialize(OnBuildTotemClick);

//			inventoryPanel.NavigationPanel.NavigateClicked += direction => InspectPlayerInventory(_SpawnManager.GetPlayerInstanceAtRelativePosition(inventoryPanel.InspectedPlayer, direction), true);

//			inventoryPanel.PlayClick += OnPlayCardClicked;
//			inventoryPanel.DiscardClick += OnDiscardClicked;
//			inventoryPanel.ChooseClick += OnChooseCardClicked;
//			inventoryPanel.ExamineClick += OnExamineCardClicked;
//			inventoryPanel.RequestTradeClick += OnRequestTradeCardClicked;

//			battlePanel.commandsPanel.BattleClicked += OnBattleClicked;
//			battlePanel.commandsPanel.DoNothingClicked += OnDoNothingClicked;
//			battlePanel.commandsPanel.OnChoseIfToEvade += OnMeAsOpponentChoseIfToCancelBattle;
//			battlePanel.commandsPanel.ReadyClicked += OnReadyClicked;
//			//battlePanel.commandsPanel.CloseClicked += OnCloseClicked;

//			Base.Entity.EntityLongPressed += OnEntityLongPressed;

//			inspectPanel.commandsPanel.RangedBattleClicked += OnInspectPanelRangedBattleClicked;
//			inspectPanel.commandsPanel.ChooseClicked += OnInspectPanelChooseClicked;
//			inspectPanel.commandsPanel.MiscClicked += OnInspectPanelMiscClicked;
//			inspectPanel.commandsPanel.LootClicked += OnInspectPanelLootClicked;
//			inspectPanel.InspectedEntityChanged += OnInspectPanelEntityChanged;

//			inspectPanel.Showing += OnInspectPanelShowing;
//			inspectPanel.Closed += OnInspectPanelClosed;
//		}

//		// For debugging
//		//#if UNITY_EDITOR
//		void Update()
//		{
//			if (!Application.isEditor && (PhotonNetwork.player == null || !PhotonNetwork.playerName.Contains("luc-test-wind")))
//				return;

//			if (!Input.GetKey(KeyCode.LeftControl))
//				return;

//			if (!IsMyTurn)
//				return;

//			if (Input.GetMouseButtonDown(2))
//			{
//				var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//				RaycastHit hit;
//				if (Physics.Raycast(ray, out hit, 1000f, 1 << LayerMask.NameToLayer("Cell") | 1 << LayerMask.NameToLayer("OuterCell")))
//				{
//					var cell = hit.collider.GetComponent<Cell>();
//					if (cell.ContainedBlock)
//						OnAllowedAreaSelected(cell.AllowedAreaHighlight);
//				}
//			}
//			else if (Input.GetMouseButtonDown(3))
//			{
//				CurrentPlayer.Stats.AddEffect(new Config.Effects.Effect() { type = Stat.ATK, amount = 20, dynamicallyCreated = true });
//				UpdateStatsUI();
//			}
//		}
////#endif


//        #region PunBehaviour events
//        public override void OnLeftRoom()
//        {
//			Debug.Log("PM: OnLeftRoom");
//			//Task8.CreateAndStart(3f, 0, 0f, 0f, null, () => SceneManager.LoadScene(0));
//			//SceneManager.LoadScene(0);
//			StartCoroutine(LoadMainMenuDelayed());
//			//PhotonNetwork.Disconnect(); // will callOnDisconnectedFromPhoton below
//		}

//		public override void OnDisconnectedFromPhoton()
//		{
//			Debug.Log("PM: OnDisconnectedFromPhoton");
//			StartCoroutine(LoadMainMenuDelayed());
//		}
//		#endregion

//		#region PunGameManager events
//		protected override void OnNewTurn()
//        {
//			base.OnNewTurn();

//            turnText.text = "<color=silver>Turn #" + _PGM.CurrentTurnNumber + "</color>\n" + CurrentPlayer.ShortNickNameWithColoredClass;
//			//Debug.Log("PlayerManager: OnNewTurn; playerID=" + playerID + "; isOurTurn=" + (playerID == PhotonNetwork.player.ID));
//			// Make sure the inventory represents the current player's cards
//			InspectPlayerInventory(CurrentPlayer, false);
//			UpdateInteractivityForCurrentPlayer(true);
//			if (!statsPanel.InspectedPlayer)
//				statsPanel.InspectedPlayer = CurrentPlayer;
//			UpdateStatsUI();

//			// Notify the Master Client we received/executed the command, so it can be ready to accept new ones (from us, if it's our turn)
//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnPlayerEndedTurn()
//		{
//			base.OnPlayerEndedTurn();

//			UpdateInteractivityForCurrentPlayer(false);

//			//int levelsAchieved = CurrentPlayer.Stats.UpdateStatsOnTurnEnd();
//			////if (levelsAchieved > 0)
//			////	Debug.Log("PlayerManager: current player gained " + levelsAchieved + " levels. Current level: " + CurrentPlayer.Stats.Level);

//			//if (OpponentOfCurrentPlayer) // A battle took place
//			//	WinnerOfCurrentBattle.OnWonBattle(CurrentPlayer == WinnerOfCurrentBattle ? OpponentOfCurrentPlayer : CurrentPlayer);

//			if (LosingBlockOfCurrentBattle)
//			{
//				// Make the left items not free anymore. The winner had a chance to pick them up for free, but since he declined them, they are now like any other cost item
//				foreach (var temporarilyFreeItem in LosingBlockOfCurrentBattle.GetChildrenOfType<CommonItem>())
//					temporarilyFreeItem.IsFreeUntilNextPickUp = false;
//			}

//			_SpawnManager.ReLinkNonPickedUpMonsterItems();

//			UpdateStatsUI();

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnMoveToCellAccepted(int cellIndex)
//		{
//			//if (IsMyTurn)
//			//	_PGM.Ev_PlayerMoved -= PlayerMoveToCellAccepted;
//			UpdateInteractivityForCurrentPlayer(false);

//			MoveToCell(_Board.AllCellsIncludingOuterCell[cellIndex]);
//		}

//		protected override void OnOtherMoveToCellAccepted(int startCellIndex, int indexInBlock, int cellIndex)
//		{
//			UpdateInteractivityForCurrentPlayer(false);

//			var character = _Board.AllCellsIncludingOuterCell[startCellIndex].ContainedBlock.GetChild(indexInBlock) as Character;
//			var destCell = _Board.AllCellsIncludingOuterCell[cellIndex];

//			CurrentPlayer.Stats.ChangeTemporaryStat(Stat.ATTRACT_ENEMY, -1, false);

//			character.Anim_MoveTo(destCell.ContainedBlock, () => OnOtherPlayerMoveToCellAnimationEnded(character, destCell));
//		}

//		protected override void OnDungeonPushAccepted(int shifterIndex)
//		{
//			ShiftDungeon(_Board.AllShiftPoints[shifterIndex]);
//		}

//		// Case 1(most common): current player moved to a cell
//		// Case 2: a battle just happened and the winning player has found ore in his current cell (this won't be the case for the range attacks)
//		protected override void OnGetOreAccepted(int cellIndex)
//		{
//			var cell = _Board.AllCellsIncludingOuterCell[cellIndex];
//			var ore = cell.ContainedBlock.PopOre();

//			WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.Ore += ore.Amount;
//			_SpawnManager.DestroyOre(ore);
//			UpdateStatsUI();

//			if (IsMyTurnOutsideBattleOrIsPostBattleAndIWon)
//				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell); // continue interracting with the cell
//			else if (!IsPostBattle)
//				HighlightReachableCells(); // for others

//			_PGM.OnCurrentActionExecuted();
//		}

//		// Case 1: current player discarded a card outside battle
//		// Case 2: a battle happened the and winning player discarded something to make room for any item on the ground
//		protected override void OnDiscardAccepted(int itemIndexInInventory)
//		{
//			WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.ItemsInInventory[itemIndexInInventory].Owner = null;

//			// Restore the battle panel state, so "READY" can be pressed
//			if (IsMyTurnInBattleAndBattleNotEnded)
//				battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.IN_BATTLE;

//			if (IsMyTurnOutsideBattleOrIsPostBattleAndIWon)
//			{
//				//// We won and we've just discarded something => close the context menu, if it's opened
//				//if (IsPostBattleAndIWon)
//				//{
//				//if (contextPanel.IsShown)
//				//	contextPanel.Cancel();
//				//}

//				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell); // continue interracting with the cell
//				//UpdateInventoryInteractability();
//				//if ()
//				//inventoryPanel.CardsInteractable = true;
//			}
//			_PGM.OnCurrentActionExecuted();
//		}

//		// Case 1: current player picked a card outside battle
//		// Case 2: a battle happened and the winning player picked up something from the current block (where loser dropped it) after it discarded something to make room
//		protected override void OnPickUpItemAccepted(int itemIndexInBlock)
//		{
//			var item = WinningPlayerOfCurrentBattleOrCurrentPlayer.GetParentAsBlock().GetChild(itemIndexInBlock) as BaseItem;

//			// In case it needs to be bought, release the needed amount of treasures
//			var itemAsCommonItem = item as CommonItem;
//			if (itemAsCommonItem != null && itemAsCommonItem.CurrentCost > 0)
//			{
//				WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.ReleaseTreasures(itemAsCommonItem.CurrentCost, false);
//			}

//			item.Owner = WinningPlayerOfCurrentBattleOrCurrentPlayer;

//			//if (IsPostBattleAndCurrentPlayerWon || Ism)
//				UpdateStatsUI();

//			if (IsMyTurnOutsideBattleOrIsPostBattleAndIWon)
//				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);// continue interracting with the cell
//			else if (!IsPostBattle)
//				HighlightReachableCells(); // for others
//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnPlayerBuildTotemAccepted(TotemType totemType)
//		{
//			_SpawnManager.SpawnTotem(CurrentPlayer.GetParentCell(), CurrentPlayer, _Design.totems[totemType]);
//			CurrentPlayer.Stats.Ore -= _Design.totems.costPerTotem;
//			// Re-calculate the reachable cells, since player has one more move now
//			UpdateInteractivityForCurrentPlayer(true);
//			UpdateStatsUI();

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnSetupBattleAccepted(int cellIndex, int characterIndexInBlock)
//		{
//			//Debug.Log("OnSetupBattleAccepted IsMyTurnInBattle=" + IsMyTurnInBattle);
//			OpponentOfCurrentPlayer = _Board.AllCellsIncludingOuterCell[cellIndex].ContainedBlock.GetChild(characterIndexInBlock) as Character;
//			endTurnButton.interactable = false;
//			battlePanel.Show(CurrentPlayer, OpponentOfCurrentPlayer);

//			bool pvp = OpponentOfCurrentPlayer is Player;
//			if (pvp)
//			{
//				if (IsMyTurn)
//				{
//					SetBattleCommandPanelStateOnceAfterMasterReady(
//						OpponentOfCurrentPlayer.Stats.HasSpider ?
//							BattleCommandsPanel.ButtonsState.WAITING // waiting to see if opponent will evade with spider
//							 : BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE // waiting for local player's choice
//						);
//				}
//				else if (AmIBattlingCurrentPlayer && Me.Stats.HasSpider)
//					SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_CANCEL_BATTLE);
//			}
//			else
//			{
//				if (PhotonNetwork.isMasterClient)
//					_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated(CurrentPlayer.ActorID));
//			}

//			if (!Me.Stats.InBattle)
//				// We're a spectator => see the empty results panel for now
//				battlePanel.ActivateBattleResultsPanel();

//			UpdateInteractivityForCurrentPlayer(false);
//			UpdateInventoryInteractability(true); // but the cards in inventory should be visible

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnPlayCardAccepted(int itemIndexInInventory)
//		{
//			var playerToConsumeItem = CurrentPlayerInBattleOrCurrentPlayer;
//			var item = playerToConsumeItem.Stats.ItemsInInventory[itemIndexInInventory];

//			ConsumeItem(playerToConsumeItem, item);

//			//playerToConsumeItem.Stats.ConsumeItem(item);
//			//_SpawnManager.graveyard.Add(item);
//			// TODO show on UI

//			// Restore the battle panel state, so "READY" can be pressed
//			if (IsMyTurnInBattleAndBattleNotEnded)
//				battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.IN_BATTLE;

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnBattlePlayerInitiates()
//		{
//			battlePanel.logText.text = CurrentPlayer.ShortNickNameWithColoredClass + " initiates!";

//			if (PhotonNetwork.isMasterClient)
//				_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated(CurrentPlayer.ActorID));

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnBattlePlayerStaysIdle()
//		{
//			battlePanel.logText.text = CurrentPlayer.ShortNickNameWithColoredClass + " prefers not to battle...";

//			if (AmIBattlingCurrentPlayer)
//				SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE);
//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnBattleApproachedPlayerChoseIfToCancelBattle(bool cancel)
//		{
//			string logText;
//			if (cancel)
//			{
//				logText = OpponentOfCurrentPlayer.Design.name + " evaded using The Spider!";

//				// Moving the spider card into the discard pile
//				var opponentStats = (OpponentOfCurrentPlayer as Player).Stats;
//				opponentStats.ItemsInInventory[opponentStats.GetIndexOfItemWithDesign(_Design.monsters.SPIDER.rewardItem)].Owner = null;

//				// After the master confirmed everyone executed the action, close the panel as if it was a tie (enter the same state as if in a tie) + end turn
//				// _PGM.OnCurrentActionExecuted will be called in OnBattlePanelHiddenAfterBattle 
//				battlePanel.Close(battlePanel.autoHideDelay + _Design.general.battlePanelAdditionalHideDelayWhenSpider, () => OnBattlePanelHiddenAfterBattle(true));
//			}
//			else
//			{
//				logText = OpponentOfCurrentPlayer.Design.name + " chose not to evade with The Spider";

//				// The normal flow (same state as if the opponent haven't had the spider)
//				if (IsMyTurn)
//					SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE);
//				_PGM.OnCurrentActionExecuted();
//			}

//			battlePanel.logText.text = logText;
//		}

//		protected override void OnBattleApproachedPlayerInitiates()
//		{
//			battlePanel.logText.text = ".. but " + (OpponentOfCurrentPlayer as Player).ShortNickNameWithColoredClass + " initiates!";

//			if (PhotonNetwork.isMasterClient)
//				_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated((OpponentOfCurrentPlayer as Player).ActorID));

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnBattleApproachedPlayerAlsoStaysIdle()
//		{
//			battlePanel.logText.text = (OpponentOfCurrentPlayer as Player).ShortNickNameWithColoredClass + " agrees not to battle";
//			battlePanel.SetBattleWinner(null);
//			battlePanel.Close(() =>
//			{
//				//if (IsMyTurn)
//				//	_PGM.DoOnceAfterMasterReady(_PGM.RequestEndMyTurn);
//				_PGM.DoAfterMasterReady(() =>
//				{

//					UpdateInteractivityForCurrentPlayer(true);

//				});

//				_PGM.OnCurrentActionExecuted();
//			});
//		}

//		protected override void OnPlayerBattleTurnStarted(int playerID)
//		{
//			CurrentPlayerInBattle = _SpawnManager.GetPlayerInstanceByActorID(playerID);
//			battlePanel.logText.text = CurrentPlayerInBattle.ShortNickNameWithColoredClass + " is preparing to attack !";

//			// When no one has pressed READY yet, this is the first turn
//			// The first battle turn marks the beginning of the battle
//			if (_PGM.PlayersReadyForBattle == 0)
//			{
//				CurrentPlayer.Stats.UpdateStatsOnBattleStarted(OpponentOfCurrentPlayer);
//				OpponentOfCurrentPlayer.Stats.UpdateStatsOnBattleStarted(CurrentPlayer);

//				// Play the knife automatically, if the current player is a non-archer and it has it in inventory
//				if (CurrentPlayer.Design != _Design.players.ARCHER)
//				{
//					if (CurrentPlayer.RelativePosSimpleTo(OpponentOfCurrentPlayer) == RelativePositionSimple.NEIGHBOR_DIRECT)
//					{
//						int index = CurrentPlayer.Stats.GetIndexOfItemWithDesign(_Design.items.commonItems.THROWING_DAGGER);
//						if (index != -1) // at the moment of the implementation the index can't be -1, but maybe in the future you can have the effect without having the knife
//							ConsumeItem(CurrentPlayer, CurrentPlayer.Stats.ItemsInInventory[index]);
//					}
//				}
//			}

//			CurrentPlayerInBattle.Stats.UpdateStatsOnBattleTurnStart();


//			//Debug.Log("OnPlayerBattleTurnStarted IsMyTurnInBattle=" + IsMyTurnInBattle + "; CurrentPlayerInBattle=" + CurrentPlayerInBattle.name);
//			if (IsMyTurnInBattle)
//			{
//				if (Me.Design.canPickUpItems && Me.Stats.NumBattleItemsInInventory > 0) // human & has battle items
//				{
//					SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState.IN_BATTLE, () => {
//						IsMyTurnInBattleAndCanPlayCards = true;
//						UpdateInventoryInteractability();
//					});
//				}
//				else // animal || human with no battle items => nothing to play => automatically ready
//					SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState.WAITING, _PGM.RequestReadyToBattle);
//			}
//			else
//				UpdateStatsUI();

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnPlayerReadyForBattleAccepted(int playerID)
//		{
//			battlePanel.logText.text = _SpawnManager.GetPlayerInstanceByActorID(playerID).ShortNickNameWithColoredClass + " is ready !";

//			if (PhotonNetwork.isMasterClient)
//			{
//				bool pvp = battlePanel.SecondCharacter is Player;
//				bool battleCanBegin = true;

//				if (pvp)
//				{
//					// The first one ended his battle-turn => now it's the next one's battle-turn
//					if (_PGM.PlayersReadyForBattle == 1)
//					{
//						battleCanBegin = false; // the other one also needs to send the "ready" signal

//						var nextPlayer = CurrentPlayer.ActorID == playerID ? (OpponentOfCurrentPlayer as Player) : CurrentPlayer;
//						_PGM.DoAfterMasterReady(() => _PGM.DispatchBattleTurnStated(nextPlayer.ActorID));
//					}
//				}

//				if (battleCanBegin)
//					_PGM.DoAfterMasterReady(DispatchBattleOutcome);
//			}

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnBattleResultsObtainedFromMaster(BattleStats battleStats)
//		{
//			battlePanel.logText.text = "";

//			Action endResultCountingAction = () =>
//			{
//				// _PGM.OnCurrentActionExecuted() will be called at the end of the animation
//				battlePanel.BeginCounting(battleStats.firstPlayerScore, battleStats.secondPlayerScore, () => OnBattleResultsShown(battleStats));
//			};

//			// In order to prevent a more complicated battle flow, only show the intermeriary tie if there's no serpent present in either of the players' inventory
//			var serpentItemDesign = _Design.monsters.SERPENT.rewardItem;
//			if (CurrentPlayer.Stats.GetIndexOfItemWithDesign(serpentItemDesign) == -1 && OpponentOfCurrentPlayer.Stats.GetIndexOfItemWithDesign(serpentItemDesign) == -1)
//			{
//				var tieStats = battleStats.statsOfFirstTieBeforeFinalRound;
//				// If the battle is not pre-determined, then the final outcome is definitely not a tie.
//				// And if the tieStats are not null, then at least 1 tie happened before the final outcome => first, show how a tie is happening, then the actual outcome
//				if (!battleStats.finalBattlePointsArePreDetermined && tieStats != null)
//				{
//					battlePanel.BeginCounting(
//						tieStats.firstPlayerScore, 
//						tieStats.secondPlayerScore, 
//						() => 
//						{
//							// Set the tie stats
//							battlePanel.resultsPanel.SetDetails(
//								tieStats.firstPlayerScoreFromAtk,
//								tieStats.secondPlayerScoreFromAtk,
//								tieStats.firstPlayerScoreFromMagic, battlePanel.FirstCharacter.Design.magicType,
//								tieStats.secondPlayerScoreFromMagic, battlePanel.SecondCharacter.Design.magicType
//							);

//							battlePanel.AnimateTie(endResultCountingAction, false);
//						}
//					);

//					// The endResultCountingAction will be executed after the tie animation ends
//					return;
//				}
//			}

//			// Not showing the animation for the tie. Directly play the final counting animation and end the battle
//			endResultCountingAction();
//		}

//		protected override void Client_OnPlayerEliminated(int playerId)
//		{

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnPostBattleWinnerDoneLooting()
//		{
//			// End our turn after the looting is done (we or the other player(if he won) did the looting)
//			//Debug.Log("OnPostBattleWinnerDoneLooting ISmyturn="+IsMyTurn);
//			if (IsMyTurn)
//				_PGM.DoAfterMasterReady(_PGM.RequestEndMyTurn);

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnItemTransferAccepted(int donorPlayerID, int itemIndexInInventory, int receiverPlayerID, object[] receiverTradedItemIndexInInventoryAndAcceptedStatus)
//		{
//			var donor = _SpawnManager.GetPlayerInstanceByActorID(donorPlayerID);
//			var donorItem = donor.Stats.ItemsInInventory[itemIndexInInventory];
//			var receiver = _SpawnManager.GetPlayerInstanceByActorID(receiverPlayerID);
//			int receiverTradedItemIndexInInventory = (int)receiverTradedItemIndexInInventoryAndAcceptedStatus[0];
//			bool updateInteractivityToTrueAfterMasterReadyIfItsMyTurn = false;

//			if (receiverTradedItemIndexInInventory == -1) // stealing
//			{
//				donorItem.Owner = receiver;
//				CurrentPlayer.Stats.ChangeTemporaryStat(Stat.CARD_STEALING, -1, false);
//				updateInteractivityToTrueAfterMasterReadyIfItsMyTurn = true;
//			}
//			else // trading
//			{
//				var receiverTradedItem = receiver.Stats.ItemsInInventory[receiverTradedItemIndexInInventory];
//				bool? accepted = (bool?)receiverTradedItemIndexInInventoryAndAcceptedStatus[1];

//				if (accepted == null) // not asked yet. need to first ask the potential donor
//				{
//					if (donor == Me)
//					{
//						contextPanel.Show(
//							receiver.ShortNickNameWithColoredClass + ": I'll give you " + receiverTradedItem.name + " for " + donorItem.name,
//							new string[] { "Deal!", "No, thanks" },
//							null,
//							result =>
//							{
//								_PGM.DoAfterMasterReady(() =>
//									_PGM.RequestAnswerTradeProposal(donorPlayerID, itemIndexInInventory, receiverPlayerID, receiverTradedItemIndexInInventory, result == 0)
//								);
//								_PGM.OnCurrentActionExecuted();
//							},
//							_Design.general.battlePanelTradeAutoDecisionTimeout
//						);

//						return; // _PGM.OnCurrentActionExecuted() will be called after answer
//					}
//				}
//				else
//				{
//					// The other player was asked and it answered
//					Debug.Log("Trade accepted = " + accepted.Value);
//					Trading_MyDesiredCard = null;
//					if (accepted.Value)
//					{
//						// In case one of them has a full inventory, first free one slot and then add them one at a time
//						donorItem.Owner = receiverTradedItem.Owner = null;
//						donorItem.Owner = receiver;
//						receiverTradedItem.Owner = donor;
//					}

//					updateInteractivityToTrueAfterMasterReadyIfItsMyTurn = true;
//				}
//			}

//			if (updateInteractivityToTrueAfterMasterReadyIfItsMyTurn && IsMyTurn)
//				_PGM.DoAfterMasterReady(() => {
//					UpdateInteractivityForCurrentPlayer(true);
//					InspectPlayerInventory(Me, true, true);
//					//inventoryPanel.SelectNone();
//				});

//			_PGM.OnCurrentActionExecuted();
//		}

//		protected override void OnGameResult(int _)
//		{
//			totemsPanel.gameObject.SetActive(false);
//			inventoryPanel.gameObject.SetActive(false);
//			// Not calling OnCurrentRequestExecuting, because at this point no other request should be
//		}
//		#endregion


//		#region UI events
//		void OnEndTurnClicked()
//        {
//			if (contextPanel.IsShown)
//				contextPanel.Cancel();

//			UpdateInteractivityForCurrentPlayer(false);

//			_PGM.RequestEndMyTurn();
//        }

//        void OnLeaveRoomClicked()
//        {
//            endTurnButton.interactable = leaveRoomButton.interactable = false;
//            var result = PhotonNetwork.LeaveRoom();
//            Debug.Log("LeaveRoom result =" + result);
//        }

//        void OnBuildTotemClick(TotemType totemType)
//		{
//			UpdateInteractivityForCurrentPlayer(false);
//			_PGM.RequestBuildTotem(totemType);
//        }

//        void OnAllowedAreaSelected(AllowedArea allowedArea)
//		{
//			UpdateInteractivityForCurrentPlayer(false);
//			//_PGM.Ev_PlayerMoved += PlayerMoveToCellAccepted;
//			_PGM.RequestMove(_Board.GetCellIndex(allowedArea.GetParentAsCell()));
//		}

//		void OnShifterSelected(Shifter shifter)
//		{
//			UpdateInteractivityForCurrentPlayer(false);
//			_PGM.RequestShiftDungeon(_Board.GetShiterIndex(shifter));
//		}

//		void OnPlayCardClicked(CardModel cardModel)
//		{
//			inventoryPanel.CardsInteractable = false; // prevent new request before result arrives
//			// Prevent sending "READY" before receiving the confirmation
//			if (IsMyTurnInBattle)
//				battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
//			_PGM.RequestPlayCard(CurrentPlayerInBattleOrCurrentPlayer.Stats.GetIndexOfItemWithDesign(cardModel.design as BaseItemDesign));
//		}

//		void OnDiscardClicked(CardModel cardModel)
//		{
//			inventoryPanel.CardsInteractable = false; // prevent new request before result arrives

//			// Prevent sending "READY" before receiving the confirmation
//			if (IsMyTurnInBattle)
//				battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;

//			// Commented: this should always happen, as discarding a card should refresh the options (a good example is showing a "buy" panel for a cost item. 
//			// the buy button should be updated if the player discards a treasure meanwhile, possibly making the item unaffordable)
//			//// We won and we've just discarded something => close the context menu, if it's opened
//			//if (IsPostBattleAndIWon)
//			//{
//				if (contextPanel.IsShown)
//					contextPanel.Cancel();
//			//}

//			// Cards can only be discarded outside battle or by the winninng player in post-battle
//			_PGM.RequestDiscard(WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.GetIndexOfItemWithDesign(cardModel.design as BaseItemDesign));
//		}

//		void OnChooseCardClicked(CardModel cardModel)
//		{
//			UpdateInteractivityForCurrentPlayer(false);

//			var cardModelDesign = cardModel.design as BaseItemDesign;
//			Player donor;
//			BaseItemDesign donorItem;
//			int myTradedItemIndex = -1;
//			if (Trading_MyDesiredCard == null) // stealing
//			{
//				donor = inventoryPanel.InspectedPlayer;
//				donorItem = cardModelDesign;
//			}
//			else // trading
//			{
//				donor = (_SpawnManager.GetEntityByDesign(Trading_MyDesiredCard) as BaseItem).Owner as Player;
//				donorItem = Trading_MyDesiredCard as BaseItemDesign;

//				myTradedItemIndex = Me.Stats.GetIndexOfItemWithDesign(cardModelDesign);
//				// Trading: Ask the other player
//				Debug.Log("Asking for " + Trading_MyDesiredCard.name + " in return of " + cardModel.design.name);
//			}

//			_PGM.RequestTransferItem(
//				donor.ActorID,
//				donor.Stats.GetIndexOfItemWithDesign(donorItem),
//				Me.ActorID,
//				myTradedItemIndex
//			);
//		}

//		void OnExamineCardClicked(CardModel cardModel)
//		{
//			inventoryPanel.Close(0f, null);
//			inspectPanel.Show(_SpawnManager.GetEntityByDesign(cardModel.design));
//		}

//		void OnRequestTradeCardClicked(CardModel cardModel)
//		{
//			//UpdateInteractivityForCurrentPlayer(false);
//			Trading_MyDesiredCard = cardModel.design;
//			InspectPlayerInventory(Me, true);
//			inventoryPanel.SelectFirst(); // so the player can see he can choose one
//		}

//		void OnBattleClicked()
//		{
//			battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
//			_PGM.RequestInitiateBattle();
//		}

//		void OnDoNothingClicked()
//		{
//			battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
//			_PGM.RequestStayIdleInBattle();
//		}

//		void OnMeAsOpponentChoseIfToCancelBattle(bool cancel)
//		{
//			battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
//			_PGM.RequestChooseIfToCancelBattle(cancel);
//		}

//		void OnReadyClicked()
//		{
//			IsMyTurnInBattleAndCanPlayCards = false;
//			battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
//			UpdateInventoryInteractability();
//			_PGM.RequestReadyToBattle();
//		}

//		//void OnCloseClicked()
//		//{
//		//	//_PGM.RequestReadyToBattle();
//		//}


//		void OnEntityLongPressed(Base.Entity entity)
//		{
//			if (entity is Ore)
//				return;

//			if (inspectPanel.IsShownOrShowing)
//				return;

//			//if (!IsMyTurn)
//			//	return;

//			inspectPanel.Show(entity);
//			// OnInspectedEntityChanged will be called after
//		}
//		#endregion


//		#region callbacks from Board
//		void OnDungeonShifted()
//        {
//			UpdateInteractivityForCurrentPlayer(true);

//			_PGM.OnCurrentActionExecuted();
//        }
//		#endregion


//		#region callbacks from Player
//		void OnPlayerMoveToCellAnimationEnded(Cell cell)
//		{
//			//Debug.Log("PlayerManager.OnPlayerMoveToCellAnimationEnded " + cell.name);
//			CurrentPlayer.SetParentBlock(cell.ContainedBlock, SpawnManager.allEntitiesSpawnPositionDelta);

//			if (IsMyTurn)
//				_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);
//			else
//			{
//				HighlightReachableCells(); // for others
//				UpdateInventoryInteractability();
//			}

//			_PGM.OnCurrentActionExecuted();
//		}

//		void OnOtherPlayerMoveToCellAnimationEnded(Character character, Cell cell)
//		{
//			//Debug.Log("PlayerManager.OnPlayerMoveToCellAnimationEnded " + cell.name);
//			character.SetParentBlock(cell.ContainedBlock, SpawnManager.allEntitiesSpawnPositionDelta);

//			_PGM.DoAfterMasterReady(() =>
//			{
//				if (IsMyTurn)
//					RequestStartBattleWith(character);
//			});

//			_PGM.OnCurrentActionExecuted();
//		}

//		// Only called on our turn outside battle or on post battle when we won and need to do looting
//		void OnPlayerReadyToInteractWithCell() { OnPlayerReadyToInteractWithCell(false); }

//		void OnPlayerReadyToInteractWithCell(bool onlyToPickUpItems)
//		{
//			//if (IsMyTurn) // commented: this will only be called on our turn

//			var cell = WinningPlayerOfCurrentBattleOrCurrentPlayer.GetParentCell();
//			bool canPickUpItems = WinningPlayerOfCurrentBattleOrCurrentPlayer.Design.canPickUpItems;

//			if (!onlyToPickUpItems)
//			{
//				var enemies = WinningPlayerOfCurrentBattleOrCurrentPlayer.GetEnemiesInCell();
//				if (enemies.Count > 0) // Someone to fight
//				{
//					var otherChar = enemies[0];

//					RequestStartBattleWith(otherChar);
//					return;

//				}

//				// No one to fight


//				// If the cell is the Exit cell && the player in it has all the 3 escape items, the game will end, and the master will notify all clients. No additional looting will be made
//				// The winner can be either the current player, or (in some rare cases), the opponent of the current player, if they're in the same spot
//				if (DidPlayerEscape(WinningPlayerOfCurrentBattleOrCurrentPlayer))
//				{
//					UnHighlightLastReachableCells();

//					if (PhotonNetwork.isMasterClient)
//						_PGM.DispatchGameResultEvent(WinningPlayerOfCurrentBattleOrCurrentPlayer.ActorID);

//					return;
//				}
//			}

//			// Get the ore, if any; OnPlayerReadyToInterractWithCell will be called again once done
//			if (canPickUpItems && cell.ContainedBlock.ContainsOre())
//			{
//				UnHighlightLastReachableCells();
//				_PGM.RequestGetOre(_Board.GetCellIndex(cell));

//				return;
//			}

//			// Show context menu for picking up items
//			var items = cell.ContainedBlock.GetChildrenOfType<BaseItem>();
//			if (canPickUpItems && items.Count > 0)
//			{
//				bool waitingForUserToChoose = false;
//				Action onDoNothing = () =>
//				{
//					// We decide we're done looting => inform master
//					if (IsPostBattleAndIWon)
//					{
//						_PGM.RequestPostBattleWinnerDoneLooting();
//					}
//					else // our turn outside battle
//					{
//						//endTurnButton.interactable = true;
//						bool waitedForUserTochoose = waitingForUserToChoose;
//						if (waitedForUserTochoose)
//						{
//							UpdateInteractivityForCurrentPlayer(true);
//							//UpdateInventoryInteractability();
//							//HighlightReachableCells();
//							//totemsPanel.UpdateBuildOptions(CurrentPlayer);
//						}
//						//else
//						//	endTurnButton.interactable = true;
//					}
//				};

//				waitingForUserToChoose = HandleItemsInCurrentBlock(onDoNothing, items.ToArray());
//				if (waitingForUserToChoose)
//				{
//					UpdateInventoryInteractability(); // non interactable until decision
//					UnHighlightLastReachableCells(); // cannot move until decision
//					return;
//				}

//				// Either the player can't do anything with the current item, or he declined to pick it up
//				onDoNothing();
//			}
//			else
//			{
//				//Debug.Log("No items");
//				if (IsPostBattleAndIWon) // we won & no more items to get => inform master we're done
//				{
//					//Debug.Log("Will RequestPostBattleWinnerDoneLooting");
//					// TODO also let user choose if to loot or not
//					_PGM.RequestPostBattleWinnerDoneLooting();
//					return;
//				}
//			}


//			if (IsMyTurn)
//			{
//				endTurnButton.interactable = true;

//				//if (!IsPreIntraOrPostBattle) // commented: this method is not called pre- or intra- battle
//				if (!IsPostBattle)
//					totemsPanel.UpdateBuildOptions(CurrentPlayer);
//			}

//			UpdateInteractivityForCurrentPlayer(true);
//			//UpdateInventoryInteractability();
//			//RefreshReachableCells();
//		}
//		#endregion


//		#region callbacks related to Battle
//		// Only called on master
//		void DispatchBattleOutcome()
//		{
//			_PGM.Ev_MasterReadyForNewRequest -= DispatchBattleOutcome;

//			battlePanel.CalculateBattleOutcome();
//			_PGM.DispatchBattleResult(battlePanel.currentBattleStats);
//		}

//		void OnBattleResultsShown(BattleStats battleStats)
//		{
//			CurrentPlayer.Stats.UpdateStatsOnBattleEnded();
//			OpponentOfCurrentPlayer.Stats.UpdateStatsOnBattleEnded();

//			UpdateStatsUI();

//			// For debugging the serpent
//			//int firstResultPostBattle = battlePanel.SecondCharacter.Design == _Design.monsters.SERPENT ? 100 : 9;
//			//int secondResultPostBattle = 10;
//			//int firstATKPostBattle = 5;
//			//int secondATKPostBattle = 5;
			
//			int firstResultPostBattle = battleStats.firstPlayerScore;
//			int secondResultPostBattle = battleStats.secondPlayerScore;
//			int firstATKPostBattle = battleStats.firstPlayerScoreFromAtk;
//			int secondATKPostBattle = battleStats.secondPlayerScoreFromAtk;

//			bool serpentShown = false;

//			WinnerOfCurrentBattle = firstResultPostBattle < secondResultPostBattle ? battlePanel.SecondCharacter : (firstResultPostBattle > secondResultPostBattle ? battlePanel.FirstCharacter : null);

//			if (WinnerOfCurrentBattle)
//			{
//				var loserAsPlayer = (WinnerOfCurrentBattle == CurrentPlayer ? OpponentOfCurrentPlayer : CurrentPlayer) as Player;
//				if (loserAsPlayer)
//				{
//					int loserScore = Mathf.Min(firstResultPostBattle, secondResultPostBattle);
//					int winnerScore = Mathf.Max(firstResultPostBattle, secondResultPostBattle);

//					var serpentItemDesign = _Design.monsters.SERPENT.rewardItem;
//					var serpentAmount = serpentItemDesign.GetEffects(loserAsPlayer, WinnerOfCurrentBattle)[0].amount;
//					int winnerScoreAfterSerpent = winnerScore + serpentAmount;
//					if (winnerScoreAfterSerpent < 0)
//						winnerScoreAfterSerpent = 0;

//					// The serpent can appear, if it exist in the loser's inventory
//					if (winnerScoreAfterSerpent <= loserScore)
//					{
//						int serpentIndex = loserAsPlayer.Stats.GetIndexOfItemWithDesign(serpentItemDesign);
//						if (serpentIndex != -1)
//						{
//							if (loserAsPlayer == battlePanel.FirstCharacter)
//							{
//								secondATKPostBattle += serpentAmount;
//								secondResultPostBattle += serpentAmount;
//							}
//							else
//							{
//								firstATKPostBattle += serpentAmount;
//								firstResultPostBattle += serpentAmount;
//							}

//							// Choose the new winner or the tie
//							if (firstResultPostBattle == secondResultPostBattle)
//								WinnerOfCurrentBattle = null; // tie
//							else
//								WinnerOfCurrentBattle = loserAsPlayer; // the loser actually won in the end

//							// Visually show the serpent & override final scores
//							battlePanel.resultsPanel.OverrideResults(firstResultPostBattle, secondResultPostBattle);
//							battlePanel.resultsPanel.ShowSerpent();

//							// Discard the item
//							loserAsPlayer.Stats.ItemsInInventory[serpentIndex].Owner = null;

//							serpentShown = true;
//						}
//					}
//				}
//			}

//			battlePanel.SetBattleWinner(WinnerOfCurrentBattle);
//			battlePanel.resultsPanel.SetDetails(
//				firstATKPostBattle,
//				secondATKPostBattle, 
//				battleStats.firstPlayerScoreFromMagic, battlePanel.FirstCharacter.Design.magicType,
//				battleStats.secondPlayerScoreFromMagic, battlePanel.SecondCharacter.Design.magicType
//			);

//			// _PGM.OnCurrentActionExecuted will be called in OnBattlePanelHiddenAfterBattle 
//			Action closeAction = () => battlePanel.Close(battlePanel.autoHideDelay + (serpentShown ? _Design.general.battlePanelAdditionalHideDelayWhenSpider : 0f), () => OnBattlePanelHiddenAfterBattle(false));

//			if (!WinnerOfCurrentBattle) // first, show the tie animation, then close the panel
//				battlePanel.AnimateTie(closeAction, true);
//			else
//				closeAction();
//		}

//		void OnBattlePanelHiddenAfterBattle(bool battleCancelled)
//		{
//			//Debug.Log("Winning char: " + (winningChar == null ? "none" : winningChar.name));

//			//WinnerOfCurrentBattleAsPlayer = WinnerOfCurrentBattle as Player; // may be null, of course, if the monster won

//			if (WinnerOfCurrentBattle)
//			{
//				CurrentPlayer.Stats.NumDungeonPushesConsumed += CurrentPlayer.Stats.RemainingDungeonPushes; //consume all dungeon pushes
//				CurrentPlayer.Stats.NumMovesConsumed += CurrentPlayer.Stats.RemainingMoves; //consume all moves

//				LosingBlockOfCurrentBattle = LosingCharacterOfCurrentBattle.GetParentAsBlock();
//				// CurrentPlayer vs a Player
//				Player losingCharAsPlayer = LosingCharacterOfCurrentBattle as Player;

//				WinnerOfCurrentBattle.OnWonBattle(LosingCharacterOfCurrentBattle);

//				// The loosing char is a player and it was eliminated => the master will handle this
//				if (losingCharAsPlayer && !losingCharAsPlayer.Stats.HasSoul)
//				{
//					if (PhotonNetwork.isMasterClient)
//					{
//						_PGM.DoAfterMasterReady(() => _PGM.DispatchPlayerEliminatedEvent_Master(losingCharAsPlayer.ActorID));
//					}
//					//int eliminated = _SpawnManager.GetEliminatedPlayers().Count;
//					//int current = PhotonNetwork.room.PlayerCount;
//					//if (eliminated >= current)
//					//// Game will end => don't re-enter this method for item pick-up
//					//{

//					//}
//					//else
//					//// Game won't end => dispatch the elimination event & execute PostBattle after
//					//{

//					//}
//				}
//				else
//				{
//					PostBattleWinnerDoLooting();
//					return; // already calls OnCurrentActionExecuted
//				}

//				//if (WinnerOfCurrentBattleAsPlayer)
//				//{
//				//	WinnerOfCurrentBattleAsPlayer
//				//}
//				//	// Get XP + all ore + all items for which the winner has free slots
//				//	WinnerOfCurrentBattleAsPlayer.Stats.UpdateStatsOnEnemyKilled(losingChar);



//			}
//			// It was a tie or the battle was cancelled
//			else
//			{
//				if (IsMyTurn)
//				{
//					_PGM.DoAfterMasterReady(() => {
//						//if (battleCancelled)
//						//	_PGM.RequestEndMyTurn();
//						//else
//						//	UpdateInteractivityForCurrentPlayer(true);

//						// Update: actually, once a battle has started, the turn should end afterwards, regardless of the outcome
//						_PGM.RequestEndMyTurn();
//					});
//				}
//				//if (IsMyTurn)
//				//	_PGM.DoOnceAfterMasterReady(_PGM.RequestEndMyTurn);
//			}

//			_PGM.OnCurrentActionExecuted();
//		}

//		void PostBattleWinnerDoLooting()
//		{
//			var opponentAsPlayer = OpponentOfCurrentPlayer as Player;
//			bool pvp = opponentAsPlayer != null;
//			bool currentPlayerWon = WinnerOfCurrentBattle == CurrentPlayer;
//			var winningBlock = WinnerOfCurrentBattle.GetParentAsBlock();
//			// CurrentPlayer vs a Player
//			Player losingCharAsPlayer = LosingCharacterOfCurrentBattle as Player;

//			bool winnerCanPickUpItems = WinnerOfCurrentBattleAsPlayer && WinnerOfCurrentBattleAsPlayer.Design.canPickUpItems;

//			if (pvp)
//			{
//				// Drop the remaining items on the losing block spot; the winning player cannot pick up automatically them due to full inventory; will do it manually, if it's also on the same spot
//				// Cost items are also pickable without the need to buy them (only until the end of the turn or until the player picks them)
//				// BUT, if the winner cannot pick up items (animal class), they are discarded
//				while (losingCharAsPlayer.Stats.ItemsInInventory.Length > 0)
//				{
//					var item = losingCharAsPlayer.Stats.ItemsInInventory[0];
//					var itemAsCommonItem = item as CommonItem;
//					if (itemAsCommonItem && winnerCanPickUpItems)
//						itemAsCommonItem.IsFreeUntilNextPickUp = true;

//					item.Owner = winnerCanPickUpItems ? LosingBlockOfCurrentBattle : null;
//				}

//				bool winnerCanCollectMoreItemsBeforeTurnEnds = winnerCanPickUpItems && LosingBlockOfCurrentBattle == winningBlock && LosingBlockOfCurrentBattle.GetChildrenOfType<BaseItem>().Count > 0;

//				if (winnerCanCollectMoreItemsBeforeTurnEnds) // The turn doesn't end at the moment; only after the winner picks up / leaves loot
//				{
//					if (Me == WinnerOfCurrentBattleAsPlayer)
//						_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);
//				}
//				else // nothing to loot => end turn normally, if it's our turn
//				{
//					//Debug.Log("Will end my turn because there's no loot IsMyTurn="+ IsMyTurn);
//					if (IsMyTurn)
//						_PGM.DoAfterMasterReady(_PGM.RequestEndMyTurn);
//				}
//			}
//			else
//			{
//				// CurrentPlayer vs a Monster/Totem
//				var losingCharAsMonster = LosingCharacterOfCurrentBattle as Monster;
//				var losingCharAsTotem = LosingCharacterOfCurrentBattle as Totem;
//				if (currentPlayerWon)
//				{
//					if (losingCharAsMonster)
//					{
//						// Item left on spot of the winning char, so it can pick it, if it's a human
//						// If it's a beast, the item gets no owner (i.e. the monster will be respawned in the next turn)
//						losingCharAsMonster.RewardItem.Owner = winnerCanPickUpItems ? WinnerOfCurrentBattle.GetParentAsBlock() : null;
//					}
//					else if (losingCharAsTotem)
//					{
//						_SpawnManager.DestroyTotem(losingCharAsTotem);
//					}
						
//					if (IsMyTurn)
//						_PGM.DoAfterMasterReady(OnPlayerReadyToInteractWithCell);
//				}
//				// Monster/Totem defeated the current player
//				else
//				{
//					// Drop all items into the graveyard
//					CurrentPlayer.Stats.RemoveOwnershipFromAllItems();

//					// Drop the ore on the opponnent's spot (for example, in the ranged attack case, the ore would remain unprotected if would've been dropped on the loser's spot)
//					int oreDropped = CurrentPlayer.Stats.Ore;
//					//Debug.Log(oreDropped);
//					CurrentPlayer.Stats.Ore = 0;
//					var existingOreInBlock = winningBlock.PeekOre();
//					//Debug.Log("existingOreInBlock="+ existingOreInBlock);

//					// Increase the amount for the existing ore instance in block
//					if (existingOreInBlock)
//						existingOreInBlock.Amount += oreDropped;
//					// Spawn a new ore instance with the player's lost amount
//					else if (oreDropped > 0)
//							_SpawnManager.SpawnOre(winningBlock, oreDropped);

//					if (IsMyTurn)
//						_PGM.DoAfterMasterReady(_PGM.RequestEndMyTurn);
//				}
//			}

//			_PGM.OnCurrentActionExecuted();
//		}
//		#endregion


//		#region callbacks from InspectPanel
//		void OnInspectPanelShowing()
//		{
//			UpdateInteractivityForCurrentPlayer(false);
//			//UpdateInventoryInteractability();
//		}

//		void OnInspectPanelClosed()
//		{
//			UpdateInteractivityForCurrentPlayer(true);
//			//UpdateInventoryInteractability();
//		}

//		void OnInspectPanelEntityChanged(Entity entity)
//		{
//			var state = InspectCommandsPanel.ButtonsState.CLOSE_ONLY;
//			bool entityIsInDungeon = entity.GetParentAsBlock() != null;
//			if (entityIsInDungeon)
//			{
//				if (entity is Character && IsMyTurn)
//				{
//					if (Me.Stats[Stat.ATTRACT_ENEMY] > 0
//						&& entity != Me  // Can't attract self
//						&& !(entity is Totem)) // can't attract totems
//					{
//						state = InspectCommandsPanel.ButtonsState.CHOOSER; // choosing which enemy to attract
//					}
//					else if (
//						Me.Stats[Stat.RANGED_BATTLE] > 0
//						&& entity.RelativePosSimpleTo(Me) == RelativePositionSimple.NEIGHBOR_DIRECT
//						&& Me.GetParentCell().IsAdjacentAndNotObstructedByWall(entity.GetParentCell(), false)
//					){
//						state = InspectCommandsPanel.ButtonsState.RANGED_BATTLE;
//					}
//				}
//			}
//			else
//			{

//			}

//			var meBlock = Me.GetParentCell().ContainedBlock;
//			inspectPanel.commandsPanel.LootingActive =
//				IsMyTurn &&
//				entityIsInDungeon && Me.RelativePosSimpleTo(entity) == RelativePositionSimple.IDENTICAL &&
//				meBlock.GetChildrenOfType<BaseItem>().Count > meBlock.PeekOreAmount() // the number of pickable non-ore items is positive
//				&& Me.Design.canPickUpItems
//				&& Me.GetEnemiesInCell().Count == 0;
//			inspectPanel.commandsPanel.State = state;
//		}

//		void OnInspectPanelRangedBattleClicked()
//		{
//			Me.Stats.ChangeTemporaryStat(Stat.RANGED_BATTLE, -1, false);
//			RequestStartBattleWith(inspectPanel.InspectedEntity as Character);
//		}
//		void OnInspectPanelChooseClicked()
//		{
//			if (inspectPanel.IsClosing)
//				return;

//			if (Me.Stats[Stat.ATTRACT_ENEMY] > 0)
//			{
//				UpdateInteractivityForCurrentPlayer(false);
//				inspectPanel.Close(0f, () => {
//					var cell = inspectPanel.InspectedEntity.GetParentCell();
//					_PGM.RequestMoveOther(_Board.GetCellIndex(cell), cell.ContainedBlock.GetIndexOfChild(inspectPanel.InspectedEntity), _Board.GetCellIndex(Me.GetParentCell()));
//				});
//			}
//		}
//		void OnInspectPanelMiscClicked()
//		{
//		}
//		void OnInspectPanelLootClicked()
//		{
//			if (inspectPanel.IsClosing)
//				return;

//			//UpdateInteractivityForCurrentPlayer(false);
//			inspectPanel.Close(
//				0f, 
//				() => {
//					OnPlayerReadyToInteractWithCell(true);
//				},
//				false // prevent UpdateInteractivityForCurrentPlayer(true) from being called
//			);
//		}
//		#endregion





//		#region Cell reachability highlights
//		void HighlightReachableCells()
//		{
//			if (CurrentPlayer.Stats[Stat.GOD_MOVES] > 0)
//			{
//				// Commented: Will remove only after the player actually moved
//				//CurrentPlayer.Stats.RemoveEffectsForStat(Stat.GOD_MOVES);
//				foreach (var cell in _Board.AllCellsIncludingOuterCell)
//					cell.AllowedAreaHighlight.SetHighlight(1, !IsMyTurn);

//				_LastHighLightedCells.AddRange(_Board.AllCellsIncludingOuterCell);

//				return;
//			}

//			int remainingMoves = CurrentPlayer.Stats.RemainingMoves;
//			var playerCell = CurrentPlayer.GetParentCell();

//			//        var reachableCellsPerNumberOfMoves = 
//			//_BoardHelper.GetReachableCells(playerCell, CurrentPlayer, remainingMoves, 
//			//	Mathf.Min(remainingMoves, CurrentPlayer.Stats[Stat.MOVES_THROUGH_WALLS]), // can move through walls as long as <remainingMoves> is bigger than 0
//			//	Mathf.Min(remainingMoves, CurrentPlayer.Stats[Stat.DIAGONAL_MOVES]), // can move diagonally as long as <remainingMoves> is bigger than 0
//			//	Mathf.Min(remainingMoves, CurrentPlayer.Stats[Stat.BATTLE_EVADING]) // can move through enemies as long as <remainingMoves> is bigger than 0
//			//);

//			var pathfindingContext = PathfindingCalculateBestPaths();

//			//Debug.Log("Found " + pathfindingContext.mapDestCellToBestSolution.Count + " possible moves");
//			foreach (var kv in pathfindingContext.mapDestCellToBestSolution)
//			{
//				var cell = kv.Key;
//				var solution = kv.Value;

//				_LastHighLightedCells.Add(cell);
//				cell.AllowedAreaHighlight.SetHighlight(1, !IsMyTurn);
//				cell.SetDebugInfo(solution.ToString(playerCell, _Design.general.debug_CellPathTextSize));
//			}

//			//for (int i = 0; i < reachableCellsPerNumberOfMoves.Count; ++i)
//			//{
//			//    _LastHighLightedCells.AddRange(reachableCellsPerNumberOfMoves[i]);

//			//    foreach (var nearCell in reachableCellsPerNumberOfMoves[i])
//			//        nearCell.AllowedAreaHighlight.SetHighlight(i+1, !IsMyTurn);
//			//}
//		}

//		void UnHighlightLastReachableCells()
//		{
//			foreach (var lastHighlighted in _LastHighLightedCells)
//			{
//				lastHighlighted.SetDebugInfo("");
//				lastHighlighted.AllowedAreaHighlight.SetHighlight(0);
//			}
//			_LastHighLightedCells.Clear();
//		}

//		void RefreshReachableCells()
//		{
//			UnHighlightLastReachableCells();
//			HighlightReachableCells();
//		}
//		#endregion


//		#region Inventory handling
//		// Only called on our turn outside battle or on post battle when we won and need to do looting
//		// returns true if an action is needed from the player (i.e. pending dialog)
//		bool HandleItemsInCurrentBlock(Action negativeAction, params BaseItem[] items)
//		{
//			int numTreasuresPlayerHas = WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.Treasures;
//			int numItemsInBlock = items.Length;
//			CommonItem[] asCommonItems = new CommonItem[numItemsInBlock];

//			for (int i = 0; i < numItemsInBlock; ++i)
//				asCommonItems[i] = items[i] as CommonItem;

//			int itemsInInventory = WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.ItemsInInventory.Length;
//			//Debug.Log("itemsininventory=" + itemsInInventory);
//			//int itemsInInventoryMinusItemCost = itemsInInventory; // we must 

//			string[] affirmatives = new string[numItemsInBlock];
//			bool[] enabledOptions = new bool[numItemsInBlock + 1]; // +1 for the last "Leave" button
//			enabledOptions[numItemsInBlock] = true;

//			int freeSlots = _Design.general.inventoryCapacity - itemsInInventory;
//			// When checking if there are slots for the current item, we must treat the 
//			// treasures in inventory that we'll use to buy it as empty slots, since they will be used in exchange
//			int highestAffordableCost = 0;
//			bool affordable = false;
//			bool atLeastOneOptionAvailable = false;
//			bool atLeastOneItemAffordable = false;
//			for (int i = 0; i < numItemsInBlock; ++i)
//			{
//				string affirmative = "Take ";
//				string affirmativeAppendix = "";
//				var itemAsCommonItem = asCommonItems[i];
//				int cost = 0;
//				if (itemAsCommonItem != null)
//				{
//					cost = itemAsCommonItem.CurrentCost;
//					if (cost > 0)
//					{
//						affirmative = "Buy ";
//						affirmativeAppendix = " [cost " + cost + "]";

//						if (highestAffordableCost < cost)
//							highestAffordableCost = cost;
//					}
//				}

//				affordable = cost <= WinningPlayerOfCurrentBattleOrCurrentPlayer.Stats.Treasures;
//				atLeastOneItemAffordable = atLeastOneItemAffordable || affordable;
//				enabledOptions[i] = affordable && freeSlots + cost > 0;
//				atLeastOneOptionAvailable = atLeastOneOptionAvailable || enabledOptions[i];
//				affirmative += items[i].Design.name + affirmativeAppendix;
//				affirmatives[i] = affirmative;
//			}

//			//int freeSlotsIgnoringTreasuresForExchange = freeSlots + highestAffordableCost;
//			//bool inspectMeAndUpdateInventory = false;
//			if (IsPostBattleAndIWon)
//			{
//				// No items are affordable => done looting
//				if (!atLeastOneItemAffordable)
//				{
//					_PGM.RequestPostBattleWinnerDoneLooting();
//					return false;
//				}

//				// Inspect us & make cards discardable
//				// also make cards not playable during showing the context panel
//				//inspectMeAndUpdateInventory = true;
//				//InspectPlayerAndUpdateInventoryInteractivity(Me);

//				//// Do not bother showing a menu if nothing can be selected
//				//// But if post-battle and we won, the menu should still show, because the player can discard cards and this menu will be shown again
//				//if (!atLeastOneOptionAvailable)
//				//{
//				//	//// We decide we're done looting => inform master
//				//	//if (IsPostBattleAndIWon)
//				//	//{
//				//	//	_PGM.RequestPostBattleWinnerDoneLooting();
//				//	//}
//				//	return;
//				//}
//			}
//			else // our turn outside battle
//			{
//				if (!atLeastOneItemAffordable)
//					return false;

//				//inspectMeAndUpdateInventory = true;// make cards not playable during showing the context panel
//			}
//			//else
//			//{
//			//	if (IsMyTurnOutsideBattle)
//			//		UpdateInventoryInteractability();
//			//}

//			////if (IsMyTurnOutsideBattle)
//			////{
//			////	if (freeSlotsIgnoringTreasuresForExchange == 0)
//			////		// Nothing to do; the player may discard and a callback to this method will be done again
//			////		return;
//			////}

//			//// At this point, if the item costs, the player surely has enough treasures to buy it
//			//// There's also room for the item

//			var stringOptions = new List<string>(affirmatives);
//			string cancelOptionText = "Leave";
//			var monsterItem = Array.Find(items, i => i is MonsterItem);
//			if (monsterItem)
//				cancelOptionText += " & discard reward";

//			stringOptions.Add(cancelOptionText);

//			var showPos = WinningPlayerOfCurrentBattleOrCurrentPlayer.transform.position;
//			Action showInitialPanelAction = null;
//			showInitialPanelAction = () =>
//			contextPanel.Show(
//				showPos,
//				"Looting:",
//				stringOptions.ToArray(),
//				enabledOptions,
//				answer =>
//				{
//					// Cancelled; most probably, by a discard event, which will show again the dialog, with updated choices
//					if (answer == -1)
//						return;

//					if (answer == numItemsInBlock) // negative
//					{
//						if (!monsterItem)
//						{
//							if (negativeAction != null)
//								negativeAction();

//							return;
//						}

//						// If there's a monster item, a confirmation is needed, because it'll be discarded immediately
//						contextPanel.Show(
//							showPos,
//							"You're about to discard '" + monsterItem.Design.name + "'. Are you sure?",
//							new string[] { "Yes", "No" },
//							confirmationAnswer =>
//							{
//								// Cancelled; most probably, by a discard event, which will show again the dialog, with updated choices
//								if (answer == -1)
//									return;

//								if (confirmationAnswer == 0)
//								{
//									if (negativeAction != null)
//										negativeAction();
//								}
//								else
//								{
//									showInitialPanelAction(); // loop => from the beginning
//								}
//							}
//						);
//					}
//					else
//						// Request to buy/pick-up the item
//						_PGM.RequestPickUpItem(items[answer].GetParentAsBlock().GetIndexOfChild(items[answer]));
//				}
//			);

//			showInitialPanelAction();

//			//if (inspectMeAndUpdateInventory)

//			// Inspect us & possibly make cards discardable
//			// make cards not playable during showing the context panel
//			InspectPlayerInventory(Me, true);

//			return true;
//		}
//		#endregion


//		#region other methods
//		void UpdateShiftersInteractability()
//		{
//			bool shiftersEnabledForMe = IsMyTurn && CurrentPlayer.Stats.RemainingDungeonPushes > 0;
//			_Board.SetShiftingEnabled(shiftersEnabledForMe);
//		}

//		void UpdateInteractivityForCurrentPlayer(bool active)
//		{
//			endTurnButton.interactable = active && IsMyTurn;
//			if (active)
//			{
//				RefreshReachableCells();
//				UpdateShiftersInteractability();

//				if (IsMyTurn)
//					totemsPanel.UpdateBuildOptions(CurrentPlayer);
//				else
//					totemsPanel.SetAllButtonsInteractable(false);
//			}
//			else
//			{
//				UnHighlightLastReachableCells();
//				_Board.SetShiftingEnabled(false);
//				totemsPanel.SetAllButtonsInteractable(false);
//			}
//			UpdateInventoryInteractability(active);
//		}

//		void MoveToCell(Cell cell)
//		{
//			var path = new List<Cell>();
//			// God moves don't use path movement; they jump directly and only use simpe moves
//			if (CurrentPlayer.Stats[Stat.GOD_MOVES] > 0) 
//			{
//				CurrentPlayer.Stats.ChangeTemporaryStat(Stat.GOD_MOVES, -1, false);
//				++CurrentPlayer.Stats.NumMovesConsumed;
//			}
//			else
//			{
//				var pathfindingContext = PathfindingCalculateBestPaths();
//				if (pathfindingContext.mapDestCellToBestSolution.ContainsKey(cell)) // this will be false only when using the debugging shortcut for moving
//				{
//					var solution = pathfindingContext.mapDestCellToBestSolution[cell];
//					path = solution.path;
//					CurrentPlayer.Stats.SubtractMovementCosts(solution.cost);
//				}
//			}

//			//Debug.Log("PlayerManager.MoveToCell " + cell.name);
//			if (path.Count == 0) // direct movement
//				path.Add(cell);

//			//EnumEffects(CurrentPlayer); // debug
//			UnHighlightLastReachableCells();
//			UpdateStatsUI();
//			CurrentPlayer.Anim_MoveOnPath(path, () => OnPlayerMoveToCellAnimationEnded(cell));
//		}

//		void ShiftDungeon(Shifter shifter)
//		{
//			//// Preventing further selections
//			//shifter.gameObject.SetActive(false);
//			CurrentPlayer.Stats.NumDungeonPushesConsumed++;
//			UpdateInteractivityForCurrentPlayer(false);
//			UpdateStatsUI();

//			// Move to it and then place the cell
//			_Board.Anim_ShiftDungeon(shifter, OnDungeonShifted);
//		}

//		void ConsumeItem(Player playerToConsumeItem, BaseItem item)
//		{
//			var itemDesignAsEffectsItem = item.Design as IEffectsItem;
//			Character opponentOfPlayerThatConsumesTheItem;
//			if (IsIntraBattle)
//				opponentOfPlayerThatConsumesTheItem = playerToConsumeItem == CurrentPlayer ? OpponentOfCurrentPlayer : CurrentPlayer;
//			else
//				opponentOfPlayerThatConsumesTheItem = OpponentOfCurrentPlayer;

//			EffectManager.Instance.CastItem(playerToConsumeItem, itemDesignAsEffectsItem, opponentOfPlayerThatConsumesTheItem);
//			item.Owner = null;

//			bool updateInventoryInteractabilityAfter = true;
//			if (item.Design.cardType == CardType.BATTLE)
//			{
//				playerToConsumeItem.Stats.NumBattleCardsPlayed++;

//				battlePanel.OnCardPlayed(playerToConsumeItem, item.Design);
//			}
//			else if (item.Design.cardType == CardType.UTILITY)
//			{
//				var effects = itemDesignAsEffectsItem.GetEffects(playerToConsumeItem, null);
//				foreach (var eff in effects)
//				{
//					if (eff.type == Stat.MOVES
//						|| eff.type == Stat.MOVES_THROUGH_WALLS
//						|| eff.type == Stat.DIAGONAL_MOVES
//						|| eff.type == Stat.BATTLE_EVADING
//						|| eff.type == Stat.GOD_MOVES
//						|| eff.type == Stat.GHOST_MOVES)
//					{
//						RefreshReachableCells();
//						continue;
//					}

//					if (eff.type == Stat.DUNGEON_PUSH)
//					{
//						UpdateShiftersInteractability();
//						continue;
//					}

//					if (IsMyTurn)
//					{
//						if (eff.type == Stat.CARD_STEALING)
//						{
//							InspectPlayerInventory(_SpawnManager.GetPlayerInstanceAtRelativePosition(Me, +1), true);
//							inventoryPanel.SelectFirst(); // so the player can see he can choose one
//							UpdateStatsUI();
//							updateInventoryInteractabilityAfter = false;
//						}
//					}
//				}
//				//foreach (var eff in items)
//				//{
//				//	if (eff.type == Stat.ATTRACT_ENEMY)
//				//	{

//				//		break;
//				//	}
//				//}
//			}

//			//playerToConsumeItem.Stats.ConsumeItem(item);
//			//_SpawnManager.graveyard.Add(item);
//			// TODO show on UI

//			if (updateInventoryInteractabilityAfter)
//			{
//				// Update inventory interactivity if there's a battle and we were the ones that playerd the card OR it's our turn (case in which we just played a utility card)
//				if (IsMyTurnInBattle || IsMyTurnOutsideBattle)
//				{
//					//inventoryPanel.CardsInteractable = true;
//					UpdateInventoryInteractability();
//				}
//			}

//			UpdateStatsUI();
//		}

//		void RequestStartBattleWith(Character otherChar)
//		{
//			//EnumEffects(otherChar);
//			var cell = otherChar.GetParentCell();
//			_PGM.RequestSetupBattle(_Board.GetCellIndex(cell), cell.ContainedBlock.GetIndexOfChild(otherChar));
//			UnHighlightLastReachableCells();
//			if (inspectPanel.IsShownOrShowing)
//				inspectPanel.Close(0f, null);
//		}

//		void EnumEffects(Character character) { Debug.Log(character + " " + character.Stats.EffectsToString()); }

//		IEnumerator LoadMainMenuDelayed()
//		{
//			try { UpdateInteractivityForCurrentPlayer(false); } catch { }
//			float secsToWait = 2f;
//			Debug.Log("Loading main menu after " + secsToWait + " seconds - otherwise OnConnectedToPhoton is called (possibly PUN bug)");
//			yield return new WaitForSeconds(secsToWait);
//			SceneManager.LoadScene(0);
//		}

//		Pathfinding.PathfindingContext PathfindingCalculateBestPaths()
//		{
//			var pathfindingBudget = new Pathfinding.PathfindingCurrency(
//				CurrentPlayer.Stats.RemainingMoves,
//				CurrentPlayer.Stats[Stat.MOVES_THROUGH_WALLS],
//				CurrentPlayer.Stats[Stat.BATTLE_EVADING],
//				CurrentPlayer.Stats[Stat.DIAGONAL_MOVES],
//				CurrentPlayer.Stats[Stat.GHOST_MOVES]
//			);
//			var pathfindingContext = new Pathfinding.PathfindingContext(CurrentPlayer, pathfindingBudget);
//			_BoardHelper.ComputeBestPaths(pathfindingContext);

//			return pathfindingContext;
//		}

//		void InspectPlayerInventory(Player player, bool forceRedraw, bool? interactabilityOverride = null)
//		{
//			bool redraw = inventoryPanel.InspectedPlayer != player && (forceRedraw || (Array.FindAll(player.Stats.ItemsInInventory, i => i.Design.cardType != CardType.NOT_PLAYABLE).Length > 0 && inventoryPanel.InspectedPlayer != player));
//			Action inspectAndUpdateAction = () =>
//			{
//				inventoryPanel.InspectedPlayer = player;
//				UpdateInventoryInteractability(interactabilityOverride);
//			};

//			if (redraw)
//			{
//				inspectAndUpdateAction();
//				inventoryPanel.Show(false);

//				//inventoryPanel.Close(0f, () =>
//				//{
//				//	inspectAndUpdateAction();
//				//	inventoryPanel.Show();
//				//});
//			}
//			else
//				inspectAndUpdateAction();
//		}

//		void UpdateInventoryInteractability(bool? interactability = null)
//		{
//			bool cardsChooseable = false, canPlayCards = false, canDiscardCards = false, examinableAndCanExamine = false, cardsTradeable = false;
//			CardType[] playableCardTypes = null;

//			var myInventoryInspected = inventoryPanel.InspectedPlayer == Me;
//			bool canEndTurn = endTurnButton.interactable;
//			bool interactableByOverrideOrTrue = interactability ?? true;

//			bool cardsInteractable =
//					interactableByOverrideOrTrue
//					&& myInventoryInspected
//					&& !inspectPanel.IsShownOrShowing
//					&& (IsMyTurnInBattleAndCanPlayCards && Me.Stats.RemainingBattleCardsToPlay > 0 // playing battle cards during our battle-turn
//						|| IsMyTurnOutsideBattle  // playing utility cards during our turn & ready to do the next thing (end turn is only activated then)
//						|| IsPostBattleAndIWon); // discarding cards after a battle that we won
			
//			if (cardsInteractable)
//			{
//				if (Trading_MyDesiredCard == null)
//				{
//					canPlayCards = (canEndTurn || IsMyTurnInBattleAndCanPlayCards) // in battle can't end turn, but should be able to play cards
//						&& !IsPostBattleAndIWon && !contextPanel.IsShown;
//					playableCardTypes = new CardType[] { Me.Stats.InBattle ? CardType.BATTLE : CardType.UTILITY };
//					canDiscardCards = !IsPreIntraOrPostBattle || IsPostBattleAndIWon;
//				}
//				else
//					cardsChooseable = true;
//				//inventoryPanel.TreasuresDiscardable = 
//			}
//			else if (interactableByOverrideOrTrue)
//			{
//				if (myInventoryInspected)
//				{

//				}
//				else
//				{
//					if (canEndTurn)
//					{
//						if (IsMyTurnOutsideBattle && !inspectPanel.IsShownOrShowing)
//						{
//							cardsInteractable = true;
//							if (Me.Stats[Stat.CARD_STEALING] > 0 && Me.Stats.ItemsInInventory.Length < _Design.general.inventoryCapacity)
//							{
//								//Debug.Log("Stealing mode");
//								cardsChooseable = true;
//							}
//							else if (Me.Stats.ItemsInInventory.Length > 0)
//							{
//								cardsTradeable = true;
//							}
//						}
//					}
//				}
//			}
//			examinableAndCanExamine = !inspectPanel.IsShownOrShowing;
//			bool chooseableOrTradeable = cardsChooseable || cardsTradeable;
//			bool meInspectedAndNotChooseableOrTradeable = myInventoryInspected && !chooseableOrTradeable;

//			inventoryPanel.SetCardsCanPlay(canPlayCards, playableCardTypes);
//			if (meInspectedAndNotChooseableOrTradeable)
//				inventoryPanel.MakeAllPlayableIfCanPlayThem();
//			else
//				inventoryPanel.MakeAllNotPlayable();

//			inventoryPanel.CardsInteractable = cardsInteractable;
//			inventoryPanel.CardsChoosable = cardsChooseable;
//			inventoryPanel.CardsTradeable = cardsTradeable;
//			inventoryPanel.CanDiscardCards = canDiscardCards;
//			inventoryPanel.CardsDiscardable = meInspectedAndNotChooseableOrTradeable;
//			inventoryPanel.CardsExaminableAndCanExamine = examinableAndCanExamine;
//			inventoryPanel.PlayButtonText = IsIntraBattle ? "Equip" : "Activate";
//			inventoryPanel.RebuildLayoutAndUpdateCanvases();

//			inventoryPanel.Interactable = interactableByOverrideOrTrue;
//			if (!interactableByOverrideOrTrue && IsMyTurn)
//				inventoryPanel.Close(0f, null);
//		}

//		void UpdateStatsUI()
//        {
//			statsPanel.UpdateFromInspectedPlayer();
//        }

//		void SetBattleCommandPanelStateOnceAfterMasterReady(BattleCommandsPanel.ButtonsState state, Action optionalAction = null)
//		{
//			_PGM.DoAfterMasterReady(() => 
//			{
//				battlePanel.commandsPanel.State = state;
//				if (state == BattleCommandsPanel.ButtonsState.IN_BATTLE)
//					InspectPlayerInventory(Me, false);
//				else if (state == BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE)
//					battlePanel.commandsPanel.DoNothingText = IsMyTurn ? "Let me pass!" : "Allow to pass";

//				if (optionalAction != null)
//					optionalAction();
//			});
//		}

//		bool DidPlayerEscape(Player player)
//		{
//			// If the cell is the Exit cell, this means the game can end. (if all escape items are in inventory)
//			if (_SpawnManager.exitAreaInstance.ParentBlock == player.GetParentAsBlock())
//			{
//				if (player.Stats.GetInventoryItemsOfType<EscapeItem>().Length == 3) // game ended; the master will notify all clients
//				{
//					return true;
//				}
//			}
//			return false;
//		}

//		void OnDestroy()
//        {
//            leaveRoomButton.onClick.RemoveListener(OnLeaveRoomClicked);
//            endTurnButton.onClick.RemoveListener(OnEndTurnClicked);

//			Base.Entity.EntityLongPressed -= OnEntityLongPressed;

//			Debug.Log("Destr PM");
//		}
//		#endregion

//	}

//}
