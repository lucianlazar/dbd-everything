﻿using UnityEngine;
using System.Collections;
using Photon;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Core;
using UnityEngine.SceneManagement;
using System;
using Com.TheFallenGames.DBD.World;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.UI.Totems;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.UI;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.UI.Inspect;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Pathfinding;
using System.Linq;
using Com.TheFallenGames.DBD.Config.Effects;
using Com.TheFallenGames.DBD.Config.Characters;
using System.Threading;
using Com.TheFallenGames.DBD.Threading;
using UnityEngine.Serialization;

namespace Com.TheFallenGames.DBD.Core.PlayerManagers
{
	public class CPUPlayerManager : BasePlayerManager
	{
		[Header("CPU config")] [SerializeField] [Range(0f, 1f)]
		float _WinChanceToInitialteFight;
		[SerializeField] [Range(0f, 1f)]
		float _WinChanceWithCharThatKnowsDragonVoiceToInitialteFight;
		[SerializeField] [Tooltip("Used when compared to monster items, which are considered to have the same level as their original owners (the monsters)")]
		int _CommonItemsEstimatedLevel;
		[Range(0f, 1f)] [SerializeField]
		float _MinXP01ToPrioritizeMOVEOverATKTotems;
		[Range(0f, 1f)] [SerializeField]
		float _MaxXP01ToPrioritizeXPOverATKTotems;
		[Range(1f, 5f)] [SerializeField]
		float _ThinkingMinSeconds;
		[Range(1, 100)] [SerializeField]
		int _MaxTotalBacktrackDungeonPushesToReachPlayer;
		[Range(1, 100)] [SerializeField]
		int _MaxBacktrackDungeonPushesToReachPlayerBeroreOptimizing;
		[Range(1, 100)] [SerializeField] [Tooltip("Used only if the player has dungeon pushes by design. Otherwise, only the 'remaining' dungeon pushes will be used")]
		int _MaxTotalBacktrackDungeonPushesToReachOther;
		[Range(1, 100)] [SerializeField] [Tooltip("After this amount of backtracking depth is reached (number of shifters), if neither the cpu nor the target are not on the outer cell, only the rows/columns which the player is on and their direct neighbors will be considered for shifting (to optimize the thinking time)")]
		int _MaxBacktrackDungeonPushesToReachOtherBeforeOptimizing;
		[Range(1, 100)] [SerializeField]
		int _MaxBacktrackDungeonPushesToReachAnyInOptimizedModeOnly;

		public bool IsThinking { get { return _DungeonShiftingSolutionFinderInProgress != null; } }

		float XP01 { get { return Me.Stats.XP / PlayerStats.MAX_XP; } }
		bool HaveRangedBattleEffect { get { return Me.Stats[Stat.RANGED_BATTLE] > 0; } }
		bool _MovedRandomlyThisTurn;
		Block _BlockOnLastTurn;
		Cell _CellOnLastTurn;
		Cell _TargetCellOnLastTurn;
		DungeonShiftingSolutionFinderToMinimizePathCost _DungeonShiftingSolutionFinderInProgress;
		string _MyNickNameCached;


		protected override void Start()
		{
			base.Start();
			_MyNickNameCached = Me.NickName;
		}

		protected override void Update()
		{
			base.Update();

			if (Time.frameCount % 60 == 0)
				if (IsThinking)
					Me.SetDisplayName(_MyNickNameCached + _DungeonShiftingSolutionFinderInProgress.GetProgressInfo());
		}

		protected override void OnDestroy()
		{
			if (_DungeonShiftingSolutionFinderInProgress != null)
				_DungeonShiftingSolutionFinderInProgress.Cancel();

			base.OnDestroy();
		}

		#region GameplayManager events	
		public override void OnChooseFirstIfToBattle() { ChooseIfToBattleOpponent(_GM.OpponentOfCurrentPlayer); }

		public override void OnChooseIfToBattleAfterCurrentPlayerPrefersNotTo() { ChooseIfToBattleOpponent(_GM.CurrentPlayer); }

		public override void OnAnswerTradeProposal(Player receiver, BaseItem donorItem, BaseItem receiverTradedItem)
		{
			// CPU never accepts trades, because it can't be that smart to know which strategy his human opponent is brewing
			_GM.Submit_AnswerTradeProposal(Me, donorItem, receiver, receiverTradedItem, false); 
		}

		public override void OnChooseIfToCancelBattle()
		{
			bool cancel = !CheckIfStrongerThanReachableEnemy(_GM.CurrentPlayer);
			Debug.Log("CPU: OnChooseIfToCancelBattle: cancel=" + cancel);
			_GM.Submit_ChooseIfToCancelBattle(cancel);
		}

		public override void OnReadyToAct(DBDEvent completedEvent, object eventData = null) { ExecuteAfter(_ThinkingMinSeconds, () => OnReadyToActDelayed(completedEvent, eventData)); }

		public override void OnPostStealCard() { OnReadyToAct(DBDEvent.TRANSFER_ITEM); }

		public override void OnNewTurn()
		{
			base.OnNewTurn();

			_MovedRandomlyThisTurn = false;
			OnReadyToAct();
		}

		public override void OnTurnEnded()
		{
			base.OnTurnEnded();

			_BlockOnLastTurn = Me.GetParentAsBlock();
		}
		#endregion

		protected override void OnReadyToPlayBattleItems()
		{
			var myPotential = new CharacterBattlePotential(Me);
			var opponent = _GM.CurrentPlayer == Me ? _GM.OpponentOfCurrentPlayer : _GM.CurrentPlayer;
			myPotential.ComputeSelfOnlyWithOpponentSerpentIfAny(opponent);
			var opponentPotentialWithAllEffectsAndNoMoreCardsPlayedByUs = new CharacterBattlePotential(opponent);
			opponentPotentialWithAllEffectsAndNoMoreCardsPlayedByUs.ComputeWithOpponent(
				Me, 
				opponent is Player ? (opponent as Player).Stats.RemainingBattleCardsToPlay : 0,
				0 // we're interested in the opponent's potential in case we don't cast any more cards
			);
			float chance = myPotential.GetChanceToWinAgainst(opponentPotentialWithAllEffectsAndNoMoreCardsPlayedByUs);

			if (chance < GetMinRequiredWinChance(opponent))
			{
				var item = FindMostPowerfulBattleItemInInventory(Me, opponent);
				Debug.Log("CPU: OnReadyToPlayBattleItems: playing " + item.Design.name);
				_GM.Submit_UseItem(item.Design);
			}
			else
			{
				Debug.Log("CPU: OnReadyToPlayBattleItems: ready! ");
				_GM.Submit_ReadyToBattle();
			}
		}

		protected override bool Loot(BaseItem[] items)
		{
			var s = "";
			foreach (var item in items)
				s += item.Design.name + ", ";
			Debug.Log("CPU: Loot: " + s);

			var lootAction = GetLootAction(items, false);
			Debug.Log("CPU: lootAction= " + lootAction);

			if (lootAction == null)
				return false;

			lootAction.Submit(_GM);
			return true;
		}

		void ExecuteAfter(float seconds, Action action) { StartCoroutine(ExecuteAfterCoroutine(seconds, action)); }

		IEnumerator ExecuteAfterCoroutine(float seconds, Action action)
		{
			yield return new WaitForSeconds(seconds);
			if (action != null)
				action();
		}

		void DoOrApproachMainGoal(Board.Descriptor boardDescriptor, Goal mainGoal, Action<bool> onDone)
		{
			if (onDone == null)
				onDone = b => { };
			string prefix = "CPU: DoOrApproachToMainGoal: best target cell";
			if (mainGoal == null)
			{
				Debug.Log(prefix + " not found");
				onDone(false);
				return;
			}

			var myCellDesc = boardDescriptor.GetExistingCellDesc(Me.GetParentCell());
			if (mainGoal.cell == myCellDesc)
			{
				Debug.Log(prefix + " is already reached." + (mainGoal.enemyToAttackFromCell == null ? "" : "attacking enemy " + mainGoal.enemyToAttackFromCell.Design.name));
				if (mainGoal.enemyToAttackFromCell)
				{
					_GM.Submit_StartBattleWith(mainGoal.enemyToAttackFromCell);
					onDone(true);
					return;
				}
				onDone(false);
				return;
			}

			prefix += "(" + mainGoal.cell + ")";
			var pctx_real = _GM.PathfindingCalculateBestPathsUsingAsMuchSpecialMovesAsPossible(boardDescriptor);
			if (pctx_real.mapDestCellToBestSolution.ContainsKey(mainGoal.cell))
			{
				Debug.Log(prefix + " is reachable. Moving to it");
				//_GM.Submit_MoveToCell(mainGoal.cell.describedCell);
				MoveToCellWithCheckingForIntermediaryLoot(mainGoal.cell.describedCell, boardDescriptor, pctx_real);
				onDone(true);
				return;
			}
			
			// Approach the desired cell
			var reachableCellsIncludingMine = pctx_real.mapDestCellToBestSolution.ToList().ConvertAll(kv => kv.Key);
			reachableCellsIncludingMine.Add(myCellDesc);
			if (reachableCellsIncludingMine.Count > 1)
			{
				List<Cell.Descriptor> directCells = null, indirectCells = new List<Cell.Descriptor>();
				var closestCellResult = SortCellsByClosestAndGetClosest(boardDescriptor, reachableCellsIncludingMine, ref directCells, ref indirectCells, mainGoal.cell);
				var directAndIndirectReachableCellsSorted = new List<Cell.Descriptor>(directCells);
				directAndIndirectReachableCellsSorted.AddRange(indirectCells);
				//if (!result.isDirectlyReachable)
				//{
				//	SortCellsByClosestAndGetClosest(boardDescriptor, reachableCellsIncludingMine, ref directCells, ref indirectCells, mainGoal.cell);
				//	throw new UnityException("!result.isDirectlyReachable after providing only reachable cells ???");
				//}
				//Cell.Descriptor cellToMoveTo = result.cell;
				//List<Cell.Descriptor> listContainingCellTMoveTo;
				string howReachableString;
				if (closestCellResult.isDirectlyReachable)
				{
					howReachableString = "directly (from target) reachable";
					//listContainingCellTMoveTo = directCells;
				}
				else
				{
					howReachableString = "indirectly (from target) reachable";
					//listContainingCellTMoveTo = indirectCells;
				}
				prefix += " is not reachable. ";
				if (closestCellResult.cell == myCellDesc)
				{
					Debug.Log(prefix + "We're already on the " + howReachableString + " cell which is the closest to it: " + closestCellResult.cell + ". Staying in the current cell");
				}
				else
				{
					var prefix2 = prefix + "Found a " + howReachableString + " cell which is the closest to it: " + closestCellResult.cell;
					var myPotential = GetMyBattlePotential();
					var pfc_me = _GM.PathfindingCalculateBestPathsUsingAsMuchSpecialMovesAsPossible(boardDescriptor);
					for (int i = 0; i < directAndIndirectReachableCellsSorted.Count; ++i)
					{
						var cellToMoveTo = directAndIndirectReachableCellsSorted[i];
						if (cellToMoveTo == myCellDesc) // won't happen in the first iteration
						{
							Debug.Log(prefix2 + ". But it has an unbeatable enemy and no cell other than current is closer. Staying in the current cell");
							break;
						}

						var enemies = pctx_real.enemiesInfo.GetEnemies(cellToMoveTo.describedCell.ContainedBlock);
						Cell.Descriptor _, cellToAttackFromAtRange = null;
						if (enemies.Count == 0 
							|| enemies.TrueForAll(e => GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, e, false, out _) >= 1f) 
							||
								// If not all enemies are beatable non-ranged, maybe they beatable at range
								HaveRangedBattleEffect && 
								enemies.TrueForAll(e => GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, e, true, out cellToAttackFromAtRange) >= 1f)
							)
						{
							// If the enemy is only beatable at range, but that cell is not directly reachable by us, ignore it
							if (cellToAttackFromAtRange != null && !pfc_me.mapDestCellToBestSolution.ContainsKey(cellToAttackFromAtRange))
								continue;

							string prefix3 = (cellToMoveTo == closestCellResult.cell ? ". Moving to it" : (". But it has an unbeatable enemy. Found another one on the path:" + cellToMoveTo));
							string atRangeString = "";
							bool cellToStartRangedAttackIsAlreadyReached = false;
							if (cellToAttackFromAtRange != null)
							{
								cellToStartRangedAttackIsAlreadyReached = cellToAttackFromAtRange == myCellDesc;
								atRangeString = " at range, which can be attacked from cell " + cellToAttackFromAtRange + (cellToStartRangedAttackIsAlreadyReached ? " (already reached. so staring attack now)" : ", so moving to it");

								if (!cellToStartRangedAttackIsAlreadyReached)
									cellToMoveTo = cellToAttackFromAtRange;
							}
							var info = prefix2 + prefix3 + (enemies.Count == 0 ? "(it contains no enemy)." : " (it contains a beatable enemy" + atRangeString + ").");

							if (cellToStartRangedAttackIsAlreadyReached)
							{
								_GM.Submit_StartBattleWith(enemies[0]);
								onDone(true);
								Debug.Log(info + "Starting battle with it");
								return;
							}

							info += "Searching for a shifting solution before moving to potentially shorten the path. ";
							PlayDungeonShiftingItemOrShiftDungeon(boardDescriptor, mainGoal, done => {
								if (!done)
								{
									info += "No dungeon shift solution present. Moving to the cell directly";
									MoveToCellWithCheckingForIntermediaryLoot(cellToMoveTo.describedCell, boardDescriptor, pctx_real);
								}
								else
									info += "Dungeon shift solution found & executed. Moving will be done shortly, in OnReadyToAct";

								Debug.Log(info);
								onDone(true);
							});
							return;
						}
					}
				}
			}

			onDone(false);
		}

		void DoShiftingPathfinding(Board.Descriptor boardDescriptor, Goal mainGoal, Action<bool> onDone)
		{
			string prefix = "CPU: DoShiftingPathfinding: ";
			int remPushes = Me.Stats.RemainingDungeonPushes;
			if (remPushes == 0) // todo play items if needed
			{
				Debug.Log(prefix + "no pushes left");
				if (onDone != null)
					onDone(false);
				return;
			}

			bool hasDungeonPushesOnlyThisTurn = Me.Design.numDungeonPushes == 0;
			int backtrackingTotalPushes = _MaxTotalBacktrackDungeonPushesToReachOther, backtrackingPushesBeforeOptimizing = _MaxBacktrackDungeonPushesToReachOtherBeforeOptimizing;
			Cell targetCell;
			if (mainGoal.enemyToAttackFromCell)
			{
				targetCell = mainGoal.enemyToAttackFromCell.GetParentCell();
				if (mainGoal.enemyToAttackFromCell is Player)
				{
					backtrackingTotalPushes = _MaxTotalBacktrackDungeonPushesToReachPlayer ;
					backtrackingPushesBeforeOptimizing = _MaxBacktrackDungeonPushesToReachPlayerBeroreOptimizing;
				}
			}
			else
			{
				targetCell = mainGoal.cell.describedCell;
			}

			if (hasDungeonPushesOnlyThisTurn)
				backtrackingTotalPushes = remPushes;

			var shiftSolFinder = 
				new DungeonShiftingSolutionFinderToMinimizePathCost(
					targetCell, 
					Me, 
					_Board, 
					new PathfindingCurrency.Comparer_PrioritizeConsumingFewerSpecialMovesWhenResourcesUnknown(Me.Design),
					backtrackingTotalPushes,
					backtrackingPushesBeforeOptimizing,
					_MaxBacktrackDungeonPushesToReachAnyInOptimizedModeOnly
				);

			_DungeonShiftingSolutionFinderInProgress = shiftSolFinder;
			shiftSolFinder.Compute(shiftSol => {
				_DungeonShiftingSolutionFinderInProgress = null;
				if (shiftSol.shiftersPath.Count == 0)
				{
					prefix += "And no shifting configuration is better than the current one: backtrackingPushes=" + backtrackingTotalPushes
					+ ", backtrackingPushesBeforeOptimizing=" + backtrackingPushesBeforeOptimizing
					+ ", hasDungeonPushesOnlyThisTurn=" + hasDungeonPushesOnlyThisTurn;

					if (hasDungeonPushesOnlyThisTurn)
					{
						Debug.Log("hasDungeonPushesOnlyThisTurn=true; TODO solve target not reachable & no way of approaching it in 1 push (currently, only the eagle when surrounded by stronger enemies can be a victim of this)");
					}
					else
					{
						Debug.Log("TODO solve target not reachable & no way of approaching it in 1 push");
					}

					Debug.Log(prefix);
					if (onDone != null)
						onDone(false);
					return;
				}

				Debug.Log(prefix + " Found & executing shift solution: " + shiftSol.ToString(_Board));
				_GM.Submit_ShiftDungeon(_Board.AllShiftPoints[shiftSol.shiftersPath[0]]);
				if (onDone != null)
					onDone(true);
			});
		}
		
		void OnReadyToActDelayed(DBDEvent completedEvent, object eventData = null)
		{
			if (BuildTotemIfPossible())
				return;

			var bDesc = _Board.CreateDescriptor();			
			var myCell = Me.GetParentCell();
			var myCellDesc = bDesc.GetExistingCellDesc(myCell);
			Goal mainGoal = null;
			bool triedToGetMainGoal = false;

			if (Me.Design.canPickUpItems)
			{
				if (completedEvent == DBDEvent.PLAYER_USE_ITEM)
				{
					var usedItem = eventData as BaseItem;
					if (usedItem.Design.cardType == CardType.UTILITY)
						if (DoImmediateActionAfterUsedUtilityItem(usedItem, bDesc, ref mainGoal, ref triedToGetMainGoal))
							return;
				}

				if (PlayStealCardItemIfPresentAndWorthIt())
					return;

				if (PlayAttractEnemyItemIfPresentAndWorthIt(bDesc))
					return;

				// Consume all utility items that boost the number/ways of moves/moving
				if (PlayMoveBoosterItemIfPresentAndWorthIt())
					return;

				if (PlayBattleEvadingItemIfPresent())
					return;
			}

			_GM.UpdateInteractivityForCurrentPlayer(true);

			if (mainGoal == null && !triedToGetMainGoal)
				mainGoal = GetAndPrintMainGoal(bDesc);

			Action attendShortTermGoalAction = () => 
				AttendShortTermGoal(bDesc, mainGoal, done =>
				{
					if (done)
						return;

					// Nothing to be done
					Debug.Log("CPU: OnReadyToAct: Nothing more to do. Ending turn");
					_CellOnLastTurn = myCell;
					_TargetCellOnLastTurn = mainGoal == null ? null : mainGoal.cell.describedCell;
					_GM.Submit_EndMyTurn();
				});

			if (mainGoal == null)
				attendShortTermGoalAction();
			else
				DoOrApproachMainGoal(bDesc, mainGoal, done => {
					if (done)
						return;

					attendShortTermGoalAction();
				});
		}

		bool DoImmediateActionAfterUsedUtilityItem(BaseItem usedItem, Board.Descriptor bDesc, ref Goal mainGoal, ref bool triedToGetMainGoal)
		{
			var myCell = Me.GetParentCell();
			var myCellDesc = bDesc.GetExistingCellDesc(myCell);
			var usedItemDesignAsIEffectItem = usedItem.Design as IEffectsItem;
			if (usedItem.Design.cardType == CardType.UTILITY)
			{
				var effects = usedItemDesignAsIEffectItem.GetEffects(Me, null);
				if (effects.Exists(eff => eff.type == Stat.CARD_STEALING))
				{
					// Has just used a steal card => choose who to steal from and what
					var item = GetBestItemToSteal();
					if (!item)
						throw new UnityException("(???) GetBestItemToBeStolen=null after using " + usedItem.Design.name);

					Debug.Log("CPU: OnReadyToAct: stealing " + item.Design.name + " from " + item.OwnerAsPlayer.ShortNickNameWithColoredClass + " (using " + usedItem.Design.name + ")");
					_GM.Submit_TransferItem(item.OwnerAsPlayer, item.Design, Me, null);

					return true;
				}

				if (effects.Exists(eff => eff.type == Stat.ATTRACT_ENEMY))
				{
					// Has just used an "attract" card => choose who bring to us
					Cell.Descriptor _;
					var enemy = GetBestEnemyToAttract(bDesc, false, out _);
					if (!enemy)
						throw new UnityException("(???) GetBestEnemyToAttract=null after using " + usedItem.Design.name);

					Debug.Log("CPU: OnReadyToAct: attracting " + enemy.Design.name + " (using " + usedItem.Design.name + ")");
					_GM.Submit_MoveOther(enemy, myCell);

					return true;
				}

				if (effects.Exists(eff => eff.type == Stat.GOD_MOVES))
				{
					// Jump to the other player, if it can be defeated
					mainGoal = GetAndPrintMainGoal(bDesc);
					triedToGetMainGoal = true;
					if (mainGoal != null)
					{
						if (mainGoal.cell == myCellDesc) // no need to move => do other actions
						{ }
						else
						{
							Debug.Log("CPU: OnReadyToAct: using 1 GOD move " + " (using " + usedItem.Design.name + ")");
							_GM.Submit_MoveToCell(mainGoal.cell.describedCell);
							return true;
						}
					}
				}
			}

			return false;
		}

		void AttendShortTermGoal(Board.Descriptor bDesc, Goal mainGoal, Action<bool> onDone)
		{
			if (onDone == null)
				onDone = b => { };
			var myCell = Me.GetParentCell();
			var myCellDesc = bDesc.GetExistingCellDesc(myCell);
			var pfc = _GM.PathfindingCalculateBestPathsUsingAsMuchSpecialMovesAsPossible(bDesc, HaveRangedBattleEffect);
			var listReachableCells = pfc.mapDestCellToBestSolution.ToList();
			//Debug.Log("TODO replace GetMoveActionForBestReachableEnemyToBattle with mainGoal altogether");
			bool alreadyOnTheMainGoalCell = mainGoal != null && mainGoal.cell == myCellDesc;
			bool foundReachableCells = listReachableCells.Count > 0;
			bool hasPushesOrCanHavePushesThisTurn = Me.Stats.RemainingDungeonPushes > 0 || CheckStatBoosterItem(false, Stat.DUNGEON_PUSH);
			bool willHavePushesNextTurn = Me.Design.numDungeonPushes > 0;
			bool canMoveAroundForShortTermGoal = !willHavePushesNextTurn || !alreadyOnTheMainGoalCell && foundReachableCells && !hasPushesOrCanHavePushesThisTurn;
			if (canMoveAroundForShortTermGoal)
			{
				var moveActionForBestReachableEnemyToBattle = GetMoveActionForBestReachableEnemyToBattle(bDesc, pfc, HaveRangedBattleEffect);
				if (moveActionForBestReachableEnemyToBattle != null)
				{
					// Start the ranged battle directly, since no moving is necessary
					var targetCellToMoveTo = moveActionForBestReachableEnemyToBattle.GetCurrentTargetCellToMoveTo();
					if (myCellDesc == targetCellToMoveTo)
					{
						Debug.Log("CPU: OnReadyToAct: starting battle directly (we're already on the target cell: " + targetCellToMoveTo + ")");
						_GM.Submit_StartBattleWith(moveActionForBestReachableEnemyToBattle.enemy);
						onDone(true);
						return;
					}

					Debug.Log("CPU: OnReadyToAct: moveToEnemy: from " + myCell.ToShortString() + " to " + moveActionForBestReachableEnemyToBattle);
					MoveToCellWithCheckingForIntermediaryLoot(targetCellToMoveTo.describedCell, bDesc, pfc);
					onDone(true);
					return;
				}
				// No enemy to battle

				// Remove the reachable cells which have enemies
				listReachableCells.RemoveAll(kv => pfc.enemiesInfo.HasEnemy(kv.Key.block));

				// Remove the cells which are considered reachable only because we've added a fake RemainingMove while calculating the best paths to consider potential ranged battles
				if (HaveRangedBattleEffect)
				{
					int myRemainingMoves = Me.Stats.RemainingMoves;
					listReachableCells.RemoveAll(kv => kv.Value.cost.moves == myRemainingMoves + 1);
				}

				// Search for items to be picked up
				var listDesirableItems = new List<BaseItem>();
				if (Me.Design.canPickUpItems)
				{
					for (int i = 0; i < listReachableCells.Count; i++)
					{
						var cell = listReachableCells[i].Key;
						var lootAction = GetLootAction(cell.block.GetChildrenOfType<BaseItem>().ToArray(), true);
						if (lootAction != null)
							listDesirableItems.Add(lootAction.desiredItem);
					}
				}

				if (listDesirableItems.Count > 0)
				{
					// Estimate which item is most desirable and go there
					listDesirableItems.Sort((a, b) => GetItemValueFor(Me, b) - GetItemValueFor(Me, a));
					Debug.Log("CPU: OnReadyToAct: moveToItem " + listDesirableItems[0].Design.name + ", " + listDesirableItems[0].GetParentCell().ToShortString());
					MoveToCellWithCheckingForIntermediaryLoot(listDesirableItems[0].GetParentCell(), bDesc, pfc);
					onDone(true);
					return;
				}

				PlayDungeonShiftingItemOrShiftDungeon(bDesc, mainGoal, done =>
				{
					if (done)
					{
						onDone(true);
						return;
					}

					// Move randomly, but only sometimes
					if (Me.GetParentAsBlock() != _BlockOnLastTurn && !_MovedRandomlyThisTurn && listReachableCells.Count > 0 && UnityEngine.Random.Range(0, 3) == 0)
					{
						_MovedRandomlyThisTurn = true;
						var cellToMoveTo = listReachableCells[UnityEngine.Random.Range(0, listReachableCells.Count)].Key.describedCell;
						Debug.Log("CPU: OnReadyToAct: moveTo(randomly)" + ", " + cellToMoveTo.ToShortString());
						MoveToCellWithCheckingForIntermediaryLoot(cellToMoveTo, bDesc, pfc);
						onDone(true);
						return;
					}

					onDone(false);
				});

				return;
			}

			Debug.Log("CPU: OnReadyToAct: canMoveAroundForShortTermGoal=false. " +
						(foundReachableCells ? "Some cells are reachable. " : "No reachable cells. ") +
						(hasPushesOrCanHavePushesThisTurn ? "trying to shift dungeon first, if worth it" : "no dungeon pushes. Waiting to continue main goal next turn")
			); 

			PlayDungeonShiftingItemOrShiftDungeon(bDesc, mainGoal, onDone);
		}

		void MoveToCellWithCheckingForIntermediaryLoot(Cell cellToMoveTo, Board.Descriptor bDesc, PathfindingContext pfc)
		{
			var myCell = Me.GetParentCell();
			var myCellDesc = bDesc.GetExistingCellDesc(myCell);
			var cellToMoveToDesc = bDesc.GetExistingCellDesc(cellToMoveTo);
			var pathSol = pfc.mapDestCellToBestSolution[cellToMoveToDesc];
			foreach (var cell in pathSol.path)
			{
				if (cell == myCellDesc)
					continue;

				if (!pfc.enemiesInfo.HasEnemy(cell.block) && cell.block.GetChildrenOfType<BaseItem>().Count > 0)
				{
					// Some item(s) are on the path to the target. Stop first at them to potentially pick/buy them
					if (cell != cellToMoveToDesc)
						Debug.Log("CPU: OnReadyToAct: moveToIntermediaryCellToGetItems " + cell.describedCell.ToShortString());

					cellToMoveTo = cell.describedCell;
					break;
				}
			}
			if (cellToMoveTo == myCell)
				throw new UnityException("cellToMoveTo == myCell");

			_GM.Submit_MoveToCell(cellToMoveTo);
		}

		bool CheckIfStrongerThanReachableEnemy(Character opponent, bool showLogs = true)
		{
			float chance01 = GetWinChance01AgainstReachableEnemy_AdjustedForIndividualRequiredChance_Simple_FromCurrentCell(opponent);
			float minReq = GetMinRequiredWinChance(opponent);
			bool result = chance01 >= 1f;
			if (showLogs)
				Debug.Log("CPU: CheckIfStrongerThan: " + opponent + ", result= " + result + " ( winCh=" + minReq * chance01 + ", minReq= " + minReq + ")");
			return result;
		}

		void ChooseIfToBattleOpponent(Character opponent)
		{
			bool accepted = CheckIfStrongerThanReachableEnemy(opponent);
			Debug.Log("CPU: ChooseIfToBattleOpponent: " + opponent + ", accepted=" + accepted);
			if (accepted)
				_GM.Submit_InitiateBattle(Me);
			else
				_GM.Submit_StayIdleInBattle(Me);
		}

		int GetItemValueFor(Player forPlayer, BaseItem item)
		{
			var asMonsterItem = item as MonsterItem;
			if (asMonsterItem)
				return asMonsterItem.OriginalOwner.Design.level * 50;

			var asCommon = item as CommonItem;
			if (asCommon)
				return asCommon.Design.Cost * 100;

			int escapeItemValue = IsExitReachable() ? 100000 : 30;
			if (item is EscapeItem)
				return escapeItemValue;

			int treasureValue = 50 / Math.Max(1, forPlayer.Stats.Treasures); // the more you have, the less are they important
			if (item is Treasure)
				return treasureValue;

			var asOre = item as Ore;
			if (asOre)
				return asOre.Amount * 2;

			throw new UnityException("Can't find type of item: " + item + ", design = " + item.Design);
		}

		Goal GetAndPrintMainGoal(Board.Descriptor boardDescriptor) { var goal = GetMainGoal(boardDescriptor); Debug.Log("GetMainGoal:" + goal); return goal; }
		Goal GetMainGoal(Board.Descriptor boardDescriptor)
		{
			//// Debug
			//return new Goal() {
			//	goalType = GoalType.KILL_PLAYER,
			//	cell = Array.Find(boardDescriptor.allCellsIncludingOuter, c => c.describedCell.ContainedBlock.GetChildrenOfType<Player>().Exists(p => p.NickName.ToLower().Contains("unity"))),
			//	enemyToAttackFromCell = _GM.localHumanPlayerManager.Me
			//};

			//return Array.Find(boardDescriptor.allCellsIncludingOuter, c => c.describedCell.ContainedBlock.GetChildrenOfType<Player>().Exists(p => p.NickName.ToLower().Contains("unity")));
			//if (boardDescriptor.outerCell.describedCell.ContainedBlock.GetChildrenOfType<Player>().Count == 0 
			//	|| boardDescriptor.outerCell.describedCell.ContainedBlock.GetChildrenOfType<Player>().Exists(p => p.NickName.ToLower().Contains("unity")))
			//	return boardDescriptor.outerCell;
			bool allCellsReachable = Me.Stats[Stat.GOD_MOVES] > 0;

			Goal mainGoal = new Goal();
			bool atRange = Me.Stats[Stat.RANGED_BATTLE] > 0;

			bool triedToGetBestPlayerToBattleAndFoundNone = false;
			var escapeItems = Me.Stats.GetInventoryItemsOfType<EscapeItem>();
			bool hasAllEscapeItems = escapeItems.Length == _Design.items.escapeItems.GetAll().Length;
			var myPotential = GetMyBattlePotential();
			Cell.Descriptor _;
			Goal nextGoal = null;
			if (IsExitReachable())
			{
				if (hasAllEscapeItems)
				{
					// Having all escape items => escaping
					mainGoal.cell = boardDescriptor.GetExistingCellDesc(_SpawnManager.exitAreaInstance.ParentBlock.GetParentAsCell());
					return mainGoal.Set(GoalType.GET_ESCAPE_ITEM);
				}

				// Perma-kill some player, if possible
				// TODO take proximity into account when there'll be multiple other players
				if (Me.Stats.KnowsDragonsVoice)
				{
					if (mainGoal.enemyToAttackFromCell = GetBestPlayerToBattle(boardDescriptor, atRange, out mainGoal.cell, !allCellsReachable))
						return mainGoal.Set(GoalType.PERMAKILL_PLAYER);
					triedToGetBestPlayerToBattleAndFoundNone = true;
				}

				// Find other beat-able players who have escape items
				// TODO take proximity into account when there'll be multiple other players
				var otherPlayers = GetOtherPlayers();
				foreach (var otherPlayer in otherPlayers)
				{
					if (otherPlayer.Stats.GetInventoryItemsOfType<EscapeItem>().Length == 0)
						continue;

					if (GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, otherPlayer, atRange, out mainGoal.cell) >= 1f)
					{
						mainGoal.enemyToAttackFromCell = otherPlayer;
						return mainGoal.Set(GoalType.KILL_PLAYER, new Goal(GoalType.GET_ESCAPE_ITEM));
					}
				}

				// Find other escape items
				var escapeItem = GetClosestEscapeItemToPickNextIfNoEnemyOrWeakerEnemy(boardDescriptor, myPotential);
				if (escapeItem)
				{
					mainGoal.cell = boardDescriptor.GetExistingCellDesc(escapeItem.GetParentCell());
					return mainGoal.Set(GoalType.GET_ESCAPE_ITEM);
				}
			}
			else
			{
				// Kill the dragon, if possible
				if (mainGoal.enemyToAttackFromCell = GetDragonIfShouldBattleIt(boardDescriptor, atRange, out mainGoal.cell))
					return mainGoal.Set(GoalType.KILL_DRAGON);
				//if (hasAllEscapeItems)
				//{
				//}

				// Perma-kill some player, if possible
				// TODO take proximity into account when there'll be multiple other players
				if (Me.Stats.KnowsDragonsVoice)
				{
					if (mainGoal.enemyToAttackFromCell = GetBestPlayerToBattle(boardDescriptor, atRange, out mainGoal.cell, !allCellsReachable))
						return mainGoal.Set(GoalType.PERMAKILL_PLAYER);
					triedToGetBestPlayerToBattleAndFoundNone = true;
				}

				// If not strong enough to battle the dragon & reached max xp, search for common items
				var getCommonItemGoal = new Goal(GoalType.GET_COMMON_ITEM, new Goal(GoalType.KILL_DRAGON));
				if (XP01 == 1f)
				{
					var comm = GetBestCommonItemToPickNextIfNoEnemyOrWeakerEnemy(boardDescriptor, myPotential, false, false);
					if (comm && comm.Design.Cost <= Me.Stats.Treasures) // it may not be affortable, thus the check
					{
						mainGoal.cell = boardDescriptor.GetExistingCellDesc(comm.GetParentCell());
						return mainGoal.SetTypeAndNextGoalFrom(getCommonItemGoal);
					}

					var treas = GetClosestTreasureToPickNextIfNoEnemyOrWeakerEnemy(boardDescriptor, myPotential);
					if (treas)
					{
						mainGoal.cell = boardDescriptor.GetExistingCellDesc(treas.GetParentCell());
						return mainGoal.Set(GoalType.GET_TREASURE_ITEM, getCommonItemGoal);
					}
				}
				else
				{
					nextGoal = new Goal(GoalType.REACH_MAX_XP, getCommonItemGoal);
				}
			}

			if (!triedToGetBestPlayerToBattleAndFoundNone)
				if (mainGoal.enemyToAttackFromCell = GetBestPlayerToBattle(boardDescriptor, atRange, out mainGoal.cell, !allCellsReachable))
					return mainGoal.Set(GoalType.KILL_PLAYER, nextGoal);

			if (!allCellsReachable && !IsExitReachable())
				if (mainGoal.enemyToAttackFromCell = GetBestTotemToBattle(boardDescriptor, atRange, out mainGoal.cell, !allCellsReachable))
					return mainGoal.Set(GoalType.KILL_TOTEM, nextGoal);

			if (mainGoal.enemyToAttackFromCell = GetBestMonsterExceptDragonToBattle(boardDescriptor, atRange, out mainGoal.cell, !allCellsReachable))
				return mainGoal.Set(GoalType.KILL_REGULAR_MONSTER, nextGoal);

			nextGoal = new Goal(GoalType.KILL_PLAYER, nextGoal);
			var commonItem = GetBestCommonItemToPickNextIfNoEnemyOrWeakerEnemy(boardDescriptor, GetMyBattlePotential(), !allCellsReachable, true);
			if (commonItem)
			{
				mainGoal.cell = boardDescriptor.GetExistingCellDesc(commonItem.GetParentCell());
				return mainGoal.Set(GoalType.GET_COMMON_ITEM, nextGoal);
			}

			var treasure = GetClosestTreasureToPickNextIfNoEnemyOrWeakerEnemy(boardDescriptor, GetMyBattlePotential());
			if (treasure)
			{
				mainGoal.cell = boardDescriptor.GetExistingCellDesc(treasure.GetParentCell());
				return mainGoal.Set(GoalType.GET_TREASURE_ITEM, new Goal(GoalType.GET_COMMON_ITEM, nextGoal));
			}

			return null;
		}

		ClosestCellResult GetClosestCell(Board.Descriptor boardDescriptor, Cell[] cells, Cell.Descriptor originOverride = null)
		{ return GetClosestCell(boardDescriptor, Array.ConvertAll(cells, c => boardDescriptor.GetExistingCellDesc(c)), originOverride); }
		ClosestCellResult GetClosestCell(Board.Descriptor boardDescriptor, List<Cell> cells, Cell.Descriptor originOverride = null)
		{ return GetClosestCell(boardDescriptor, cells.ConvertAll(c => boardDescriptor.GetExistingCellDesc(c)), originOverride); }
		ClosestCellResult GetClosestCell(Board.Descriptor boardDescriptor, Cell.Descriptor[] cells, Cell.Descriptor originOverride = null)
		{ return GetClosestCell(boardDescriptor, new List<Cell.Descriptor>(cells), originOverride); }
		ClosestCellResult GetClosestCell(Board.Descriptor boardDescriptor, List<Cell.Descriptor> cells, Cell.Descriptor originOverride = null)
		{
			List<Cell.Descriptor> _ = null, __ = null;
			return SortCellsByClosestAndGetClosest(boardDescriptor, cells, ref _, ref __, originOverride);
		}

		/// <summary>
		/// if <indirectlyReachable> is null, it'll only be properly sorted if there'll be no directly reachable cells. both lists will be initialized, regardless of this
		/// </summary>
		ClosestCellResult SortCellsByClosestAndGetClosest(
			Board.Descriptor boardDescriptor,
			List<Cell.Descriptor> cells,
			ref List<Cell.Descriptor> directlyReachable,
			ref List<Cell.Descriptor> indirectlyReachable,
			Cell.Descriptor originOverride = null
		)
		{ return Utils.SortCellsByClosestAndGetClosest(Me, boardDescriptor, cells, ref directlyReachable, ref indirectlyReachable, originOverride); }

		EscapeItem GetClosestEscapeItemToPickNextIfNoEnemyOrWeakerEnemy(Board.Descriptor boardDescriptor, CharacterBattlePotential myPotential)
		{
			var all = GetExitingItemsToPickNextIfNoEnemyOrWeakerEnemyFrom(boardDescriptor, _SpawnManager.escapeItemInstances, myPotential);
			if (all.Count > 0)
				return GetClosestItem(all, boardDescriptor) as EscapeItem;

			return null;
		}

		CommonItem GetBestCommonItemToPickNextIfNoEnemyOrWeakerEnemy(
			Board.Descriptor boardDescriptor, 
			CharacterBattlePotential myPotential, 
			bool favorProximityMoreThanCost,
			bool affordableOnly)
		{
			var all = _SpawnManager.GetCommonItemsSortedByDesignCostDesc();
			if (all.Count > 0)
			{
				var commonItems = GetExitingItemsToPickNextIfNoEnemyOrWeakerEnemyFrom(boardDescriptor, all.ToArray(), myPotential).ConvertAll(i => i as CommonItem);
				if (affordableOnly)
				{
					for (int i = 0; i < commonItems.Count;)
					{
						var c = commonItems[i];
						if (Me.Stats.Treasures >= c.Design.Cost)
						{
							++i;
							continue;
						}
						commonItems.RemoveAt(i);
					}
				}

				if (commonItems.Count > 0)
				{
					if (!favorProximityMoreThanCost) // keep the first one and the ones with the same cost as it
						commonItems.RemoveAll(c => c.Design.Cost != commonItems[0].Design.Cost);

					return GetClosestItem(commonItems, boardDescriptor) as CommonItem;
				}
			}

			return null;
		}
		Treasure GetClosestTreasureToPickNextIfNoEnemyOrWeakerEnemy(Board.Descriptor boardDescriptor, CharacterBattlePotential myPotential)
		{
			var all = GetExitingItemsToPickNextIfNoEnemyOrWeakerEnemyFrom(boardDescriptor, _SpawnManager.treasureInstances, myPotential);
			if (all.Count > 0)
				return GetClosestItem(all, boardDescriptor) as Treasure;

			return null;
		}

		List<BaseItem> GetExitingItemsToPickNextIfNoEnemyOrWeakerEnemyFrom(
			Board.Descriptor boardDescriptor, 
			BaseItem[] items, 
			CharacterBattlePotential myPotential
		){
			List<BaseItem> result = new List<BaseItem>();
			// Find other items on the ground (and with weaker enemies, if any)
			foreach (var item in items)
			{
				if (item.OwnerAsBlock)
				{
					var enemies = Me.GetEnemiesInCell(item.GetParentCell());
					Cell.Descriptor _;
					if (enemies.Count == 0
						||
						enemies.TrueForAll(e =>
							GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, e, false /*the item is on the enemy's spot*/, out _) >= 1f)
						)
						result.Add(item);
				}
			}

			return result;
		}

		BaseItem GetClosestItem<T>(List<T> all, Board.Descriptor boardDescriptor) where T : BaseItem
		{
			var allCells = all.ConvertAll(i => boardDescriptor.GetExistingCellDesc(i.GetParentCell()));
			var closest = GetClosestCell(boardDescriptor, allCells);

			return all[allCells.IndexOf(closest.cell)];
		}

		LootAction GetLootAction(BaseItem[] items, bool includeOreInCellIfNoItemCan)
		{
			bool maxXPReached = XP01 == 1f;
			int numItems = items.Length;
			//int itemsInInventory = Me.Stats.ItemsInInventory.Length;
			int freeSlots = Me.Stats.FreeInventorySlots;
			int numMyTreasures = Me.Stats.Treasures;


			// ### Buy everything (as it's better than having the treasure sitting there, useless)
			var itemsList = new List<BaseItem>(items);
			var commonItems = new List<CommonItem>();
			var monsterItems = new List<MonsterItem>();
			var escapeItems = new List<EscapeItem>();
			var treasures = new List<Treasure>();
			foreach (var item in itemsList)
			{
				var asCommonItem = item as CommonItem;
				MonsterItem asMonsterItem;
				EscapeItem asEscapeItem;
				Treasure asTreasure;
				if (asCommonItem)
					commonItems.Add(asCommonItem);
				else if ((asMonsterItem = item as MonsterItem))
					monsterItems.Add(asMonsterItem);
				else if ((asEscapeItem = item as EscapeItem))
					escapeItems.Add(asEscapeItem);
				else if ((asTreasure = item as Treasure))
					treasures.Add(asTreasure);
			}
			var mainGoal = GetAndPrintMainGoal(new Board.Descriptor(_Board));

			// ### If it's late game, pick up all escape items, discarding anything (the weakest of all available, actually) just to make place
			//if (maxXPReached || IsExitReachable())
			if (mainGoal.goalType == GoalType.GET_ESCAPE_ITEM)
			{
				if (escapeItems.Count > 0)
				{
					if (freeSlots > 0)
						return new LootAction(escapeItems[0]);

					// Make room, no matter what
					var weakest = FindWeakestItemInInventory(int.MaxValue, false);
					if (!weakest)
					{
						string s = "";
						foreach (var item in Me.Stats.ItemsInInventory)
							s += item.Design.name + ", ";
						throw new UnityException("Can't find weakest item to discard?? Available: " + s);
					}

					//// When considering discarding for escape items just because max xp was reached, then only discard utility items
					//// If exit is revealed, escape items are the most precious
					//if (!maxXPReached || weakest.Design.cardType != CardType.UTILITY)
					return new LootAction(escapeItems[0], weakest);
				}
			}


			// ### Buy as much Common items as possible, discarding estimatively weaker items if needed
			if (commonItems.Count > 0)
			{
				if (commonItems.Count > 1)
					commonItems.Sort((a, b) => (b.CurrentCost - a.CurrentCost));
				// Buying the most expensive item that we can afford
				foreach (var item in commonItems)
				{
					// Temporarily free items can only be picked up if there are free slots
					// Temporarily free items will be seen at the end of the list, since it's sorted by curCost desc, so we set preserveTreasures=false
					if (item.CurrentCost == 0 && freeSlots == 0)
					{
						var weakest = FindWeakestItemInInventory(item, false);
						if (weakest)
							return new LootAction(item, weakest);
					}

					if (item.CurrentCost <= numMyTreasures)
						return new LootAction(item);

					itemsList.Remove(item);
				}
			}
			// All affordable items were bought at this point

			if (mainGoal.goalType == GoalType.GET_TREASURE_ITEM)
			{
				if (treasures.Count > 0)
				{
					if (freeSlots > 0)
						return new LootAction(treasures[0]);

					// Make room, no matter what
					var weakest = FindWeakestItemInInventory(int.MaxValue, true);
					if (!weakest)
					{
						string s = "";
						foreach (var item in Me.Stats.ItemsInInventory)
							s += item.Design.name + ", ";
						throw new UnityException("Can't find weakest item to discard?? Available: " + s);
					}

					//// When considering discarding for treasures only discard utility items
					//if (weakest.Design.cardType != CardType.UTILITY)
					return new LootAction(treasures[0], weakest);
				}
			}

			// ### Get as much Monster items as possible, discarding estimatively weaker items if needed
			if (monsterItems.Count > 0)
			{
				if (monsterItems.Count > 1)
					monsterItems.Sort((a, b) => (b.OriginalOwner.Design.level - a.OriginalOwner.Design.level));

				// Picking MonsterItems whose owners have the biggest level
				var monsterItemToPotentiallyPick = monsterItems[0];

				if (freeSlots > 0)
					return new LootAction(monsterItemToPotentiallyPick);

				var weakestItem = FindWeakestItemInInventory(monsterItemToPotentiallyPick, false);
				if (weakestItem)
					return new LootAction(monsterItemToPotentiallyPick, weakestItem);

				foreach (var i in monsterItems)
					itemsList.Remove(i);
			}

			var ore = itemsList.Find(i => i is Ore);
			if (ore)
				itemsList.Remove(ore);

			if (itemsList.Count > 0)
			{
				var itemToPick = itemsList[UnityEngine.Random.Range(0, itemsList.Count)];

				if (freeSlots > 0)
					return new LootAction(itemToPick);
			}

			if (includeOreInCellIfNoItemCan && ore)
				return new LootAction(ore);

			return null;
		}

		MoveToReachableEnemyAction GetMoveActionForBestReachableEnemyToBattle(Board.Descriptor boardDescriptor, PathfindingContext pfc, bool hasRangedBattleEffect)
		{
			// Search for enemies to beat
			var myPotential = new CharacterBattlePotential(Me);
			var moveActionsForEnemiesWithHighChanceToBeDefeated = new List<MoveToReachableEnemyAction>();
			int myRemainingMoves = Me.Stats.RemainingMoves;
			var myCell = Me.GetParentCell();
			var myCellDesc = boardDescriptor.GetExistingCellDesc(myCell);
			var mapEnemyToWinChance = new Dictionary<Character, float>();
			string s = "";
			foreach (var kv in pfc.mapDestCellToBestSolution)
			{
				var cell = kv.Key;
				var enemies = pfc.enemiesInfo.GetEnemies(cell.block);
				s += cell.describedCell.ToShortString() + ", ";

				// Ignore multiple enemies in the same cell for now
				// Also ignore the cell if there's no enemy (of course)
				if (enemies.Count != 1)
					continue;

				if (kv.Key == myCellDesc) // ignore the ones in own cell
					continue;

				var enemy = enemies[0];
				Cell.Descriptor myOverriddenCellOnBattle = null;
				if (hasRangedBattleEffect) // the enemy may only be reached for a ranged-battle
				{
					var pathSolution = kv.Value;
					var path = pathSolution.path;
					int pathLen = path.Count;
					var prevCellOnPath = path[pathLen - 2];
					bool cellIsDirectlyReachableFromPreviousCellOnPath = prevCellOnPath.IsAdjacentAndNotObstructedByWall(cell, false);
					// Cn't start range attacks if diagonally or obstructed by a wall or from a cell where there are enemies
					bool enemyCanBeAttackedAtRange = cellIsDirectlyReachableFromPreviousCellOnPath && !pfc.enemiesInfo.HasEnemy(prevCellOnPath.block);
					bool enemyApproachableOnlyForRangedBattleAndIfDirectlyReachable = pathSolution.cost.moves == myRemainingMoves + 1;
					if (enemyApproachableOnlyForRangedBattleAndIfDirectlyReachable)
					{
						if (!enemyCanBeAttackedAtRange) // can't attack at range => ignore
							continue;

						myOverriddenCellOnBattle = prevCellOnPath;
					}
					else // the enemy can be reached physically, if needed (in case a ranged effect is not possible)
					{
						if (enemyCanBeAttackedAtRange)
						{
							myOverriddenCellOnBattle = prevCellOnPath;
						}
						else
						{
							// Reach the enemy physically, as usual
						}
					}
				}
				
				float chance01 = myPotential.ComputeWithOpponentAndGetWinChance(enemy, myOverriddenCellOnBattle) / GetMinRequiredWinChance(enemy);

				if (chance01 >= 1f)
				{
					s += "[" + enemy.GetParentCell().ToShortString() + ", ovr=" + (myOverriddenCellOnBattle == null ? "null" : myOverriddenCellOnBattle.describedCell.ToShortString()) + "], ";

					mapEnemyToWinChance[enemy] = chance01;
					moveActionsForEnemiesWithHighChanceToBeDefeated.Add(
						new MoveToReachableEnemyAction(
							boardDescriptor, 
							enemy, 
							myOverriddenCellOnBattle
							//myOverriddenCellOnBattle == myCellDesc ? null : myOverriddenCellOnBattle // no need to move. the battle can start directly
						)
					);
				}
			}
			//Debug.Log("Reachable(bg="+ pfc.budget + "): " + s);

			if (moveActionsForEnemiesWithHighChanceToBeDefeated.Count > 0)
			{
				moveActionsForEnemiesWithHighChanceToBeDefeated.Sort((c1, c2) =>
				{
					//return pfc.mapDestCellToBestSolution[boardDescriptor.GetExistingCellDesc(c1.enemy.GetParentCell())].cost
					//		.CompareTo(
					//		pfc.mapDestCellToBestSolution[boardDescriptor.GetExistingCellDesc(c2.enemy.GetParentCell())].cost);

					return (int)((mapEnemyToWinChance[c2.enemy] - mapEnemyToWinChance[c1.enemy]) * 10000);
				});

				int indexOfEnemyToBattle = moveActionsForEnemiesWithHighChanceToBeDefeated.Count - 1; // the one with the lowest chance to be defaeted, as it usually yields bigger rewards
				if (
					Me.Stats.GetInventoryItemsOfType<EscapeItem>().Length >= 2 // if having 2 or more escape items, it's not worth it to risk
					|| Me.Stats.NumBattleItemsInInventory > 0) // if having one or more battle items, it's not worth it to lose them
				{
					indexOfEnemyToBattle = 0; // .. so take the one with the highest chance;
				}

				return moveActionsForEnemiesWithHighChanceToBeDefeated[indexOfEnemyToBattle];
			}
			return null;
		}

		BaseItem FindWeakestItemInInventory(MonsterItem givenMonsterItem, bool preservetreasures)
		{
			int givenItemLevel = givenMonsterItem.OriginalOwner.Design.level;
			return FindWeakestItemInInventory(givenItemLevel, preservetreasures);
		}

		BaseItem FindWeakestItemInInventory(CommonItem givenCommonItem, bool preservetreasures)
		{
			int givenItemLevel = _CommonItemsEstimatedLevel;
			return FindWeakestItemInInventory(givenItemLevel, preservetreasures);
		}

		BaseItem FindWeakestItemInInventory(int givenItemLevel, bool preservetreasures)
		{
			var itemsPotentiallyWeaker = new List<BaseItem>(Me.Stats.ItemsInInventory);
			if (itemsPotentiallyWeaker.Count == 0)
				return null;

			if (!preservetreasures)
			{
				// Treasures are always(ish) weaker than common items
				var treasure = itemsPotentiallyWeaker.Find(i => i is Treasure);
				if (treasure)
					return treasure;
			}

			// Preserve escape items if exit point reachable (AKA 'late game')
			var escapeItemsAreStronger = IsExitReachable();
			if (escapeItemsAreStronger)
				itemsPotentiallyWeaker.RemoveAll(i => i is EscapeItem);
			if (itemsPotentiallyWeaker.Count == 0)
				return null;

			// Preserve MonsterItems with higher level
			itemsPotentiallyWeaker.RemoveAll(i => { var v = i as MonsterItem; return v != null && v.OriginalOwner.Design.level >= givenItemLevel; });
			if (itemsPotentiallyWeaker.Count == 0)
				return null;

			// Preserve common items when the given monster item is not stronger than _CommonItemsEstimatedLevel
			if (givenItemLevel < _CommonItemsEstimatedLevel)
				itemsPotentiallyWeaker.RemoveAll(i => i is CommonItem);

			// The weaker are escape items, if not in late game
			if (!escapeItemsAreStronger)
			{
				var escapeItem = itemsPotentiallyWeaker.Find(i => i is EscapeItem);
				if (escapeItem)
					return escapeItem;
			}

			var monsterItem = itemsPotentiallyWeaker.Find(i => i is MonsterItem);
			if (monsterItem)
				return monsterItem;

			var commonItem = itemsPotentiallyWeaker.Find(i => i is CommonItem);
			if (commonItem)
				return commonItem;

			return null;
		}

		BaseItem FindMostPowerfulBattleItemInInventory(Character inventoryOwner, Character other)
		{
			var battleItems = new List<BaseItem>(inventoryOwner.Stats.ItemsInInventory).FindAll(i => i.Design.cardType == CardType.BATTLE);
			if (battleItems.Count == 0)
				return null;
			var effectGroups = battleItems.ConvertAll(i => (i.Design as IEffectsItem).GetEffects(inventoryOwner, other));
			var mapEffectGroupToItem = new Dictionary<List<Effect>, BaseItem>();
			for (int i = 0; i < battleItems.Count; ++i)
				mapEffectGroupToItem[effectGroups[i]] = battleItems[i];

			// Most powerful first
			effectGroups.Sort((a, b) => (int)((GetEffectGroupAveragePotential(b, inventoryOwner.Design, other.Design) - GetEffectGroupAveragePotential(a, inventoryOwner.Design, other.Design)) * 1000));

			return mapEffectGroupToItem[effectGroups[0]];
		}

		// Counting negative stats for enemies as positive stats for owner
		float GetEffectGroupAveragePotential(List<Effect> effectGroup, CharacterDesign inventoryOwnerDesign, CharacterDesign enemyDesign)
		{
			float power = 0f;
			foreach (var eff in effectGroup)
			{
				bool selfIsOwner = eff.owner == Effect.OwnerEnum.SELF;
				if (eff.type == Stat.ATK)
				{
					power += selfIsOwner ? eff.amount : -eff.amount;
				}
				else if (eff.type == Stat.SKILL)
				{
					var affectedDesign = selfIsOwner ? inventoryOwnerDesign : enemyDesign;
					float skillPower = ((affectedDesign.minSkillPower + affectedDesign.maxSkillPower) / 2f) * eff.amount;
					if (!selfIsOwner)
						skillPower = -skillPower;
					power += skillPower;
				}
			}

			return power;
		}

		bool IsExitReachable() { return /*_SpawnManager.exitAreaInstance.IsOpen &&*/ IsDragonDeadOrNotOnExitCell(); }

		bool IsDragonDeadOrNotOnExitCell() { return _SpawnManager.exitAreaInstance.ParentBlock != _SpawnManager.dragonInstance.GetParentAsBlock(); }

		List<Player> GetOtherPlayers() {var v = new List<Player>(_SpawnManager.playerInstances); v.Remove(Me); return v; }

		BaseItem GetBestItemToSteal()
		{
			Comparison<BaseItem> comparison = (a, b) => (GetItemValueFor(Me, b) - GetItemValueFor(Me, a)); // the most powerful first
			var itemsList = new List<BaseItem>();
			foreach (var enemy in GetOtherPlayers())
			{
				//var enemyItem = FindMostPowerfulBattleItemInInventory(enemy, Me);
				var enemyItemsList = new List<BaseItem>(enemy.Stats.ItemsInInventory);
				if (enemyItemsList.Count > 0)
				{
					enemyItemsList.Sort(comparison);
					itemsList.Add(enemyItemsList[0]);
				}
			}

			if (itemsList.Count > 0)
			{
				itemsList.Sort(comparison); 
				return itemsList[0];
			}

			return null;
		}

		float GetMinRequiredWinChance(Character c) { return c.Stats.KnowsDragonsVoice ? _WinChanceWithCharThatKnowsDragonVoiceToInitialteFight : _WinChanceToInitialteFight; }

		Character GetBestEnemyToAttract(Board.Descriptor boardDescriptor, bool atRange, out Cell.Descriptor cellToMoveTo)
		{
			bool prioritizeDragonOverPlayers = !Me.Stats.KnowsDragonsVoice;
			Character result;
			if (prioritizeDragonOverPlayers)
				result = GetDragonIfShouldBattleIt(boardDescriptor, atRange, out cellToMoveTo);
			else
				result = GetBestPlayerToBattle(boardDescriptor, atRange, out cellToMoveTo, false);

			if (!result)
			{
				if (prioritizeDragonOverPlayers)
					result = GetBestPlayerToBattle(boardDescriptor, atRange, out cellToMoveTo, false);
				else
					result = GetDragonIfShouldBattleIt(boardDescriptor, atRange, out cellToMoveTo);

				if (!result)
				{
					//if (includeTotems)
					//	result = GetBestTotemToBattle(boardDescriptor, atRange, out cellToMoveTo, false);

					if (!result)
						result = GetBestMonsterExceptDragonToBattle(boardDescriptor, atRange, out cellToMoveTo, false);
				}
			}

			return result;
		}

		Character GetBestPlayerToBattle(Board.Descriptor boardDescriptor, bool atRange, out Cell.Descriptor cellToMoveTo, bool favorProximity)
		{ return GetBestEnemyToBattleFrom(boardDescriptor, GetOtherPlayers().ConvertAll(p => p as Character), atRange, out cellToMoveTo, favorProximity); }
		
		Character GetBestTotemToBattle(Board.Descriptor boardDescriptor, bool atRange, out Cell.Descriptor cellToMoveTo, bool favorProximity)
		{ return GetBestEnemyToBattleFrom(boardDescriptor, new List<Character>(Array.FindAll(_SpawnManager.totemInstances, t => t.Owner != Me)), atRange, out cellToMoveTo, favorProximity); }

		Character GetBestMonsterExceptDragonToBattle(Board.Descriptor boardDescriptor, bool atRange, out Cell.Descriptor cellToMoveTo, bool favorProximity)
		{ return GetBestEnemyToBattleFrom(boardDescriptor, new List<Character>(_SpawnManager.monsterInstances), atRange, out cellToMoveTo, favorProximity); }

		Character GetDragonIfShouldBattleIt(Board.Descriptor boardDescriptor, bool atRange, out Cell.Descriptor cellToMoveTo)
		{
			if (_SpawnManager.dragonInstance.GetParentAsBlock() == null)
			{
				cellToMoveTo = null;
				return null;
			}

			float winCh01_MinReq =
				GetWinChance01Against_AdjustedForIndividualRequiredChance(
					boardDescriptor,
					GetMyBattlePotential(),
					_SpawnManager.dragonInstance,
					atRange,
					out cellToMoveTo
				);
			bool can = winCh01_MinReq >= 1f;
			if (can)
				Debug.Log("CPU: ShouldBattleDragon= " + can + ", winCh01_MinReq=" + winCh01_MinReq);

			return can ? _SpawnManager.dragonInstance : null;
		}

		// The one with the lowest 01 chance, but which has a 01 chance >= 1f
		Character GetBestEnemyToBattleFrom(Board.Descriptor boardDescriptor, List<Character> characters, bool atRange, out Cell.Descriptor cellToMoveTo, bool favorProximity)
		{
			characters = new List<Character>(characters);

			var myPotential = new CharacterBattlePotential(Me);
			// Dead or without min req win chance beat-able
			characters.RemoveAll(c => c.GetParentAsBlock() == null || myPotential.ComputeWithOpponentAndGetWinChance(c) < GetMinRequiredWinChance(c));
			Character enemyToReturn = null;
			if (characters.Count > 0)
			{
				Cell.Descriptor _;
				// First will have the smallest chance
				Comparison<Character> smallestWinChanceComparison = (a, b) =>
					(int)(
						(
							GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, a, atRange, out _)
							-
							GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, b, atRange, out _)
						)
						* 1000
					);

				if (favorProximity)
				{
					var cells = characters.ConvertAll(c => boardDescriptor.GetExistingCellDesc(c.GetParentCell()));
					List<Cell.Descriptor> closestDirect = null, closestIndirect = new List<Cell.Descriptor>();
					SortCellsByClosestAndGetClosest(boardDescriptor, cells, ref closestDirect, ref closestIndirect);

					if (closestDirect.Count > 0) // among those directly reachable, choose the one with the lowest chance
					{
						var charsSortedBySmallestWinChance = closestDirect.ConvertAll(c => characters[cells.IndexOf(c)]);
						charsSortedBySmallestWinChance.Sort(smallestWinChanceComparison);
						enemyToReturn = charsSortedBySmallestWinChance[0];
					}
					else
						enemyToReturn = characters[cells.IndexOf(closestIndirect[0])]; // the closest one, regardless of the win chance
				}
				else
				{
					characters.Sort(smallestWinChanceComparison);
					var enemy = characters[0];
					enemyToReturn = enemy;
				}

				// Get the cell before returning the result
				GetWinChance01Against_AdjustedForIndividualRequiredChance(boardDescriptor, myPotential, enemyToReturn, atRange, out cellToMoveTo);
			}
			else
				cellToMoveTo = null;

			return enemyToReturn;
		}
		
		float GetWinChance01AgainstReachableEnemy_AdjustedForIndividualRequiredChance_Simple_FromCurrentCell(Character enemy)
		{
			Cell.Descriptor _;
			return GetWinChance01Against_AdjustedForIndividualRequiredChance(
						_Board.CreateDescriptor(), 
						GetMyBattlePotential(), 
						enemy, 
						Me.RelativePosSimpleTo(enemy) == RelativePositionSimple.NEIGHBOR_DIRECT, 
						out _
					);
		}

		/// <summary>1f doesn't mean 100% per se, but that the MINIMUM chance is met</summary>
		float GetWinChance01Against_AdjustedForIndividualRequiredChance(Board.Descriptor boardDescriptor, CharacterBattlePotential myPotential, Character enemy, bool atRange, out Cell.Descriptor cellToMoveTo)
		{
			float minChance = GetMinRequiredWinChance(enemy);
			float rawChance = Utils.GetWinChanceAgainst(boardDescriptor, myPotential, GetMinRequiredWinChance(enemy), enemy, atRange, out cellToMoveTo);
			return rawChance / minChance;
		}

		///// <summary>Assuming the dragon is alive</summary>
		//bool ShouldTryToDefeatDragon(bool atRange, out Cell cellToMoveTo)
		//{
		//	return 
		//		GetWinChanceAgainst(GetMyBattlePotential(), _SpawnManager.dragonInstance, _WinChanceWithDragonToInitialteFight, atRange, out cellToMoveTo) 
		//		> _WinChanceWithDragonToInitialteFight;
		//}

		CharacterBattlePotential GetMyBattlePotential() { return new CharacterBattlePotential(Me); }

		#region Action checking
		bool ShiftDungeonSemiRandomly()
		{
			var meCell = Me.GetParentCell();
			int meRow = meCell.Row;
			int meColumn = meCell.Column;
			int oRow = _Board.OuterCell.Row;
			int oColumn = _Board.OuterCell.Column;
			int targetRow, targetColumn;

			if (meCell == _Board.OuterCell) // we're outside => push it in
			{
				targetRow = meRow;
				targetColumn = meColumn;
			}
			else
			{
				if (oColumn == -1 || oColumn == _Board.NumColumns) // sideways
				{
					// Move above or below board, shifting our column
					targetColumn = meColumn;
					if (meRow == 0) // do not go outside yet. further implementation can check what's the best position to shift the dungeon when cpu is outside
						targetRow = -1;
					else if (meRow == _Board.NumRows - 1)
						targetRow = _Board.NumRows;
					else // not in any extreme
						targetRow = UnityEngine.Random.Range(0, 2) == 0 ? -1 : _Board.NumRows; // random between above/below
				}
				else // above/below
				{
					// Move to the left/right side, shifting our row
					targetRow = meRow;
					if (meColumn == 0)
						targetColumn = -1;
					else if (meColumn == _Board.NumColumns - 1)
						targetColumn = _Board.NumColumns;
					else // not in any extreme
						targetColumn = UnityEngine.Random.Range(0, 2) == 0 ? -1 : _Board.NumColumns; // left/right
				}
			}

			Debug.Log("CPU: shifting dungeon");
			_GM.Submit_ShiftDungeon(_Board.GetShiter(targetRow, targetColumn));
			return true;
		}
		bool BuildTotemIfPossible()
		{
			if (!Me.Design.canBuildTotems || Me.Stats.Ore < _Design.totems.costPerTotem)
				return false;

			TotemType type = TotemType.COUNT_;
			int xpTotems = Me.Stats.GetOwnedTotemsOfType(TotemType.XP).Length;
			int moveTotems = Me.Stats.GetOwnedTotemsOfType(TotemType.MOVE).Length;
			int atkTotems = Me.Stats.GetOwnedTotemsOfType(TotemType.ATK).Length;

			if (xpTotems == 0 && Me.Stats.XP <= PlayerStats.MAX_XP * _MaxXP01ToPrioritizeXPOverATKTotems)
				type = TotemType.XP;
			else
			{
				if (XP01 >= _MinXP01ToPrioritizeMOVEOverATKTotems)
				{
					if (moveTotems < _Design.totems.maxInstancesPerTotemType)
						type = TotemType.MOVE;
					else if (atkTotems < _Design.totems.maxInstancesPerTotemType
						&& (XP01 > _MaxXP01ToPrioritizeXPOverATKTotems || xpTotems == _Design.totems.maxInstancesPerTotemType))
						type = TotemType.ATK;
					else if (xpTotems < _Design.totems.maxInstancesPerTotemType)
						type = TotemType.XP;
				}
				else
				{
					if (xpTotems < _Design.totems.maxInstancesPerTotemType && XP01 <= _MaxXP01ToPrioritizeXPOverATKTotems)
						type = TotemType.XP;
					else if (atkTotems < _Design.totems.maxInstancesPerTotemType)
						type = TotemType.ATK;
					else if (moveTotems < _Design.totems.maxInstancesPerTotemType)
						type = TotemType.MOVE;
				}
			}

			if (type != TotemType.COUNT_)
			{
				Debug.Log("CPU: Building totem: " + type);
				_GM.Submit_BuildTotem(type);
				return true;
			}

			return false;
		}

		void PlayDungeonShiftingItemOrShiftDungeon(Board.Descriptor boardDescriptor, Goal mainGoal, Action<bool> onDone)
		{
			if (onDone == null)
				onDone = b => { };

			if (Me.Stats.RemainingDungeonPushes > 0)
			{
				if (mainGoal != null)
				{
					DoShiftingPathfinding(boardDescriptor, mainGoal, onDone);

					// Not shifting randomly, if the current configuration is good
					return;
				}

				if (ShiftDungeonSemiRandomly())
				{
					onDone(true);
					return;
				}
			}
			else if (PlayDungeonShiftingItemIfPresent())
			{
				onDone(true);
				return;
			}

			onDone(false);
		}
		bool PlayDungeonShiftingItemIfPresent() { return PlayStatBoosterItemIfPresent(Stat.DUNGEON_PUSH); }
		bool PlayMoveBoosterItemIfPresentAndWorthIt() { return PlayStatBoosterItemIfPresent(Stat.MOVES, Stat.MOVES_THROUGH_WALLS, Stat.GOD_MOVES, Stat.DIAGONAL_MOVES, Stat.GHOST_MOVES); }
		bool PlayBattleEvadingItemIfPresent() { return PlayStatBoosterItemIfPresent(Stat.BATTLE_EVADING); }
		bool PlayAttractEnemyItemIfPresentAndWorthIt(Board.Descriptor boardDescriptor) { Cell.Descriptor _;  return GetBestEnemyToAttract(boardDescriptor, false, out _) != null && PlayStatBoosterItemIfPresent(Stat.ATTRACT_ENEMY); }
		bool PlayStealCardItemIfPresentAndWorthIt() { return GetBestItemToSteal() != null && PlayStatBoosterItemIfPresent(Stat.CARD_STEALING); }
		bool PlayStatBoosterItemIfPresent(params Stat[] stats) { return CheckStatBoosterItem(true, stats); }
		bool CheckStatBoosterItem(bool playIfPresent, params Stat[] stats)
		{
			var allEscapeItems = _Design.items.escapeItems.GetAll();
			var escapeItemsMostImportant = IsExitReachable();
			foreach (var item in Me.Stats.ItemsInInventory)
			{
				if (item.Design.cardType != CardType.UTILITY)
					continue;

				// Don't use escape items if they're needed to escape now
				if (escapeItemsMostImportant && Array.IndexOf(allEscapeItems, item.Design) != -1)
					continue;

				var asEffectsItem = item.Design as IEffectsItem;
				foreach (var eff in asEffectsItem.GetEffects(Me, null))
					if (Array.IndexOf(stats, eff.type) != -1)
					{
						if (playIfPresent)
						{
							Debug.Log("CPU: Acquiring '" + eff.type + "'  using '" + item.Design.name + "'");
							_GM.Submit_UseItem(item.Design);
						}
						return true;
					}
			}

			return false;
		}
		#endregion


		abstract class TakeItemBaseAction
		{
			public BaseItem desiredItem;
			public BaseItem itemToDiscardBefore;
			//public Action submitAction;


			public TakeItemBaseAction(BaseItem desiredItem) : this(desiredItem, null) { }

			public TakeItemBaseAction(BaseItem desiredItem, BaseItem itemToDiscardBeforePickUp)
			{
				this.desiredItem = desiredItem;
				this.itemToDiscardBefore = itemToDiscardBeforePickUp;
			}


			public void Submit(GameplayManager gm)
			{
				if (itemToDiscardBefore)
					gm.Submit_DiscardItem(itemToDiscardBefore.Design);
				else if (desiredItem)
					SubmitTakeItem(gm);
			}

			protected abstract void SubmitTakeItem(GameplayManager gm);

			public override string ToString()
			{
				return "desiredItem=" + (desiredItem == null ? "null" : desiredItem.Design.name) +
						", itemToDiscardBefore=" + (itemToDiscardBefore == null ? "null" : itemToDiscardBefore.Design.name);
			}
		}


		class LootAction : TakeItemBaseAction
		{
			public LootAction(BaseItem desiredItem) : this(desiredItem, null) { }
			public LootAction(BaseItem desiredItem, BaseItem itemToDiscardBefore) : base(desiredItem, itemToDiscardBefore) { }


			protected override void SubmitTakeItem(GameplayManager gm) { gm.Submit_PickUpItem(desiredItem); }
		}
		

		class MoveToReachableEnemyAction
		{
			public Board.Descriptor boardDescriptor;
			public Character enemy;
			public Cell.Descriptor cellToMoveToBeforeAttacking;


			public MoveToReachableEnemyAction(Board.Descriptor boardDescriptor, Character enemy, Cell.Descriptor cellToMoveToBeforeAttacking)
			{
				this.boardDescriptor = boardDescriptor;
				this.enemy = enemy;
				this.cellToMoveToBeforeAttacking = cellToMoveToBeforeAttacking;
			}

			public Cell.Descriptor GetCurrentTargetCellToMoveTo() { return cellToMoveToBeforeAttacking == null ? boardDescriptor.GetExistingCellDesc(enemy.GetParentCell()) : cellToMoveToBeforeAttacking; }

			public override string ToString()
			{ return enemy.Design.name + (cellToMoveToBeforeAttacking == null ? "" : ", Ranged attack: move to " + cellToMoveToBeforeAttacking.describedCell.ToShortString()); }
		}


		class Goal
		{
			public Cell.Descriptor cell;
			public Character enemyToAttackFromCell;
			public GoalType goalType;
			public Goal nextGoal;


			public Goal() { }
			public Goal(GoalType goalType, Goal nextGoal = null) { Set(goalType, nextGoal); }

			public Goal Set(GoalType goalType, Goal nextGoal = null) { this.goalType = goalType; this.nextGoal = nextGoal; return this; }
			public Goal SetTypeAndNextGoalFrom(Goal other) { return Set(other.goalType, other.nextGoal); }


			public override string ToString()
			{
				return goalType + ": " + cell + (enemyToAttackFromCell == null ? "" : "; victim=" + enemyToAttackFromCell) + (nextGoal == null ? "" : (". Purpose: " + nextGoal));
			}
		}


		///// <summary>
		///// Used when the goal cell is a direct neighbor, but it's not reachable and no 1-shift solution could put the board into a better configuration. 
		///// In this case, backtracking for an arbitrary number of shift moves is not feasible in a decent amount of time, so the dungeon will be shifted so that the player would rotate around the goal until it's reachable
		///// </summary>
		//class ShiftingSolutionForUnreachableNearGoal
		//{
		//	public Cell.Descriptor goalCell

		//}


		enum GoalType
		{
			GET_COMMON_ITEM,
			GET_ESCAPE_ITEM,
			GET_TREASURE_ITEM,
			PERMAKILL_PLAYER,
			KILL_PLAYER,
			KILL_DRAGON,
			KILL_TOTEM,
			KILL_REGULAR_MONSTER,
			REACH_MAX_XP,
		}


		class DungeonShiftingSolutionFinderToMinimizePathCost
		{
			Board.Descriptor _InitialBoardDescriptor;
			PathfindingCurrency.Comparer _CurrencyComparer;
			Player _Me;
			Cell _MyCell, _TargetCell;
			//Block _MyBlock, _TargetBlock;
			int _NumShifters;
			PathfindingCurrency _UnlimitedBudget;
			int _MaxDepth, _DepthBeforeStartingToOptimize, _MaxDepthInOptimizedOnlyMode;
			EnemiesInfo _EnemiesInfo;
			bool _Cancelled;
			object _CancelLock = new object();
			object _InfoLock = new object();
			int _NumIterations, _NumIterationsInCurrentDepth;
			Board _Board;
			int _CurrentDepthTarget;
			bool _OptimizedOnly;

			
			public class ShiftingBranch
			{
				public List<int> shiftersPath;
				public Board.Descriptor boardDescriptor;
				public PathfindingSolution pathfindingSolution;


				public string ToString(Board board)
				{
					string s = "shifters: ";
					foreach (var item in shiftersPath)
					{
						var sh = board.AllShiftPoints[item];
						s += "R" + sh.Row + "C" + sh.Column + ", ";
					}
					s += "\n" + pathfindingSolution.ToString(10);

					return s;
				}
			}

			public class Info
			{
				public int currentDepthTarget;
				public int currentMaxDepth;
				public int totalIterations;
				public int iterationsInCurrentDepth;
				public bool optimizedOnly;

				public override string ToString() { return "{" + currentDepthTarget + "/" + currentMaxDepth + ":" + iterationsInCurrentDepth + "(T" + totalIterations + ")"+(optimizedOnly?"-Optim":"")+"}"; }
			}

			public Info GetProgressInfo()
			{
				lock (_InfoLock)
					return new Info()
					{
						currentDepthTarget = _CurrentDepthTarget,
						currentMaxDepth = _MaxDepth,
						totalIterations = _NumIterations,
						iterationsInCurrentDepth = _NumIterationsInCurrentDepth,
						optimizedOnly = _OptimizedOnly
					};
			}

			public void Cancel()
			{
				lock (_CancelLock)
					_Cancelled = true;
			}

			public DungeonShiftingSolutionFinderToMinimizePathCost(
				Cell targetCell, 
				Player me, 
				Board board,
				PathfindingCurrency.Comparer currencyComparer,
				int allowedShifts,
				int allowedShiftsBeforeOptimizing,
				int allowedShiftsInOptimizedOnlyMode)
			{
				_UnlimitedBudget = PathfindingCurrency.CreateUnlimited(me.Stats[Stat.GHOST_MOVES]);
				_TargetCell = targetCell;
				_Board = board;
				_MyCell = me.GetParentCell();
				_Me = me;
				_InitialBoardDescriptor = _Board.CreateDescriptor(_MyCell, _TargetCell);
				_CurrencyComparer = currencyComparer;
				_NumShifters = _Board.AllShiftPoints.Length;
				_MaxDepth = allowedShifts;
				if (_MaxDepth < 1)
					throw new UnityException("_AllowedShifts < 1");
				_DepthBeforeStartingToOptimize = allowedShiftsBeforeOptimizing;
				if (_DepthBeforeStartingToOptimize < 0 || _DepthBeforeStartingToOptimize > _MaxDepth)
					throw new UnityException("_AllowedShiftsUntilOptimizing < 0 || _AllowedShiftsUntilOptimizing > _TotalAllowedShifts");
				_MaxDepthInOptimizedOnlyMode = allowedShiftsInOptimizedOnlyMode;
				if (_MaxDepthInOptimizedOnlyMode < _MaxDepth)
					throw new UnityException("_AllowedShiftsInOptimizedOnlyMode < _MaxDepth");
				_EnemiesInfo = new EnemiesInfo(_Me);
				_EnemiesInfo.PreCacheNow(_Board); // won't be able to access unity's stuff inside the thread
			}

			public void Compute(Action<ShiftingBranch> onDone)
			{

				//var pctx_unlimited = new PathfindingContext(_Me, _InitialBoardDescriptor.GetExistingCellDesc(_MyCell), _UnlimitedBudget, _CurrencyComparer, _EnemiesInfo);
				//_InitialBoardDescriptor.ComputeBestPaths(pctx_unlimited);
				//var solBef = pctx_unlimited.mapDestCellToBestSolution[_InitialBoardDescriptor.GetExistingCellDesc(_TargetCell)];

				//ComputeBestShiftingSolutionToMinimizePathCost(_InitialBoardDescriptor, new List<int>(), solution);
				//onDone(solution);

				var t = new Thread(() =>
				{
					try
					{
						var solution = ComputeBestShiftingSolutionToMinimizePathCost();

						lock (_CancelLock)
							if (_Cancelled)
								return;

						if (solution.shiftersPath.Count == 0 || solution.pathfindingSolution.cost.moves > 1 || solution.pathfindingSolution.cost.TotalSpecialMoves > 0)
						{
							lock (_InfoLock)
							{
								_OptimizedOnly = true;
								_MaxDepth = _MaxDepthInOptimizedOnlyMode;
							}

							solution = ComputeBestShiftingSolutionToMinimizePathCost();
						}

						lock (_CancelLock)
							if (_Cancelled)
								return;

						if (onDone != null)
							UnityThreadDispatcher.Instance.Enqueue(() => onDone(solution));
					}
					//catch (Exception e) { Debug.LogException(e); }
					catch { }
				});
				t.Priority = System.Threading.ThreadPriority.AboveNormal;
				t.Start();

				//Debug.Log("Bef:\n" + solBef.ToString(10) + "\nAft:\n" + solution.pathfindingSolution.ToString(10) + "\nbef-aft=" + _CurrencyComparer.Compare(solBef.cost, solution.pathfindingSolution.cost));
				//return solution;
			}

			PathfindingSolution ComputePathfindingSolution(Board.Descriptor boardDescriptor)
			{
				lock (_InfoLock)
				{
					++_NumIterations;
					++_NumIterationsInCurrentDepth;
				}

				var myCellDesc = boardDescriptor.cellsContainingTrackedBlocks[0];
				var targetCellDesc = boardDescriptor.cellsContainingTrackedBlocks[1];

				var pctx_unlimited = new PathfindingContext(/*_Me,*/ myCellDesc, _UnlimitedBudget, _CurrencyComparer, _EnemiesInfo);
				boardDescriptor.ComputeBestPaths(pctx_unlimited);
				//if (myCellDesc.IsAdjacentAndNotObstructedByWall(targetCellDesc, false) && currentBestSolution != null)
				//{
				//	var thisSol = pctx_unlimited.mapDestCellToBestSolution[targetCellDesc];
				//	Debug.Log(myCellDesc + "->" + targetCellDesc + ": " + myCellDesc.DeltaTo(targetCellDesc)
				//		+ "\n this=" + thisSol.ToString(10)
				//		+ "\n best=" + currentBestSolution.pathfindingSolution.ToString(10)
				//		+ "\n this-best=" + _CurrencyComparer.Compare(thisSol.cost, currentBestSolution.pathfindingSolution.cost)
				//	);
				//}

				PathfindingSolution sol;
				if (!pctx_unlimited.mapDestCellToBestSolution.TryGetValue(targetCellDesc, out sol))
					throw new UnityException("CPU.DoPathfinding: can't find target cell even with unlimited moves budget??? targetCell=" + targetCellDesc + ", numReachable="+ pctx_unlimited.mapDestCellToBestSolution.Keys.Count);

				return sol;
			}

			/// <summary>returns the number of shifters to iterate</summary>
			int GetOptimizedShiftersArrayToIterate(Board.Descriptor currentBoardDescriptor, out Shifter[] shifters)
			{
				// If neither player nor target are on the outer cell, only shifting rows/columns directly impacting the player or his direct neighbor cells
				var actorCell = currentBoardDescriptor.cellsContainingTrackedBlocks[0];
				shifters = new Shifter[12];
				int r, c;
				int i = 0;
				//r = actorCell.Row - 1;
				//if (r > -1)
				//{
				//	shifters[i++] = _Board.GetShiter(r, -1);
				//	shifters[i++] = _Board.GetShiter(r, _Board.NumColumns);
				//}
				r = actorCell.Row;
				shifters[i++] = _Board.GetShiter(r, -1);
				shifters[i++] = _Board.GetShiter(r, _Board.NumColumns);
				//r = actorCell.Row + 1;
				//if (r < _Board.NumRows)
				//{
				//	shifters[i++] = _Board.GetShiter(r, -1);
				//	shifters[i++] = _Board.GetShiter(r, _Board.NumColumns);
				//}

				//c = actorCell.Column - 1;
				//if (c > -1)
				//{
				//	shifters[i++] = _Board.GetShiter(-1, c);
				//	shifters[i++] = _Board.GetShiter(_Board.NumRows, c);
				//}
				c = actorCell.Column;
				shifters[i++] = _Board.GetShiter(-1, c);
				shifters[i++] = _Board.GetShiter(_Board.NumRows, c);

				//c = actorCell.Column + 1;
				//if (c < _Board.NumColumns)
				//{
				//	shifters[i++] = _Board.GetShiter(-1, c);
				//	shifters[i++] = _Board.GetShiter(_Board.NumRows, c);
				//}

				return i;
			}


			ShiftingBranch ComputeBestShiftingSolutionToMinimizePathCost()
			{
				var initialShiftingBranch = 
					new ShiftingBranch() {
						boardDescriptor = _InitialBoardDescriptor,
						pathfindingSolution = ComputePathfindingSolution(_InitialBoardDescriptor),
						shiftersPath = new List<int>()
					};
				var currentBestShiftingBranch = initialShiftingBranch;
				var branchesAtCurrentDepth = new List<ShiftingBranch>() { initialShiftingBranch };
				ShiftingBranch curBranch;
				lock (_InfoLock)
				{
					_CurrentDepthTarget = 1;
					_NumIterations = _NumIterationsInCurrentDepth = 0;
				}

				while (_CurrentDepthTarget <= _MaxDepth && (currentBestShiftingBranch.pathfindingSolution.cost.moves > 1 || currentBestShiftingBranch.pathfindingSolution.cost.TotalSpecialMoves > 0))
				{
					var newBranches = ComputeBestShiftingSolutionToMinimizePathCost(branchesAtCurrentDepth);

					if (newBranches == null) // thread work cancelled
						return null;

					for (int i = 0; i < newBranches.Count; i++)
					{
						curBranch = newBranches[i];

						int compareResult;
						if ((compareResult = _CurrencyComparer.Compare(curBranch.pathfindingSolution.cost, currentBestShiftingBranch.pathfindingSolution.cost)) < 0)  // smaller moving cost
							currentBestShiftingBranch = curBranch;
					}

					branchesAtCurrentDepth = newBranches;
					lock (_InfoLock)
					{
						++_CurrentDepthTarget;
						_NumIterationsInCurrentDepth = 0;
					}
				}

				return currentBestShiftingBranch;
			}

			List<ShiftingBranch> ComputeBestShiftingSolutionToMinimizePathCost(List<ShiftingBranch> branchesAtCurrentDepth)
			{
				var oneMoreDepthLevelBranches = new List<ShiftingBranch>();
				List<ShiftingBranch> branches;
				for (int i = 0; i < branchesAtCurrentDepth.Count; i++)
				{
					branches = ComputeBestShiftingSolutionToMinimizePathCost(branchesAtCurrentDepth[i]);
					if (branches == null) // work aborted
						return null;

					oneMoreDepthLevelBranches.AddRange(branches);
				}

				return oneMoreDepthLevelBranches;
			}

			List<ShiftingBranch> ComputeBestShiftingSolutionToMinimizePathCost(ShiftingBranch branch)
			{
				lock (_CancelLock)
					if (_Cancelled)
						return null;

				var shifterToIgnore = branch.shiftersPath.Count > 0 ? 
										branch.boardDescriptor.board.GetOpposingShiter(branch.shiftersPath[branch.shiftersPath.Count - 1]) 
											: null;
				Shifter[] shifters;
				int numBranchesToIterateThrough;
				List<ShiftingBranch> branches;
				// If neither player nor target are on the outer cell, only shifting rows/columns directly impacting the player or his direct neighbor cells
				var actorCell = branch.boardDescriptor.cellsContainingTrackedBlocks[0];
				var noneOfKeyCellsAreOuter = actorCell != branch.boardDescriptor.outerCell && branch.boardDescriptor.cellsContainingTrackedBlocks[1] != branch.boardDescriptor.outerCell;
				if (noneOfKeyCellsAreOuter && (_OptimizedOnly || branch.shiftersPath.Count >= _DepthBeforeStartingToOptimize))
					numBranchesToIterateThrough = GetOptimizedShiftersArrayToIterate(branch.boardDescriptor, out shifters);
				else
				{
					shifters = _Board.AllShiftPoints;
					numBranchesToIterateThrough = _NumShifters;
				}

				branches = new List<ShiftingBranch>(numBranchesToIterateThrough);
				bool reachedTheLeafOfCurrentBranch = branch.shiftersPath.Count + 1 == _CurrentDepthTarget;
				Shifter s;
				ShiftingBranch b;
				for (int i = 0, shifterIndex; i < numBranchesToIterateThrough; ++i)
				{
					s = shifters[i];
					if (s == shifterToIgnore) // prevent computing left+right, right+left, up+down etc. (they cancel each other)
					{
						--numBranchesToIterateThrough;
						--i;
						continue;
					}
					shifterIndex = s.IndexInShiftersArray;
					b = new ShiftingBranch();
					branches.Add(b);

					b.boardDescriptor = branch.boardDescriptor.GetShifted(shifterIndex);
					b.shiftersPath = new List<int>(branch.shiftersPath) { shifterIndex };
					b.pathfindingSolution = ComputePathfindingSolution(b.boardDescriptor);

					if (reachedTheLeafOfCurrentBranch)
					{
						// No need to go to other solutions on current depth if the least costly solution was already found on this branch and this is the leaf
						var curCost = b.pathfindingSolution.cost;
						if (curCost.moves == 1 && curCost.TotalSpecialMoves == 0)
						{
							numBranchesToIterateThrough = i+1;
							break;
						}
					}
				}

				if (!reachedTheLeafOfCurrentBranch)
				{
					var branchesToIterateThrough = branches;
					branches = new List<ShiftingBranch>();
					for (int i = 0; i < numBranchesToIterateThrough; ++i)
						branches.AddRange(ComputeBestShiftingSolutionToMinimizePathCost(branchesToIterateThrough[i]));
				}

				return branches;
			}
		}
	}
}
