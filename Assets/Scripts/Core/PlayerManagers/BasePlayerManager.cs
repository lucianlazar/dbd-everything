﻿using UnityEngine;
using System.Collections;
using Photon;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Core;
using UnityEngine.SceneManagement;
using System;
using Com.TheFallenGames.DBD.World;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.UI.Totems;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.UI;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.UI.Inspect;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Core.PlayerManagers
{
	public abstract class BasePlayerManager : PunBehaviour
	{
		// Set by GameStateManager in OnGameStarted
		public Player Me { get; set; }

		public bool IsMyTurn { get { return _GM.CurrentPlayer == Me; } }
		public bool IsMyTurnInBattle { get { return Me == _GM.CurrentPlayerInBattle; } }
		public bool IsMyTurnInBattleAndBattleNotEnded { get { return IsMyTurnInBattle && !_GM.IsPostBattle; } }
		public bool AmIBattlingCurrentPlayer { get { return Me == _GM.OpponentOfCurrentPlayer; } }
		public bool IsMyTurnInBattleAndCanPlayCards { get; set; }
		public bool IsMyTurnOutsideBattle { get { return IsMyTurn && !_GM.IsPreIntraOrPostBattle; } }
		public bool IsPostBattleAndIWon { get { return _GM.IsPostBattle && _GM.WinnerOfCurrentBattleAsPlayer == Me; } }
		public bool IsMyTurnOutsideBattleOrIsPostBattleAndIWon { get { return IsMyTurnOutsideBattle || IsPostBattleAndIWon; } }
		public EntityDesign Trading_MyDesiredCard { get; set; }

		protected Design _Design;
		protected SpawnManager _SpawnManager;
		protected Board _Board;
		//protected BoardHelper _BoardHelper;
		protected GameplayManager _GM;


		protected virtual void Awake()
		{
			_Design = Design.Instance;
			_SpawnManager = FindObjectOfType<SpawnManager>();
			_Board = FindObjectOfType<Board>();
			_GM = FindObjectOfType<GameplayManager>();

			//_PGM.Ev_NewTurn += OnNewTurn;
			//_PGM.Ev_PlayerEndedTurn += OnPlayerEndedTurn;
			//_PGM.Ev_PlayerMoveToCellAccepted += OnMoveToCellAccepted;
			//_PGM.Ev_OtherMoveToCellAccepted += OnOtherMoveToCellAccepted;
			//_PGM.Ev_PlayerShiftDungeonAccepted += OnDungeonPushAccepted;
			//_PGM.Ev_PlayerGetOreAccepted += OnGetOreAccepted;
			//_PGM.Ev_PlayerDiscardAccepted += OnDiscardAccepted;
			//_PGM.Ev_PlayerPickUpItemAccepted += OnPickUpItemAccepted;
			//_PGM.Ev_PlayerBuildTotemAccepted += OnPlayerBuildTotemAccepted;
			//_PGM.Ev_PlayerSetupBattleAccepted += OnSetupBattleAccepted;
			//_PGM.Ev_PlayerPlayCardAccepted += OnPlayCardAccepted;
			//_PGM.Ev_Battle_PlayerInitiates += OnBattlePlayerInitiates;
			//_PGM.Ev_Battle_PlayerStaysIdle += OnBattlePlayerStaysIdle;
			//_PGM.Ev_Battle_ApproachedPlayerChoseIfToCancelBattle += OnBattleApproachedPlayerChoseIfToCancelBattle;
			//_PGM.Ev_Battle_ApproachedPlayerInitiates += OnBattleApproachedPlayerInitiates;
			//_PGM.Ev_Battle_ApproachedPlayerAlsoStaysIdle += OnBattleApproachedPlayerAlsoStaysIdle;
			//_PGM.Ev_PlayerReadyForBattle += OnPlayerReadyForBattleAccepted;
			//_PGM.Ev_PlayerBattleTurnStarted += OnPlayerBattleTurnStarted;
			//_PGM.Ev_BattleResults += OnBattleResultsObtainedFromMaster;
			//_PGM.Ev_Client_PlayerEliminated += Client_OnPlayerEliminated;
			//_PGM.Ev_PostBattleWinnerDoLooting += PostBattleWinnerDoLooting;
			//_PGM.Ev_PostBattleWinnerDoneLooting += OnPostBattleWinnerDoneLooting;
			//_PGM.Ev_ItemTransfer += OnItemTransferAccepted;
			//_PGM.Ev_GameResult += OnGameResult;
		}
		protected virtual void Start()
		{
			//_BoardHelper = new BoardHelper(_Board);
		}
		protected virtual void Update() { }
		protected virtual void OnDestroy()
		{
			//Debug.Log("Destr BasePM");
		}


		#region GameplayManager events
		public abstract void OnChooseFirstIfToBattle();
		public abstract void OnChooseIfToBattleAfterCurrentPlayerPrefersNotTo();
		public abstract void OnAnswerTradeProposal(Player receiver, BaseItem donorItem, BaseItem receiverTradedItem);
		public abstract void OnChooseIfToCancelBattle();
		public abstract void OnReadyToAct(DBDEvent completedEvent, object eventData = null);

		public virtual void OnNewTurn()
		{

		}

		public virtual void OnTurnEnded()
		{
			if (Trading_MyDesiredCard != null)
			{
				Debug.Log("Trading: Desired card selected, but the other card not. Trade canceled");
				Trading_MyDesiredCard = null;
			}
		}
		public virtual void OnPreStealCard() { }
		public abstract void OnPostStealCard();

		public void OnLootCell(Cell cell)
		{
			bool canPickUpItems = Me.Design.canPickUpItems;
			if (canPickUpItems)
			{
				// Get the ore, if any; OnPlayerReadyToInterractWithCell will be called again once done
				if (cell.ContainedBlock.ContainsOre())
				{
					_GM.Submit_GetOre(cell);
					return;
				}

				// Pick items
				var items = cell.ContainedBlock.GetChildrenOfType<BaseItem>();

				if (items.Count > 0)
				{

					bool controlTaken = Loot(items.ToArray());
					if (controlTaken)
						return;
				}
			}
			
			OnFinishedLootingCell();
		}
		#endregion


		public void OnReadyToAct() { OnReadyToAct(DBDEvent.NONE); }

		public void OnReadyToActInBattle()
		{
			// Restore the battle panel state, so "READY" can be pressed
			if (Me.Stats.NumBattleItemsInInventory > 0 && Me.Stats.RemainingBattleCardsToPlay > 0) // has battle items & can play more
				OnReadyToPlayBattleItems();
			else // no battle items || already played all available slots => nothing to play => automatically ready
				SubmitReadyToBattle();
		}


		protected abstract void OnReadyToPlayBattleItems();
		/// <summary>returns wether the control was taken. must call <see cref="OnFinishedHandlingItemsInCurrentCell(bool)"/></summary>
		protected abstract bool Loot(params BaseItem[] items);

		protected virtual void OnFinishedHandlingItemsInCell() { OnFinishedLootingCell(); }

		protected virtual void OnBeforeSubmitReadyToBattle() { }

		protected void SubmitReadyToBattle()
		{
			OnBeforeSubmitReadyToBattle();
			_GM.Submit_ReadyToBattle();
		}

		/// <summary>Returns wether the control was taken</summary>
		protected void OnFinishedLootingCell()
		{
			// We decide we're done looting => inform master
			if (IsPostBattleAndIWon)
				_GM.Submit_PostBattleWinnerDoneLooting();
			else
				// our turn outside battle
				OnReadyToAct();
		}
	}
}
