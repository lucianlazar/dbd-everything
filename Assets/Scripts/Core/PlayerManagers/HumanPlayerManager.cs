﻿using UnityEngine;
using System.Collections;
using Photon;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Core;
using UnityEngine.SceneManagement;
using System;
using Com.TheFallenGames.DBD.World;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.UI.Totems;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.UI;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Network.Data;
using Com.TheFallenGames.DBD.UI.Inspect;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Actors;

namespace Com.TheFallenGames.DBD.Core.PlayerManagers
{
	/// <summary>Human means here that it's not the CPU. It's not the same meaning as a "human class"</summary>
	public class HumanPlayerManager : BasePlayerManager
	{

		protected override void Start()
		{
			base.Start();


			// Board initializes in Awake, so its info is available only in our Start()
			foreach (var cell in _GM.board.AllCellsIncludingOuterCell)
				cell.AllowedAreaHighlight.onSelected = OnAllowedAreaSelected;

			_GM.inventoryPanel.PlayClick += OnPlayCardClicked;
			_GM.inventoryPanel.DiscardClick += OnDiscardClicked;
			_GM.inventoryPanel.ChooseClick += OnChooseCardClicked;
			_GM.inventoryPanel.ExamineClick += OnExamineCardClicked;
			_GM.inventoryPanel.RequestTradeClick += OnRequestTradeCardClicked;
			_GM.inventoryPanel.NavigationPanel.NavigateClicked +=
				dir => _GM.InspectPlayerInventory(_SpawnManager.GetPlayerInstanceAtRelativePosition(_GM.inventoryPanel.InspectedPlayer, dir), true);

			_GM.battlePanel.commandsPanel.BattleClicked += OnBattleClicked;
			_GM.battlePanel.commandsPanel.DoNothingClicked += OnDoNothingClicked;
			_GM.battlePanel.commandsPanel.OnChoseIfToEvade += OnMeAsOpponentChoseIfToCancelBattle;
			_GM.battlePanel.commandsPanel.ReadyClicked += OnReadyClicked;

			_GM.inspectPanel.Showing += OnInspectPanelShowing;
			_GM.inspectPanel.Closed += OnInspectPanelClosed;
			_GM.inspectPanel.InspectedEntityChanged += OnInspectPanelEntityChanged;
			_GM.inspectPanel.commandsPanel.RangedBattleClicked += OnInspectPanelRangedBattleClicked;
			_GM.inspectPanel.commandsPanel.ChooseClicked += OnInspectPanelChooseClicked;
			_GM.inspectPanel.commandsPanel.MiscClicked += OnInspectPanelMiscClicked;
			_GM.inspectPanel.commandsPanel.LootClicked += OnInspectPanelLootClicked;

			Base.Entity.EntityLongPressed += OnEntityLongPressed;

			_GM.board.OuterCell.onPlaced += OnShifterSelected;
			_GM.totemsPanel.Initialize(OnBuildTotemClick);

			_GM.endTurnButton.onClick.AddListener(OnEndTurnClicked);
			_GM.leaveRoomButton.onClick.AddListener(OnLeaveRoomClicked);
		}
		// For debugging
		//#if UNITY_EDITOR
		protected override void Update()
		{
			base.Update();

			if (!Application.isEditor && (PhotonNetwork.player == null || !PhotonNetwork.playerName.Contains("luc-test-wind")))
				return;

			if (!Input.GetKey(KeyCode.LeftControl))
				return;

			if (!IsMyTurn)
				return;

			if (Input.GetMouseButtonDown(2))
			{
				var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit, 1000f, 1 << LayerMask.NameToLayer("Cell") | 1 << LayerMask.NameToLayer("OuterCell")))
				{
					var cell = hit.collider.GetComponent<Cell>();
					if (cell.ContainedBlock)
					{
						_GM.UpdateInteractivityForCurrentPlayer(false);
						_GM.Submit_MoveToCell(cell);
					}
				}
			}
			else if (Input.GetMouseButtonDown(3))
			{
				Me.Stats.AddEffect(new Config.Effects.Effect() { type = Stat.ATK, amount = 20, dynamicallyCreated = true });
				_GM.UpdateStatsUI();
			}
		}
		//#endif
		protected override void OnDestroy()
		{
			_GM.endTurnButton.onClick.RemoveListener(OnEndTurnClicked);
			_GM.leaveRoomButton.onClick.RemoveListener(OnLeaveRoomClicked);

			Base.Entity.EntityLongPressed -= OnEntityLongPressed;

			base.OnDestroy();
		}


		#region UI events
		void OnAllowedAreaSelected(AllowedArea allowedArea)
		{
			_GM.UpdateInteractivityForCurrentPlayer(false);
			_GM.Submit_MoveToCell(allowedArea.GetParentAsCell());
		}

		void OnPlayCardClicked(CardModel cardModel)
		{
			_GM.inventoryPanel.CardsInteractable = false; // prevent new request before result arrives
													  // Prevent sending "READY" before receiving the confirmation
			if (IsMyTurnInBattle)
				_GM.battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
			_GM.Submit_UseItem(cardModel.design as BaseItemDesign);
		}

		void OnDiscardClicked(CardModel cardModel)
		{
			_GM.inventoryPanel.CardsInteractable = false; // prevent new request before result arrives

			// Prevent sending "READY" before receiving the confirmation
			if (IsMyTurnInBattle)
				_GM.battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;

			// Commented: this should always happen, as discarding a card should refresh the options (a good example is showing a "buy" panel for a cost item. 
			// the buy button should be updated if the player discards a treasure meanwhile, possibly making the item unaffordable)
			//// We won and we've just discarded something => close the context menu, if it's opened
			//if (IsPostBattleAndIWon)
			//{
			if (_GM.contextPanel.IsShown)
				_GM.contextPanel.Cancel();
			//}
			_GM.Submit_DiscardItem(cardModel.design as BaseItemDesign);
		}

		void OnChooseCardClicked(CardModel cardModel)
		{
			_GM.UpdateInteractivityForCurrentPlayer(false);

			var cardModelDesign = cardModel.design as BaseItemDesign;
			Player donor;
			BaseItemDesign donorItemDesign;
			BaseItemDesign tradedItemDesign = null;
			if (Trading_MyDesiredCard == null) // stealing
			{
				donor = _GM.inventoryPanel.InspectedPlayer;
				donorItemDesign = cardModelDesign;
			}
			else // trading
			{
				donor = (_SpawnManager.GetEntityByDesign(Trading_MyDesiredCard) as BaseItem).Owner as Player;
				donorItemDesign = Trading_MyDesiredCard as BaseItemDesign;

				tradedItemDesign = cardModelDesign;
				// Trading: Ask the other player
				Debug.Log("Asking for " + Trading_MyDesiredCard.name + " in return of " + cardModel.design.name);
			}
			_GM.Submit_TransferItem(donor, donorItemDesign, Me, tradedItemDesign);
		}

		void OnExamineCardClicked(CardModel cardModel)
		{
			_GM.inventoryPanel.Close(0f, null);
			_GM.inspectPanel.Show(_SpawnManager.GetEntityByDesign(cardModel.design));
		}

		void OnRequestTradeCardClicked(CardModel cardModel)
		{
			//UpdateInteractivityForCurrentPlayer(false);
			Trading_MyDesiredCard = cardModel.design;
			_GM.InspectPlayerInventory(Me, true);
			_GM.inventoryPanel.SelectFirst(); // so the player can see he can choose one
		}


		void OnBattleClicked()
		{
			_GM.battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
			_GM.Submit_InitiateBattle(Me);
		}

		void OnDoNothingClicked()
		{
			_GM.battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
			_GM.Submit_StayIdleInBattle(Me);
		}

		void OnMeAsOpponentChoseIfToCancelBattle(bool cancel)
		{
			_GM.battlePanel.commandsPanel.State = BattleCommandsPanel.ButtonsState.WAITING;
			_GM.Submit_ChooseIfToCancelBattle(cancel);
		}

		void OnReadyClicked() { SubmitReadyToBattle(); }



		#region Callbacks from InspectPanel
		void OnInspectPanelShowing() { _GM.UpdateInteractivityForCurrentPlayer(false); }
		void OnInspectPanelClosed() { _GM.UpdateInteractivityForCurrentPlayer(true); }

		void OnInspectPanelEntityChanged(Entity entity)
		{
			var state = InspectCommandsPanel.ButtonsState.CLOSE_ONLY;
			bool entityIsInDungeon = entity.GetParentAsBlock() != null;
			if (entityIsInDungeon)
			{
				if (entity is Character && IsMyTurn)
				{
					if (Me.Stats[Stat.ATTRACT_ENEMY] > 0
						&& entity != Me  // Can't attract self
						&& !(entity is Totem)) // can't attract totems
					{
						state = InspectCommandsPanel.ButtonsState.CHOOSER; // choosing which enemy to attract
					}
					else if (
						Me.Stats[Stat.RANGED_BATTLE] > 0
						//&& entity.RelativePosSimpleTo(Me) == RelativePositionSimple.NEIGHBOR_DIRECT // commented: the next condition implies it
						&& Me.GetParentCell().Desc.IsAdjacentAndNotObstructedByWall(entity.GetParentCell().Desc, false)
					)
					{
						state = InspectCommandsPanel.ButtonsState.RANGED_BATTLE;
					}
				}
			}
			else
			{

			}

			var meBlock = Me.GetParentCell().ContainedBlock;
			_GM.inspectPanel.commandsPanel.LootingActive =
				IsMyTurn &&
				entityIsInDungeon && Me.RelativePosSimpleTo(entity) == RelativePositionSimple.IDENTICAL &&
				meBlock.GetChildrenOfType<BaseItem>().Count > meBlock.PeekOreAmount() // the number of pickable non-ore items is positive
				&& Me.Design.canPickUpItems
				&& Me.GetEnemiesInCell().Count == 0;
			_GM.inspectPanel.commandsPanel.State = state;
		}

		void OnInspectPanelRangedBattleClicked()
		{
			_GM.Submit_StartBattleWith(_GM.inspectPanel.InspectedEntity as Character);
		}
		void OnInspectPanelChooseClicked()
		{
			if (_GM.inspectPanel.IsClosing)
				return;

			if (Me.Stats[Stat.ATTRACT_ENEMY] > 0)
			{
				_GM.UpdateInteractivityForCurrentPlayer(false);
				_GM.inspectPanel.Close(0f, () => {
					var cell = _GM.inspectPanel.InspectedEntity.GetParentCell();
					_GM.Submit_MoveOther(_GM.inspectPanel.InspectedEntity as Character, Me.GetParentCell());
				});
			}
		}
		void OnInspectPanelMiscClicked() { }
		void OnInspectPanelLootClicked()
		{
			if (_GM.inspectPanel.IsClosing)
				return;

			//UpdateInteractivityForCurrentPlayer(false);
			_GM.inspectPanel.Close(0f, () => OnLootCell(Me.GetParentCell()), false /*prevent UpdateInteractivityForCurrentPlayer(true) from being called*/);
		}
		#endregion


		void OnEntityLongPressed(Base.Entity entity)
		{
			if (entity is Ore)
				return;

			if (_GM.inspectPanel.IsShownOrShowing)
				return;

			if (_GM.IsPreBattle || _GM.IsIntraBattle)
				return;

			//if (!localPlayerManager.IsMyTurn)
			//	return;

			_GM.inspectPanel.Show(entity);
			// OnInspectedEntityChanged will be called after
		}

		void OnShifterSelected(Shifter shifter)
		{
			_GM.UpdateInteractivityForCurrentPlayer(false);
			_GM.Submit_ShiftDungeon(shifter);
		}

		void OnBuildTotemClick(TotemType totemType)
		{
			_GM.UpdateInteractivityForCurrentPlayer(false);
			_GM.Submit_BuildTotem(totemType);
		}

		void OnEndTurnClicked()
		{
			if (_GM.contextPanel.IsShown)
				_GM.contextPanel.Cancel();

			_GM.Submit_EndMyTurn();
		}

		void OnLeaveRoomClicked()
		{
			_GM.endTurnButton.interactable = _GM.leaveRoomButton.interactable = false;
			var result = PhotonNetwork.LeaveRoom();
			Debug.Log("LeaveRoom result =" + result);
		}
		#endregion


		#region GameplayManager events
		public override void OnNewTurn()
		{
			base.OnNewTurn();

#if UNITY_EDITOR
			if (Launcher.s_debug_HumanEndsTurnImmediately)
				_GM.Submit_EndMyTurn();
#endif

		}
		public override void OnReadyToAct(DBDEvent completedEvent, object eventData = null)
		{
			_GM.UpdateInteractivityForCurrentPlayer(true);
		}

		public override void OnPreStealCard()
		{
			_GM.InspectPlayerInventory(_SpawnManager.GetPlayerInstanceAtRelativePosition(Me, +1), true);
			_GM.inventoryPanel.SelectFirst(); // so the player can see he can choose one
		}

		public override void OnPostStealCard()
		{
			_GM.UpdateInteractivityForCurrentPlayer(true);
			_GM.InspectPlayerInventory(Me, true, true);
		}

		public override void OnChooseFirstIfToBattle()
		{ _GM.SetBattleCommandPanelStateNow(BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_BATTLE); }

		public override void OnChooseIfToBattleAfterCurrentPlayerPrefersNotTo() { OnChooseFirstIfToBattle(); }

		public override void OnAnswerTradeProposal(Player receiver, BaseItem donorItem, BaseItem receiverTradedItem)
		{
			_GM.contextPanel.Show(
					receiver.ShortNickNameWithColoredClass + ": I'll give you " + receiverTradedItem.name + " for " + donorItem.name,
					new string[] { "Deal!", "No, thanks" },
					null,
					result => _GM.Submit_AnswerTradeProposal(Me, donorItem, receiver, receiverTradedItem, result == 0),
					_Design.general.battlePanelTradeAutoDecisionTimeout
				);
		}

		public override void OnChooseIfToCancelBattle() { _GM.SetBattleCommandPanelStateNow(BattleCommandsPanel.ButtonsState.LOCAL_PLAYER_CHOOSING_IF_TO_CANCEL_BATTLE); }
		#endregion


		protected override void OnReadyToPlayBattleItems()
		{
			_GM.SetBattleCommandPanelStateNow(BattleCommandsPanel.ButtonsState.IN_BATTLE);
			IsMyTurnInBattleAndCanPlayCards = true;
			_GM.UpdateInventoryInteractability();
		}

		protected override bool Loot(params BaseItem[] items)
		{
			_GM.UpdateInventoryInteractability(); // non interactable until decision
			_GM.UnHighlightLastReachableCells(); // cannot move until decision

			int numItems = items.Length;
			CommonItem[] asCommonItems = Array.ConvertAll(items, i => i as CommonItem);

			string[] affirmatives = new string[numItems];
			bool[] enabledOptions = new bool[numItems + 1]; // +1 for the last "Leave" button
			enabledOptions[numItems] = true;

			int freeSlots = Me.Stats.FreeInventorySlots;
			// When checking if there are slots for the current item, we must treat the 
			// treasures in inventory that we'll use to buy it as empty slots, since they will be used in exchange
			bool atLeastOneItemAffordable = false;
			for (int i = 0; i < numItems; ++i)
			{
				string affirmative = "Take ";
				string affirmativeAppendix = "";
				var itemAsCommonItem = asCommonItems[i];
				int cost = 0;
				if (itemAsCommonItem != null)
				{
					cost = itemAsCommonItem.CurrentCost;
					if (cost > 0)
					{
						affirmative = "Buy ";
						affirmativeAppendix = " [cost " + cost + "]";
					}
				}

				bool affordable = cost <= Me.Stats.Treasures;
				atLeastOneItemAffordable = atLeastOneItemAffordable || affordable;
				enabledOptions[i] = affordable && freeSlots + cost > 0;
				affirmative += items[i].Design.name + affirmativeAppendix;
				affirmatives[i] = affirmative;
			}

			// No items are affordable => nothing to do => cancel
			if (!atLeastOneItemAffordable)
				return false;

			//// At this point, if the item costs, the player surely has enough treasures to buy it
			//// There's also room for the item

			var stringOptions = new List<string>(affirmatives);
			string cancelOptionText = "Leave";
			var monsterItem = Array.Find(items, i => i is MonsterItem);
			if (monsterItem)
				cancelOptionText += " & discard reward";

			stringOptions.Add(cancelOptionText);

			var showPos = Me.transform.position;
			Action showInitialPanelAction = null;
			showInitialPanelAction = () =>
			_GM.contextPanel.Show(
				showPos,
				"Looting:",
				stringOptions.ToArray(),
				enabledOptions,
				answer =>
				{
					// Cancelled; most probably, by a discard event, which will show again the dialog, with updated choices
					if (answer == -1)
						return;

					if (answer == numItems) // negative
					{
						if (!monsterItem)
						{
							OnFinishedHandlingItemsInCell();// true); // true=continuing callback flow, since the method's caller waited for us
							return;
						}

						// If there's a monster item, a confirmation is needed, because it'll be discarded immediately
						_GM.contextPanel.Show(
							showPos,
							"You're about to discard '" + monsterItem.Design.name + "'. Are you sure?",
							new string[] { "Yes", "No" },
							confirmationAnswer =>
							{
								// Cancelled; most probably, by a discard event, which will show again the dialog, with updated choices
								if (answer == -1)
									return;

								if (confirmationAnswer == 0)
								{
									OnFinishedHandlingItemsInCell();// true); // true=continuing callback flow, since the method's caller waited for us
								}
								else
									showInitialPanelAction(); // loop => from the beginning
							}
						);
					}
					else
						// Request to buy/pick-up the item
						_GM.Submit_PickUpItem(items[answer]);
				}
			);

			showInitialPanelAction();

			// Inspect us & possibly make cards discardable
			// make cards not playable during showing the context panel
			_GM.InspectPlayerInventory(Me, true);

			return true;
		}

		protected override void OnBeforeSubmitReadyToBattle()
		{
			IsMyTurnInBattleAndCanPlayCards = false;
			_GM.SetBattleCommandPanelStateNow(BattleCommandsPanel.ButtonsState.WAITING);
			_GM.UpdateInventoryInteractability();
		}
	}

}
