﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Items;
using System;

namespace Com.TheFallenGames.DBD.World
{
    public class Wall : MonoBehaviour
    {
		public GameObject wall;
		public GameObject pillar;

		public Wall prevWall;

		public void Activate()
		{
			wall.SetActive(true);
			pillar.SetActive(true);

			// Only activate the pillar & rotate it
			if (!prevWall.wall.activeSelf)
			{
				if (!prevWall.pillar.activeSelf)
				{
					prevWall.pillar.SetActive(true);
					prevWall.pillar.transform.Rotate(0f, -90f, 0f);
				}
			}
		}
	}
}
