﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;
using Com.TheFallenGames.DBD.Data;

namespace Com.TheFallenGames.DBD.World
{
    public class OuterCell : Cell, InputToEvent.IEventHandler
	{
		public Action<Shifter> onPlaced;

		public Shifter ContainingShifter
        {
            get { return _ContainingShifter; }
            set
            {
                if (_ContainingShifter != value)
                {
					//if (_ContainingShifter)
					//	_ContainingShifter.IsAtOuterCell = false;

					// Commented: shifters will be enabled manually when needed/allowed
					//// Enabling the previous shifter, if any
					//if (_ContainingShifter)
					//    _ContainingShifter.gameObject.SetActive(true);

					_ContainingShifter = value;
					if (_ContainingShifter)
					{
						//_ContainingShifter.IsAtOuterCell = true;

						//// Disabling the current shifter
						//_ContainingShifter.gameObject.SetActive(false);

                        // Moving to it on the xz plane
                        var pos = _ContainingShifter.transform.position;
						pos.y = transform.position.y;
						transform.position = pos;

						Row = _ContainingShifter.Row;
                        Column = _ContainingShifter.Column;
                    }
                }
            }
        }

		const float DRAG_START_MIN_SCREEN_DISTANCE_REQUIRED = 5f;

		public bool DragEnabled { get; set; }

        Shifter _ContainingShifter;
        GameObject _RaycastPlane;
        //bool _Dragging;
        BoxCollider _Collider;
		//bool _WaitingForPotentialDrag;
		//Vector2 _PointerPosOnStartPotentialDrag;
		Block _DraggedCloneBlock;
		Board _Board;
		Shifter _PreviousHighlightedShifter;
		DragState _DragState;
		int raycastPlaneLayerMask, shifterLayerMask;


        protected override void Start()
        {
			base.Start();

			DragEnabled = true;
			_Board = FindObjectOfType<Board>();
			_RaycastPlane = GameObject.Find("RaycastPlane");
            _Collider = GetComponent<BoxCollider>();
			raycastPlaneLayerMask = 1 << _RaycastPlane.layer;
			shifterLayerMask = 1 << LayerMask.NameToLayer("Shifter");
		}

		#region implementation for InputToEvent.IEventHandler
		public void OnPress(BaseEventData eventData) { if (DragEnabled) StartPotentialDrag(); }
		public void OnLongPress(BaseEventData eventData)
		{
			// Won't be considered a long-click by the Entity, because it moved too much
			if (InputToEvent.Instance.DragVector.magnitude > C.MAX_DRAG_SCREEN_DISTANCE_ALLOWED_TO_STILL_CONSIDER_LONG_CLICK_ON_ENTITY)
				return;

			if (_DragState == DragState.WAITING_FOR_POTENTIAL_DRAG) // no dragging if pressed enough to bring the cell context panel
				EndPotentialDrag();
			else if (_DragState == DragState.DRAGGING) // same: if the drag has already started, clear the cloned block and cancel it
				EndDrag(false);
		}
		public void OnPressCanceled(BaseEventData eventData) { }
		public void OnRelease(BaseEventData eventData) { OnPointerUp(); }
		public void OnReleaseAfterCanceled(BaseEventData eventData) { OnPointerUp(); }
		public void OnClick(BaseEventData eventData) { }
		public void OnDoubleClick(BaseEventData eventData) { }
		#endregion

		public void OnReachedTargetShifter()
		{
			DestroyDraggedCloneBlock();
		}

		//public override Cell.Descriptor CreateDescriptor() { return new Descriptor(this, ContainedBlock.CreateDescriptor()); }
		public override Cell.Descriptor CreateDescriptor() { return new Descriptor(this); }

		void OnPointerUp()
		{
			if (_DragState == DragState.WAITING_FOR_POTENTIAL_DRAG)
				EndPotentialDrag();
			else if (_DragState == DragState.DRAGGING)
				EndDrag(true);
		}

		void DestroyDraggedCloneBlock()
		{
			if (_DraggedCloneBlock)
			{
				Destroy(_DraggedCloneBlock.gameObject);
				_DraggedCloneBlock = null;
			}
		}

		void StartPotentialDrag() { _DragState = DragState.WAITING_FOR_POTENTIAL_DRAG; }
		void EndPotentialDrag() { _DragState = DragState.NONE; }
		void StartDrag() { _DragState = DragState.DRAGGING; }
		void EndDrag(bool proceedShifting)
		{
			_DragState = DragState.NONE;
			if (_PreviousHighlightedShifter)
			{
				_PreviousHighlightedShifter.SetPotentialShiftHighlight(false);

				if (proceedShifting)
				{
					_DraggedCloneBlock.transform.position = _PreviousHighlightedShifter.transform.position;

					if (onPlaced != null)
					{
						onPlaced(_PreviousHighlightedShifter);
						return; // don't destroy the block yet; wait for the moving animation to end
					}
				}
			}
			DestroyDraggedCloneBlock();
		}


		protected /*override*/ void Update()
		{
			if (_DragState != DragState.WAITING_FOR_POTENTIAL_DRAG)
				return;

			Vector2 dragVector = InputToEvent.Instance.DragVector;
			if (dragVector.magnitude > DRAG_START_MIN_SCREEN_DISTANCE_REQUIRED)
			{
				_DraggedCloneBlock = _Board.InstantiateBlock(transform, false);
				_DraggedCloneBlock.SetActiveWalls(Array.ConvertAll(ContainedBlock.Walls, w => (byte)w));
				EndPotentialDrag();
				StartDrag();
			}
		}

		void FixedUpdate()
		{
			if (_DragState != DragState.DRAGGING)
				return;

			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			Shifter closestShifter = null;
			if (Physics.Raycast(ray, out hit, 500, raycastPlaneLayerMask))
			{
				_DraggedCloneBlock.transform.position = hit.point + Vector3.up * .5f;
				Vector3 colliderCenterWorldSpace = _DraggedCloneBlock.transform.TransformPoint(_Collider.center); // it doesn't matter that we use the _Collider from the outer cell, since its center is in object space.
				var colliders = Physics.OverlapBox(colliderCenterWorldSpace, _Collider.size / 2, transform.rotation, shifterLayerMask, QueryTriggerInteraction.Collide);
				if (colliders.Length > 0)
				{
					BoxCollider closest = colliders[0] as BoxCollider;
					float dist, minDist = Vector3.Distance(colliderCenterWorldSpace, closest.transform.TransformPoint(closest.center));
					for (int i = 1; i < colliders.Length; ++i)
					{
						var curCol = colliders[i] as BoxCollider;
						dist = Vector3.Distance(colliderCenterWorldSpace, curCol.transform.TransformPoint(curCol.center));
						if (dist < minDist)
						{
							minDist = dist;
							closest = curCol;
						}
					}

					closestShifter = closest.GetComponent<Shifter>();
					closestShifter.SetPotentialShiftHighlight(true);
				}
			}

			if (_PreviousHighlightedShifter)
			{
				if (_PreviousHighlightedShifter == closestShifter) // nothing has changed
					return;

				_PreviousHighlightedShifter.SetPotentialShiftHighlight(false);
			}

			_PreviousHighlightedShifter = closestShifter; // can be null
		}


		enum DragState
		{
			NONE,
			WAITING_FOR_POTENTIAL_DRAG,
			DRAGGING
		}


		public new class Descriptor : Cell.Descriptor
		{
			public override int Row { get; set; }
			public override int Column { get; set; }


			public Descriptor(Cell cell) : this(cell, cell.ContainedBlock, cell.Row, cell.Column) { }

			Descriptor(Cell cell, Block block, int row, int column) : base(cell, block, row, column) { }


			public override Cell.Descriptor GetDeepClone() { return new Descriptor(describedCell, block, Row, Column); }
		}
	}
}