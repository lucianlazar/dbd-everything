﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.World
{
    public interface IGridElement
    {
        int Row { get; set; }
        int Column { get; set; }

        IGridElement Left { get; set; }
        IGridElement Above { get; set; }
        IGridElement Right { get; set; }
        IGridElement Below { get; set; }
    }
}
