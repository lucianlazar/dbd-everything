﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Items;
using System;

namespace Com.TheFallenGames.DBD.World
{
    public class HitEffectsHolder : MonoBehaviour
    {
		public static HitEffectsHolder Instance { get; private set; }

		void Awake()
		{
			Instance = this;
		}

		void OnDestroy()
		{
			Instance = null;
		}
	}
}
