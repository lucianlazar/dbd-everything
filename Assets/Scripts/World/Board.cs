﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Items;
using Com.TheFallenGames.DBD.Pathfinding;
using Com.TheFallenGames.DBD.Data;

namespace Com.TheFallenGames.DBD.World
{
    /// <summary>
    /// The grid is as follows (rows start from the bottom when considering their visual representation and in the scene)
    ///   C0 C1 C2..
    /// ..
    /// R2
    /// R1
    /// R0
    /// </summary>
    public class Board : MonoBehaviour
    {
        //public Action<bool> onDungeonShifted;

        public Block blockPrefab;
        public Block cloneBlockPrefab;
		public Cell cellPrefab;
        public OuterCell outerCellPrefab;
        public CornerCell cornerCellPrefab;
        public Shifter shiftPointPrefab;

        //public Cell OutsideCell { get; private set; }
        public Cell[][] Cells { get; private set; }
        
        public OuterCell OuterCell { get; private set; }
        public Cell[] AllCells { get; private set; }
        public Cell[] AllCellsIncludingOuterCell { get; private set; }
		public Shifter[] LeftShiftPoints { get; private set; }
        public Shifter[] TopShiftPoints { get; private set; }
        public Shifter[] RightShiftPoints { get; private set; }
        public Shifter[] BottomShiftPoints { get; private set; }
        public Shifter[] AllShiftPoints { get; private set; }
        public CornerCell TopLeftConerCell { get; set; }
        public CornerCell TopRightConerCell { get; set; }
        public CornerCell BottomRightConerCell { get; set; }
        public CornerCell BottomLeftConerCell { get; set; }
        public Dictionary<int, Dictionary<int, CornerCell>> CornerCellsIndexed { get; set; } // [-1, -1] = bottom-left one etc.
		public int NumRows { get { return _NumRows; } }
		public int NumColumns { get { return _NumColumns; } }

		public bool IsAnimatingDungeonShift { get; private set; }
		public bool IsAnimatingMove { get; set; }

        Transform _ShiftPointsParent;
        Animation8 _Anim;
        GridDesign _GridDesign;
		Dictionary<ulong, Shifter> _MapEncodedRowColToShifter = new Dictionary<ulong, Shifter>();

        // Cached from Design
        int _NumRows, _NumColumns;
		Descriptor _InitialDescriptor;

		
        void Awake()
        {
            _GridDesign = Design.Instance.grid;
            _NumRows = _GridDesign.numRows;
            _NumColumns = _GridDesign.numColumns;

            Cells = new Cell[_NumRows][];

			_ShiftPointsParent = transform.Find("ShiftPoints");
            LeftShiftPoints = new Shifter[_NumRows];
            TopShiftPoints = new Shifter[_NumColumns];
            RightShiftPoints = new Shifter[_NumRows];
            BottomShiftPoints = new Shifter[_NumColumns];

            Shifter shifter1, shifter2;

            AllCellsIncludingOuterCell = new Cell[_NumRows * _NumColumns + 1];

            for (int row = 0; row < _NumRows; ++row)
            {
                Cells[row] = new Cell[_NumColumns];
                LeftShiftPoints[row] = shifter1 = InstantiateShiftPoint(row, -1, Cells[row], Shifter.Direction.RIGHT);
                RightShiftPoints[row] = shifter2 = InstantiateShiftPoint(row, _NumColumns, Cells[row], Shifter.Direction.LEFT);
				_MapEncodedRowColToShifter[EncodeRowCol(row, -1)] = shifter1;
				_MapEncodedRowColToShifter[EncodeRowCol(row, _NumColumns)] = shifter2;
				//shifter1.OpposingShifter = shifter2;
				//shifter2.OpposingShifter = shifter1;

				var rowGO = new GameObject("R" + row);
                //rowGO.transform.position = transform.position;
                //rowGO.transform.rotation = transform.rotation;
                rowGO.transform.parent = transform;

                for (int col = 0; col < _NumColumns; ++col)
                {
                    var cellInstanceAsCell = InstantiateCell(rowGO.transform, row, col);
                    var blockInstanceAsBlock = InstantiateBlock(cellInstanceAsCell.transform);
                    cellInstanceAsCell.ContainedBlock = blockInstanceAsBlock;

                    Cells[row][col] = cellInstanceAsCell;

					cellInstanceAsCell.indexInCellsArray = row * _NumColumns + col;
					AllCellsIncludingOuterCell[cellInstanceAsCell.indexInCellsArray] = cellInstanceAsCell;
				}
            }
            Cell[] curColumn;

            // Instantiating shift points for columns (this can only be done after all the cells are initialized)
            for (int col = 0; col < _NumColumns; ++col)
            {
                curColumn = new Cell[_NumRows];
                for (int row = 0; row < _NumRows; ++row)
                {
                    curColumn[row] = Cells[row][col];
                }

                TopShiftPoints[col] = shifter1 = InstantiateShiftPoint(_NumRows, col, curColumn, Shifter.Direction.DOWN);
                BottomShiftPoints[col] = shifter2 = InstantiateShiftPoint(-1, col, curColumn, Shifter.Direction.UP);
				_MapEncodedRowColToShifter[EncodeRowCol(_NumRows, col)] = shifter1;
				_MapEncodedRowColToShifter[EncodeRowCol(-1, col)] = shifter2;
				//shifter1.OpposingShifter = shifter2;
				//shifter2.OpposingShifter = shifter1;
			}
            var list = new List<Shifter>(LeftShiftPoints.Length * 2 + TopShiftPoints.Length * 2);
            list.AddRange(LeftShiftPoints);
            list.AddRange(TopShiftPoints);
            list.AddRange(RightShiftPoints);
            list.AddRange(BottomShiftPoints);
            AllShiftPoints = list.ToArray();
			for (int i = 0; i < AllShiftPoints.Length; i++)
				AllShiftPoints[i].IndexInShiftersArray = i;

            //foreach (var v in TopShiftPoints)
            //    Debug.Log("Top" + v.transform.position);
            //foreach (var v in BottomShiftPoints)
            //    Debug.Log("BottomShiftPoints" + v.transform.position);

            OuterCell = InstantiateCell<OuterCell>(outerCellPrefab.gameObject, transform, _NumRows / 2, _NumColumns);
            OuterCell.ContainedBlock = InstantiateBlock(OuterCell.transform);
            OuterCell.ContainingShifter = RightShiftPoints[_NumRows / 2];
			OuterCell.indexInCellsArray = _NumRows * _NumColumns;
			AllCellsIncludingOuterCell[OuterCell.indexInCellsArray] = OuterCell;

			for (int i = 0; i < AllCellsIncludingOuterCell.Length - 1; ++i)
			{
				var c = AllCellsIncludingOuterCell[i];
				c.left = GetRegularCellOrNull(c.Row, c.Column - 1);
				c.topLeft = GetRegularCellOrNull(c.Row + 1, c.Column - 1);
				c.top = GetRegularCellOrNull(c.Row + 1, c.Column);
				c.topRight = GetRegularCellOrNull(c.Row + 1, c.Column + 1);
				c.right = GetRegularCellOrNull(c.Row, c.Column + 1);
				c.bottomRight = GetRegularCellOrNull(c.Row - 1, c.Column + 1);
				c.bottom = GetRegularCellOrNull(c.Row - 1, c.Column);
				c.bottomLeft = GetRegularCellOrNull(c.Row - 1, c.Column - 1);
			}

			CornerCellsIndexed = new Dictionary<int, Dictionary<int, CornerCell>>();
            CornerCellsIndexed[-1] = new Dictionary<int, CornerCell> ();
            CornerCellsIndexed[_NumRows] = new Dictionary<int, CornerCell> ();
            CornerCellsIndexed[_NumRows][-1] = TopLeftConerCell = InstantiateCell<CornerCell>(cornerCellPrefab.gameObject, transform, _NumRows, -1);
            CornerCellsIndexed[_NumRows][_NumColumns] = TopRightConerCell = InstantiateCell<CornerCell>(cornerCellPrefab.gameObject, transform, _NumRows, _NumColumns);
            CornerCellsIndexed[-1][_NumColumns] = BottomRightConerCell = InstantiateCell<CornerCell>(cornerCellPrefab.gameObject, transform, -1, _NumColumns);
            CornerCellsIndexed[-1][-1] = BottomLeftConerCell = InstantiateCell<CornerCell>(cornerCellPrefab.gameObject, transform, -1, -1);

            _Anim = new Animation8(InterpolationFunction8.SIN_SLOWOUT);
            _Anim.AnimationSmoothness = AnimationSmoothnessLevel8.TWO;
            _Anim.DontDestroyOnNaturalEnd = true;
            var animProps = new AnimatedPropertiesGroup8();
            animProps.AnimateFloats = true;
            _Anim.AddAnimatedPropertiesGroup(animProps);
        }

		void Update()
		{
			if (_InitialDescriptor == null)
				_InitialDescriptor = CreateDescriptor();
		}

		public Descriptor CreateDescriptor(params Cell[] cellsContainingTrackedBlocks)
		{ return new Descriptor(this, cellsContainingTrackedBlocks); }
		//public Descriptor CreateDescriptor(Descriptor.PathfindingPolicy pathfindingPolicy, params Cell[] cellsContainingTrackedBlocks)
		//{ return new Descriptor(this, pathfindingPolicy, cellsContainingTrackedBlocks); }

		//public Descriptor CreateDescriptor(params Cell[] cellsContainingTrackedBlocks)
		//{ return CreateDescriptor(Descriptor.PathfindingPolicy.CONSUME_GHOST_MOVES_AS_SOON_AS_POSSIBLE_REGARDLESS_IF_NEEDED_OR_NOT, cellsContainingTrackedBlocks); }

		/// <summary> if ==true: Not enabling the shifter where the outer cell is, if exceptShifterAtOuterCell is also true </summary>
		public void SetShiftingEnabled(bool enabled)//, bool exceptShifterAtOuterCell)
        {
			OuterCell.DragEnabled = enabled;

			//foreach (var shifter in AllShiftPoints)
			//             if (!exceptShifterAtOuterCell || shifter.Row != OuterCell.Row || shifter.Column != OuterCell.Column)
			//                 shifter.gameObject.SetActive(enabled);
		}

		//internal int GetCellIndex(Cell cell) { return Array.IndexOf(AllCellsIncludingOuterCell, cell); }
		//internal int GetShiterIndex(Shifter shifter) { return Array.IndexOf(AllShiftPoints, shifter); }
		internal Shifter GetOpposingShiter(Shifter shifter) { return GetOpposingShiter(shifter.IndexInShiftersArray); }
		internal int GetOpposingShiterIndex(int shifterIndex) { return (shifterIndex + (_NumRows + _NumColumns)) % AllShiftPoints.Length; }
		internal Shifter GetOpposingShiter(int shifterIndex) { return AllShiftPoints[GetOpposingShiterIndex(shifterIndex)]; }
		internal Shifter GetShiter(int row, int column) { return _MapEncodedRowColToShifter[EncodeRowCol(row, column)]; }
		//{
		//	if (row == -1)
		//		return BottomShiftPoints[column];
		//	if (row == _NumRows)
		//		return TopShiftPoints[column];
		//	if (column == -1)
		//		return LeftShiftPoints[row];
		//	if (column == _NumColumns)
		//		return RightShiftPoints[row];

		//	throw new UnityException("row, column: " + row + ", " + column + " do not correspond to an outer space. numRows, numCols: " + _NumRows + ", " + _NumColumns);
		//}

		internal void Anim_ShiftDungeon(Shifter shifter, Action onDone)
        {
			IsAnimatingDungeonShift = true;
			// Already there => place it directly
			if (OuterCell.ContainingShifter == shifter)
			{
				OuterCell.OnReachedTargetShifter();
				Anim_PlaceOuterCell(onDone);
				return;
			}

            // Move to it and then place the cell
            Anim_MoveOuterCellToShifter(shifter, () =>
            {
                OuterCell.ContainingShifter = shifter; // update the container
				OuterCell.OnReachedTargetShifter();
				Anim_PlaceOuterCell(onDone);
            });
        }

        internal void BuildWalls(List<byte[]> wallsAndTheirPositions)
        {
            for (int i = 0; i < wallsAndTheirPositions.Count; ++i)
                AllCellsIncludingOuterCell[i].ContainedBlock.SetActiveWalls(wallsAndTheirPositions[i]);
        }

		Cell GetRegularCellOrNull(int row, int col) { if (row < 0 || row >= _NumRows || col < 0 || col >= _NumColumns) return null; return Cells[row][col]; }

        IGridElement GetOuterCellNextCheckPointWhenMovingToShifter(Shifter shifter)
        {
            // We're sideways (left/right)
            if (OuterCell.Column == -1 || OuterCell.Column == _NumColumns)
            {
                // Shifter is on the same side as we => he is next
                if (shifter.Column == OuterCell.Column)
                    return shifter;

                // Shifter is top/bottom
                if (shifter.Row == _NumRows || shifter.Row == -1)
                {
                    // Shifter is on the same row as we and we're at top/bottom => he is next
                    if (shifter.Row == OuterCell.Row)
                        return shifter;

                    // Shifter is on another top/bottom row => go up/down
                    return CornerCellsIndexed[shifter.Row][OuterCell.Column];
                }

                // Shifter is sideways and on the opposite side and not top/bottom

                // The outer cell can go directly left/right => do so
                if (OuterCell.Row == -1 || OuterCell.Row == _NumRows)
                    return CornerCellsIndexed[OuterCell.Row][shifter.Column];

                // The outer cell is also not top/bottom => go up/down after calculating what's the shortest path 

                int sumRows = OuterCell.Row + shifter.Row;
                int maxSumToGoBottom = _NumRows - 1;

                // Distance using bottom corner is less
                if (sumRows <= maxSumToGoBottom)
                    return CornerCellsIndexed[-1][OuterCell.Column];

                // Distance using top corner is less
                return CornerCellsIndexed[_NumRows][OuterCell.Column];
            }

            // We're top/bottom, but in no corner

            // Shifter is on the same rown as we => he is next
            if (shifter.Row == OuterCell.Row)
                return shifter;

            // Shifter is left/right
            if (shifter.Column == -1 || shifter.Column == _NumColumns)
            {
                // Shifter is on another left/right column => go up/down
                return CornerCellsIndexed[OuterCell.Row][shifter.Column];
            }

            // Shifter is top/bottom and on the opposite side and not left/right => go left/right after calculating what's the shortest path 

            int sumCols = OuterCell.Column + shifter.Column;
            int maxSumToGoLeft = _NumColumns - 1;

            // Distance using left corner is less
            if (sumCols <= maxSumToGoLeft)
                return CornerCellsIndexed[OuterCell.Row][-1];

            // Distance using right corner is less
            return CornerCellsIndexed[OuterCell.Row][_NumColumns];
        }

		ulong EncodeRowCol(int row, int col) { unchecked { return (ulong)(uint)row << 32 | (uint)col; } }

		// TODO extract these into the cell's class
		void Anim_MoveOuterCellToShifter(Shifter shifter, Action onDone)
        {
            if (_Anim.IsPlaying)
            {
                Debug.LogError("_Anim.IsPlaying in BeginPlacingOuterCell (???)");
                return;
            }

            MoveOuterCellToShifterOneCheckPointAtATimeRec(shifter, GetOuterCellNextCheckPointWhenMovingToShifter(shifter), onDone);
        }

        void MoveOuterCellToShifterOneCheckPointAtATimeRec(Shifter targetShifter, IGridElement nextCheckpoint, Action onDone)
        {
            //Debug.Log("moving to " + (nextCheckpoint as UnityEngine.Object).name);
            if (nextCheckpoint is Shifter && (nextCheckpoint as Shifter) == targetShifter)
            {
                // After reaching the next target (the final one), the animation is done
                MoveOuterCellToNextCheckpoint(targetShifter, onDone);
                return;
            }

            // The next checkpoint is not the shifter; call the current method again (recursively) with the checkpoint after <nextCheckpoint> when it reached the current <nextCheckpoint>
            MoveOuterCellToNextCheckpoint(
                nextCheckpoint, 
                () => MoveOuterCellToShifterOneCheckPointAtATimeRec(
                        targetShifter, 
                        GetOuterCellNextCheckPointWhenMovingToShifter(targetShifter), 
                        onDone
                    )
                );
        }

        void MoveOuterCellToNextCheckpoint(IGridElement element, Action onDone)
        {
            if (_Anim.IsPlaying)
            {
                Debug.LogError("_Anim.IsPlaying in MoveOuterCellTo (???)");
                return;
            }

            _Anim.AnimationTime = .5f;
            var props = _Anim.GetAnimatedPropertiesGroup(0);
            props.AnimateVectors3 = true;
            props.InitialVector3 = OuterCell.transform.position;
            props.TargetVector3 = GetGlobalPosFromDiscrete(element.Row, element.Column);

            // Move animations
            props.OnAnimationProgressCallback = () =>
            {
                OuterCell.transform.position = props.CurrentVector3;
                return true;
            };

            // Start and resolving links after anim is done
            _Anim.Start(() =>
            {
                // Needed to compute the next checkpoint
                OuterCell.Row = element.Row;
                OuterCell.Column = element.Column;

                props.AnimateVectors3 = false;
                onDone();
            });
        }

        void Anim_PlaceOuterCell(Action onDone)
        {
            if (_Anim.IsPlaying)
            {
                Debug.LogError("_Anim.IsPlaying in OnOuterCellPlaced (???)");
                return;
            }

            Shifter shifter = OuterCell.ContainingShifter;

            var props = _Anim.GetAnimatedPropertiesGroup(0);
            _Anim.AnimationTime = 1f;
            props.InitialFirstFloat = 0f;
            props.TargetFirstFloat = 1f;

            // Move animations
            Vector3[] startingPositions = Array.ConvertAll(shifter.workingCells, c => c.ContainedBlock.transform.position);
            Vector3[] endingPositions = Array.ConvertAll(startingPositions, p => p + shifter.ShiftDiscreteValue * _GridDesign.distanceBetweenCells);
            Vector3 outerBlockStart = OuterCell.ContainedBlock.transform.position;
            Vector3 outerBlockEnd = outerBlockStart + shifter.ShiftDiscreteValue * _GridDesign.distanceBetweenCells;
            props.OnAnimationProgressCallback = () =>
            {
                for (int i = 0; i < shifter.workingCells.Length; ++i)
                    shifter.workingCells[i].ContainedBlock.transform.position = Vector3.Lerp(startingPositions[i], endingPositions[i], props.CurrentFirstFloat);

                OuterCell.ContainedBlock.transform.position = Vector3.Lerp(outerBlockStart, outerBlockEnd, props.CurrentFirstFloat);

                return true;
            };

            // Start and resolving links after anim is done
            _Anim.Start(() =>
            {
                int increment = shifter.ExitIndex == 0 ? 1 : -1;
                var exitingBlock = shifter.ExitCell.ContainedBlock;
                for (int i = shifter.ExitIndex; ; i += increment)
                {
					// this will be handled outside
					if (i == shifter.EntryIndex)
							break;

                    shifter.workingCells[i].ContainedBlock = shifter.workingCells[i + increment].ContainedBlock;
                }

                // Placing the out block inside the entry cell
                shifter.EntryCell.ContainedBlock = OuterCell.ContainedBlock;

                //// Moving the outer cell AFTER the exit cell
                //OuterCell.transform.position = shifter.ExitCell.transform.position + shifter.ShiftDiscreteValue * distanceBetweenCells;

                // Assigning the exitted block (and thus, placing it at) to the outer cell
                OuterCell.ContainedBlock = exitingBlock;

                // The outer cell is now at the opposing shifter's position
                //OuterCell.ContainingShifter = shifter.OpposingShifter;
                OuterCell.ContainingShifter = GetOpposingShiter(shifter.IndexInShiftersArray);

				IsAnimatingDungeonShift = false;
				if (onDone != null)
                    onDone();
            });
        }

        Vector3 GetGlobalPosFromDiscrete(int row, int column)
        {
            return transform.position
                + new Vector3(column * _GridDesign.distanceBetweenCells, 0, row * _GridDesign.distanceBetweenCells)
                - new Vector3(_GridDesign.distanceBetweenCells * _NumColumns, 0, _GridDesign.distanceBetweenCells * _NumRows) / 2f;
        }

        public Block InstantiateBlock(Transform cellTransform, bool realBlock = true)
        {
			if (realBlock)
				return (Instantiate(blockPrefab.gameObject, cellTransform.position, cellTransform.rotation, cellTransform) as GameObject).GetComponent<Block>();

			return (Instantiate(cloneBlockPrefab.gameObject, cellTransform.position, cellTransform.rotation) as GameObject).GetComponent<Block>();
        }
        Cell InstantiateCell(Transform parent, int row, int col) { return InstantiateCell<Cell>(cellPrefab.gameObject, parent, row, col); }
        T InstantiateCell<T>(GameObject prefab, Transform parent, int row, int col) where T : Cell
        {
            var cell = (Instantiate(prefab, GetGlobalPosFromDiscrete(row, col), Quaternion.identity, parent) as GameObject).GetComponent<T>();
            cell.Row = row;
            cell.Column = col;
            cell.name = "R" + row + "C" + col;

            return cell;
        }
        Shifter InstantiateShiftPoint(int row, int col, Cell[] workingCells, Shifter.Direction direction)
        {
            var sp = (Instantiate(shiftPointPrefab.gameObject, GetGlobalPosFromDiscrete(row, col), Quaternion.identity, _ShiftPointsParent) as GameObject).GetComponent<Shifter>();
            sp.name = "ShiftPoint (" + row + "," + col + ")";
            sp.Row = row;
            sp.Column = col;
            sp.workingCells = workingCells;
            sp.shiftDirection = direction;

			var pos = sp.transform.position;
			pos.y = _ShiftPointsParent.position.y;
			sp.transform.position = pos;

			return sp;
        }


		public class Descriptor
		{
			public OuterCell.Descriptor outerCell;
			public Cell.Descriptor[] allCellsIncludingOuter;
			//public Cell.Descriptor[][] cellsMatrix;
			//public Shifter.Descriptor[] shifters;
			public Board board;
			public int NumRows { get { return board._NumRows; } }
			public int NumColumns { get { return board._NumColumns; } }
			public Cell.Descriptor[] cellsContainingTrackedBlocks;
			//public PathfindingPolicy pathfindingPolicy;


			//public enum PathfindingPolicy
			//{
			//	NONE = 0,
			//	CONSUME_GHOST_MOVES_AS_SOON_AS_POSSIBLE_REGARDLESS_IF_NEEDED_OR_NOT = 1 << 0,

			//}
			//public override int GetHashCode()
			//{
			//	int h = base.GetHashCode();
			//	h ^= outerCell.GetHashCode();
			//	foreach (var item in allCellsIncludingOuter)
			//		h ^= item.GetHashCode();
			//	foreach (var item in cellsContainingTrackedBlocks)
			//		h ^= item.GetHashCode();

			//	return h;
			//}


			public Descriptor(Board board, /*PathfindingPolicy pathfindingPolicy, */params Cell[] cellsContainingTrackedBlocks)
			{
				this.board = board;
				//this.pathfindingPolicy = pathfindingPolicy;
				allCellsIncludingOuter = new Cell.Descriptor[board.AllCellsIncludingOuterCell.Length];
				for (int i = 0; i < allCellsIncludingOuter.Length; i++)
					allCellsIncludingOuter[i] = board.AllCellsIncludingOuterCell[i].Desc;
				outerCell = allCellsIncludingOuter[allCellsIncludingOuter.Length - 1] as OuterCell.Descriptor; // last one is outer
				this.cellsContainingTrackedBlocks = new Cell.Descriptor[cellsContainingTrackedBlocks.Length];
				for (int i = 0; i < cellsContainingTrackedBlocks.Length; i++)
					this.cellsContainingTrackedBlocks[i] = allCellsIncludingOuter[cellsContainingTrackedBlocks[i].indexInCellsArray];
				CreateCellsMatrix();

				//shifters = new Shifter.Descriptor[board.AllShiftPoints.Length];
				//for (int i = 0; i < board.AllShiftPoints.Length; i++)
				//	shifters[i] = board.AllShiftPoints[i].CreateDescriptor(this);
				//AssignOpposingShifters();
			}

			Descriptor(Board board, /*PathfindingPolicy pathfindingPolicy,*/ Cell.Descriptor[] cellsContainingTrackedBlocks, OuterCell.Descriptor outerCell, Cell.Descriptor[] allCellsIncludingOuter)//, Shifter.Descriptor[] shifters)
			{
				this.board = board;
				//this.pathfindingPolicy = pathfindingPolicy;
				this.outerCell = outerCell;
				this.allCellsIncludingOuter = allCellsIncludingOuter;
				this.cellsContainingTrackedBlocks = cellsContainingTrackedBlocks;
				CreateCellsMatrix();
				//this.shifters = shifters;
				//AssignOpposingShifters();
			}

			void CreateCellsMatrix()
			{
				//cellsMatrix = new Cell.Descriptor[NumRows][];
				//Cell.Descriptor desc;
				//for (int i = 0; i < NumRows; i++)
				//{
				//	var arr = new Cell.Descriptor[NumColumns];
				//	for (int j = 0; j < NumColumns; j++)
				//	{
				//		desc = allCellsIncludingOuter[i * NumColumns + j];
				//		arr[j] = desc;
				//	}
				//	cellsMatrix[i] = arr;
				//}
			}

			public Descriptor GetShifted(int shifterIndex)
			{
				var shifted = GetDeepClone();
				//shifted.Shift(shifters[shifterIndex]);
				shifted.Shift(shifterIndex);

				return shifted;
			}

			public Descriptor GetDeepClone()
			{
				var copyAllCells = new Cell.Descriptor[allCellsIncludingOuter.Length];
				for (int i = 0; i < copyAllCells.Length; i++)
				{
					var c = allCellsIncludingOuter[i].GetDeepClone();
					copyAllCells[i] = c;
				}
				OuterCell.Descriptor outerCellCopy = copyAllCells[copyAllCells.Length - 1] as OuterCell.Descriptor; // outer is last

				var copyCellsContainingTrackedBlocks = new Cell.Descriptor[cellsContainingTrackedBlocks.Length];
				for (int i = 0; i < copyCellsContainingTrackedBlocks.Length; ++i)
					copyCellsContainingTrackedBlocks[i] = copyAllCells[cellsContainingTrackedBlocks[i].describedCell.indexInCellsArray];

				//var copyShifters = new Shifter.Descriptor[shifters.Length];
				//for (int i = 0; i < copyShifters.Length; i++)
				//	copyShifters[i] = shifters[i].GetDeepClone(copyAllCells);

				return new Descriptor(board, /*pathfindingPolicy,*/ copyCellsContainingTrackedBlocks, outerCellCopy, copyAllCells);//, copyShifters);
			}

			//void AssignOpposingShifters()
			//{
			//	// Assign the opposing shifter for all after they were created
			//	for (int i = 0; i < shifters.Length; i++)
			//	{
			//		var shifter = shifters[i];
			//		for (int j = 0; j < shifters.Length; j++)
			//		{
			//			var shifter2 = shifters[j];

			//			if (shifter.describedShifter.OpposingShifter == shifter2.describedShifter)
			//			{
			//				shifter.opposingShifter = shifter2;
			//				break;
			//			}
			//		}
			//	}
			//}

			public Cell.Descriptor GetExistingCellDesc(Cell cell)
			{
				return allCellsIncludingOuter[cell.indexInCellsArray];
				//Cell.Descriptor r = null;
				//for (int i = 0; i < allCellsIncludingOuter.Length; i++)
				//{
				//	r = allCellsIncludingOuter[i];
				//	if (r.describedCell == cell)
				//		return r;
				//}
				//return null;
			}


			public Cell.Descriptor GetOuterCellLeft() { return GetCell(outerCell.Row, outerCell.Column - 1); }
			public Cell.Descriptor GetOuterCellTopLeft() { return GetCell(outerCell.Row + 1, outerCell.Column - 1); }
			public Cell.Descriptor GetOuterCellBottomLeft() { return GetCell(outerCell.Row - 1, outerCell.Column - 1); }
			public Cell.Descriptor GetOuterCellRight() { return GetCell(outerCell.Row, outerCell.Column + 1); }
			public Cell.Descriptor GetOuterCellTopRight() { return GetCell(outerCell.Row + 1, outerCell.Column + 1); }
			public Cell.Descriptor GetOuterCellBottomRight() { return GetCell(outerCell.Row - 1, outerCell.Column + 1); }
			public Cell.Descriptor GetOuterCellTop() { return GetCell(outerCell.Row + 1, outerCell.Column); }
			public Cell.Descriptor GetOuterCellBottom() { return GetCell(outerCell.Row - 1, outerCell.Column); }

			public Cell.Descriptor GetCell(int row, int column) { return allCellsIncludingOuter[row * NumColumns + column]; }

			public Cell.Descriptor GetCellWithBlock(Block block)
			{
				Cell.Descriptor cell;
				for (int i = 0; i < allCellsIncludingOuter.Length; i++)
				{
					cell = allCellsIncludingOuter[i];
					if (cell.block == block)
						return cell;
				}

				return null;
			}

			/// <summary>
			/// Diagonal blocks are considered reachable
			/// The outer cell is also returned if it's near
			/// </summary>
			public void GetSurroundingCells(Cell.Descriptor originDesc, bool includeDiagonal, bool includeObstructedByWalls, List<OneCellMovementSolution> results, List<Cell.Descriptor> ignored)
			{
				Cell.Descriptor dc;
				Cell originCell = originDesc.describedCell;
				var originBlock = originDesc.block;


				// If the origin is the outer cell, this is treated manually
				if (originDesc == outerCell)
				{
					if (originDesc.Column == -1)
					{
						dc = GetOuterCellRight();
						AddOneCellMovementSolutionIfPossible_Hor(dc, originBlock, dc.block, includeObstructedByWalls, ignored, results);

						if (includeDiagonal)
						{
							if (originDesc.Row > 0)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellBottomRight(), ignored, results);
							if (originDesc.Row < NumRows - 1)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellTopRight(), ignored, results);
						}
					}
					else if (originDesc.Column == NumColumns)
					{
						dc = GetOuterCellLeft();
						AddOneCellMovementSolutionIfPossible_Hor(dc, dc.block, originBlock, includeObstructedByWalls, ignored, results);

						if (includeDiagonal)
						{
							if (originDesc.Row > 0)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellBottomLeft(), ignored, results);
							if (originDesc.Row < NumRows - 1)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellTopLeft(), ignored, results);
						}
					}
					else if (originDesc.Row == -1)
					{
						dc = GetOuterCellTop();
						AddOneCellMovementSolutionIfPossible_Vert(dc, originBlock, dc.block, includeObstructedByWalls, ignored, results);

						if (includeDiagonal)
						{
							if (originDesc.Column > 0)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellTopLeft(), ignored, results);
							if (originDesc.Column < NumColumns - 1)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellTopRight(), ignored, results);
						}
					}
					else
					{
						// top row
						dc = GetOuterCellBottom();
						AddOneCellMovementSolutionIfPossible_Vert(dc, dc.block, originBlock, includeObstructedByWalls, ignored, results);

						if (includeDiagonal)
						{
							if (originDesc.Column > 0)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellBottomLeft(), ignored, results);
							if (originDesc.Column < NumColumns - 1)
								AddOneCellMovementSolutionIfPossible_Diag(GetOuterCellBottomRight(), ignored, results);
						}
					}

					return;
				}


				// Also search for- and eventually include the outer cell
				int dColToOuter = originDesc.DeltaColumnTo(outerCell);
				if (dColToOuter == 0)
				{
					int dRowToOuter = originDesc.DeltaRowTo(outerCell);
					if (dRowToOuter == 1) // outer is above
						AddOneCellMovementSolutionIfPossible_Vert(outerCell, originDesc.block, outerCell.block, includeObstructedByWalls, ignored, results);
					else if (dRowToOuter == -1) // outer is below
						AddOneCellMovementSolutionIfPossible_Vert(outerCell, outerCell.block, originDesc.block, includeObstructedByWalls, ignored, results);
				}
				else if (dColToOuter == -1)
				{
					int dRowToOuter = originDesc.DeltaRowTo(outerCell);
					if (dRowToOuter == 0) // outer is left
						AddOneCellMovementSolutionIfPossible_Hor(outerCell, outerCell.block, originDesc.block, includeObstructedByWalls, ignored, results);
					else if (includeDiagonal)
					{
						if (dRowToOuter == 1 || dRowToOuter == -1) // outer is top-left or bottom-left
							AddOneCellMovementSolutionIfPossible_Diag(outerCell, ignored, results);
					}
				}
				else if (dColToOuter == 1)
				{
					int dRowToOuter = originDesc.DeltaRowTo(outerCell);
					if (dRowToOuter == 0) // outer is right
						AddOneCellMovementSolutionIfPossible_Hor(outerCell, originDesc.block, outerCell.block, includeObstructedByWalls, ignored, results);
					else if (includeDiagonal)
					{
						if (dRowToOuter == 1 || dRowToOuter == -1) // outer is top-right or bottom-right
							AddOneCellMovementSolutionIfPossible_Diag(outerCell, ignored, results);
					}
				}


				// Left-center,-down,-up cells
				if (originDesc.Column > 0)
				{
					//cell = cellsMatrix[origin.Row][origin.Column - 1];
					dc = allCellsIncludingOuter[originCell.left.indexInCellsArray];
					AddOneCellMovementSolutionIfPossible_Hor(dc, dc.block, originBlock, includeObstructedByWalls, ignored, results);

					if (includeDiagonal)
					{
						if (originCell.bottomLeft)
						{
							dc = allCellsIncludingOuter[originCell.bottomLeft.indexInCellsArray];
							if (ignored == null || !ignored.Contains(dc))
								results.Add(new OneCellMovementSolution(dc, SingleCellMoveCost.DIAGONAL_MOVE));
						}

						if (originCell.bottomRight)
						{
							dc = allCellsIncludingOuter[originCell.bottomRight.indexInCellsArray];
							if (ignored == null || !ignored.Contains(dc))
								results.Add(new OneCellMovementSolution(dc, SingleCellMoveCost.DIAGONAL_MOVE));
						}
					}
				}

				// Right-center,-down,-up cells
				if (originDesc.Column < NumColumns - 1)
				{
					//cell = cellsMatrix[origin.Row][origin.Column + 1];
					dc = allCellsIncludingOuter[originCell.right.indexInCellsArray];
					AddOneCellMovementSolutionIfPossible_Hor(dc, originBlock, dc.block, includeObstructedByWalls, ignored, results);

					if (includeDiagonal)
					{
						if (originDesc.Row > 0)
						{
							//cell = cellsMatrix[origin.Row - 1][origin.Column + 1];
							dc = allCellsIncludingOuter[originCell.bottomRight.indexInCellsArray];
							if (ignored == null || !ignored.Contains(dc))
								results.Add(new OneCellMovementSolution(dc, SingleCellMoveCost.DIAGONAL_MOVE));
						}
						if (originDesc.Row < NumRows - 1)
						{
							//cell = cellsMatrix[origin.Row + 1][origin.Column + 1];
							dc = allCellsIncludingOuter[originCell.topRight.indexInCellsArray];
							if (ignored == null || !ignored.Contains(dc))
								results.Add(new OneCellMovementSolution(dc, SingleCellMoveCost.DIAGONAL_MOVE));
						}
					}
				}

				// Down cell
				if (originCell.bottom)
				{
					//cell = cellsMatrix[origin.Row - 1][origin.Column];
					dc = allCellsIncludingOuter[originCell.bottom.indexInCellsArray];
					AddOneCellMovementSolutionIfPossible_Vert(dc, dc.block, originBlock, includeObstructedByWalls, ignored, results);
				}

				// Up cell
				if (originCell.top)
				{
					//cell = cellsMatrix[origin.Row + 1][origin.Column];
					dc = allCellsIncludingOuter[originCell.top.indexInCellsArray];
					AddOneCellMovementSolutionIfPossible_Vert(dc, originBlock, dc.block, includeObstructedByWalls, ignored, results);
				}
			}

			public void ComputeBestPaths(PathfindingContext context) { AStar(context, PathfindingSolution.CreateInitial(context.origin)); }

			//void Shift(Shifter.Descriptor shifter)
			//void TT(Cell.Descriptor donor, Block donorBlock, Cell.Descriptor receiver)
			//{
			//	int idx;
			//	if ((idx = Array.IndexOf(cellsContainingTrackedBlocks, donor)) != -1)
			//	{
			//		cellsContainingTrackedBlocks[idx] = receiver;
			//	}
			//	receiver.block = donorBlock;
			//}

			void Shift(int shifterIndex)
			{
				var shifter = board.AllShiftPoints[shifterIndex];
				int shiftingDir = shifter.EntryIndex == 0 ? 1 : -1;
				var exitCellDesc = allCellsIncludingOuter[shifter.ExitCell.indexInCellsArray];
				var entryCellDesc = allCellsIncludingOuter[shifter.EntryCell.indexInCellsArray];
				var exitingBlock = exitCellDesc.block;
				int incrementInLoop = -shiftingDir; // going from exit to entry


				// Transfer the blocks
				for (int i = shifter.ExitIndex; ;)// i += incrementInLoop)
				{
					// this will be handled outside
					if (i == shifter.EntryIndex)
						break;

					var curCell = allCellsIncludingOuter[shifter.workingCells[i].indexInCellsArray];
					//var followingCell = allCellsIncludingOuter[shifter.workingCells[i + incrementInLoop].IndexInCellsArray];
					var followingCell = allCellsIncludingOuter[shifter.workingCells[i += incrementInLoop].indexInCellsArray];
					curCell.block = followingCell.block;
					//TT(followingCell, followingCell.block, curCell);
					//curCell.block = followingCell.block;

					//int idx;
					//if ((idx = Array.IndexOf(cellsContainingTrackedBlocks, followingCell)) != -1)
					//	cellsContainingTrackedBlocks[idx] = curCell;
				}

				// Placing the out block inside the entry cell
				entryCellDesc.block = outerCell.block;
				//TT(outerCell, outerCell.block, entryCellDesc);

				// Assigning the exitted block to the outer cell
				outerCell.block = exitingBlock;
				//TT(exitCellDesc, exitingBlock, outerCell);

				// The outer cell is now at the opposing shifter's position
				var opposingShifter = board.GetOpposingShiter(shifter.IndexInShiftersArray);
				outerCell.Row = opposingShifter.Row;
				outerCell.Column = opposingShifter.Column;

				// Change the tracked cells to the ones that contain their block, in case that block will be shifted
				for (int i = 0; i < cellsContainingTrackedBlocks.Length; ++i)
				{
					var cell = cellsContainingTrackedBlocks[i];
					if (cell == outerCell) // the outer cell's block is now inside the entry cell
						cellsContainingTrackedBlocks[i] = entryCellDesc;
					else if (cell == exitCellDesc) // the exit cell's block is now inside the outer cell
						cellsContainingTrackedBlocks[i] = outerCell;
					else if (cell.Row == shifter.Row) // left/right shifter
						cellsContainingTrackedBlocks[i] = allCellsIncludingOuter[cell.describedCell.indexInCellsArray + shiftingDir];
					else if (cell.Column == shifter.Column) // bottom/top shifter
						cellsContainingTrackedBlocks[i] = allCellsIncludingOuter[cell.describedCell.indexInCellsArray + shiftingDir * NumColumns];
				}
			}

			void AddOneCellMovementSolutionIfPossible_Hor(Cell.Descriptor destCell, Block left, Block right, bool includeObstructedByWalls, List<Cell.Descriptor> ignored, List<OneCellMovementSolution> results)
			{
				if (ignored != null && ignored.Contains(destCell))
					return;

				if (left.IsObstructedAtRightBy(right))
				{
					if (includeObstructedByWalls)
						results.Add(new OneCellMovementSolution(destCell, SingleCellMoveCost.THROUGH_WALL_MOVE));
				}
				else
					results.Add(new OneCellMovementSolution(destCell));
			}

			void AddOneCellMovementSolutionIfPossible_Vert(Cell.Descriptor destCell, Block bottom, Block top, bool includeObstructedByWalls, List<Cell.Descriptor> ignored, List<OneCellMovementSolution> results)
			{
				if (ignored != null && ignored.Contains(destCell))
					return;

				if (bottom.IsObstructedAtTopBy(top))
				{
					if (includeObstructedByWalls)
						results.Add(new OneCellMovementSolution(destCell, SingleCellMoveCost.THROUGH_WALL_MOVE));
				}
				else
					results.Add(new OneCellMovementSolution(destCell));
			}

			void AddOneCellMovementSolutionIfPossible_Diag(Cell.Descriptor destCell, List<Cell.Descriptor> ignored, List<OneCellMovementSolution> results)
			{
				if (ignored == null || !ignored.Contains(destCell))
					results.Add(new OneCellMovementSolution(destCell, SingleCellMoveCost.DIAGONAL_MOVE));
			}

			//bool HasValidColumn(Cell.Descriptor cell) { return cell.Column > -1 && cell.Column < NumColumns; }
			//bool HasValidRow(Cell.Descriptor cell) { return cell.Row > -1 && cell.Row < NumRows; }

			void AStar(
				PathfindingContext context, // algorithm context
				PathfindingSolution solutionSoFar // current branch's context
			)
			{
				var curPathLen = solutionSoFar.path.Count;

				if (curPathLen - 1 == context.budget.moves)
					return;

				Cell.Descriptor curCell = solutionSoFar.path[curPathLen - 1];
				int passingsThroughEnemiesForNextBranch = solutionSoFar.cost.passingsThroughEnemies;
				bool hasGhostMove = solutionSoFar.cost.ghostMoves < context.budget.ghostMoves;
				bool hasSimplePassingThroughWalls = solutionSoFar.cost.passingsThroughWalls < context.budget.passingsThroughWalls;
				bool useGhostMoveForNextBranch = false;
				//bool consumeGhostAsSoonAsPossible = (pathfindingPolicy & PathfindingPolicy.CONSUME_GHOST_MOVES_AS_SOON_AS_POSSIBLE_REGARDLESS_IF_NEEDED_OR_NOT) != 0;

				if (hasGhostMove)// && consumeGhostAsSoonAsPossible)
				{
					useGhostMoveForNextBranch = true;
				}
				else
				{
					if (curPathLen > 1) // we're not at origin
					{
						// Check if a passing through enemy is required
						if (context.enemiesInfo.HasEnemy(curCell.block))
						{
							// An enemy's present and we've already consumed the passings through enemies => can't go further
							if (solutionSoFar.cost.passingsThroughEnemies < context.budget.passingsThroughEnemies)
								++passingsThroughEnemiesForNextBranch;
							else if (hasGhostMove)
								useGhostMoveForNextBranch = true;
							else
								return;
						}
					}
				}

				var surroundingCells = new List<OneCellMovementSolution>();
				GetSurroundingCells(
					curCell,
					solutionSoFar.cost.diagonalMoves < context.budget.diagonalMoves,
					hasGhostMove || hasSimplePassingThroughWalls,
					surroundingCells,
					solutionSoFar.path
				);

				int numSurroundingCells = surroundingCells.Count;
				var solutions = new List<PathfindingSolution>();
				//Debug.Log("numSurroundingCells=" + numSurroundingCells);
				for (int i = 0; i < numSurroundingCells; ++i)
				{
					var branchAppendix = surroundingCells[i];
					var newBranchCost = new PathfindingCurrency(solutionSoFar.cost);

					++newBranchCost.moves;
					newBranchCost.diagonalMoves += branchAppendix.OneIfDiagonal;

					// No pass through wall or through enemy will be used if a ghost move will be
					if (useGhostMoveForNextBranch)
						++newBranchCost.ghostMoves;
					else
					{
						newBranchCost.passingsThroughEnemies = passingsThroughEnemiesForNextBranch;
						newBranchCost.passingsThroughWalls += branchAppendix.OneIfPassThroughWall;
						//if (branchAppendix.cost == SingleCellMoveCost.THROUGH_WALL_MOVE)
						//{
						//	if (hasSimplePassingThroughWalls)
						//		++newBranchCost.passingsThroughWalls;
						//	else
						//		++newBranchCost.ghostMoves;
						//}
					}

					PathfindingSolution solution =
						PathfindingSolution.CreateByAppending(
							new List<Cell.Descriptor>(solutionSoFar.path),
							branchAppendix.cell,
							newBranchCost
						);

					bool b = context.StoreSolutionToCellIfBetter(branchAppendix.cell, solution);
					//Debug.Log(b);
					if (b)
						solutions.Add(solution);
					//{
					//	--numSCells;
					//	--i;
					//	surroundingCells.RemoveAt(i);
					//}
				}

				//if (curPathLen + 1 <= context.budget.moves)
				for (int i = 0; i < solutions.Count; ++i)
					AStar(context, solutions[i]);
			}
		}

	}
}