﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Data;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Config;
using Assets.Scripts.Common.Interfaces;
using System;
using Assets.Scripts.Common;
using Com.TheFallenGames.DBD.Common;

namespace Com.TheFallenGames.DBD.World
{
    public class Cell : MonoBehaviour, IGridElement, IWorldObjectWithTitle
	{
        #region IGridElement
        public int Row { get; set; }
        public int Column { get; set; }

		// These are not used
        public IGridElement Left { get; set; }
        public IGridElement Above { get; set; }
        public IGridElement Right { get; set; }
        public IGridElement Below { get; set; }
		#endregion

		public Cell left, top, right, bottom;
		public Cell topLeft, topRight, bottomRight, bottomLeft;

		public int indexInCellsArray;

		public Descriptor Desc { get { return CreateDescriptor(); } }

        public Block ContainedBlock
        {
            get { return _Block; }
            set
            {
                _Block = value;
                if (_Block)
                {
                    _Block.transform.position = transform.position;
                    _Block.transform.parent = transform;
                }
            }
        }

        // Set before start
        public AllowedArea AllowedAreaHighlight
        {
            get;
            //get { return _AllowedAreaHighlight ?? (_AllowedAreaHighlight = transform.Find(typeof(AllowedArea).Name).GetComponent<AllowedArea>()); }
            set;
        }

		public Text TitleText { get; set; }

		Block _Block;
		//AllowedArea _AllowedAreaHighlight;

		protected virtual void Start()
		{
			if (Design.Instance.general.debug_CellPathActive)
				UIUtils.UnifyCanvasAndInitTitle(this, "Panel/Text");
			else
			{
				var canvas = transform.Find("Canvas");
				if (canvas)
					Destroy(canvas.gameObject);
			}
		}

		//protected virtual void Update() { }

		public override string ToString() { return transform.position.ToString() + ToShortString(); }
		public string ToShortString() { return "[r" + Row + ", c" + Column + "]"; }

		public void SetDebugInfo(string text)
		{
			if (TitleText)
			{
				// Set the parent panel active or not
				TitleText.transform.parent.gameObject.SetActive(Design.Instance.general.debug_CellPathActive);

				if (Design.Instance.general.debug_CellPathActive && TitleText)
					TitleText.text = text;
			}
		}

		public virtual Descriptor CreateDescriptor() { return new Descriptor(this); }


		public abstract class BaseDescriptor
		{
			public Cell describedCell;
			public Block block;
			public abstract int Row { get; set; }
			public abstract int Column { get; set; }

			//public override int GetHashCode()
			//{
			//	int h = base.GetHashCode();
			//	h ^= describedCell.GetHashCode();
			//	h ^= blockDescriptor.GetHashCode();
			//	h ^= Row;
			//	h ^= Column;

			//	return h;
			//}

			public BaseDescriptor(Cell cell, Block block, int row, int column)
			{
				describedCell = cell;
				this.block = block;
				Row = row;
				Column = column;
			}


			public bool IsAdjacentAndNotObstructedByWall(BaseDescriptor other, bool considerDiagonalMovement)
			{
				int rowDiff, colDiff;
				colDiff = DeltaColumnTo(other);
				if (other.Row == Row)
				{
					// Explanation
					//if (diff == 1)
					//    return !ContainedBlock.HasWall(Data.WallSide.RIGHT) && !other.ContainedBlock.HasWall(Data.WallSide.LEFT);
					//if (diff == -1)
					//    return !ContainedBlock.HasWall(Data.WallSide.LEFT) && !other.ContainedBlock.HasWall(Data.WallSide.RIGHT);
					//return false;

					if (Mathf.Abs(colDiff) == 1)
						return !block.HasWall((Data.WallSide)(1 + colDiff)) && !other.block.HasWall((Data.WallSide)(1 - colDiff));
				}

				rowDiff = DeltaRowTo(other);
				if (other.Column == Column)
				{
					if (Mathf.Abs(rowDiff) == 1)
						return !block.HasWall((Data.WallSide)(2 - rowDiff)) && !other.block.HasWall((Data.WallSide)(2 + rowDiff));
				}

				if (considerDiagonalMovement)
					return Mathf.Abs(rowDiff) == 1 && Mathf.Abs(colDiff) == 1;

				return false;
			}

			public Vector2 DeltaTo(BaseDescriptor other) { return new Vector2(DeltaColumnTo(other), DeltaRowTo(other)); }
			public int DeltaColumnTo(BaseDescriptor other) { return other.Column - Column; }
			public int DeltaRowTo(BaseDescriptor other) { return other.Row - Row; }
			public int DistColumnTo(BaseDescriptor other) { return Mathf.Abs(DeltaColumnTo(other)); }
			public int DistRowTo(BaseDescriptor other) { return Mathf.Abs(DeltaRowTo(other)); }
			/// <summary> It assumes <paramref name="other"/> is not null</summary>
			public RelativePositionSimple RelativePosSimpleTo(Descriptor other)
			{
				int dc = DistColumnTo(other);
				int dr = DistRowTo(other);

				if (dc + dr == 0)
					return RelativePositionSimple.IDENTICAL;

				if (dc + dr == 1)
					return RelativePositionSimple.NEIGHBOR_DIRECT;

				if (dc == 1 && dc == 1)
					return RelativePositionSimple.NEIGHBOR_DIAGONAL;

				return RelativePositionSimple.DISTANT;
			}

			public override string ToString() { return "[" + Row + "(" + describedCell.Row +"), " + Column + "(" + describedCell.Column + ")]"; }
		}


		public class Descriptor : BaseDescriptor
		{
			public override int Row { get { return describedCell.Row; } set { } }
			public override int Column { get { return describedCell.Column; } set { } }


			public Descriptor(Cell cell) : this(cell, cell.ContainedBlock, cell.Row, cell.Column) { }

			protected Descriptor(Cell cell, Block block, int row, int column) : base(cell, block, row, column) { }


			public virtual Descriptor GetDeepClone() { return new Descriptor(describedCell, block, Row, Column); }
		}

	}
}
