﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

namespace Com.TheFallenGames.DBD.World
{
    public class Shifter : MonoBehaviour, IGridElement//, InputToEvent.IEventHandler
	{
        #region IGridElement
        public int Row { get; set; }
        public int Column { get; set; }

        public IGridElement Left { get; set; }
        public IGridElement Above { get; set; }
        public IGridElement Right { get; set; }
        public IGridElement Below { get; set; }
        #endregion

        //public Func<Shifter, bool> onAllowPress;
        //public Func<Shifter, bool> onAllowLongPress;
        //public Action<Shifter> onSelected;

        [HideInInspector]
        public Cell[] workingCells;

        public Direction shiftDirection
        {
            get { return _ShiftDirection; }
            set
            {
                _ShiftDirection = value;

                EntryIndex = 0;
                ExitIndex = workingCells.Length - 1;
                bool swapEntryExit = false;
                switch (_ShiftDirection)
                {
                    case Direction.LEFT:
                        ShiftDiscreteValue = new Vector3(-1, 0, 0);
                        swapEntryExit = true;
                        break;
                    case Direction.UP:
                        ShiftDiscreteValue = new Vector3(0, 0, 1);
                        break;
                    case Direction.RIGHT:
                        ShiftDiscreteValue = new Vector3(1, 0, 0);
                        break;
                    case Direction.DOWN:
                        ShiftDiscreteValue = new Vector3(0, 0, -1);
                        swapEntryExit = true;
                        break;
                }

				//transform.LookAt(transform.position + ShiftDiscreteValue);

                if (swapEntryExit)
                {
                    var i = EntryIndex;
                    EntryIndex = ExitIndex;
                    ExitIndex = i;
                }
            }
        }
        public Vector3 ShiftDiscreteValue
        {
            get;
            private set;
        }

		public int IndexInShiftersArray { get; set; }
        //public Shifter OpposingShifter { get; set; }
        public int EntryIndex { get; private set; }
        public int ExitIndex { get; private set; }
        public Cell EntryCell { get { return workingCells[EntryIndex]; } }
        public Cell ExitCell { get { return workingCells[ExitIndex]; } }
		//public bool Interractable { set { gameObject.Seta} }

		//bool FullyDragged
		//{
		//	get { return _FullyDragged; }
		//	set
		//	{
		//		_FullyDragged = value;
		//		_ColorWhileDragging = _FullyDragged ? Color.green : NOT_FULLY_DRAGGED_COLOR;
		//	}
		//}

		//public bool IsAtOuterCell
		//{
		//	set
		//	{
		//		// The outer cell should be pushed from a different position
		//		Vector3 centerToSet;
		//		if (value)
		//			centerToSet = _ColliderInitialCenterPos + new Vector3(0f, 0f, -.75f);
		//		else
		//			centerToSet = _ColliderInitialCenterPos;

		//		if (_Collider.center != centerToSet)
		//			_Collider.center = centerToSet;
		//	}
		//}

		//const float ACTIVATION_DRAG_DISTANCE_NEEDED_AS_FACTOR_OF_SHIFTER_SIZE = .2f;
		//const float ACTIVATION_DRAG_MAX_DISTANCE_NEEDED_AS_FACTOR_OF_SHIFTER_SIZE = 2f;
		//readonly Color NOT_FULLY_DRAGGED_COLOR = new Color(.5f, .5f, .5f, .3f);

        Direction _ShiftDirection;
  //      bool _Pressing;
  //      Vector3 _InitialScale;
		////Renderer _Renderer;
		//SpriteRenderer _SpriteRenderer;
		////MaterialPropertyBlock _PropBlock;
		//Color _ColorWhileDragging;
		//bool _FullyDragged;
		//Vector3 _ColliderInitialCenterPos;
		//BoxCollider _Collider;

		//void Awake()
		//{
		//	_Collider = GetComponent<BoxCollider>();
		//	_ColliderInitialCenterPos = _Collider.center;
		//}

		//void Start()
  //      {
  //          _InitialScale = transform.localScale;
		//	//_Renderer = transform.GetChild(0).GetComponent<Renderer>();
		//	_SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
		//	//_PropBlock = new MaterialPropertyBlock();
		//}

  //      void Update()
  //      {
		//	Color colorToSet;
  //          if (_Pressing)
  //          {
		//		var v1x = new Vector3(1f, 0f);
		//		var shifterLeftEdgePosInScreen = Camera.main.WorldToScreenPoint(transform.position - v1x);
		//		var shifterRightEdgePosInScreen = Camera.main.WorldToScreenPoint(transform.position + v1x);
		//		var shifterSizeInScreen = Vector3.Distance(shifterLeftEdgePosInScreen, shifterRightEdgePosInScreen);

		//		Vector3 dragVectorWithZAsY = InputToEvent.Instance.DragVector;
		//		dragVectorWithZAsY.z = dragVectorWithZAsY.y;
		//		dragVectorWithZAsY.y = 0f;

		//		float targetDirectionSimilarity = Mathf.Clamp01(Vector3.Dot(ShiftDiscreteValue, dragVectorWithZAsY.normalized));

		//		float screenDist = dragVectorWithZAsY.magnitude;
		//		//Debug.Log("shifterSizeInScreen=" + shifterSizeInScreen);
		//		//Debug.Log("targetDirectionSimilarity=" + targetDirectionSimilarity);
		//		//Debug.Log("screenDist=" + screenDist);
		//		float dragAmountUntilMinNeeded01 = screenDist / (shifterSizeInScreen * ACTIVATION_DRAG_DISTANCE_NEEDED_AS_FACTOR_OF_SHIFTER_SIZE);
		//		FullyDragged = targetDirectionSimilarity > .9f
		//			&& dragAmountUntilMinNeeded01 >= 1f
		//			&& screenDist < shifterSizeInScreen * ACTIVATION_DRAG_MAX_DISTANCE_NEEDED_AS_FACTOR_OF_SHIFTER_SIZE;

		//		dragAmountUntilMinNeeded01 = Mathf.Clamp01(dragAmountUntilMinNeeded01);

		//		transform.localScale = Vector3.Lerp(_InitialScale, Vector3.one * 2f, Mathf.Min(dragAmountUntilMinNeeded01, targetDirectionSimilarity));
		//		colorToSet = _ColorWhileDragging;
		//	}
  //          else
  //          {
		//		FullyDragged = false;
		//		transform.localScale = _InitialScale;
		//		colorToSet = Color.clear;
  //          }
		//	_SpriteRenderer.color = colorToSet;
		//	//_Renderer.GetPropertyBlock(_PropBlock);
		//	//_PropBlock.SetColor("_Color", _ColorToSet);
		//	//_Renderer.SetPropertyBlock(_PropBlock);
		//}

		//#region implementation for InputToEvent.IEventHandler
		//public void OnPress(BaseEventData eventData) { StartPress(); }
		//public void OnLongPress(BaseEventData eventData) { }
		//public void OnPressCanceled(BaseEventData eventData) { _Pressing = false; }
		//public void OnRelease(BaseEventData eventData) { EndPress(); }
		//public void OnReleaseAfterCanceled(BaseEventData eventData) { /*EndPress();*/ }
		//public void OnClick(BaseEventData eventData) { }
		//public void OnDoubleClick(BaseEventData eventData) { }
		//#endregion

		public void SetPotentialShiftHighlight(bool potentialShift)
		{
			foreach (var cell in workingCells)
				cell.ContainedBlock.SetPotentialShiftHighlight(potentialShift);
		}

		//public Descriptor CreateDescriptor(Board.Descriptor boardDescriptor) { return new Descriptor(this, boardDescriptor.allCellsIncludingOuter); }

		//void StartPress()
		//{
		//	if (onAllowPress == null || onAllowPress(this))
		//		_Pressing = true;
		//}
		//void EndPress()
		//{
		//	_Pressing = false;

		//	if (FullyDragged)
		//	{
		//		if (onSelected != null)
		//			onSelected(this);
		//	}
		//}

		//void Select()
		//{
		//    //Debug.Log("OnLongPress " + name);
		//    if (onSelected != null)
		//        onSelected(this, true);
		//}

		public override string ToString() { return "#" + IndexInShiftersArray + "[" + Row + ", " + Column + "]"; }


		public enum Direction
        {
            LEFT,
            UP,
            RIGHT,
            DOWN
        }


		//public class Descriptor
		//{
		//	public Cell.Descriptor[] workingCells;

		//	public Shifter describedShifter;
		//	//public int indexInShiftersArray;
		//	//public Descriptor opposingShifter; // assigned outside
		//	//public int entryIndex;
		//	//public int exitIndex;
		//	public Cell.Descriptor EntryCell { get { return workingCells[describedShifter.EntryIndex]; } }
		//	public Cell.Descriptor ExitCell { get { return workingCells[describedShifter.ExitIndex]; } }
		//	//public int Row { get { return describedShifter.Row; } }
		//	//public int Column { get { return describedShifter.Column; } }


		//	public Descriptor (Shifter describedShifter, Cell.Descriptor[] allCellsIncludingOuter)
		//		: this(describedShifter, new Cell.Descriptor[describedShifter.workingCells.Length], describedShifter.EntryIndex, describedShifter.ExitIndex)
		//	{
		//		for (int i = 0, j = 0; i < allCellsIncludingOuter.Length; i++)
		//		{
		//			if (j == describedShifter.workingCells.Length)
		//				break; // done

		//			var cell = allCellsIncludingOuter[i];
		//			if (Array.IndexOf(describedShifter.workingCells, cell.describedCell) == -1)
		//				continue;

		//			workingCells[j++] = cell;
		//		}
		//	}

		//	Descriptor (Shifter describedShifter, Cell.Descriptor[] workingCells, int entryIndex, int exitIndex)
		//	{
		//		this.describedShifter = describedShifter;
		//		//indexInShiftersArray = describedShifter.IndexInShiftersArray;
		//		this.workingCells = workingCells;
		//		//this.entryIndex = entryIndex;
		//		//this.exitIndex = exitIndex;
		//	}


		//	public Descriptor GetDeepClone(Cell.Descriptor[] newAllCellsIncludingOuter) { return new Descriptor(describedShifter, newAllCellsIncludingOuter); }
		//}
    }
}