﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Items;
using System;

namespace Com.TheFallenGames.DBD.World
{
    public class Block : DBDObject, IEnumerable<Entity>
    {
        public int NumContainedEntities { get { return _Children.Count; } }
        public WallSide[] Walls { get { return _ActiveWalls; } }

		//public Descriptor Desc { get { return CreateDescriptor(); } }

		Wall[] _Walls = new Wall[4];
        WallSide[] _ActiveWalls;
		bool[] _WallsActiveState = new bool[4];
		Renderer _FloorRenderer;
		Renderer[][] _ActiveWallsRenderers;
		MaterialPropertyBlock _FloorMaterialPropertyBlock;
		MaterialPropertyBlock[] _ActiveWallsPropertyBlocks;
		Cell _ParentAsCell;


        void Awake()
        {
			UpdateParentAsCell();

            var walls = transform.Find("Walls");
            for (int i = 0; i < 4; ++i)
                _Walls[i] = walls.GetChild(i).GetComponent<Wall>();

			// Randomly rotate the floor, so not all floors would look the same
			var floorGO = transform.Find("Floor");
			_FloorRenderer = floorGO.GetComponent<Renderer>();
			floorGO.Rotate(0f, UnityEngine.Random.Range(0, 4) * 90f, 0f);

			_FloorMaterialPropertyBlock = new MaterialPropertyBlock();
		}

		void OnTransformParentChanged() { UpdateParentAsCell(); }

		//public Cell GetParentAsCell() { return transform.parent == null ? null : transform.parent.GetComponent<Cell>(); }
		public Cell GetParentAsCell() { return _ParentAsCell; }


		/// <summary> returns the ore component if there's ore</summary>
		public Ore PeekOre()
		{
			foreach (var entity in _Children)
				if (entity is Ore)
					return entity as Ore;

			return null;
		}

		public int PeekOreAmount()
		{
			foreach (var entity in _Children)
				if (entity is Ore)
					return (entity as Ore).Amount;

			return 0;
		}

		/// <summary> removes the ore, if there's any. returns true if the was</summary>
		public Ore PopOre()
		{
			var ore = PeekOre();
			if (ore)
				_Children.Remove(ore);

			return ore;
		}

		public bool ContainsOre() { return PeekOreAmount() > 0; }

        public void SetActiveWalls(byte[] activeWalls)
        {
            _ActiveWalls = new WallSide[activeWalls.Length];
			_ActiveWallsRenderers = new Renderer[activeWalls.Length][];
			_ActiveWallsPropertyBlocks = new MaterialPropertyBlock[activeWalls.Length];

			for (int i = 0; i < _WallsActiveState.Length; i++)
				_WallsActiveState[i] = false;

			for (int i = 0; i < activeWalls.Length; ++i)
            {
                byte activeWallIndex = activeWalls[i];
				_WallsActiveState[activeWallIndex] = true;
				_ActiveWalls[i] = (WallSide)activeWallIndex;
				var wall = _Walls[activeWallIndex];
				wall.Activate();
				_ActiveWallsRenderers[i] = wall.GetComponentsInChildren<Renderer>();
				_ActiveWallsPropertyBlocks[i] = new MaterialPropertyBlock();
			}
        }

		public void SetPotentialShiftHighlight(bool potentialShift)
		{
			var color = potentialShift ? new Color (1f, 1f, 117/ 255f, 1f) : Color.white;
			SetRendererColor(_FloorRenderer, _FloorMaterialPropertyBlock, color);
			for (int i = 0; i < _ActiveWallsRenderers.Length; i++)
				for (int j = 0; j < _ActiveWallsRenderers[i].Length; j++)
				{
					SetRendererColor(_ActiveWallsRenderers[i][j], _FloorMaterialPropertyBlock, color);
					//SetRendererColor(_ActiveWallsRenderers[i][j], _ActiveWallsPropertyBlocks[i], color);
				}
		}

		public IEnumerator<Entity> GetEnumerator() { return _Children.GetEnumerator(); }
        IEnumerator IEnumerable.GetEnumerator() { return this.GetEnumerator(); }

		//public Descriptor CreateDescriptor() { return new Descriptor(this); }


		public bool HasWall(WallSide wallSide) { return _WallsActiveState[(int)wallSide]; }
		public bool IsObstructedAtRightBy(Block right) { return HasWall(WallSide.RIGHT) || right.HasWall(WallSide.LEFT); }
		public bool IsObstructedAtTopBy(Block top) { return HasWall(WallSide.TOP) || top.HasWall(WallSide.BOTTOM); }



		void SetRendererColor(Renderer rend, MaterialPropertyBlock props, Color color)
		{
			// Avoiding creating a new material by accessing Renderer.material
			rend.GetPropertyBlock(props);
			props.SetColor("_Color", color);
			rend.SetPropertyBlock(props);
		}

		void UpdateParentAsCell() { _ParentAsCell = transform.parent == null ? null : transform.parent.GetComponent<Cell>(); }

		//public class Descriptor
		//{
		//	public Block describedBlock;
		//	public WallSide[] walls { get { return describedBlock._ActiveWalls; } }

		//	//public override int GetHashCode()
		//	//{
		//	//	int h = base.GetHashCode();
		//	//	h ^= describedBlock.GetHashCode();
		//	//	foreach (var item in walls)
		//	//		h ^= item.GetHashCode();
		//	//	return h;
		//	//}


		//	public Descriptor(Block block)
		//	{
		//		describedBlock = block;
		//		//var w = block.Walls;
		//		//walls = new WallSide[w.Length];
		//		//for (int i = 0; i < w.Length; i++)
		//		//	walls[i] = w[i];
		//	}

		//	public Descriptor GetDeepClone() { return new Descriptor(describedBlock); }

		//	public bool HasWall(WallSide wallSide) { return System.Array.IndexOf(walls, wallSide) != -1; }
		//	public bool IsObstructedAtRightBy(Descriptor right) { return HasWall(WallSide.RIGHT) || right.HasWall(WallSide.LEFT); }
		//	public bool IsObstructedAtTopBy(Descriptor top) { return HasWall(WallSide.TOP) || top.HasWall(WallSide.BOTTOM); }
		//}
	}
}
