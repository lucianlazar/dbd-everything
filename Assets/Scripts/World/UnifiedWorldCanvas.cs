﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.TheFallenGames.DBD.World
{
	public class UnifiedWorldCanvas : MonoBehaviour
	{
		public static UnifiedWorldCanvas Instance { get; private set; }

		List<CanvasFollowerChildInfo> followerChildren = new List<CanvasFollowerChildInfo>();


		void Awake()
		{
			Instance = this;
		}

		void Update()
		{
			if (followerChildren == null)
				return; // not initialized yet

			for (int i = 0; i < followerChildren.Count; i++)
				followerChildren[i].FollowIfNeeded();
		}

		void OnDestroy()
		{
			Instance = null;	
		}

		public void SwallowCanvas(Transform canvas)
		{
			while (canvas.childCount > 0)
			{
				var follower = canvas.GetChild(0);
				var info = new CanvasFollowerChildInfo(follower, canvas.parent);
				follower.SetParent(transform, true);
				followerChildren.Add(info);
			}
			Destroy(canvas.gameObject);	
		}


		class CanvasFollowerChildInfo
		{
			Vector3 deltaAtInit;
			Transform follower, followed;

			public CanvasFollowerChildInfo(Transform follower, Transform followed)
			{
				this.follower = follower;
				this.followed = followed;
				deltaAtInit = followed.position - follower.position;
			}

			public void FollowIfNeeded()
			{
				bool active = followed && followed.gameObject.activeInHierarchy;
				follower.gameObject.SetActive(active);
				if (active)
					follower.position = followed.position - deltaAtInit;
			}
		}
	}
}
