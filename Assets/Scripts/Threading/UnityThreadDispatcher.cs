﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Com.TheFallenGames.DBD.Threading
{
	public class UnityThreadDispatcher : MonoBehaviour
	{
		static UnityThreadDispatcher _Instance = null;
		static readonly Queue<Action> _ExecutionQueue = new Queue<Action>();


		public static UnityThreadDispatcher Instance
		{
			get
			{
				if (_Instance == null)
					throw new UnityException("no UnityMainThreadDispatcher instance found in the scene");
				return _Instance;
			}
		}


		void Awake()
		{
			if (_Instance == null)
			{
				_Instance = this;
				DontDestroyOnLoad(gameObject);
			}
		}

		void Update()
		{
			lock (_ExecutionQueue)
			{
				while (_ExecutionQueue.Count > 0)
					_ExecutionQueue.Dequeue().Invoke();
			}
		}

		void OnDestroy() { _Instance = null; }

		public void Enqueue(IEnumerator action)
		{
			lock (_ExecutionQueue)
			{
				_ExecutionQueue.Enqueue(() => StartCoroutine(action));
			}
		}

		public void Enqueue(Action action) { Enqueue(ActionWrapper(action)); }

		IEnumerator ActionWrapper(Action a)
		{
			a();
			yield return null;
		}
	}
}