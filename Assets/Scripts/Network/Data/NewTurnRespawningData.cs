﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Network.Data
{
    public class NewTurnRespawningData
	{
		internal byte[] players;
		internal byte[] monsters;
		internal byte[] dragon;
		internal byte[] commonItems; 
        internal byte[] escapeItems; 
        internal byte[] treasures;

		internal List<byte> All
		{
			get
			{
				if (_All == null)
				{
					_All = new List<byte>();
					_All.AddRange(players);
					_All.AddRange(monsters);
					_All.AddRange(dragon);
					_All.AddRange(commonItems);
					_All.AddRange(escapeItems);
					_All.AddRange(treasures);
				}

				return _All;
			}
		}
		List<byte> _All;

		const byte PLAYERS = 0;
		const byte MONSTERS = 1;
		const byte DRAGON = 2;
		const byte COMMON_ITEMS = 3;
		const byte ESCAPE_ITEMS = 4;
		const byte TREASURES = 5;

		public NewTurnRespawningData()
        {
            Init();
        }

        public NewTurnRespawningData(Hashtable data)
        {
            Init();

			players = data[PLAYERS] as byte[];
			monsters = data[MONSTERS] as byte[];
			dragon = data[DRAGON] as byte[];
			commonItems = data[COMMON_ITEMS] as byte[];
			escapeItems = data[ESCAPE_ITEMS] as byte[];
			treasures = data[TREASURES] as byte[];
		}

        void Init()
        {
        }

        public Hashtable ToPhotonHashable()
		{
			var data = new Hashtable();
			data[PLAYERS] = players;
			data[MONSTERS] = monsters;
			data[DRAGON] = dragon;
			data[COMMON_ITEMS] = commonItems;
			data[ESCAPE_ITEMS] = escapeItems;
			data[TREASURES] = treasures;

            return data;
        }
    }
}
