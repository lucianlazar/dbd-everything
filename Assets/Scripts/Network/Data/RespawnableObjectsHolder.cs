﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Base;
using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Network.Data
{
    public class RespawnableObjectsHolder
	{
		internal List<Player> players;
		internal List<Monster> monsters;
		internal Dragon dragon;
		internal List<CommonItem> commonItems; 
        internal List<EscapeItem> escapeItems; 
        internal List<Treasure> treasures;

		internal List<Entity> All
		{
			get
			{
				if (_All == null)
				{
					_All = new List<Entity>();
					_All.AddRange(players.ToArray());
					_All.AddRange(monsters.ToArray());
					if (dragon)
						_All.Add(dragon);
					_All.AddRange(commonItems.ToArray());
					_All.AddRange(escapeItems.ToArray());
					_All.AddRange(treasures.ToArray());
				}

				return _All;
			}
		}
		List<Entity> _All;

	}
}
