﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Data.GameState;

namespace Com.TheFallenGames.DBD.Network.Data
{
    public class GameInitializationData : GameInitialState
	{
		public GameInitializationData() : base() { }
		public GameInitializationData(Hashtable data) : base(data) { }


		internal void GenerateRandom(int numCells, int numPlayers)
		{
			walls = GenerateGridWallsForEachBlock(numCells);
			var list = new List<byte>();
			ore = GenerateOrePositions();
			//list.AddRange(data.ore);
			var oreListInOut = new List<byte>(ore);
			items = GenerateItemsPositions(list.ToArray(), oreListInOut);
			list.AddRange(items);
			monsters = GenerateMonstersPositions(list.ToArray(), oreListInOut);
			list.AddRange(monsters);
			dragon = GenerateDragonPosition(list.ToArray(), oreListInOut);
			list.Add(dragon);
			players = GeneratePlayersPositions(numPlayers, list.ToArray(), oreListInOut);
		}

		byte[] GenerateGridWallsForEachBlock(int numBlocks)
		{
			var wallsForEachBlock = new byte[numBlocks];
			byte[][] wallPositionsForEachBlock = new byte[numBlocks][];
			var doubleWalls = Utils.ChooseRandomAsBytes(0, numBlocks, Design.Instance.grid.numBlocksWithDoubleWalls);
			for (int i = 0; i < numBlocks; ++i)
				wallsForEachBlock[i] = 1;
			for (int i = 0; i < doubleWalls.Length; ++i)
				wallsForEachBlock[doubleWalls[i]] = 2;

			for (int i = 0; i < numBlocks; ++i)
			{
				int numWalls = wallsForEachBlock[i];
				wallPositionsForEachBlock[i] = Utils.ChooseRandomAsBytes(0, 4, numWalls);
			}

			EncodeWallsPositions(wallsForEachBlock, wallPositionsForEachBlock);

			return wallsForEachBlock;
		}
		byte[] GenerateOrePositions()
		{
			int numCells = Design.Instance.grid.numCellsIncludingOuterCell;
			var result = Utils.ChooseRandomAsBytes(0, numCells, Design.Instance.items.ore.numOrePieces);
			return result;
		}

		byte[] GenerateItemsPositions(byte[] ignoredPositions, List<byte> inOut_OreOnlySpaces)
		{
			int needed = Design.Instance.items.GetAll().Length;
			var finalToIgnore = GetFinalSpacesToIgnore(needed, ignoredPositions, inOut_OreOnlySpaces);
			var result = Utils.ChooseRandomAsBytes(0, Design.Instance.grid.numCellsIncludingOuterCell, needed, finalToIgnore.ToArray());
			return result;
		}

		byte[] GenerateMonstersPositions(byte[] ignoredPositions, List<byte> inOut_OreOnlySpaces)
		{
			int needed = Design.Instance.monsters.GetAllExceptDragon().Count;
			var finalToIgnore = GetFinalSpacesToIgnore(needed, ignoredPositions, inOut_OreOnlySpaces);
			var result = Utils.ChooseRandomAsBytes(0, Design.Instance.grid.numCellsIncludingOuterCell, needed, finalToIgnore.ToArray());
			return result;
		}

		byte GenerateDragonPosition(byte[] ignoredPositions, List<byte> inOut_OreOnlySpaces)
		{
			int needed = 1;
			var finalToIgnore = GetFinalSpacesToIgnore(needed, ignoredPositions, inOut_OreOnlySpaces);
			var result = Utils.ChooseRandomAsBytes(0, Design.Instance.grid.numCellsIncludingOuterCell, needed, finalToIgnore.ToArray());
			return result[0];
		}

		byte[] GeneratePlayersPositions(int needed, byte[] ignoredPositions, List<byte> inOut_OreOnlySpaces)
		{
			var finalToIgnore = GetFinalSpacesToIgnore(needed, ignoredPositions, inOut_OreOnlySpaces);
			var result = Utils.ChooseRandomAsBytes(0, Design.Instance.grid.numCellsIncludingOuterCell, needed, finalToIgnore.ToArray());
			return result;
		}

		List<byte> GetFinalSpacesToIgnore(int needed, byte[] ignoredPositions, List<byte> inOut_OreOnlySpaces)
		{
			int numCells = Design.Instance.grid.numCellsIncludingOuterCell;
			int having = numCells - ignoredPositions.Length - inOut_OreOnlySpaces.Count; // initially, counting ore spaces as not vacant
			List<byte> finalIgnored = new List<byte>(ignoredPositions);
			finalIgnored.AddRange(inOut_OreOnlySpaces);
			while (having++ < needed) // sacrifice ore spaces
			{
				finalIgnored.Remove(inOut_OreOnlySpaces[0]);
				inOut_OreOnlySpaces.RemoveAt(0);
			}

			return finalIgnored;
		}
	}
}
