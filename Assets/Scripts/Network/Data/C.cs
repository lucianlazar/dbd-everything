﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.TheFallenGames.DBD.Network.Data
{
	public static class RoomProps
	{
		public static readonly string KEY_PREFIX = "dr";

		public static readonly string CURRENT_PLAYER_ID = KEY_PREFIX + "CPL";
		public static readonly string CURRENT_TURN_NUMBER = KEY_PREFIX + "CTN";
		public static readonly string BOARD_SETUP = KEY_PREFIX + "BST";
		public static readonly string PREV_GAME_STATE = KEY_PREFIX + "PGS";
		public static readonly string GAME_STATE = KEY_PREFIX + "GS";
		public static readonly string GAME_MODE = KEY_PREFIX + "GM";
	}

	public static class PlayerProps
	{
		public static readonly string KEY_PREFIX = "dp";

		public static readonly string PLAYER_CLASS_CHOICE = KEY_PREFIX + "PCC";
	}
}
