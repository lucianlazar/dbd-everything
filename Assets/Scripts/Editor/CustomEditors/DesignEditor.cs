﻿using Com.TheFallenGames.DBD.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Editor.CustomEditors
{
	[CustomEditor(typeof(Design))]
	public class DesignEditor : UnityEditor.Editor
	{
		int playerHitEffectsParticleSystemsSortLayer = 10;

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			EditorGUILayout.BeginHorizontal();
			{
				playerHitEffectsParticleSystemsSortLayer = EditorGUILayout.IntField("Player hit effects sort layer", playerHitEffectsParticleSystemsSortLayer);
				if (GUI.Button(EditorGUILayout.GetControlRect(GUILayout.Width(100f)), "Set"))
				{
					var playerDirs = Directory.GetDirectories(Application.dataPath + "/Resources/Characters/Players");

					foreach (var playerDir in playerDirs)
					{
						var assetDirs = Directory.GetDirectories(playerDir);
						foreach (var assetDir in assetDirs)
						{
							if (assetDir.ToLower().Contains("hit effect"))
							{
								var effectsGOsFiles = Directory.GetFiles(assetDir);
								foreach (var effectGOPath in effectsGOsFiles)
								{
									if (!effectGOPath.EndsWith(".prefab"))
										continue;

									var projRelPath = FileUtil.GetProjectRelativePath(effectGOPath);
									var go = AssetDatabase.LoadAssetAtPath<GameObject>(projRelPath);
									if (!go)
										continue;

									int i = 0;
									foreach (var ps in go.GetComponentsInChildren<ParticleSystem>())
									{
										++i;
										ps.GetComponent<Renderer>().sortingOrder = playerHitEffectsParticleSystemsSortLayer;
									}
									Debug.Log(i + " particle systems found for \"" + projRelPath.Substring(projRelPath.IndexOf("Assets") + "Assets".Length) + "\"");
								}
							}
						}
					}
				}

				AssetDatabase.Refresh();
			}
			EditorGUILayout.EndHorizontal();
		}
	}
}
