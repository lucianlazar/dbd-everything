﻿using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Com.TheFallenGames.DBD.EditorCode
{
	public class BoardEditorWindow : EditorWindow
	{
		const float CELL_SIZE = 100f;
		const float HEADER_SIZE = 20f;
		const float PARENT_HEADER_SIZE = HEADER_SIZE * 1.2f;
		const float WALL_BUTTON_TICKNESS = 9f;
		const int AUTOSAVE_INTERVAL = 1;
		const int MIN_SEND_INTERVAL = 5;

		bool _Initialized, _SavePending;
		ClassicBoardSetup _BoardSetup;
		int _SelectedCell = -1;
		Dictionary<int, GUIStyle> _MapMonsterLevelToColor = new Dictionary<int, GUIStyle>();
		int _NumRows, _NumColumns;
		int _LastSend;
		static bool _CurrentlySendingEmail;
		bool _RandomRequested;

		int CurrentUnixTimestamp
		{
			get
			{
				return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
			}
		}

		bool CanSend
		{
			get
			{
				return _Initialized && !_SavePending && (CurrentUnixTimestamp - _LastSend) > MIN_SEND_INTERVAL 
					&& !_CurrentlySendingEmail && System.IO.File.Exists(_BoardSetup.FileAbsolutePath) && HasInternet && AreAllItemsPlaced;
			}
		}

		bool HasInternet
		{
			get
			{
				return Application.internetReachability != NetworkReachability.NotReachable;
			}
		}

		bool AreAllItemsPlaced { get; set; }


		[MenuItem("DBD/Edit classic board")]
		static void Init()
		{
			var w = GetWindow<BoardEditorWindow>("Edit Board");
			w.minSize = new Vector2(CELL_SIZE * (Design.Instance.grid.numColumns + 2) + 400f, CELL_SIZE * (Design.Instance.grid.numRows + 1));
			w.maxSize = w.minSize * 1.1f;
		}


		GUIStyle centeredLabelStyle;
		GUIStyle centeredMiniLabelStyle;
		GUIStyle centeredBoldLabelStyle;
		GUIStyle centeredMiniBoldLabelStyle;
		GUIStyle centeredMiniYellowBoldLabelStyle;
		GUIStyle whiteLargeBoldLabel;
		GUIStyle itemCenteredMiniBoldLabelStyle;
		GUIStyle monsterBoxStyle;
		GUIStyle normalWallButtonStyle, selectedWallButtonStyle, normalCellButtonStyle, selectedCellButtonStyle;
		Texture2D normalWallTex, selectedWallTex, normalCellTex, selectedCellTex;

		void OnDisable()
		{
			_Initialized = false;
			AreAllItemsPlaced = false;
		}

		int curFrame;
		void Update()
		{
			if (++curFrame % (100 * AUTOSAVE_INTERVAL) == 0)
			{
				if (_SavePending && _Initialized)
				{
					_SavePending = false;
					_BoardSetup.WriteAsset();
				}
			}
			Repaint();
		}

		void OnGUI()
		{
			if (!_Initialized)
			{
				Initialize();
			}

			_NumRows = Design.Instance.grid.numRows;
			_NumColumns = Design.Instance.grid.numColumns;

			if (Event.current.type == EventType.KeyDown)
				OnKeyCodeUp(Event.current.keyCode);

			EditorGUI.BeginChangeCheck();
			{
				var cellLayoutParams = new GUILayoutOption[] { GUILayout.Width(CELL_SIZE), GUILayout.Height(CELL_SIZE) };
				EditorGUILayout.BeginHorizontal(GUILayout.Height(PARENT_HEADER_SIZE));
				{
					EditorGUILayout.LabelField("", centeredBoldLabelStyle, GUILayout.Width(HEADER_SIZE), GUILayout.Height(HEADER_SIZE)); // top-left corner
					for (int i = 0; i < _NumColumns; ++i)
					{
						EditorGUILayout.LabelField("C" + i, centeredBoldLabelStyle, GUILayout.Width(CELL_SIZE), GUILayout.Height(HEADER_SIZE));
					}
				}
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.BeginVertical(GUILayout.Width(PARENT_HEADER_SIZE));
					{
						for (int i = 0; i < _NumRows; ++i)
						{
							EditorGUILayout.LabelField("R" + (_NumRows - i - 1), centeredBoldLabelStyle, GUILayout.Width(HEADER_SIZE), GUILayout.Height(CELL_SIZE));
						}
					}
					EditorGUILayout.EndVertical();

					EditorGUILayout.BeginVertical(GUILayout.Width(CELL_SIZE * _NumColumns));
					{
						for (int i = 0; i < _NumRows; ++i)
						{
							EditorGUILayout.BeginHorizontal(GUILayout.Height(CELL_SIZE));
							{
								for (int j = 0; j < _NumColumns; ++j)
								{
									DrawCell((_NumRows - i - 1) * _NumRows + j);
								}
							}
							EditorGUILayout.EndHorizontal();
						}
					}
					EditorGUILayout.EndVertical();


					// Draw the outer cell
					EditorGUILayout.BeginVertical();
					{
						GUILayout.Space(CELL_SIZE * (_NumRows / 2) + 5f);
						DrawCell(_NumRows * _NumColumns);
					}
					EditorGUILayout.EndVertical();


					// Cell Inspecor
					EditorGUILayout.BeginVertical();
					{
						EditorGUILayout.BeginVertical(GUILayout.Height(600f), GUILayout.Width(300f));
						ValidateAndDrawProgress();
						GUILayout.Space(20f);
						DrawInspector();
						EditorGUILayout.EndVertical();

						//GUILayout.Space(500f);

						EditorGUI.BeginDisabledGroup(!CanSend);
						{
							var brect = EditorGUILayout.GetControlRect(false, 70f);
							brect.width = 200;
							brect.x += 100;
							string str = _CurrentlySendingEmail ? "Sending..." : "Send";
							if (GUI.Button(brect, str))
								Send();

							if (!HasInternet)
								EditorGUILayout.LabelField("No internet connection!");
						}
						EditorGUI.EndDisabledGroup();
					}
					EditorGUILayout.EndVertical();
				}
				EditorGUILayout.EndHorizontal();
			}
			if (EditorGUI.EndChangeCheck())
				_SavePending = true;
		}

		void OnDestroy()
		{
			_Initialized = false;
			AreAllItemsPlaced = false;
			if (_BoardSetup != null)
				_BoardSetup.Dispose();

			if (normalWallTex)
			{
				DestroyImmediate(normalWallTex);
				normalWallTex = null;
			}
			if (selectedWallTex)
			{
				DestroyImmediate(selectedWallTex);
				selectedWallTex = null;
			}
			if (normalCellTex)
			{
				DestroyImmediate(normalCellTex);
				normalCellTex = null;
			}
			if (selectedCellTex)
			{
				DestroyImmediate(selectedCellTex);
				selectedCellTex = null;
			}
		}

		void OnKeyCodeUp(KeyCode keyCode)
		{
			if (_SelectedCell != -1)
			{
				if (keyCode == KeyCode.LeftArrow)
					--_SelectedCell;
				else if (keyCode == KeyCode.RightArrow)
					++_SelectedCell;
				else if (keyCode == KeyCode.UpArrow)
					_SelectedCell += _NumColumns;
				else if (keyCode == KeyCode.DownArrow)
					_SelectedCell -= _NumColumns;
				else if (keyCode == KeyCode.R)
				{
					if ((Event.current.modifiers & (EventModifiers.Shift | EventModifiers.Control)) != 0)
					{
						if (_RandomRequested)
						{
							_RandomRequested = false;
							_BoardSetup.RandomizeEntities();
						}
						else
							_RandomRequested = true;
					}
				}

				if (_SelectedCell < 0)
					_SelectedCell = _NumRows * _NumColumns;
				else
					_SelectedCell = _SelectedCell % (_NumRows * _NumColumns + 1);
			}
		}

		void Initialize()
		{
			_Initialized = true;
			_BoardSetup = new ClassicBoardSetup();
			_BoardSetup.ReadAsset();

			centeredLabelStyle = new GUIStyle(EditorStyles.label);
			centeredLabelStyle.alignment = TextAnchor.MiddleCenter;

			centeredMiniLabelStyle = new GUIStyle(EditorStyles.centeredGreyMiniLabel);

			centeredMiniYellowBoldLabelStyle = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
			centeredMiniYellowBoldLabelStyle.normal = new GUIStyleState();
			//var darkerYellow = new Color(193 / 255f, 187 / 255f, 0f, 1f);
			var darkerYellow = new Color(220 / 255f, 220 / 255f, 0f, 1f);
			centeredMiniYellowBoldLabelStyle.normal.textColor = darkerYellow;
			centeredMiniYellowBoldLabelStyle.fontStyle = FontStyle.Bold;

			centeredBoldLabelStyle = new GUIStyle(EditorStyles.boldLabel);
			centeredBoldLabelStyle.alignment = TextAnchor.MiddleCenter;

			centeredMiniBoldLabelStyle = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
			centeredMiniBoldLabelStyle.fontStyle = FontStyle.Bold;

			whiteLargeBoldLabel = new GUIStyle(EditorStyles.whiteLargeLabel);
			whiteLargeBoldLabel.fontStyle = FontStyle.Bold;

			itemCenteredMiniBoldLabelStyle = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
			itemCenteredMiniBoldLabelStyle.normal.textColor = Color.magenta;
			itemCenteredMiniBoldLabelStyle.fontStyle = FontStyle.Bold;

			monsterBoxStyle = new GUIStyle(EditorStyles.helpBox);
			monsterBoxStyle.normal = new GUIStyleState();
			monsterBoxStyle.normal.background = Texture2D.whiteTexture;


			normalWallButtonStyle = new GUIStyle("Button");
			normalWallButtonStyle.border = new RectOffset();
			normalWallButtonStyle.normal = new GUIStyleState();
			normalWallButtonStyle.normal.background = normalWallTex = new Texture2D(1, 1);
			normalWallTex.SetPixel(0, 0, Color.gray * .2f);
			normalWallTex.Apply();

			selectedWallButtonStyle = new GUIStyle("Button");
			selectedWallButtonStyle.border = new RectOffset();
			selectedWallButtonStyle.normal = new GUIStyleState();
			selectedWallButtonStyle.normal.background = selectedWallTex = new Texture2D(1, 1);
			selectedWallTex.SetPixel(0, 0, Color.blue * .7f);
			selectedWallTex.Apply();

			normalCellButtonStyle = new GUIStyle("Button");
			normalCellButtonStyle.normal = new GUIStyleState();
			normalCellButtonStyle.normal.background = normalCellTex = new Texture2D(1, 1);
			normalCellTex.SetPixel(0, 0, new Color(248f / 255, 212f / 255, 147f / 255, .1f));
			normalCellTex.Apply();

			selectedCellButtonStyle = new GUIStyle("Button");
			//selectedCellButtonStyle.normal = new GUIStyleState();
			//selectedCellButtonStyle.normal.background = selectedCellTex = new Texture2D(1, 1);
			//selectedCellTex.SetPixel(0, 0, Color.gray * .9f);
			//selectedCellTex.Apply();
			//selectedButtonStyle.normal.background = Texture2D.whiteTexture;
		}

		void DrawCell(int index)
		{
			var rect = EditorGUILayout.GetControlRect(false, CELL_SIZE, GUILayout.Width(CELL_SIZE));

			var midRect = rect;
			midRect.width -= WALL_BUTTON_TICKNESS * 2; midRect.height -= WALL_BUTTON_TICKNESS * 2;
			midRect.x += WALL_BUTTON_TICKNESS;
			midRect.y += WALL_BUTTON_TICKNESS;

			if (DrawCellContent(midRect, index))
			{
				// TODO
				//Debug.Log("Clicked " + index);
			}


			float wallButtonComplement = (CELL_SIZE - WALL_BUTTON_TICKNESS);
			var rectLeft = rect; rectLeft.xMax -= wallButtonComplement; //rectLeft.yMin += WALL_BUTTON_TICKNESS; rectLeft.yMax -= WALL_BUTTON_TICKNESS;
			var rectRight = rect; rectRight.xMin += wallButtonComplement; //rectRight.yMax -= WALL_BUTTON_TICKNESS; rectRight.yMin += WALL_BUTTON_TICKNESS;
			var rectTop = rect; rectTop.yMax -= wallButtonComplement; //rectTop.xMax -= WALL_BUTTON_TICKNESS; rectTop.xMin += WALL_BUTTON_TICKNESS;
			var rectBottom = rect; rectBottom.yMin += wallButtonComplement; //rectBottom.xMin += WALL_BUTTON_TICKNESS; rectBottom.xMax -= WALL_BUTTON_TICKNESS;

			DrawWallButton(rectLeft, _BoardSetup.leftWalls, index);
			DrawWallButton(rectTop, _BoardSetup.topWalls, index);
			DrawWallButton(rectRight, _BoardSetup.rightWalls, index);
			DrawWallButton(rectBottom, _BoardSetup.bottomWalls, index);
		}

		void DrawWallButton(Rect rect, List<int> wallsCollection, int index)
		{
			if (GUI.Button(rect, "", GetWallButtonStyle(wallsCollection, index)))
			{
				_BoardSetup.ToggleWall(wallsCollection, index);
			}
		}

		bool DrawCellContent(Rect rect, int cellIndex)
		{
			//string content = _BoardSetup.GetContentAsString(index);
			string content = "";
			bool selected  = GUI.Button(rect, content, _SelectedCell == cellIndex ? selectedCellButtonStyle : normalCellButtonStyle);

			if (selected)
				_SelectedCell = _SelectedCell == cellIndex ? -1 : cellIndex;

			int numRows = 5;
			float rowHeight = rect.height / numRows;
			Rect[] rects = new Rect[numRows];
			int currentRect = 0;

			for (int i = 0; i < numRows; ++i)
			{
				rects[i] = rect;
				rects[i].height = rowHeight;
				rects[i].y += i * rowHeight;
			}

			if (_BoardSetup.ore.Contains(cellIndex))
				GUI.Label(rects[currentRect++], "Ore", centeredMiniYellowBoldLabelStyle);

			int objectIndexInDesign = _BoardSetup.items.IndexOf(cellIndex);
			if (objectIndexInDesign != -1)
			{
				GUI.Label(rects[currentRect++], Design.Instance.items.GetAll()[objectIndexInDesign].name, itemCenteredMiniBoldLabelStyle);
			}
			objectIndexInDesign = _BoardSetup.monsters.IndexOf(cellIndex);
			if (objectIndexInDesign != -1)
			{
				var design = Design.Instance.monsters.GetAll()[objectIndexInDesign];
				var curRect = rects[currentRect++];
				var boxRect = curRect; boxRect.x += (boxRect.width - boxRect.width * .88f) / 2; boxRect.width *= .88f;
				GUI.Box(boxRect, "", monsterBoxStyle);
				GUI.Label(curRect, design.name, GetMonsterGUIStyleByLevel(design.level));
			}
			if (_BoardSetup.spawnPoints.Contains(cellIndex))
				GUI.Label(rects[currentRect++], "{Spawn}", centeredMiniBoldLabelStyle);

			return selected;
		}

		void DrawInspector()
		{
			if (_SelectedCell == -1)
			{
				EditorGUILayout.LabelField("Select a cell's interior to edit it", EditorStyles.centeredGreyMiniLabel);
				return;
			}

			string str;
			if (_SelectedCell == _NumRows * _NumColumns)
				str = "Outer Cell";
			else
			{
				int row = _SelectedCell / _NumRows;
				int col = _SelectedCell % _NumColumns;
				str = "R" + row + "C" + col;
			}
			EditorGUILayout.LabelField("Editing " + str, whiteLargeBoldLabel, GUILayout.Height(20f));
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			{
				bool contained = _BoardSetup.ore.Contains(_SelectedCell);
				bool contains = EditorGUILayout.ToggleLeft("Ore", contained);
				if (contains != contained)
				{
					if (contains)
						_BoardSetup.AddOre(_SelectedCell);
					else
						_BoardSetup.ore.Remove(_SelectedCell);
				}

				DrawEntityPopUp("Item", GetAllItemsAsStrings(true), _BoardSetup.items);
				DrawEntityPopUp("Monster", GetAllMonstersAsStrings(true), _BoardSetup.monsters);

				contained = _BoardSetup.spawnPoints.Contains(_SelectedCell);
				contains = EditorGUILayout.ToggleLeft("Spawn point", contained);
				if (contains != contained)
				{
					if (contains)
						_BoardSetup.AddSpawnPoint(_SelectedCell);
					else
						_BoardSetup.spawnPoints.Remove(_SelectedCell);
				}
			}
			EditorGUILayout.EndVertical();
		}

		void DrawEntityPopUp(string label, string[] options, List<int> objectsList)
		{
			int prevSelectionIndex = objectsList.IndexOf(_SelectedCell) + 1;
			int newSelectionIndex = EditorGUILayout.Popup(label, prevSelectionIndex, options);
			if (newSelectionIndex != prevSelectionIndex)
			{
				// Transforming from dropdown space to design space
				--prevSelectionIndex;
				--newSelectionIndex;

				if (prevSelectionIndex != -1)
					objectsList[prevSelectionIndex] = -1;

				if (newSelectionIndex != -1)
					objectsList[newSelectionIndex] = _SelectedCell;
			}
		}

		void ValidateAndDrawProgress()
		{
			string s = "";
			int having, target;
			bool valid = true;
			s += "Ore " + (having = _BoardSetup.ore.Count) + " / " + (target = GetTargetOre()) + "\n";
			valid = valid && having == target;
			s += "Escape items " + (having = _BoardSetup.ValidEscapeItems) + "/" + (target = GetTargetEscapeItems()) + "\n";
			valid = valid && having == target;
			s += "Normal items " + (having = _BoardSetup.ValidCommonItems) + "/" + (target = GetTargetCommonItems()) + "\n";
			valid = valid && having == target;
			s += "Treasures " + (having = _BoardSetup.ValidTreasures) + "/" + (target = GetTargetTreasures()) + "\n";
			valid = valid && having == target;
			s += "Monsters " + (having = _BoardSetup.ValidMonsters) + "/" + (target = GetTargetMonsters()) + "\n";
			valid = valid && having == target;
			s += "Spawn points " + (having = _BoardSetup.ValidSpawnPoints) + "/" + (target = GetTargetSpawnPoints());
			valid = valid && having == target;

			EditorGUILayout.SelectableLabel(s, EditorStyles.helpBox, GUILayout.Height(75f), GUILayout.Width(200f));
			if (valid)
				EditorGUILayout.LabelField("All entities are present. Ready to send!", EditorStyles.whiteBoldLabel);
			else
				EditorGUILayout.LabelField("(Not all entities were placed)");

			AreAllItemsPlaced = valid;
		}

		
		int GetTargetOre() { return Design.Instance.items.ore.numOrePieces; }
		int GetTargetEscapeItems() { return Design.Instance.items.escapeItems.GetAll().Length; }
		int GetTargetCommonItems() { return Design.Instance.items.commonItems.GetAll().Length; }
		int GetTargetTreasures() { return Design.Instance.items.treasures.GetAll().Length; }
		int GetTargetMonsters() { return Design.Instance.monsters.GetAll().Length; }
		int GetTargetSpawnPoints() { return Design.Instance.grid.numSpawnPoints; }

		GUIStyle GetMonsterGUIStyleByLevel(int level)
		{
			if (_MapMonsterLevelToColor.ContainsKey(level))
				return _MapMonsterLevelToColor[level];

			var tex = Design.Instance.monsters.bordersPerLevel[level].texture;
			var color = GetAverageColor(tex);
			var newGUIStyle = new GUIStyle(centeredMiniBoldLabelStyle);
			newGUIStyle.normal = new GUIStyleState();
			newGUIStyle.normal.textColor = color;

			return _MapMonsterLevelToColor[level] = newGUIStyle;
		}


		public GUIStyle GetWallButtonStyle(List<int> wallsCollection, int i)
		{
			if (wallsCollection.Contains(i))
				return selectedWallButtonStyle;

			return normalWallButtonStyle;
		}

		Color GetAverageColor(Texture2D tex)
		{
			int min = Mathf.Min(tex.width, tex.height);
			int increment = Mathf.Max(min / 20, 1);

			Vector3 v = new Vector3();
			int numSamples = 0;
			for (int i = 0; i < tex.width; i += increment)
			{
				for (int j = 0; j < tex.height; j += increment, ++numSamples)
				{
					var col = tex.GetPixel(i, j);
					v.x += col.r;
					v.y += col.g;
					v.z += col.b;
				}
			}

			v /= numSamples;

			return new Color(v.x, v.y, v.z);
		}

		string[] GetAllItemsAsStrings(bool insertNoneAtStart) { return GetAllAsStrings(Design.Instance.items.GetAll(), insertNoneAtStart); }

		string[] GetAllMonstersAsStrings(bool insertNoneAtStart) { return GetAllAsStrings(Design.Instance.monsters.GetAll(), insertNoneAtStart); }

		string[] GetAllAsStrings(EntityDesign[] designs, bool insertNoneAtStart)
		{
			var arr = Array.ConvertAll(designs, i => i.name);
			if (insertNoneAtStart)
			{
				var list = new List<string>(arr);
				list.Insert(0, "(None)");
				arr = list.ToArray();
			}

			return arr;
		}

		void Send()
		{
			_CurrentlySendingEmail = true;
			string fileAbsolutePath = _BoardSetup.FileAbsolutePath; // Application.datapath cannot be used from a thread
			string boardAsText = _BoardSetup.ConvertToText();
			byte[] bytes = Encoding.ASCII.GetBytes(boardAsText);
			new System.Threading.Thread(new System.Threading.ThreadStart(() =>
			{
				using (MailMessage mail = new MailMessage(new MailAddress("dbdthegame@gmail.com"), new MailAddress("lucian.9k@gmail.com")))
				{
					SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
					client.Credentials = new System.Net.NetworkCredential(mail.From.Address, "1993dbdthegame");
					client.EnableSsl = true;
					System.Net.ServicePointManager.ServerCertificateValidationCallback = (_, __, ___, ____) => true;
					client.DeliveryMethod = SmtpDeliveryMethod.Network;

					mail.Subject = "DBD Classic Board Setup " + DateTime.Now.ToString();
					using (var stream = new System.IO.MemoryStream(bytes))
					{
						System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType();
						ct.MediaType = System.Net.Mime.MediaTypeNames.Text.Plain;
						ct.Name = ClassicBoardSetup.SERIALIZED_FILENAME_WITH_EXT;
						mail.Attachments.Add(new Attachment(stream, ct));
						mail.Body = boardAsText;
						client.Send(mail);
					}
					_LastSend = CurrentUnixTimestamp;
					Debug.Log("Email Sent!");
					_CurrentlySendingEmail = false;
				}
			})).Start();
		}
	}
}
