﻿using System;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.World;
using UnityEngine;
using Com.TheFallenGames.DBD.Core;

namespace Com.TheFallenGames.DBD.Actors.Items
{
    public abstract class BaseItem : MeshEntity
    {
        // Set before Start()
        public new BaseItemDesign Design { get; private set; }

        public override string EntityCategory { get { return "Item"; } }

		public bool HasOwner { get { return _Owner != null; } }
		public Character OwnerAsCharacter { get; private set; }
		public Player OwnerAsPlayer { get; private set; }
		public Block OwnerAsBlock { get; private set; }

		public virtual MonoBehaviour Owner
        {
            get { return _Owner; }
            set
            {
                if (_Owner != value)
                {
					var prev = _Owner;
					//if (_Owner)
     //                   OnCurrentNonNullOwnerWillChange();
					//else
					//	OnCurrentNullOwnerWillChange();

					_Owner = value;
					OwnerAsCharacter = _Owner as Character;
					OwnerAsPlayer = _Owner as Player;
					OwnerAsBlock = _Owner as Block;

					//if (_Owner)
     //                   OnNewNonNullOwner();
					//else
					//	OnNewNullOwner();

					OnOwnerChanged(prev);
				}
            }
        }

		MonoBehaviour _Owner;

		Vector3 _DefaultPositionDelta = SpawnManager.allEntitiesSpawnPositionDelta;
		Vector3 _PositionDelta;
		bool _SetRotation;

		public override void SetParentBlock(Block newParent, Vector3? positionDelta = null, bool setRotation = false)
		{
			_PositionDelta = positionDelta ?? _DefaultPositionDelta;
			_SetRotation = setRotation;
			Owner = newParent; // Will fire SetParentBlock itself;
		}

		//internal virtual void ConsumeByCurrentOwner()
		//{
		//	if (OwnerAsCharacter == null)
		//		throw new UnityException("Invalid state: owner is " + (OwnerAsCharacter == null ? "null!" : "a Block!"));

		//	Owner = null;
		//}

		protected override void Initialize()
        {
            base.Initialize();
            Design = base.Design as BaseItemDesign;
        }

		protected virtual void OnOwnerChanged(MonoBehaviour prevOwner)
		{
			if (prevOwner)
			{
				var prevOwnerAsChar = prevOwner as Character;
				if (prevOwnerAsChar)
					prevOwnerAsChar.Stats.RemoveItemsFromInventory(this);
			}

			if (OwnerAsCharacter)
				OwnerAsCharacter.Stats.AddItemsToInventory(this);

			// OwnerAsBlock is null => a player is the new owner or there's new owner => set no parent block 
			// else => the parent is set as expected 
			base.SetParentBlock(OwnerAsBlock, _PositionDelta, _SetRotation);
		}

		//protected virtual void OnCurrentNullOwnerWillChange() {}
		//protected virtual void OnCurrentNonNullOwnerWillChange() {}
		//protected virtual void OnNewNullOwner() {}
		//protected virtual void OnNewNonNullOwner() {}

		//protected virtual void OnOwnerChanged(Player prevOwner)
		//{
		//    if (prevOwner)
		//    {

		//        prevOwner.Stats.AqcuireItem(item);
		//    }
		//    if (_Owner)
		//        prevOwner.Stats.AqcuireItem(item);
		//}
	}
}
