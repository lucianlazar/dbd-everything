﻿using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Actors.Items
{
    public class CommonItem : BaseItem
    {
        // Set before Start()
        public new CommonItemDesign Design { get { return _Design ?? (_Design = base.Design as CommonItemDesign); } }
		CommonItemDesign _Design;

		public bool IsFreeUntilNextPickUp { get; set; }
		public int CurrentCost { get { return IsFreeUntilNextPickUp ? 0 : Design.Cost; } }


		public override void SetDisplayName(string displayName)
		{
			base.SetDisplayName("");
		}

		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			var commonItemsDesign = Config.Design.Instance.items.commonItems;
			//card.cardCategoryDesign.CopyFrom(Design.cardType == Data.CardType.BATTLE ? commonItemsDesign.battleCardCategoryDesign : commonItemsDesign.utilityCardCategoryDesign);
			card.cardCategoryDesign = Design.cardType == Data.CardType.BATTLE ? commonItemsDesign.battleCardCategoryDesign : commonItemsDesign.utilityCardCategoryDesign;

			return card;
		}

		protected override void OnOwnerChanged(MonoBehaviour prevOwner)
		{
			base.OnOwnerChanged(prevOwner);

			if (OwnerAsCharacter)
			{
				// The item was temporarily free and it was picked. 
				if (IsFreeUntilNextPickUp)
					IsFreeUntilNextPickUp = false;
			}
		}

		//private void Update()
		//{
		//	if (Design.name.Contains("ourage"))
		//	{
		//		Debug.Log(Config.Design.Instance.items.commonItems.COURAGE_POTION == Design);
		//	}
		//}
	}
}
