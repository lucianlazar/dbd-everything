﻿using Com.TheFallenGames.DBD.Actors.Items;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Core;

namespace Com.TheFallenGames.DBD.Items
{
    public class Ore : BaseItem
    {
		public int Amount
		{
			get { return _Amount; }
			set
			{
				if (_Amount != value)
				{
					_Amount = value;
					UpdateDisplayedAmount();
				}
			}
		}


		public override string MeshPathInResources { get { return null; } } // the mesh is already attached

		//protected override void Initialize()
		//{
		//    // Not calling base; the ore doesn't have a design for now
		//    //base.Initialize();
		//    name = GetType().Name;
		//}
		int _Amount;
		Vector3 _DefaultOrePositionDeltaAdditive = new Vector3(-.20f, 0f, .20f);


		public override void SetParentBlock(Block newParent, Vector3? positionDelta = default(Vector3?), bool setRotation = false)
		{
			base.SetParentBlock(newParent, positionDelta + _DefaultOrePositionDeltaAdditive, setRotation);

			// Ore's parent block will only change to null (when it's picked); 
			// but the object should be active in order for the particle system to show until it'll be actually destroyed
			gameObject.SetActive(true);
		}

		protected override void Initialize()
		{
			base.Initialize();
			Amount = 1;
		}

		public override void SetDisplayName(string displayName)
		{
			base.SetDisplayName(_Amount > 1 ? _Amount + "" : "");
		}

		public void DissolveAndDestroySelf()
        {
            Anim_TakeAndDestroySelf(false);
            //Debug.Log("TODO dispatch event through network RPC");
            //photonView.RPC("RPC_Anim_TakeAndDestroySelf", PhotonTargets.OthersBuffered);
        }

		void UpdateDisplayedAmount()
		{
			SetDisplayName(null);
		}

        void Anim_TakeAndDestroySelf(bool isRPC)
        {
            GetComponentInChildren<MeshRenderer>().enabled = false;
			Visuals.Instance.PlayOreTakenEffect(transform.position);
			Destroy(this.gameObject, 2f);
        }
    }
}
