﻿using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Actors.Items
{
    public class EscapeItem : BaseItem
    {
        // Set before Start()
		public new EscapeItemDesign Design { get { return _Design ?? (_Design = base.Design as EscapeItemDesign); } }
		EscapeItemDesign _Design;


		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			card.cardCategoryDesign = Config.Design.Instance.items.escapeItems.cardCategoryDesign;

			return card;
		}

		protected override void Initialize()
        {
            base.Initialize();
            //Design = base.Design as EscapeItemDesign;
        }

		public override void SetDisplayName(string displayName)
		{
			base.SetDisplayName("");
		}
	}
}
