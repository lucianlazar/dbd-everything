﻿using System;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.UI.Inventory;
using Com.TheFallenGames.DBD.Data.Inventory;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Actors.Items
{
    public class MonsterItem : BaseItem
    {
        // Set before Start()
		public new MonsterItemDesign Design { get { return _Design ?? (_Design = base.Design as MonsterItemDesign); } }
		MonsterItemDesign _Design;

		// Monster items are never shown in the dungeon
		public override string MeshPathInResources { get { return null; } }

		public Monster OriginalOwner { get; private set; }
		

		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			// Get the card category design for all, but select the border according to the monster's level
			card.cardCategoryDesign.CopyFrom(Config.Design.Instance.monsters.cardCategoryDesign);
			card.cardCategoryDesign.cardBorder = Config.Design.Instance.monsters.bordersPerLevel[OriginalOwner.Stats.Level];

			return card;
		}

		public override void SetDisplayName(string displayName)
        { base.SetDisplayName(OwnerAsBlock == null ? "" : Design.name + " - <color=red>dead</color>"); }

		protected override void OnOwnerChanged(MonoBehaviour prevOwner)
		{
			if (OriginalOwner == null)
			{
				if (prevOwner)
					throw new UnityException("MonsterItem.OnOwnerChanged: Prev owner not null at initialization?");

				OriginalOwner = Owner as Monster;
				if (OriginalOwner == null)
					throw new UnityException("MonsterItem.OnOwnerChanged: Owner null at initialization?");
			}

			SetDisplayName(null); // Update it to defeated/none

			base.OnOwnerChanged(prevOwner);
		}

		internal void GiveToOriginalOwner()
		{
			Owner = OriginalOwner;
		}

		//protected override void OnRespawned()
		//{
		//	base.OnRespawned();

		//	// Was respawned after the monster => link it back
		//	if (OwnerAsBlock)
		//		Owner = _OriginalOwner;
		//}
	}
}
