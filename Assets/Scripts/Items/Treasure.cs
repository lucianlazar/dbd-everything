﻿using Com.TheFallenGames.DBD.Config.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Actors.Items
{
    public class Treasure : BaseItem
    {
        // Set before Start()
		public new TreasureDesign Design { get { return _Design ?? (_Design = base.Design as TreasureDesign); } }
		TreasureDesign _Design;

		/// <summary>This is used for all treasures, but it may be different in the future</summary>
		public override string MeshNameInResources { get { return "TheOneAndOnlyTreasure"; } }


		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			card.cardCategoryDesign = Config.Design.Instance.items.treasures.cardCategoryDesign;

			return card;
		}

		protected override void Initialize()
        {
            base.Initialize();
            //Design = base.Design as TreasureDesign;
        }

        public override void SetDisplayName(string displayName)
        {
			//base.SetDisplayName("Treasure");// All are named the same
			base.SetDisplayName(""); // No more need to display the name. The mesh is displayed 
		}
	}
}
