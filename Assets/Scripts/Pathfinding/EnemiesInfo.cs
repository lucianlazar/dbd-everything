﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Pathfinding
{
	public class EnemiesInfo// : IComparable<PathfindingCurrency>
	{
		Dictionary<Block, List<Character>> mapCellToEnemies = new Dictionary<Block, List<Character>>();
		Player actor;


		public EnemiesInfo(Player actor) { this.actor = actor; }


		public void PreCacheNow(Board.Descriptor boardDescriptor)
		{
			foreach (var c in boardDescriptor.allCellsIncludingOuter)
				CacheEnemies(c.describedCell.ContainedBlock);
		}

		public void PreCacheNow(Board board)
		{
			foreach (var c in board.AllCellsIncludingOuterCell)
				CacheEnemies(c.ContainedBlock);
		}

		//public bool HasEnemy(Cell.Descriptor cell) { return HasEnemy(cell.block); }
		public bool HasEnemy(Block block) { return GetEnemies(block).Count > 0; }

		public List<Character> GetEnemies(Block block)
		{
			if (mapCellToEnemies.ContainsKey(block))
				return mapCellToEnemies[block];

			return CacheEnemies(block);
		}

		List<Character> CacheEnemies(Block block)
		{
			var en = actor.GetEnemiesInBlock(block);
			mapCellToEnemies[block] = en;

			return en;
		}
	}
}
