﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Pathfinding
{
	public class OneCellMovementSolution
	{
		public Cell.Descriptor cell;
		public SingleCellMoveCost cost;

		public byte OneIfPassThroughWall { get { return (byte)(cost == SingleCellMoveCost.THROUGH_WALL_MOVE ? 1 : 0); } }
		public byte OneIfDiagonal { get { return (byte)(cost == SingleCellMoveCost.DIAGONAL_MOVE ? 1 : 0); } }

		public OneCellMovementSolution(Cell.Descriptor cell, SingleCellMoveCost cost = SingleCellMoveCost.SIMPLE_MOVE)
		{
			this.cell = cell;
			this.cost = cost;
		}
	}
}
