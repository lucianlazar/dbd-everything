﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Pathfinding
{
	public class PathfindingContext
	{
		public readonly Cell.Descriptor origin;
		//public readonly Player actor;
		public readonly PathfindingCurrency budget;
		public readonly Dictionary<Cell.Descriptor, PathfindingSolution> mapDestCellToBestSolution;
		public readonly PathfindingCurrency.Comparer costComparer;
		public readonly EnemiesInfo enemiesInfo;


		public PathfindingContext(
			//Player actor, 
			Cell.Descriptor actorCellDecriptor, 
			PathfindingCurrency budget, 
			PathfindingCurrency.Comparer costComparer, 
			EnemiesInfo enemiesInfo
		)
		{
			this.costComparer = costComparer;
			//this.actor = actor;
			this.origin = actorCellDecriptor;
			this.budget = budget;
			this.enemiesInfo = enemiesInfo;
			mapDestCellToBestSolution = new Dictionary<Cell.Descriptor, PathfindingSolution>();
		}

		public bool StoreSolutionToCellIfBetter(Cell.Descriptor cell, PathfindingSolution potentialSolution)
		{
			PathfindingSolution existingSolution;
			if (mapDestCellToBestSolution.TryGetValue(cell, out existingSolution))
			{
				if (costComparer.Compare(potentialSolution.cost, existingSolution.cost) >= 0)
					return false;
			}

			mapDestCellToBestSolution[cell] = potentialSolution;
			return true;
		}
	}
}
