﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Pathfinding
{
	public class ClosestCellResult// : IComparable<PathfindingCurrency>
	{
		public Cell.Descriptor cell;
		public bool isDirectlyReachable;


		public ClosestCellResult() { }
		public ClosestCellResult(Cell.Descriptor cell, bool isDirectlyReachable) { this.cell = cell; this.isDirectlyReachable = isDirectlyReachable; }
	}
}
