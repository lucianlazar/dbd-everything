﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Pathfinding
{
	public class PathfindingCurrency// : IComparable<PathfindingCurrency>
	{
		public int moves;
		public int passingsThroughWalls;
		public int passingsThroughEnemies;
		public int diagonalMoves;
		public int ghostMoves;

		public int RegularMovesOnly { get { return moves - TotalSpecialMoves; } }
		public int TotalSpecialMoves { get { return ghostMoves + passingsThroughWalls + passingsThroughEnemies + diagonalMoves; } }
		public int TotalSpecialMovesWithoutPassingsThroughWalls { get { return ghostMoves + passingsThroughEnemies + diagonalMoves; } }
		public int TotalSpecialMovesWithoutPassingsThroughEnemies { get { return ghostMoves + passingsThroughWalls + diagonalMoves; } }


		public static PathfindingCurrency CreateUnlimited(int overrideGhostMoves = int.MaxValue) { return new PathfindingCurrency(int.MaxValue, int.MaxValue, int.MaxValue, int.MaxValue, overrideGhostMoves); }


		public PathfindingCurrency() { }

		public PathfindingCurrency(int moves, int passingsThroughWalls, int passingsThroughEnemies, int diagonalMoves, int ghostMoves)
		{
			this.moves = moves;
			this.passingsThroughWalls = passingsThroughWalls;
			this.passingsThroughEnemies = passingsThroughEnemies;
			this.diagonalMoves = diagonalMoves;
			this.ghostMoves = ghostMoves;
		}

		public PathfindingCurrency(PathfindingCurrency other)
		{
			moves = other.moves;
			passingsThroughWalls = other.passingsThroughWalls;
			passingsThroughEnemies = other.passingsThroughEnemies;
			diagonalMoves = other.diagonalMoves;
			ghostMoves = other.ghostMoves;
		}

		public override string ToString()
		{
			return "M" + moves
				+ (ghostMoves > 0 ? " G" + ghostMoves : "")
				+ (passingsThroughWalls > 0 ? " W" + passingsThroughWalls : "")
				+ (passingsThroughEnemies > 0 ? " E" + passingsThroughEnemies : "")
				+ (diagonalMoves > 0 ? " D" + diagonalMoves : "");
		}

		//public static bool operator >(PathfindingCurrency a, PathfindingCurrency b)
		//{
		//	return a.CompareTo(b) > 0;
		//}

		//public static bool operator <(PathfindingCurrency a, PathfindingCurrency b)
		//{
		//	return a.CompareTo(b) < 0;
		//}


		public abstract class Comparer : IComparer<PathfindingCurrency>
		{
			protected PlayerDesign _PlayerDesign;
			public Comparer(PlayerDesign playerDesign) { _PlayerDesign = playerDesign; }
			public abstract int Compare(PathfindingCurrency x, PathfindingCurrency y);
		}


		public sealed class Comparer_PrioritizeConsumingFewerTotalMovesWhenResourcesKnown : Comparer
		{
			public Comparer_PrioritizeConsumingFewerTotalMovesWhenResourcesKnown(PlayerDesign playerDesign) : base(playerDesign) { }


			public override int Compare(PathfindingCurrency a, PathfindingCurrency b)
			{
				return (a.moves - b.moves) * PlayerDesign.TOTAL_MOVES_DEFAULT_WEIGHT_WHEN_COMPARING_FOR_FEWER_TOTAL_MOVES_WHEN_RESOURCES_KNOWN +
						_PlayerDesign.ComparePathfindingCurrencies_SpecialMoves(a, b);
			}
		}


		public sealed class Comparer_PrioritizeConsumingFewerSpecialMovesWhenResourcesUnknown : Comparer
		{
			public Comparer_PrioritizeConsumingFewerSpecialMovesWhenResourcesUnknown(PlayerDesign playerDesign) : base(playerDesign) { }


			public override int Compare(PathfindingCurrency a, PathfindingCurrency b)
			{
				int aSpec = a.TotalSpecialMoves;
				int bSpec = b.TotalSpecialMoves;
				int aReg = a.moves - aSpec;
				int bReg = b.moves - bSpec;
				return _PlayerDesign.ComparePathfindingCurrencies_SpecialMoves(a, b) + (aReg - bReg);
			}
		}
	}
}
