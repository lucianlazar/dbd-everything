﻿using Com.TheFallenGames.DBD.Actors;
using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.World;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.TheFallenGames.DBD.Pathfinding
{
	public class PathfindingSolution
	{
		public List<Cell.Descriptor> path;
		public PathfindingCurrency cost;


		public static PathfindingSolution CreateInitial(Cell.Descriptor origin)
		{ var instance = new PathfindingSolution(); instance.path = new List<Cell.Descriptor>(); instance.path.Add(origin); return instance; }

		public static PathfindingSolution CreateByAppending(List<Cell.Descriptor> prevPath, Cell.Descriptor nextCell, PathfindingCurrency cost)
		{ return new PathfindingSolution(prevPath, nextCell, cost); }


		PathfindingSolution() { path = new List<Cell.Descriptor>(); cost = new PathfindingCurrency(); }

		PathfindingSolution(List<Cell.Descriptor> prevPath, Cell.Descriptor nextCell, PathfindingCurrency cost)
		{
			path = prevPath ?? new List<Cell.Descriptor>();
			path.Add(nextCell);

			this.cost = cost;
		}


		public string ToString(int pathTextSize)
		{
			if (path.Count == 0)
				return "(no path)";

			string str = "(" + cost + ")\n<size="+ pathTextSize + ">";
			var prev = path[0];

			int nums = 0;
			for (int i = 1; i < path.Count; ++i)
			{
				var node = path[i];
				str += prev.DeltaTo(node).ToString("G4");
				if (++nums == 4)
					str += "\n";
				prev = node;
			}

			return str + "</size>";
		}
	}
}
