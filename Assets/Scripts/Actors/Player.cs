﻿using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Com.TheFallenGames.DBD.World;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.Core;

namespace Com.TheFallenGames.DBD.Actors
{
    public class Player : Character
    {
        // Set before Start()
        public new PlayerDesign Design { get { return _Design ?? (_Design = base.Design as PlayerDesign); } }

        // Set in InitializeStats()
        public new PlayerStats Stats { get; private set; }

        // Set before Start()
        // Represents the player's Photon Actor ID
        public int ActorID
		{
			get { return _ActorID; }
			set
			{
				_ActorID = value;
				
				SetDisplayName(ShortNickName);
			}
		}
		public string NickName { get { return PunGameManager.Instance.GetPlayerWithActorID(ActorID).NickName; } }
		public string ShortNickName
		{
			get
			{
				string nickName = NickName;

				if (nickName.Length >= SHORT_NICKNAME_LENGTH)
					nickName = nickName.Substring(0, SHORT_NICKNAME_LENGTH - 2) + "...";

				return nickName;
			}
		}
		public string ShortNickNameWithColoredClass { get { return ShortNickName + " (<color=#" + ColorUtility.ToHtmlStringRGBA(_Design.color) + ">" + name + "</color>)"; } }

		const int SHORT_NICKNAME_LENGTH = 20;

		int _ActorID;
		PlayerDesign _Design;
		PulsatingImage _TurnIndicator;


        protected override void Awake()
        {
			_TurnIndicator = transform.Find("Canvas/TurnHighlightImage").GetComponent<PulsatingImage>();

		   base.Awake();
		}


		public override void SetUpBattleHits(BattleHitsPanel battleHitsPanel)
		{
			battleHitsPanel.SetUpHitEffectWithMiscGameObject(Design.hitAssets.GetRandomHit().GetRandomMiscGameObject());
		}

		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			// Get the card category design for all players, but preserve the border, as it's already set in PlayerDesign
			var borderAlreadySet = card.cardCategoryDesign.cardBorder;
			card.cardCategoryDesign.CopyFrom(Config.Design.Instance.players.cardCategoryDesign);
			card.cardCategoryDesign.cardBorder = borderAlreadySet;

			return card;
		}

		protected override void Initialize()
        {
            base.Initialize();

			// Update: this can only be done once ActorID is set
			//// Overriding the base setting the name of the class
			//SetDisplayName(ShortNickName);

			// Only load automatically parsed hit effects if they weren't manually assigned
			if (!Design.hitAssets.AssetsLoaded)
				Design.hitAssets.LoadGameObjects(MainAssetParentFolderPathInResources, "Hit Effect");
		}

		protected override CharacterStats InitializeStats()
		{
			Stats = new PlayerStats(this);
			Stats.HasSoul = true;

			return Stats;
		}

        // Use this for initialization
        protected virtual void Start()
        {
            //base.Start();

            //transform.GetComponentInChildren<MeshFilter>().GetComponent<Renderer>().material.color = Design.color;
        }

		internal override void OnWonBattle(Character other)
		{
			// Get XP + all ore + all items for which the winner has free slots
			Stats.UpdateStatsOnEnemyKilled(other);

			base.OnWonBattle(other);
		}

		internal void OnStartedTurn()
		{
			_TurnIndicator.enabled = true;
		}

		internal void OnEndedTurn()
		{
			_TurnIndicator.enabled = false;
		}

		public List<Character> GetEnemiesInCell(Cell cell = null)
		{
			if (!cell)
				cell = GetParentCell();
			return GetEnemiesInBlock(cell.ContainedBlock);
		}

		public List<Character> GetEnemiesInBlock(Block block = null)
		{
			if (!block)
				block = GetParentAsBlock();

			var chars = block.GetChildrenOfType<Character>();
			chars.Remove(this);

			Totem t;
			chars.RemoveAll(c => (t = (c as Totem)) != null && t.Owner == this);

			return chars;
		}

		public Pathfinding.PathfindingCurrency GetPathfindingBudget(bool addOneMove)
		{
			return new Pathfinding.PathfindingCurrency(
				   Stats.RemainingMoves + (addOneMove ? 1 : 0),
				   Stats[Stat.MOVES_THROUGH_WALLS],
				   Stats[Stat.BATTLE_EVADING],
				   Stats[Stat.DIAGONAL_MOVES],
				   Stats[Stat.GHOST_MOVES]
			   );
		}
	}
}
