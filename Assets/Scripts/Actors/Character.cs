﻿using Com.TheFallenGames.DBD.Common.Interfaces;
using Com.TheFallenGames.DBD.Config.Characters;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Base;
using Com.TheFallenGames.DBD.Data;
using Com.TheFallenGames.DBD.Data.Inventory;
using System;
using Com.TheFallenGames.DBD.World;
using System.Collections.Generic;
using Com.TheFallenGames.DBD.UI.Battle;
using Com.TheFallenGames.DBD.Data.HitAssets;

namespace Com.TheFallenGames.DBD.Actors
{
    public abstract class Character : MeshEntity
    {
        // Set before Start()
        public new CharacterDesign Design { get { return _CharacterDesign ?? (_CharacterDesign = base.Design as CharacterDesign); }  }
        public override string EntityCategory { get { return typeof(Character).Name; } }
		public override string MainAssetParentFolderPathInResources
		{
			get
			{
				// Character assets are more than 1 and so are kept in folders 
				return base.MainAssetParentFolderPathInResources + "/" + Design.name;
			}
		}

		// Set in InitializeStats()
		public CharacterStats Stats { get; private set; }
		
		CharacterDesign _CharacterDesign;
		Animation8 _MoveAnimation;
		AnimatedPropertiesGroup8 _MoveAnimationProps;
		List<Cell> _RamainingPathToTravel;


		public abstract void SetUpBattleHits(BattleHitsPanel battleHitsPanel);

		public override string ToString()
		{
			var block = GetParentAsBlock();
			var cell = block == null ? null : block.GetParentAsCell();
			return "{"+Design.name + (cell == null ? "- dead" : (" at " + cell.ToShortString())) + "}";
		}

		public void Anim_MoveTo(Block block, Action onDone)
		{
			if (_MoveAnimation.IsPlaying)
				throw new UnityException(name + ": Move to " + block.GetParentAsCell().name + ": _MoveAnimation is already playing (???)");

			_RamainingPathToTravel = new List<Cell>();
			_RamainingPathToTravel.Add(block.GetParentAsCell());
			Anim_MoveToNext(onDone);
		}

		public void Anim_MoveOnPath(List<Cell> path, Action onDone)
		{
			if (_MoveAnimation.IsPlaying)
				throw new UnityException(name + ": Move to " + path[0].name + ": _MoveAnimation is already playing (???)");

			_RamainingPathToTravel = path;
			_RamainingPathToTravel.Remove(GetParentCell());
			Anim_MoveToNext(onDone);
		}

		public void Anim_MoveToNext(Action onDone)
		{
			if (_RamainingPathToTravel.Count == 0)
			{
				if (onDone != null)
					onDone();

				return;
			}
			var next = _RamainingPathToTravel[0];
			_RamainingPathToTravel.RemoveAt(0);


			_MoveAnimationProps.InitialVector3 = transform.position;
			var targetPosWithCurrentY = next.ContainedBlock.transform.position;
			targetPosWithCurrentY.y = transform.position.y; // don't move up/down; only on XZ plane 
			_MoveAnimationProps.TargetVector3 = targetPosWithCurrentY;

			_MoveAnimation.Start(() => Anim_MoveToNext(onDone));
		}

		protected override void Initialize()
        {
            base.Initialize();

			_MoveAnimation = new Animation8(InterpolationFunction8.COS_SLOWIN);
			_MoveAnimation.AnimationSmoothness = AnimationSmoothnessLevel8.TWO;
			_MoveAnimation.AnimationTime = .3f;
			_MoveAnimation.DontDestroyOnNaturalEnd = true;
			_MoveAnimation.Name = name + "-MoveAnimation";
			_MoveAnimationProps = new AnimatedPropertiesGroup8(OnMoveAnimationCallback) { AnimateVectors3 = true };
			_MoveAnimation.AddAnimatedPropertiesGroup(_MoveAnimationProps);

			Stats = InitializeStats();
        }

        protected abstract CharacterStats InitializeStats();

		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			card.description = "<b>Level " + Stats.Level + "</b>\nAtk " + Stats[Stat.ATK] + ", Skill " + Stats[Stat.SKILL];
			var baseBattleEffects = _CharacterDesign.GetBaseBattleEffects();
			if (baseBattleEffects.Count == 1)
			{
				var eff = baseBattleEffects[0];
				var typeStr = eff.type.ToString();
				var ownerStr = eff.owner.ToString();
				card.description += "\n<b>When attacked: </b> " + (eff.amount > 0 ? "+" : "") + eff.amount + " " + (typeStr[0] + typeStr.ToLowerInvariant().Substring(1)) 
									+ " for " + (eff.owner == Config.Effects.Effect.OwnerEnum.ENEMY ? "attacker" : "self");
			}

			return card;
		}

		internal virtual void OnWonBattle(Character other)
		{
			other.OnLostBattle(this);

			if (Stats.KnowsDragonsVoice)
			{
				// Devour the other's soul
				//if (other.Stats.HasSoul)
				//{
				other.OnSoulDevoured(this);
				//}
			}
			else if (other.Stats.KnowsDragonsVoice)
			{
				// Learn the dragon's voice form the defeated char/player
				Stats.KnowsDragonsVoice = true;
			}
		}

		protected virtual void OnLostBattle(Character other)
		{
			// Dead until next turn (or forever, in some cases);
			SetParentBlock(null);
		}

		protected virtual void OnSoulDevoured(Character by)
		{
			Stats.HasSoul = false;
		}


		bool OnMoveAnimationCallback()
		{
			transform.position = _MoveAnimationProps.CurrentVector3;
			return true;
		}

	}
}
