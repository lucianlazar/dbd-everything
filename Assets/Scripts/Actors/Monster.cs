﻿using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Data;
using System;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;

namespace Com.TheFallenGames.DBD.Actors
{
    public class Monster : Character
    {
        // Set before Start()
        public new MonsterDesign Design { get { return _MonsterDesign ?? (_MonsterDesign = base.Design as MonsterDesign); } }

		// Set in InitializeStats()
		public new MonsterStats Stats { get; private set; }

		public MonsterItem RewardItem
        {
            get { return _RewardItem; }
            set
            {
                _RewardItem = value;
                if (_RewardItem)
				{
                    _RewardItem.Owner = this;

					// Setting the reward's item icon to be the same as the monster's icon itself, if there was none specified
					if (!_RewardItem.Design.icon)
						_RewardItem.Design.icon = Design.icon;

					// Setting the long description to be the same as the monster's effect description
					if (_RewardItem.Design.longDescription.Length == 0)
					{
						int index = Design.longDescription.IndexOf("DUNGEON EFFECT");
						if (index == -1)
						{
							index = Design.longDescription.IndexOf("BATTLE EFFECT");
							if (index == -1)
								throw new UnityException("TODO can't find longDescription for " + Design.name + "'s reward item. Monster's description is: " + Design.longDescription);
						}
						_RewardItem.Design.longDescription = Design.longDescription.Substring(index);
					}
				}
            }
        }


        MonsterDesign _MonsterDesign;
        MonsterItem _RewardItem;


		public override void SetUpBattleHits(BattleHitsPanel battleHitsPanel)
		{
			battleHitsPanel.SetUpHitEffectWithSpriteFrames(Design.hitAssets.GetRandomHit().animationFrames);
		}

		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			// Get the card category design for all, but select the border according to the monster's level
			card.cardCategoryDesign.CopyFrom(Config.Design.Instance.monsters.cardCategoryDesign);
			card.cardCategoryDesign.cardBorder = Config.Design.Instance.monsters.bordersPerLevel[Stats.Level];

			return card;
		}

		protected override void Initialize()
        {
            base.Initialize();

			// Only load automatically parsed hit effects if they weren't manually assigned
			if (!Design.hitAssets.AssetsLoaded)
				Design.hitAssets.LoadSprites(MainAssetParentFolderPathInResources, "Hit Effect");
		}

        protected override CharacterStats InitializeStats()
        {
            Stats = new MonsterStats(this);
			Stats.HasSoul = true;

			//SetDisplayName(Design.name + "\n<color=white><size=11>Lv " + Stats.Level + "</size></color>");
			SetDisplayName("");

			return Stats;
        }

		// A monster will only be respawned if it's reward item is in the discard pile (has no owner) or the owner is a block (i.e. the item wasn't picked up)
		protected override void OnRespawned()
		{
			base.OnRespawned();

			_RewardItem.Owner = this;

			//if (_RewardItem)
			//{
			//	if (!_RewardItem.HasOwner)
			//	{
			//		_RewardItem.Owner = this;
			//		//if (_RewardItem.OwnerAsBlock) // Respawned on block => link it back
			//		//{
			//		//	_RewardItem.Owner = this;
			//		//}
			//	}
			//}
		}
	}
}
