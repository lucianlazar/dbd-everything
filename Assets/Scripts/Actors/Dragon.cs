﻿using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Data;
using System;
using Com.TheFallenGames.DBD.Actors.Items;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.Core;

namespace Com.TheFallenGames.DBD.Actors
{
    public class Dragon : Monster
	{
        // Set before Start()
        public new DragonDesign Design { get { return _DragonDesign ?? (_DragonDesign = base.Design as DragonDesign); } }

		DragonDesign _DragonDesign;

		//protected override void Initialize()
		//{
		//	Debug.Log(MainAssetParentFolderPathInResources);
		//	Design.hitAssets.LoadSprites(MainAssetParentFolderPathInResources, "Hit Effect 1", "Hit Effect 2");

		//	base.Initialize();
		//}

		protected override CharacterStats InitializeStats()
		{
			var baseStats = base.InitializeStats();

			baseStats.KnowsDragonsVoice = true;

			return baseStats;
		}

		protected override void OnLostBattle(Character other)
		{
			base.OnLostBattle(other);

			other.Stats.KnowsDragonsVoice = true;

			// Open the exit gate, if not already
			SpawnManager.Instance.exitAreaInstance.IsOpen = true;
		}
	}
}
