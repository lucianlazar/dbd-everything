﻿using Com.TheFallenGames.DBD.Config.Characters.Monsters;
using Com.TheFallenGames.DBD.Config.Characters.Players;
using Com.TheFallenGames.DBD.Config.Characters.Totems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.TheFallenGames.DBD.Data;
using System;
using Com.TheFallenGames.DBD.Common;
using Com.TheFallenGames.DBD.Data.Inventory;
using Com.TheFallenGames.DBD.UI.Battle;

namespace Com.TheFallenGames.DBD.Actors
{
    public class Totem : Character
    {
        // Set before Start()
        public new TotemDesign Design { get { return _Design ?? (_Design = base.Design as TotemDesign); } }

        public override string MeshNameInResources { get { return Design.GetShortName(); } }


        // Set before Start()
        // Represents the Photon Actor ID of the totem's owner (player)
        public Player Owner
        {
            get
            {
                return _Owner;
            }
            set
            {
				var oldOwner = _Owner;
                _Owner = value;
				if (_Owner)
				{
					name = _Owner.name + "'s " + Design.name;
					SetDisplayName(Design.GetShortName());

					_Owner.Stats.AqcuireTotem(this);

					//MeshGameObject.GetComponent<Renderer>().material.color = Owner.Design.color;
					MeshGameObject.GetComponent<SpriteSwitcher>().SwitchSprite(_Owner.ActorID == PhotonNetwork.player.ID ? 0 : 1);
				}
				else
				{
					if (oldOwner)
						oldOwner.Stats.ReleaseTotem(this);
				}
			}
        }

        Player _Owner;
        TotemDesign _Design;
		//SpriteSwitcher _MeshSpriteSwitcher;
		//Config.Design _GameDesign;


		public override void SetUpBattleHits(BattleHitsPanel battleHitsPanel)
		{
			battleHitsPanel.SetUpNoHitEffect();
		}

		internal override CardModel GetCardModel()
		{
			var card = base.GetCardModel();

			card.cardCategoryDesign.CopyFrom(Config.Design.Instance.totems.cardCategoryDesign);

			return card;
		}

		protected override void Initialize()
        {
            //_GameDesign = Config.Design.Instance;
            base.Initialize();
        }

        protected override CharacterStats InitializeStats()
        {
            return new CharacterStats(this);
        }

		public override void SetDisplayName(string displayName)
		{
			base.SetDisplayName("");
		}
	}
}
