
ReadMe:
This pack contains 15 different particle textures.
All materials are supplied as a 2-frame sprite sheet (1024 x 2048 pixels).
The first frame contains the normal texture, the second frame contains the texture with a glow applied to make it pop.
It also contains the following example scenes to get you started:

ButtonControlledExample - this has a UI button hooked up as an example of how to trigger a particle emission from a UI component.

SpritesheetGlowExamples - this contains the 15 main particle system prefabs:
	GlowCross (cross shape)
	GlowDiamond (diamond shape)
	GlowDot (medium sized dot)
	GlowEllipse (rounded egg shape)
	GlowHeart (heart shape)
	GlowHex (hexagonal shape)
	GlowLine (bent line)
	GlowPent (pentagon shape)
	GlowPentOutline (outlined pentagon shape)
	GlowDotSmall (smaller dot)
	GlowOval (oval shape)
	GlowSquare (small square)
	GlowStar (star shape)
	GlowStarOutline (outline of star shape)
	GlowTriangle (medium triangle shape)

SpritesheetGlowExamplesColored - this contains a variety of colored examples of the particles to give you some ideas of different ways to implement them.

SpritesheetGlowExamplesLarge - this contains versions with larger bursts, more particles, bigger radius.

SpritesheetGlowExamplesTiny - this contains tiny versions with smaller bursts (less velocity), less particles, smaller radius.

TrophiesExample - this example contains a sprite showing three trophy images and bronze, silver and gold versions of the particles looping. Press play to view them running.

Tuning suggestions:
The main effect of this particle system is the secondary pop that happens when the glowing texture is revealed. This can be adjusted for a variety of effects (from camera flashes, to shimmering, to very subtle light play) by simply adjusting the Frame Over Time curve found inside the Texture Sheet Animation section of the particle system inspector.
a. moving it earlier will allow you to show more glow effects earlier in the animation.
b. moving it later will hide the popping in of the glow until the very end
c. giving it more of an angle will spread out the timing of the particles and make it more shimmery
d. moving it to the very end just as the particles start to fade out gives it an almost paparazzi kind of feel.

I've also added two examples in ShimmerExamples.unity that demonstrate how different curves can give you more or less sparkle depending on your requirements;
SingleShimmer - this particle system has a Texture Sheet Animation curve with a single curve that makes the particle glow for an instant and then return to it's normal shape.
DoubleShimmer - this one has two bumps in the Texture Sheet Animation curve so you will get two pops for each particle.






